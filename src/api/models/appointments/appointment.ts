type AppointmentItem = {
  booking_date_time: string;
  comment: string;
  completed_on: string;
  createdAt: string;
  customerName: string;
  customerPhone: string;
  customer_id: number;
  deleted: number;
  duration: number;
  employee_id: number;
  id: number;
  mark: number;
  merchant_id: number;
  avatar: string;
  name: string;
  order_id: number;
  price: string;
  product_id: number;
  refund_id: number;
  refund_percent: number;
  stylistName: string;
  updatedAt: string;
  store_id: number;
  store_name: string;
  serviceCategoryId: number;
};

type AppointmentService = {
  id: number;
  name: string;
  duration: number;
  price: number;
  category_id: number;
  category: { name: string } & {};
  product_employees: AppointmentEmployee[];
};

type AppointmentEmployee = {
  id: number;
  name: string;
  phone: string;
  image_url: string;
  products: Array<number>;
  store_id?: number;
  employee_id?: number;
  product_id: number;
};

type AppointmentCustomer = {
  id: number;
  first_name: string;
  last_name: string;
  phone: string;
};

type AppointmentTimeSlot = {
  id: number;
  time: string;
  available: boolean;
};

export {
  AppointmentItem,
  AppointmentService,
  AppointmentEmployee,
  AppointmentCustomer,
  AppointmentTimeSlot
};
