type GetOtpSuccess = {
  isEmployee: boolean;
  isMerchant: boolean;
  isSuperAdmin: boolean;
  phone: string;
};

export { GetOtpSuccess };
