import { MLString } from '../common';

/**
 * Моделька для метода на регу
 */
class ProfileSave {
  public merchant: {
    id?: number;
    phone?: string;
    country_id?: number;
    merchant_name?: string;
    settings?: {
      venue?: {
        registrationCode?: string;
        legalName?: MLString;
      };
      contact?: {
        name?: MLString;
        phone?: string;
      };
      business?: {
        email?: string;
        phone?: string;
      };
      vat?: string;
      iban?: string;
    };
    password?: string;
    email?: string;
    emailVerified?: boolean;
    emailVerification?: boolean;
  };
}

export { ProfileSave };
