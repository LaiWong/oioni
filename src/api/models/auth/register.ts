/**
 * Register model
 */
class Register {
  public country: number;
  public merchantName: string;
  public phoneNumber: string;
}

export { Register };
