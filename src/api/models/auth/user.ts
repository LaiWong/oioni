enum UserType {
  merchant = 'merchant',
  employee = 'employee',
  admin = 'admin'
}

enum UserCaption {
  manager = 'manager',
  employee = 'employee',
  admin = 'person'
}

export { UserType, UserCaption };
