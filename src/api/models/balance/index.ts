type BalanceStatistics = {
  amount: number;
  amountIncrease: number;
  balance: number;
  finished: number;
  amountStatus20: number;
  amountStatus5: number;
  maxAmount: {
    date: string;
    price: number;
  };
  pending: number;
  prevAmount: number;
  stats: {
    [date: string]: {
      totalPrice: number;
      status20Price: number;
      status5Price: number;
    };
  };
};

export { BalanceStatistics };
