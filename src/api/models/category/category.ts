type Category = {
  id: number;
  name: string;
  image_url: string;
  status: 'inactive';
  description: string;
  settings: {
    [x: string]: any;
  };
  tag_categories: {
    id: number;
    tag_id: number;
    category_id: number;
    tag: {
      id: number;
      tag: string;
    };
  }[];
};

export { Category };
