enum Language {
  et = 'et',
  en = 'en',
  ru = 'ru',
  ua = 'ua'
}

export { Language };
