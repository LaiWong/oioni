/**
 * Multi Language String
 */
class MLString {
  [x: string]: string;
}

export { MLString };
