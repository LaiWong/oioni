type CountryLanguage = {
  id?: number;
  language: string;
  active: boolean;
  settings: {
    [x: string]: string;
  };
};

export { CountryLanguage };
