import { Currency } from '../currency';
import { CountryLanguage } from '../country-language';
import { FinanceModel } from '../finance-model';

type Country = {
  id: number;
  name: string;
  names: { code: string; name: string }[];
  code: string;
  business_info: {
    params: {
      name: string;
      length: number;
    }[];
  };
  active: boolean;
  validations: string;
  validation_regex: string;
  phone_length: number;
  created_at: string;
  updated_at: string;
  currency: Currency;
  language: CountryLanguage;
  iso_code_alpha_2: string;
  finance_models_countries: {
    active: boolean;
    id: number;
    finance_model: FinanceModel;
  }[];
  countries_languages: {
    id: number;
    active: boolean;
    language: CountryLanguage;
  }[];
};

type CreateOrUpdateCountry = {
  id?: number;
  currency_id: number;
  main_lang_id: number;
  second_langs: number[];
  phone_length: number;
  iso_code_alpha_2: string;
  business_info: {
    params?: {
      name: string;
      length: number;
    }[];
  };
  finance_models: number[];
};

export { Country, CreateOrUpdateCountry };
