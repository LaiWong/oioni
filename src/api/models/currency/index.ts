type Currency = {
  id?: number;
  currency: string;
  active: boolean;
  settings: {
    [seting: string]: string;
  };
};

export { Currency };
