type Customer = {
  id: number;
  first_name: string;
  last_name: string;
  last_booking_date: string;
  income: number;
  email: string;
  phone: string;
  image_url: string;
};

export { Customer };
