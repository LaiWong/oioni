import { Appointment } from '@api';

enum DashboardAppointmentType {
  total = 'total',
  completed = 'completed',
  upcoming = 'upcoming'
}

enum DashboardBalanceTab {
  total = 'total',
  completed = 'completed',
  notFulfilled = 'notFulfilled'
}

type DashboardAppointments = {
  total: {
    appointments: Appointment[];
    total;
  };
  completed: { appointments: Appointment[]; total };
  upcoming: { appointments: Appointment[]; total };
};

export { DashboardAppointmentType, DashboardBalanceTab, DashboardAppointments };
