enum EmployeePanelTab {
  myAppointments = 'my-appointments',
  clients = 'clients',
  addAppointment = 'add-appointment',
  profile = 'profile',
  settings = 'settings'
}

export { EmployeePanelTab };
