type Specialist = {
  id: number;
  name: string;
  phone: string;
  image_url: string;
};

type Employee = {
  id: number;
  name: string;
  email: string;
  phone: string;
  image_url: string;
  language?: string;
  products: number[];
  merchant_id?: number;
  appointments?: number;
  appointmentsInQue?: number;
  supportingServices?: number;
};

enum EmployeeImageUrl {
  default = 'https://www.shareicon.net/data/2016/07/05/791224_man_512x512.png'
}

export { Specialist, Employee, EmployeeImageUrl };
