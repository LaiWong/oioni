type FinanceModel = {
  id: number;
  name: string;
  active: boolean;
  settings: {
    [x: string]: string;
  };
};

export { FinanceModel };
