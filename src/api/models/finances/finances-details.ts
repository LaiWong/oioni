type FinancesDetails = {
  need_to_pay: number;
  paid: number;
  pending: number;
  revenue: number;
};

export { FinancesDetails };
