type HistoryOfPayments = {
  merchantId: number;
  merchantAvatar: {
    image: string;
  };
  merchantName: string;
  lastPayment: {
    amount: number;
    date: string;
  };
  revenue: number;
  paid: number;
};

export { HistoryOfPayments };
