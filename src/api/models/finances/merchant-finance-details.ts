type MerchantFinanceDetails = {
  iban: string;
  id: number;
  image: string;
  name: string;
  need_to_pay: number;
  paid: number;
  pending: number;
  revenue: number;
  vat: string;
};

export { MerchantFinanceDetails };
