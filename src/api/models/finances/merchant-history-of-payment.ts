type MerchantHistoryOfPayment = {
  amount_EUR: number;
  createdAt: string;
  id: number;
  merchant_id: number;
  updatedAt: string;
};

export { MerchantHistoryOfPayment };
