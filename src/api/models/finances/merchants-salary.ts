type MerchantsSalary = {
  id: number;
  name: string;
  balance: number;
  need_to_pay: number;
  image: string;
  paid: number;
  pending: number;
  performed: number;
  revenue: number;
  iban: string;
  vat: string;
};

export { MerchantsSalary };
