type PayToMerchant = {
  merchant_id: number;
  amount_EUR: number;
};

export { PayToMerchant };
