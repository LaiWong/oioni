enum TimeRangeTypes {
  allTime = 'allTime',
  currentDay = 'currentDay',
  previousDay = 'previousDay',
  currentWeek = 'currentWeek',
  currentMonth = 'currentMonth',
  previousMonth = 'previousMonth',
  currentQuarter = 'currentQuarter',
  selectPeriod = 'selectPeriod'
}

type SelectedDates = {
  start: string;
  end: string;
};

export { TimeRangeTypes, SelectedDates };
