import { string } from 'yup';
import { Language } from '../common';
import { Category } from '../category';
import { ProductEmployees } from '../product';

/**
 * Old merchant
 */
type MerchantFull = {
  id: number;
  merchant_name: string;
  language: Language;
  country_id: number;
  settings: {
    [x: string]: any;
  };
  isAdmin?: boolean;

  status: 'active' | 'disabled';

  phone: string;
  password: string;
  created_at: string;
  stores: any[];
  emailVerified?: boolean;
  emailVerification?: boolean;
  email?: string;
};

/**
 * Merchant
 */
type Merchant = {
  id: number;
  merchant_name: string;
  created_at: string;
  services_count: number;
  users_count: number;
  status: string;
};

/**
 * Merchant bookings
 */
type MerchantBookings = {
  id: number;
  order_id: number;
  employee_id: number;
  product_id: number;
  name: string;
  duration: number;
  price: string;
  booking_date_time: string;
  comment: any;
  mark: any;
  refund_percent: any;
  status: number;
  paid: any;
  refund_id: any;
  deleted: any;
  createdAt: string;
  updatedAt: string;
  employee_name: string;
  employee_image_url: string;
  product_name: string;
  revenue: string;
};

/**
 * Merchant services
 */
type MerchantServices = {
  id: number;
  name: string;
  duration: number;
  price: number;
  old_price: number;
  priority: any;
  category: Category;
  product_employees: ProductEmployees[];
  settings: {
    name: {
      [key: string]: string;
    };
  };
};

/**
 * Merchant stores
 */
type MerchantSalons = {
  id: number;
  name: string;
  address: string;
  contacts: string;
  image_url: string;
};

/**
 * Merchant details
 */
type MerchantDetails = {
  bookings?: MerchantBookings[];
  services?: MerchantServices[];
  salons?: MerchantSalons[];
  employees?: {
    id: number;
    name: string;
    services_count: string;
    image_url: string;
    jobs: {
      employee_job: {
        employee_id: number;
        job_id: number;
      };
      employee_id: number;
      job_id: number;
      id: number;
      name: string;
    }[];
  }[];
  profile?: {
    id: number;
    merchant_name: string;
    country_id: number;
    settings: any;
    email: string;
  };
};

/**
 * Schema fields to reuse
 */
const merchantSchemaFields = {
  merchant_name: string()
    .required()
    .nullable(true)
    .label('Merchant Name'),
  settings: {
    brandName: string()
      .required()
      .nullable(true)
      .label('Brand name')
  }
};

export {
  Merchant,
  merchantSchemaFields,
  MerchantFull,
  MerchantDetails,
  MerchantBookings
};
