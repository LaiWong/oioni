import { Employee } from '../employee';

type Appointment = {
  booking_date_time: string;
  comment: string;
  completed_on: string;
  createdAt: string;
  customerName: string;
  customerPhone: string;
  customer_id: number;
  deleted: number;
  duration: number;
  employee_id: number;
  id: number;
  mark: number;
  merchant_id: number;
  avatar: string;
  name: string;
  order_id: number;
  price: string;
  product_id: number;
  refund_id: number;
  refund_percent: number;
  stylistName: string;
  updatedAt: string;
  store_id: number;
  store_name: string;
  employee: Employee;
  serviceCategoryId: number;
};

type TimeSlot = {
  id: number;
  time: string;
  available: boolean;
};

enum Stage {
  upcoming,
  previous
}

export { Appointment, TimeSlot, Stage };
