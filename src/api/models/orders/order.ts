type Order = {
  order_items: {
    booking_date_time?: any;
    comment?: any;
    createdAt: Date;
    deleted?: any;
    duration: number;
    employee_id?: any;
    id: number;
    mark?: any;
    name: string;
    order_id: number;
    price: string;
    product_id: number;
    refund_id?: any;
    refund_percent?: any;
    updatedAt: Date;
  }[];
  settings: {
    [x: string]: any;
  };
  createdAt: string;
  customer_id: number;
  discount_percent: number;
  id: number;
  merchant_id: number;
  order_status: number;
  store_id: number;
  total: string;
  transaction_id: string;
  updatedAt: string;
};

export { Order };
