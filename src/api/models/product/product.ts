import { Category } from '../category';

enum ProductStatus {
  active = 'active',
  disabled = 'disabled'
}

type Product = {
  id: number;
  name: string;
  merchant_id: number;
  price: number;
  old_price: number;
  category_id: number;
  discount: number;
  priority: any;
  settings: {
    [x: string]: any;
  };
  duration: number;
  image_url: string;
  status: ProductStatus;
  created_at: string;
  updated_at: string;
  category: Category;
  product_employees: ProductEmployees[];
};

type ProductEmployees = {
  id: number;
  product_id: number;
  employee_id: number;
  priority: number;
  store_id: number;
  employee: any;
  store: any;
};

export { Product, ProductStatus, ProductEmployees };
