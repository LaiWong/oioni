type RefundBookings = {
  canceled: {
    order_item_id: number;
    order_id: number;
    refund_percent: number;
    status: number;
    refund_id: string;
    booking_date_time: string;
    price: string;
    order: {
      id: number;
      customer_id: number;
      merchant_id: number;
      store_id: number;
      order_status: number;
      total: string;
      discount_percent: null;
      transaction_id: string;
      settings: Object;
      createdAt: string;
      updatedAt: string;
    };
  }[];
  failProcessing: {
    order_id: number;
    status: number;
    settings: {
      payedBy: string;
      info: string;
    };
    price: string;
  }[];
};

type RefundFailProcessingBookings = {
  order_id: number;
  status: number;
  settings: {
    payedBy: string;
    info: string;
  };
  price: string;
}[];

type RefundCanceledBookings = {
  order_item_id: number;
  order_id: number;
  refund_percent: number;
  status: number;
  refund_id: string;
  booking_date_time: string;
  price: string;
  order: {
    id: number;
    customer_id: number;
    merchant_id: number;
    store_id: number;
    order_status: number;
    total: string;
    discount_percent: null;
    transaction_id: string;
    settings: Object;
    createdAt: string;
    updatedAt: string;
  };
}[];

export { RefundBookings, RefundCanceledBookings, RefundFailProcessingBookings };
