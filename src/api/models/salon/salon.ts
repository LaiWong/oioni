import { Merchant } from '../merchant';
import { MLString } from '../common';

enum SalonStatus {
  active = 'active',
  disabled = 'disabled'
}

type Salon = {
  address: string;
  contacts: string;
  employee_stores: [];
  id: number;
  latitude: number;
  localization?: null;
  longitude: number;
  city?: string;
  country?: string;
  timezone?: string;
  merchant?: Merchant;
  merchant_id: number;
  name: string;
  price_level: null;
  rate: null;
  phone?: string;
  email?: string;

  services: {
    productId: number;
    employees: number[];
  }[];

  settings: {
    banners: string[];
    name: MLString;
    contacts: string;
  };
  speak_lang: string;
  status: SalonStatus;
  time_to: string;
  time_from: string;
};

export { Salon, SalonStatus };
