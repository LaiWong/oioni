type TimeSheet = {
  blackouts: Schedule[];
  id: number;
  name: string;
  schedules: Schedule[];
};

type Schedule = {
  date: Date;
  employee_id: number;
  id: number;
  short_date: string;
  store_id: number;
  time_from: string;
  time_to: string;
  timeslot_from: number;
  timeslot_to: number;
};

export { Schedule, TimeSheet };
