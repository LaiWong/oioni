import { Language } from '../common';

type Widget = {
  cover_color: string;
  description: string;
  domain: string;
  id: number;
  image_url: string | null;
  language: Language;
  merchant_id: number;
  name: string;
  text_color: string;
};

enum widgetType {
  oioni = 'oioni',
  own = 'own'
}

export { Widget, widgetType };
