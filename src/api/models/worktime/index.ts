export type Schedule = {
  date: string;
  employee_id: number;
  id: number;
  short_date: string;
  store_id: number;
  time_from: string;
  time_to: string;
  timeslot_from: number;
  timeslot_to: number;
};
export type Blackout = {
  date: string;
  employee_id: number;
  id: number;
  short_date: string;
  store_id: number;
  time_from: string;
  time_to: string;
  timeslot_from: number;
  timeslot_to: number;
};

export type Day = {
  id: number;
  formatDate: string;
  fullDate: string | Date;
  schedule: Schedule | Schedule[] | null;
  blackouts: Blackout | Blackout[] | null;
  disabled: boolean;
};
