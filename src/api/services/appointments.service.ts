import { AxiosRequestConfig } from 'axios';
import { enviroment } from '@env';
import { stringify } from 'querystring';
import { excludeEmptyFields } from '@core';

class AppointmentsService {
  /**
   * Api
   */
  public api = this.create({
    withCredentials: true,
    baseURL: enviroment.api.order,
    headers: {
      Authorization: enviroment.credentials.authorization.order
    }
  });

  /**
   * Get axios
   */
  public constructor(
    private create: (
      config: AxiosRequestConfig
    ) => (
      config: AxiosRequestConfig,
      enabled?: boolean,
      disabledToken?: boolean
    ) => any
  ) {}

  public get = (
    merchant: number,
    options: Partial<{
      perPage: number;
      page: number;
      employee_id: number;
      order_statuses: string;
      previous: boolean;
      future: boolean;
    }> = {}
  ) => {
    const query = stringify(options);

    return this.api({
      url:
        `/v1/admin/orders?includeEmployees=true&includeCustomers=true&includeOrderItems=true&includeOrderStores=true&includeCategories=true&merchant_id=${merchant}` +
        (query ? '&' + query : '')
    });
  };

  public fetchAppointments = (
    merchant: number,
    options: Partial<{
      perPage: number;
      page: number;
      employee_id: number;
      order_statuses: string;
      previous: boolean;
      future: boolean;
    }> = {}
  ) => {
    const query = stringify(options);

    return this.api({
      url:
        `/v1/admin/orders?includeEmployees=true&includeCustomers=true&includeOrderItems=true&includeOrderStores=true&includeCategories=true&merchant_id=${merchant}` +
        (query ? '&' + query : '')
    });
  };

  public fetchServices = (merchant: number) =>
    this.api({
      method: 'GET',
      url: `${enviroment.api.merchant}/merchants/${merchant}/services`,
      params: {
        status: 'active',
        active: 'false'
        // page: 1,
        // perPage: 9999
      }
    });

  public fetchEmployees = (merchant: number) =>
    this.api({
      method: 'GET',
      url: `${enviroment.api.merchant}/merchants/${merchant}/employees`,
      params: {
        status: 'active',
        includeProducts: 'true'
      }
    });

  public fetchCustomers = (merchant: number) =>
    this.api({
      method: 'GET',
      url: `${enviroment.api.merchant}/merchants/${merchant}/customers`,
      params: {
        page: 1,
        perPage: 9999
      }
    });

  public fetchEligibleTimeSlots = (params: {
    employee_id: number;
    store_id: number;
    duration: number;
    date: string;
  }) =>
    this.api({
      method: 'GET',
      url: `${enviroment.api.merchant}/availability`,
      params: {
        employee_id: params.employee_id,
        store_id: params.store_id,
        duration: params.duration,
        date: params.date
      }
    });

  public createCustomer = (params: {
    order_item_id: number;
    booking_date_time: string;
  }) => {
    this.api({
      params,
      method: 'PUT',
      url: `/v1/order_items?id=${params.order_item_id}`,
      data: {
        booking_date_time: params.booking_date_time
      }
    });
  };

  public createAppointment = (params: {
    merchant_id: number;
    first_name: string;
    last_name: string;
    customer_phone: string;
    employee_id: number;
    store_id: number;
    product_id: number;
    duration: number;
    date: string;
  }) =>
    this.api({
      method: 'POST',
      url: `${enviroment.api.customer}/customers`,
      data: {
        language: 'en',
        phone: params.customer_phone.replace(/\D/g, ''),
        first_name: params.first_name,
        last_name: params.last_name
      }
    }).then(({ data }) => {
      const customer_id = data.result.id;
      return this.api({
        method: 'POST',
        url: '/v1/orders',
        data: {
          customer_id: customer_id,
          merchant_id: params.merchant_id,
          store_id: params.store_id,
          order_status: 3
        }
      })
        .then(({ data }) =>
          this.api({
            method: 'POST',
            url: '/v1/order_items',
            data: {
              order_id: data.data.id,
              items: [
                {
                  product_id: params.product_id,
                  employee_id: params.employee_id,
                  booking_date_time: params.date
                }
              ]
            }
          })
        )
        .then(({ data }) =>
          this.api({
            method: 'PUT',
            url: `/v1/orders?id=${data?.data[0]?.order_id}`,
            data: {
              order_status: 20
            }
          })
        );
    });

  public updateAppointment = (params: {
    id: number;
    employee_id: number;
    store_id: number;
    product_id: number;
    duration: number;
    date: string;
  }) =>
    this.api({
      method: 'PUT',
      url: `/v1/order_items?id=${params.id}`,
      data: {
        booking_date_time: params.date
      }
    });

  public deleteAppointment = (id: number) =>
    this.api({
      method: 'PUT',
      url: `/v1/order_items?id=${id}`,
      data: {
        deleted: 1
      }
    });

  public validateCustomerSend = (phone: string) =>
    this.api({
      method: 'POST',
      url: enviroment.api.customer + '/authorization/otpSend',
      data: {
        phone: phone.replace(/\D/g, '')
      }
    });

  public validateCustomerCheck = (phone: string, code: string) =>
    this.api(
      {
        method: 'POST',
        url: enviroment.api.customer + '/authorization/otpCheck',
        data: {
          phone: phone.replace(/\D/g, ''),
          otp: code
        }
      },
      true,
      true
    );

  /**
   * Get available slots list
   */
  public slots = ({
    date,
    duration,
    store_id,
    employee_id
  }: {
    date: string;
    store_id: number;
    duration: number;
    employee_id: number;
  }) =>
    this.api({
      baseURL: enviroment.api.merchant,
      headers: {
        Authorization: enviroment.credentials.authorization.merchant
      },
      url: `/availability?employee_id=${employee_id}&store_id=${store_id}&duration=${duration}&date=${date}`
    });

  /**
   * Pending balances
   */
  public pendingBalance = () =>
    this.api({
      url: '/v1/admin/balance'
    });

  /**
   * Balance statistics
   */
  public balanceStatistics = () =>
    this.api({
      url: '/v1/admin/balance/statistics'
    });

  /**
   * Update appointment
   */
  public update = (
    id: number,
    data: {
      deleted: 0;
      booking_date_time: string;
    }
  ) =>
    this.api({
      data,
      method: 'PUT',
      url: `/v1/order_items?id=${id}`
    });

  public getRefundBookings = ({
    page,
    perPage
  }: {
    page: number;
    perPage: number;
  }) =>
    this.api({
      method: 'GET',
      url: `/v1/admin/orders/refund?`
      // stringify(excludeEmptyFields({ page, perPage }, { emptyStrings: false }))
    });

  public updateRefundBookings = (data: {
    order_id?: number;
    order_item_id?: number;
    mark: boolean;
  }) =>
    this.api({
      method: 'PUT',
      url: `/v1/admin/orders/refund?${stringify(excludeEmptyFields(data))}`
    });
}

export { AppointmentsService };
