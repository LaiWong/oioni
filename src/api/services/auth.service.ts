import { AxiosRequestConfig } from 'axios';
import { enviroment } from '@env';

class AuthService {
  /**
   * Get axios
   */
  public constructor(
    private create: (
      config: AxiosRequestConfig
    ) => (config: AxiosRequestConfig, enabled?: boolean) => any
  ) {}

  /**
   * Api
   */
  public api = this.create({
    withCredentials: true,
    baseURL: enviroment.api.merchant,
    headers: {
      Authorization: enviroment.credentials.authorization.merchant
    }
  });

  /**
   * Log in
   */
  public login = (data: { email: string; password: string }) =>
    this.api(
      {
        data,
        method: 'POST',
        url: '/merchants/authorization'
      },
      false
    );

  /**
   * Recover
   */
  public recover = (data: { email: string }) =>
    this.api({
      data,
      method: 'POST',
      url: '/merchants/refreshPassword'
    });

  /**
   * Send code
   */
  public sendOtp = ({
    phone,
    isRegistration
  }: {
    phone: string;
    isRegistration: boolean;
  }) =>
    this.api({
      data: { phone },
      method: 'POST',
      url: `/merchants/otpSend${isRegistration ? `?registration=true` : ''}`
    });

  /**
   * Check otp
   */
  public checkOtp = (data: { phone: string; otp: string }) =>
    this.api({
      data,
      method: 'POST',
      url: '/merchants/otpCheck'
    });

  /**
   * Get countries
   */
  public countries = () =>
    this.api({
      url: '/countries', //?clearCache=true
      baseURL: enviroment.api.customer,
      headers: {
        Authorization: enviroment.credentials.authorization.customer
      }
    });

  /**
   * Logout
   */
  public logout = () =>
    this.api({
      url: '/merchant/logout'
    });
}

export { AuthService };
