import axios, { AxiosInstance, AxiosRequestConfig } from 'axios';
import { enviroment } from '@env';
import { omitEmptyQueryParams } from '@core';

type CreateRequestF = (
  config: AxiosRequestConfig
) => (config: AxiosRequestConfig) => any;

class BaseService {
  /**
   * Get axios
   */
  protected constructor(private create: CreateRequestF) {}

  /**
   * Api
   */
  public api = this.create({
    withCredentials: true,
    baseURL: enviroment.api.merchant,
    headers: {
      Authorization: enviroment.credentials.authorization.merchant
    }
  });
}

export { BaseService, CreateRequestF };
