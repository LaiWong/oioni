import { enviroment } from '@env';
import { AxiosRequestConfig } from 'axios';
import { stringify } from 'querystring';
import { excludeEmptyFields } from '@core';

class BlackoutService {
  /**
   * Get axios
   */
  public constructor(
    private create: (
      config: AxiosRequestConfig
    ) => (config: AxiosRequestConfig) => any
  ) {}

  /**
   * Api
   */
  public api = this.create({
    withCredentials: true,
    baseURL: enviroment.api.merchant,
    headers: {
      Authorization: enviroment.credentials.authorization.merchant
    }
  });

  /**
   * Get all blackout
   */
  public getBlackout = ({
    store_id,
    employee_id,
    date
  }: {
    store_id: number;
    employee_id: number;
    date: string;
  }) =>
    this.api({
      method: 'GET',
      url: `/blackouts?employee_id=${employee_id}&store_id=${store_id}&date=${date}&schedules=true`
    });

  /**
   * Update blackout
   */
  public updateBlackout = (data: {
    employee_id: string;
    store_id: string;
    time_from: string;
    time_to: string;
    date_from: string;
    date_to: string;
  }) =>
    this.api({
      data,
      method: 'PUT',
      url: '/blackouts'
    });

  /**
   *
   * Create blackout
   */
  public createBlackout = (data: {
    employee_id: string;
    store_id: string;
    time_from: string;
    time_to: string;
    date: string;
  }) =>
    this.api({
      data,
      method: 'POST',
      url: '/blackouts'
    });

  /**
   *
   * Delete blackout
   */
  public deleteBlackout = (data: {
    employee_id: string;
    store_id: string;
    time_from: string;
    time_to: string;
    date_from: string;
    date_to: string;
  }) =>
    this.api({
      data,
      method: 'DELETE',
      url: `/blackouts`
    });

  /**
   *
   * Delete blackouts by ID
   */
  public deleteBlackoutsById = (data: { ids: number[] }) =>
    this.api({
      data,
      method: 'DELETE',
      url: `/blackouts`
    });

  /**
   * upsert blackouts
   */
  public upsertBlackouts = (data: {
    employee_id: number;
    store_id: number;
    time_from: string;
    time_to: string;
    dates: string[];
  }) =>
    this.api({
      data,
      method: 'PUT',
      url: '/blackouts'
    });

  /**
   *
   * Update blackouts by ID
   */
  public updateBlackoutsById = (data: {
    store_id: number;
    employee_id: number;
    time_from: string;
    time_to: string;
    ids: number[];
  }) =>
    this.api({
      data,
      method: 'PUT',
      url: '/blackouts'
    });
}

export { BlackoutService };
