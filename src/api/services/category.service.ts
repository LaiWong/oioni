import { enviroment } from '@env';
import axios, { AxiosInstance, AxiosRequestConfig } from 'axios';

class CategoryService {
  /**
   * Get axios
   */
  public constructor(
    private create: (
      config: AxiosRequestConfig
    ) => (config: AxiosRequestConfig) => any
  ) {}

  /**
   * Api
   */
  public api = this.create({
    withCredentials: true,
    baseURL: enviroment.api.merchant,
    headers: {
      Authorization: enviroment.credentials.authorization.merchant
    }
  });

  /**
   * Get categories
   */
  public get = () =>
    this.api({
      url: '/categories?status=active'
    });
}

export { CategoryService };
