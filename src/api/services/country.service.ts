import { AxiosRequestConfig } from 'axios';
import { enviroment } from '@env';
import { stringify } from 'querystring';
import { excludeEmptyFields } from '@core';
import { CreateOrUpdateCountry } from '@api';

class CountryService {
  public api = this.create({
    withCredentials: true,
    baseURL: enviroment.api.customer,
    headers: {
      Authorization: enviroment.credentials.authorization.merchant
    }
  });

  public constructor(
    private create: (
      config: AxiosRequestConfig
    ) => (config: AxiosRequestConfig) => any
  ) {}

  public getCountries = ({
    id,
    page,
    perPage
  }: {
    id?: number;
    page?: number;
    perPage?: number;
  }) =>
    this.api({
      url:
        `/countries?` +
        stringify(
          excludeEmptyFields(
            {
              id
              // page,
              // perPage
            },
            { emptyStrings: false }
          )
        ),
      method: 'GET'
    });

  public createCountry = (data: CreateOrUpdateCountry) =>
    this.api({
      url: `/admin/countries`,
      data,
      method: 'POST'
    });

  public updateCountry = (data: CreateOrUpdateCountry) =>
    this.api({
      url: `/admin/countries`,
      data,

      method: 'PUT'
    });
}

export { CountryService };
