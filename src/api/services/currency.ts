import { AxiosRequestConfig } from 'axios';
import { enviroment } from '@env';
import { stringify } from 'querystring';
import { excludeEmptyFields } from '@core';
import { Currency } from '../models';

class CurrencyService {
  /**
   * Get axios
   */
  public constructor(
    private create: (
      config: AxiosRequestConfig
    ) => (config: AxiosRequestConfig, enabled?: boolean) => any
  ) {}

  /**
   * Api
   */
  public api = this.create({
    withCredentials: true,
    baseURL: enviroment.api.customer,
    headers: {
      Authorization: enviroment.credentials.authorization.merchant
    }
  });

  /**
   * Get Currency
   */
  public getCurrencies = ({
    id,
    currency,
    active
  }: {
    id?: number;
    currency?: string;
    active?: boolean;
  }) =>
    this.api({
      method: 'GET',
      url: `/admin/currencies?${stringify(
        excludeEmptyFields({ id, currency, active })
      )}`
    });

  /**
   * Create Currency
   */
  public createCurrency = (data: Currency) =>
    this.api({
      data,
      method: 'POST',
      url: '/admin/currencies'
    });

  /**
   * Update Currency
   */
  public updateCurrency = (data: Currency) =>
    this.api({
      data,
      method: 'PUT',
      url: '/admin/currencies'
    });

  /**
   * Delete Currency
   */
  public deleteCurrency = (id: number) =>
    this.api({
      data: { id },
      method: 'DELETE',
      url: '/admin/currencies'
    });
}

export { CurrencyService };
