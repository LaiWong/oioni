import axios, { AxiosInstance, AxiosRequestConfig } from 'axios';
import { enviroment } from '@env';
import { stringify } from 'querystring';
import { omitEmptyQueryParams } from '@core';
import { Customer } from '../models';
import { BaseService, CreateRequestF } from './base.service';

class CustomersService extends BaseService {
  public constructor(createRrequest: CreateRequestF) {
    super(createRrequest);
  }

  /**
   * Get customers
   */
  public get = ({
    merchant,
    page,
    perPage,
    search
  }: {
    merchant: number;
    page?: number;
    search?: string;
    perPage?: number;
  }) =>
    this.api({
      method: 'GET',
      url:
        `/merchants/${merchant}/customers?` +
        omitEmptyQueryParams(
          {
            page,
            search,
            perPage
          },
          { emptyStrings: false }
        )
    });
}

export { CustomersService };
