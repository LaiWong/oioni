import { AxiosRequestConfig } from 'axios';
import { enviroment } from '@env';
import { stringify } from 'querystring';
import { excludeEmptyFields } from '@core';
import { Employee } from '../models';

class EmployeeService {
  /**
   * Get axios
   */
  public constructor(
    private create: (
      config: AxiosRequestConfig
    ) => (config: AxiosRequestConfig) => any
  ) {}

  /**
   * Api
   */
  public api = this.create({
    withCredentials: true,
    baseURL: enviroment.api.merchant,
    headers: {
      Authorization: enviroment.credentials.authorization.merchant
    }
  });

  /**
   * Get employee user data
   */
  public getUserData = () =>
    this.api({
      url: `/employee`
    });

  /**
   * Get specialists
   */
  public specialists = (merchant: number) =>
    this.api({
      method: 'GET',
      url: `/merchants/${merchant}/employees?`
    });

  /**
   * Get employees
   */
  public get = ({
    merchant,
    page,
    includeProducts,
    perPage,
    search
  }: {
    page?: number;
    search?: string;
    perPage?: number;
    merchant: number;
    includeProducts?: boolean;
  }) =>
    this.api({
      method: 'GET',
      url:
        `/merchants/${merchant}/employees?includeServices=true&includeOrders=true&` +
        stringify(
          excludeEmptyFields(
            {
              page,
              search,
              perPage,
              includeProducts
            },
            { emptyStrings: false }
          )
        )
    });

  /**
   * Get single employee
   */
  public getOne = (merchant: number, id: number) =>
    this.api({
      method: 'GET',
      url: `/merchants/${merchant}/employees?id=${id}&includeProducts=true`
    });

  /**
   * Create employee
   */
  public post = (data: Partial<Employee>) =>
    this.api({
      data,
      method: 'POST',
      url: '/employees'
    });

  /**
   * Update employee
   */
  public put = (id: number, data: Partial<Employee>) =>
    this.api({
      data,
      method: 'PUT',
      url: `/employees/${id}`
    });

  /**
   * Delete employee
   */
  public delete = (id: number, merchant: number) =>
    this.api({
      method: 'DELETE',
      url: `/merchants/${merchant}/employees/${id}`
    });
}

export { EmployeeService };
