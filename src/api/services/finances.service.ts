import { AxiosRequestConfig } from 'axios';
import { enviroment } from '@env';
import { PayToMerchant, SelectedDates } from '../models';
import moment from 'moment';
import { stringify } from 'querystring';
import { excludeEmptyFields } from '@core';

const formatTime = (time: string) =>
  moment(time, 'D MMMM YYYY').format('YYYY-MM-DD');

const getStartAndEndDate = (
  selectedDates: SelectedDates,
  isCamelCase?: boolean
) =>
  `${
    selectedDates?.start
      ? `&${isCamelCase ? 'startDate' : 'start_date'}=${formatTime(
          selectedDates.start
        )}`
      : ''
  }${
    selectedDates?.end
      ? `&${isCamelCase ? 'endDate' : 'end_date'}=${formatTime(
          selectedDates.end
        )}`
      : ''
  }`;

class FinancesService {
  /**
   * Get axios
   */
  public constructor(
    private create: (
      config: AxiosRequestConfig
    ) => (config: AxiosRequestConfig) => any
  ) {}

  /**
   * Api
   */
  public api = this.create({
    withCredentials: true,
    baseURL: enviroment.api.merchant,
    headers: {
      Authorization: enviroment.credentials.authorization.merchant
    }
  });

  /**
   * Get finances details
   */
  public getFinancesData = (selectedDates: SelectedDates) =>
    this.api({
      url: `/admin/merchants/finances/global?${getStartAndEndDate(
        selectedDates
      )}`,
      method: 'GET'
    });

  /**
   * Get merchants salary details
   */
  public getMerchantsSalaryData = ({
    page,
    perPage,
    selectedDates
  }: {
    perPage: number;
    page: number;
    selectedDates: {
      start: string;
      end: string;
    };
  }) =>
    this.api({
      url: `/admin/merchants/finances?page=${page}&perPage=${perPage}${getStartAndEndDate(
        selectedDates
      )}`,
      method: 'GET'
    });

  /**
   * Get finances history
   */
  public getFinancesHistory = ({
    perPage,
    page,
    search,
    selectedDates
  }: {
    perPage: number;
    page: number;
    search?: string;
    selectedDates: SelectedDates;
  }) =>
    this.api({
      url:
        `/admin/merchants/payments?` +
        stringify(
          excludeEmptyFields(
            {
              name: search,
              page,
              perPage,
              status
            },
            { emptyStrings: false }
          )
        ) +
        getStartAndEndDate(selectedDates, true),
      method: 'GET'
    });

  /**
   * Get merchant finances details
   */
  public getMerchantFinancesDetails = ({
    id,
    selectedDates
  }: {
    id: number;
    selectedDates: SelectedDates;
  }) =>
    this.api({
      url: `/admin/merchants/finances?merchant_id=${id}${getStartAndEndDate(
        selectedDates
      )}`,
      method: 'GET'
    });

  /**
   * Get merchant history of payment
   */
  public getMerchantHistoryOfPayment = ({
    id,
    selectedDates
  }: {
    id: number;
    selectedDates: SelectedDates;
  }) =>
    this.api({
      url: `/admin/merchants/payments/history?merchant_id=${id}${getStartAndEndDate(
        selectedDates,
        true
      )}`,
      method: 'GET'
    });

  /**
   * Pay to merchant
   */
  public payToMerchant = (data: PayToMerchant) =>
    this.api({
      data,
      url: `/admin/merchants/payments`,
      method: 'POST'
    });
}

export { FinancesService, formatTime };
