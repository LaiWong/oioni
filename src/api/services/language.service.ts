import { AxiosRequestConfig } from 'axios';
import { enviroment } from '@env';
import { stringify } from 'querystring';
import { excludeEmptyFields } from '@core';
import { CountryLanguage } from '../models';

class LanguageService {
  /**
   * Get axios
   */
  public constructor(
    private create: (
      config: AxiosRequestConfig
    ) => (config: AxiosRequestConfig, enabled?: boolean) => any
  ) {}

  /**
   * Api
   */
  public api = this.create({
    withCredentials: true,
    baseURL: enviroment.api.customer,
    headers: {
      Authorization: enviroment.credentials.authorization.merchant
    }
  });

  /**
   * Get Languages
   */
  public getLanguages = ({
    id,
    language,
    active
  }: {
    id?: number;
    language?: string;
    active?: boolean;
  }) =>
    this.api({
      method: 'GET',
      url: `/admin/languages?${stringify(
        excludeEmptyFields({ id, language, active })
      )}`
    });

  /**
   * Create Language
   */
  public createLanguage = (data: CountryLanguage) =>
    this.api({
      data,
      method: 'POST',
      url: '/admin/languages'
    });

  /**
   * Update Language
   */
  public updateLanguage = (data: CountryLanguage) =>
    this.api({
      data,
      method: 'PUT',
      url: '/admin/languages'
    });

  /**
   * Delete Language
   */
  public deleteLanguage = (id: number) =>
    this.api({
      data: { id },
      method: 'DELETE',
      url: '/admin/languages'
    });
}

export { LanguageService };
