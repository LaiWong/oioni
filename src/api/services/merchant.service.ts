import { MerchantFull, SelectedDates } from '@api';
import { enviroment } from '@env';
import { AxiosRequestConfig } from 'axios';
import { Language, ProfileSave } from '../models';
import { stringify } from 'querystring';
import { excludeEmptyFields } from '@core';

// import { formatTime } from '@api';

class MerchantService {
  /**
   * Get axios
   */
  public constructor(
    private create: (
      config: AxiosRequestConfig
    ) => (config: AxiosRequestConfig, enabled?: boolean) => any
  ) {}

  /**
   * Api
   */
  public api = this.create({
    withCredentials: true,
    baseURL: enviroment.api.merchant,
    headers: {
      Authorization: enviroment.credentials.authorization.merchant
    }
  });

  /**
   * Register
   */
  public save = (data: ProfileSave) =>
    this.api({
      data,
      method: 'POST',
      url: '/admin/merchants'
    });

  /**
   * Get merchant user data
   */
  public getUserData = () =>
    this.api(
      {
        url: `/merchant`
      },
      false
    );

  /**
   * Change password
   */
  public changePassword = ({
    id,
    token,
    password
  }: {
    id: string;
    password: string;
    token: string;
  }) =>
    this.api({
      data: {
        token,
        password
      },
      method: 'PUT',
      url: `/merchants/${id}/updatePassword`
    });

  /**
   * Upload file
   */
  public upload = (file: File) => {
    const data = new FormData();

    data.append('file', file);

    return this.api({
      data,
      method: 'POST',
      url: '/upload'
    });
  };

  /**
   * Get old merchants
   */
  public oldMerchants = ({
    id,
    page,
    search,
    perPage
  }: {
    id?: number;
    page?: number;
    search?: string;
    perPage?: number;
  }) =>
    this.api({
      url:
        `/admin/merchants/grid?` +
        stringify(
          excludeEmptyFields(
            {
              id,
              page,
              search,
              perPage
            },
            { emptyStrings: false }
          )
        )
    });

  /**
   * Get merchants
   */
  public merchants = ({
    id,
    page,
    search,
    perPage
  }: {
    id?: number;
    page?: number;
    search?: string;
    perPage?: number;
  }) =>
    this.api({
      url:
        `/v2/merchants?` +
        stringify(
          excludeEmptyFields(
            {
              id,
              page,
              search,
              perPage
            },
            { emptyStrings: false }
          )
        )
    });

  /**
   * Get merchant details
   */
  public merchantDetails = ({
    id,
    key,
    perPage,
    page,
    selectedDates,
    excludeDeleted
  }: {
    id: number;
    page?: number;
    key: 'bookings' | 'services' | '' | 'stores' | 'employees';
    perPage?: number;
    selectedDates?: SelectedDates;
    excludeDeleted?: boolean;
  }) =>
    this.api({
      url:
        `/v2/merchants/${id}/${key}?${
          key === 'services' ? 'active=false&' : ''
        }` +
        // ${selectedDates ? `start_date=${formatTime(selectedDates.start)}&end_date=${formatTime(selectedDates.end)}&`: ''}
        stringify(
          excludeEmptyFields(
            {
              page,
              perPage,
              excludeDeleted
            },
            { emptyStrings: false }
          )
        )
    });

  /**
   * Set merchant status
   */
  public setStatus = (id: number, status: MerchantFull['status']) =>
    this.api({
      method: 'PUT',
      url: '/merchants/' + id,
      data: {
        status
      }
    });

  /**
   * Get merchant user data
   */
  public getTranslations = (locale: Language) =>
    this.api(
      {
        url: `/translations?` + stringify({ locale })
      },
      false
    );

  /**
   * Send link for changing password
   */
  public sendLink = (data: { email: string }) =>
    this.api({
      data,
      url: `/merchants/refreshPassword`,
      method: 'POST'
    });
}

export { MerchantService };
