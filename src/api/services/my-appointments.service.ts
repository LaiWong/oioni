import axios, { AxiosInstance, AxiosRequestConfig } from 'axios';
import { enviroment } from '@env';
import { Appointment } from '../models';
import { stringify } from 'querystring';

class MyAppointmentsService {
  /**
   * Get axios
   */
  public constructor(
    private create: (
      config: AxiosRequestConfig
    ) => (config: AxiosRequestConfig) => any
  ) {}

  /**
   * Api
   */
  public api = this.create({
    withCredentials: true,
    baseURL: enviroment.api.merchant,
    headers: {
      Authorization: enviroment.credentials.authorization.merchant
    }
  });

  /**
   * Get appointments
   */
  public get = (
    employee: number,
    options: Partial<{
      date: string;
      month: string;
    }> = {}
  ) => {
    const query = stringify(options);

    return this.api({
      url: `/employees/${employee}/appointments?${query}`
    });
  };

  /**
   * Get time sheet
   */
  public getTimeSheet = (options: { employee_id: number; date: string }) => {
    const query = stringify(options);

    return this.api({
      url: `/schedules/week?${query}`
    });
  };
}

export { MyAppointmentsService };
