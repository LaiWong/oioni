import { AxiosRequestConfig } from 'axios';
import { enviroment } from '@env';
import { stringify } from 'querystring';
import { excludeEmptyFields } from '@core';
import { FinanceModel } from '../models';

class PaymentService {
  /**
   * Get axios
   */
  public constructor(
    private create: (
      config: AxiosRequestConfig
    ) => (config: AxiosRequestConfig, enabled?: boolean) => any
  ) {}

  /**
   * Api
   */
  public api = this.create({
    withCredentials: true,
    baseURL: enviroment.api.customer,
    headers: {
      Authorization: enviroment.credentials.authorization.merchant
    }
  });

  /**
   * Get Finance Models
   */
  public getFinanceModels = ({
    id,
    name,
    active
  }: {
    id?: number;
    name?: string;
    active?: boolean;
  }) =>
    this.api({
      method: 'GET',
      url: `/admin/finance-models?${stringify(
        excludeEmptyFields({ id, name, active })
      )}`
    });

  /**
   * Create Finance Models
   */
  public createFinanceModels = (data: FinanceModel) =>
    this.api({
      data,
      method: 'POST',
      url: '/admin/finance-models'
    });

  /**
   * Update Finance Models
   */
  public updateFinanceModels = (data: FinanceModel) =>
    this.api({
      data,
      method: 'PUT',
      url: '/admin/finance-models'
    });

  /**
   * Delete Finance Models
   */
  public deleteFinanceModels = (id: number) =>
    this.api({
      data: { id },
      method: 'DELETE',
      url: '/admin/finance-models'
    });
}

export { PaymentService };
