import { AxiosRequestConfig } from 'axios';
import { enviroment } from '@env';
import { stringify } from 'querystring';
import { Product, ProductStatus } from '../models';
import { excludeEmptyFields } from '@core';

class ProductService {
  /**
   * Get axios
   */
  public constructor(
    private create: (
      config: AxiosRequestConfig
    ) => (config: AxiosRequestConfig) => any
  ) {}

  /**
   * Api
   */
  public api = this.create({
    withCredentials: true,
    baseURL: enviroment.api.merchant,
    headers: {
      Authorization: enviroment.credentials.authorization.merchant
    }
  });

  /**
   * Add product
   */
  public add = (
    data: Pick<
      Product,
      | 'merchant_id'
      | 'category_id'
      | 'name'
      | 'price'
      | 'duration'
      | 'status'
      | 'settings'
    >
  ) =>
    this.api({
      data,
      method: 'POST',
      url: '/products'
    });

  /**
   * Update product
   */
  public update = (
    id: number,
    data: Pick<
      Product,
      | 'merchant_id'
      | 'category_id'
      | 'name'
      | 'price'
      | 'duration'
      | 'status'
      | 'settings'
    >
  ) =>
    this.api({
      data,
      method: 'PUT',
      url: '/products/' + id
    });

  /**
   * Get product(s)
   */
  public get = ({
    name,
    page,
    perPage,
    merchant,
    status
  }: {
    name?: string;
    page?: number;
    perPage?: number;
    merchant: number;
    status?: ProductStatus;
  }) =>
    this.api({
      url:
        `/merchants/${merchant}/services?status=active&active=false&` +
        stringify(
          excludeEmptyFields(
            {
              page,
              name,
              perPage,
              status
            },
            { emptyStrings: false }
          )
        )
    });

  /**
   * Get single product
   */
  public getOne = ({ id, merchant }) =>
    this.api({
      url: `/products?merchant_id=${merchant}&id=${id}&active=false`
    });

  /**
   * Get names
   */
  public names = (merchant?: number) =>
    this.api({
      url: `/service-names${merchant ? `?merchant_id=${merchant}` : ''}`
    });

  /**
   * Get service dict
   */
  public getDictionary = (merchant: number) =>
    this.api({
      url: `/merchants/${merchant}/service-names?status=active`
    });

  /**
   * Set services order
   */
  public setOrder = (data: { merchant_id: number; products: number[] }) =>
    this.api({
      url: `/products/order`,
      data,
      method: 'POST'
    });
}

export { ProductService };
