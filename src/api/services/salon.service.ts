import { enviroment } from '@env';
import { AxiosRequestConfig } from 'axios';
import { stringify } from 'querystring';
import { excludeEmptyFields } from '@core';
import { Salon, SalonStatus } from '../models';

class SalonService {
  /**
   * Get axios
   */
  public constructor(
    private create: (
      config: AxiosRequestConfig
    ) => (config: AxiosRequestConfig) => any
  ) {}

  /**
   * Api
   */
  public api = this.create({
    withCredentials: true,
    baseURL: enviroment.api.merchant,
    headers: {
      Authorization: enviroment.credentials.authorization.merchant
    }
  });

  public getMerchantSalons = (merchant_id: number) =>
    this.api({
      url: `/merchants?id=${merchant_id}`
    });

  /**
   * Get grid
   */
  public get = ({
    id,
    page,
    search,
    perPage,
    merchant,
    status
  }: {
    id?: number;
    page?: number;
    search?: string;
    perPage?: number;
    status?: SalonStatus;
    merchant: number;
  }) =>
    this.api({
      url:
        `/merchants/${merchant}/stores?` +
        stringify(
          excludeEmptyFields(
            {
              id,
              page,
              search,
              perPage,
              status
            },
            { emptyStrings: false }
          )
        )
    });

  /**
   * Update salon
   */
  public put = ({ id, ...rest }: Partial<Salon>) =>
    this.api({
      data: { ...rest },
      method: 'PUT',
      url: `/stores/${id}`
    });

  /**
   * Create salon
   */
  public post = (data: Partial<Salon>) =>
    this.api({
      data,
      method: 'POST',
      url: '/stores'
    });
}

export { SalonService };
