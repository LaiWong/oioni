import { enviroment } from '@env';
import { AxiosRequestConfig } from 'axios';
import { stringify } from 'querystring';
import { excludeEmptyFields } from '@core';
import { Salon } from '../models';

class ScheduleService {
  /**
   * Get axios
   */
  public constructor(
    private create: (
      config: AxiosRequestConfig
    ) => (config: AxiosRequestConfig) => any
  ) {}

  /**
   * Api
   */
  public api = this.create({
    withCredentials: true,
    baseURL: enviroment.api.merchant,
    headers: {
      Authorization: enviroment.credentials.authorization.merchant
    }
  });

  /**
   * Get all schedules
   */
  public getAllSchedules = ({
    status,
    store_id,
    date,
    employee_id
  }: {
    status?: string;
    store_id: number;
    date: string;
    employee_id: number;
  }) =>
    this.api({
      method: 'GET',
      url:
        `/schedules?` +
        stringify(
          excludeEmptyFields(
            {
              status,
              store_id,
              date,
              employee_id
            },
            { emptyStrings: false }
          )
        )
    });

  /**
   * Update schedule
   */
  public updateSchedules = (data: {
    employee_id: string;
    store_id: string;
    time_from: string;
    time_to: string;
    date_from: string;
    date_to: string;
  }) =>
    this.api({
      data,
      method: 'PUT',
      url: '/schedules'
    });
  /*Create schedule */
  public createSchedule = (data: {
    employee_id: string;
    store_id: string;
    time_from: string;
    time_to: string;
    date: string;
  }) =>
    this.api({
      data,
      method: 'POST',
      url: '/schedules'
    });
  /**
   *  Get schedule for week
   * */
  public getScheduleForWeek = ({
    status,
    date,
    employee_id
  }: {
    status: string;
    date: string;
    employee_id: string;
  }) =>
    this.api({
      method: 'GET',
      url: `/schedules/week?date=${date}&status=${status}&employee_id=${employee_id}`
    });
  /**
   *
   * Delete Schedule
   */
  public deleteSchedule = (data: {
    employee_id: string;
    store_id: string;
    time_from: string;
    time_to: string;
    date_from: string;
    date_to: string;
  }) =>
    this.api({
      data,
      method: 'DELETE',
      url: `/schedules`
    });
  /**
   *
   * Delete Schedules By id
   */
  public deleteSchedulesById = (data: { ids: number[] }) =>
    this.api({
      data,
      method: 'DELETE',
      url: `/schedules`
    });

  public getCalendarData = ({
    employee_id,
    store_id,
    date_from,
    date_to
  }: {
    employee_id: number;
    store_id: number;
    date_from: string;
    date_to: string;
  }) =>
    this.api({
      method: 'GET',
      url: `/schedules?employee_id=${employee_id}&store_id=${store_id}&date_from=${date_from}&date_to=${date_to}&blackouts=true`
    });

  /**
   * upsert schedules
   */
  public upsertSchedules = (data: {
    employee_id: number;
    store_id: number;
    time_from: string;
    time_to: string;
    dates: string[];
  }) =>
    this.api({
      data,
      method: 'PUT',
      url: '/schedules'
    });

  /**
   * Update schedules by id
   */
  public updateSchedulesById = (data: {
    store_id: number;
    employee_id: number;
    time_from: string;
    time_to: string;
    ids: number[];
  }) =>
    this.api({
      data,
      method: 'PUT',
      url: '/schedules'
    });
}

export { ScheduleService };
