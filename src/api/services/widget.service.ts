import { enviroment } from '@env';
import { AxiosRequestConfig } from 'axios';
import { Language } from '../models';

class WidgetService {
  /**
   * Get axios
   */
  public constructor(
    private create: (
      config: AxiosRequestConfig
    ) => (config: AxiosRequestConfig) => any
  ) {}

  /**
   * Api
   */
  public api = this.create({
    withCredentials: true,
    baseURL: enviroment.api.merchant,
    headers: {
      Authorization: enviroment.credentials.authorization.merchant
    }
  });

  /**
   * Get widget by domain
   */
  public getWidgetByDomain = ({ domain }: { domain: string }) =>
    this.api({
      method: 'GET',
      url: `/widget?domain=${domain}`
    });

  /**
   * Get merchant widgets
   */
  public getMerchantWidgets = ({ merchant_id }: { merchant_id: number }) =>
    this.api({
      method: 'GET',
      url: `/merchants/${merchant_id}/widgets`
    });

  /**
   * Create merchant widget
   */
  public createMerchantWidget = ({
    merchant_id,
    domain,
    name,
    description,
    language,
    cover_color,
    text_color,
    image_url
  }: {
    merchant_id: number;
    domain: string;
    name: string;
    description: string;
    language: Language;
    cover_color: string;
    text_color: string;
    image_url: string;
  }) =>
    this.api({
      data: {
        domain,
        name,
        description,
        language,
        cover_color,
        text_color,
        image_url
      },
      method: 'POST',
      url: `/merchants/${merchant_id}/widgets`
    });

  /**
   * Update merchant widget
   */
  public updateMerchantWidget = ({
    merchant_id,
    widget_id,
    domain,
    name,
    description,
    language,
    cover_color,
    text_color,
    image_url
  }: {
    merchant_id: number;
    widget_id: number;
    domain: string;
    name: string;
    description: string;
    language: Language;
    cover_color: string;
    text_color: string;
    image_url: string;
  }) =>
    this.api({
      data: {
        domain,
        name,
        description,
        language,
        cover_color,
        text_color,
        image_url
      },
      method: 'PUT',
      url: `/merchants/${merchant_id}/widgets/${widget_id}`
    });

  /**
   *
   * Delete merchant widget
   */
  public deleteWidget = ({
    merchant_id,
    widget_id
  }: {
    merchant_id: number;
    widget_id: number;
  }) =>
    this.api({
      method: 'DELETE',
      url: `/merchants/${merchant_id}/widgets/${widget_id}`
    });
}

export { WidgetService };
