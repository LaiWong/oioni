import * as React from 'react';
import { ButtonProps } from './button.props';
import * as styles from './button.scss';
import classNames from 'classnames';
import { capitalize } from '@core/utils';

/**
 * Renders Button
 */
const Button: React.FC<ButtonProps> = ({
  size,
  theme,
  children,
  className,
  ...props
}) => (
  <button
    className={classNames(
      styles.button,
      className,
      styles['buttonTheme' + capitalize(theme)],
      styles['buttonSize' + capitalize(size)]
    )}
    {...(props as any)}
  >
    {children}
  </button>
);

Button.defaultProps = {
  size: 'sm',
  theme: 'primary'
};

export { Button };
