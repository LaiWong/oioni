import * as React from 'react';
import { CardProps } from './card.props';
import * as styles from './card.scss';
import classNames from 'classnames';

/**
 * Renders Card
 */
const Card: React.FC<CardProps> = ({
  children,
  disabled,
  className,
  ...props
}) => (
  <div
    className={classNames(styles.card, className, {
      [styles.disabled]: disabled
    })}
    {...props}
  >
    {children}
    {disabled && <div className={styles.overlay} />}
  </div>
);

export { Card };
