/**
 * Props
 */
type CardProps = React.HTMLAttributes<HTMLDivElement> & {
  /**
   * Show overlay
   */
  disabled?: boolean;
};

export { CardProps };
