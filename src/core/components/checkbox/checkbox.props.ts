import { ControlProps } from '@core';

/**
 * Props
 */
type CheckboxProps = ControlProps<boolean> & {};

export { CheckboxProps };
