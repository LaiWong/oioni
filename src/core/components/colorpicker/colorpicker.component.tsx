import React from 'react';
import classNames from 'classnames';
import * as styles from './colorpicker.scss';
import SketchPicker from 'react-color';
import { hoc } from '@core/utils';
import { useColorPickerProps } from './colorpicker.props';
import down from '../../../public/img/widget-select-arrow-down.svg';

export const ColorPicker = hoc(
  useColorPickerProps,
  ({
    displayColorPicker,
    onClick,
    handleClose,
    value,
    disabled,
    onChange,
    placeholder,
    onBlur,
    focused,
    hasValue,
    reference,
    className,
    label,
    ...props
  }) => (
    <div>
      {label && <label className={styles.label}>{label}</label>}
      <div
        ref={reference}
        className={classNames(styles.picker, className, {
          [styles.uploadDisabled]: disabled,
          [styles.uploadHasValue]: hasValue,
          [styles.uploadHasLabel]: Boolean(label)
        })}
      >
        <div onClick={onClick} className={styles.placeholder}>
          <div
            style={{ backgroundColor: `${value}` }}
            className={styles.indicator}
          />
          <div className={styles.colorValue}>{value}</div>
          <div>
            <img
              style={{ width: '1.5rem' }}
              src={require('img/widget-select-arrow-down.svg')}
              alt='arrow down'
            />
          </div>
        </div>
        {displayColorPicker ? (
          <div className={styles.popover}>
            <SketchPicker
              disableAlpha={true}
              color={value}
              onChange={onChange}
            />
          </div>
        ) : null}
      </div>
    </div>
  )
);
