import { useState, useRef } from 'react';
import { ColorPickerProps } from 'react-color';
import { ControlProps } from '@core';
import { useClickOutside } from '@core/hooks';

type ColorPickerProps = ControlProps<any> & {};

export const useColorPickerProps = ({
  value,
  disabled,
  onChange,
  placeholder
}: ColorPickerProps) => {
  const [displayColorPicker, setDisplayColorPicker] = useState(false);
  const [focused, setFocused] = useState(false);
  const reference = useRef<HTMLDivElement>(null);
  const hasValue = Boolean(value) || value == 0;
  const onClick = () => {
    if (disabled) return;
    setDisplayColorPicker(!displayColorPicker);
    setFocused(!focused);
  };
  const onBlur = () => {
    setFocused(false);
  };
  const handleClose = () => {
    setDisplayColorPicker(false);
  };
  useClickOutside(reference, () => {
    if (focused) {
      setFocused(false);
      handleClose();
    }
  });

  return {
    displayColorPicker,
    onClick,
    handleClose,
    value,
    disabled,
    onChange,
    placeholder,
    onBlur,
    focused,
    hasValue,
    reference
  };
};
