import * as React from 'react';
import { ConfirmProps } from './confirm.props';
import * as styles from './confirm.scss';
import classNames from 'classnames';

/**
 * Renders Confirm
 */
const Confirm: React.FC<ConfirmProps> = ({
  className,
  children,
  onClose,
  confirmRef
}) => (
  <div className={classNames(styles.confirm, className)} ref={confirmRef}>
    {onClose && <div className={styles.close} onClick={onClose} />}
    <div className={styles.content}>{children}</div>
  </div>
);

export { Confirm };
