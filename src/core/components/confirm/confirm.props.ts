import { Ref } from 'react';

/**
 * Props
 */
type ConfirmProps = {
  /**
   * On close modal
   */
  onClose?: () => any;

  /**
   * Confirm ref
   */
  confirmRef?: Ref<HTMLDivElement>;

  /**
   * Class name
   */
  className?: string;
};

export { ConfirmProps };
