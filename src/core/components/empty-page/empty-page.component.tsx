import * as React from 'react';
import * as styles from './empty-page.scss';
import { EmptyPageProps } from './empty-page.props';
import { useTranslation } from 'react-i18next';

/**
 * Renders EmptyPage
 */
const EmptyPage: React.FC<EmptyPageProps> = ({ title }) => {
  const { t } = useTranslation();
  return (
    <div className={styles.container}>
      <h6>
        {t('superAdmin.emptyPage.no')} {title}
      </h6>
    </div>
  );
};

export { EmptyPage };
