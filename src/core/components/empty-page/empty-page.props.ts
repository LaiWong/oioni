/**
 * Props
 */
type EmptyPageProps = {
  /**
   * Title
   */
  title: string;
};

export { EmptyPageProps };
