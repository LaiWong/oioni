import { withField } from '@core/utils';
import { Input } from '../input';
import { Select } from '../select';
import { Textarea } from '../textarea';
import { Upload } from '../upload';
import { ColorPicker } from '../colorpicker';
import { TimeRange } from '../time-range';
import { InputNumber } from '../input-number';

/**
 * Form field
 */
const Field = {
  Input: withField(Input),
  Select: withField(Select),
  Upload: withField(Upload),
  Textarea: withField(Textarea),
  ColorPicker: withField(ColorPicker),
  TimeRange: withField(TimeRange),
  InputNumber: withField(InputNumber)
};

export { Field };
