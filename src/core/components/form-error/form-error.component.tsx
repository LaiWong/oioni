import * as React from 'react';
import { FormErrorProps } from './form-error.props';
import * as styles from './form-error.scss';
import { hoc } from '@core/utils';
import { useFormikContext } from 'formik';
import i18next from 'i18next';

const extract = (error: string | object) => {
  if (typeof error == 'object') {
    let result;
    let errors = Object.values(error);

    for (const item of errors) {
      result = extract(item);

      if (result) break;
    }

    return result;
  }

  return error;
};

/**
 * Use form error props
 */
const useFormErrorProps = ({ children }: FormErrorProps) => {
  const { errors, submitCount } = useFormikContext();
  const error = extract(errors);

  const content = children || error;
  const submitted = submitCount > 0;

  return {
    error,
    content,
    visible: content && submitted
  };
};

/**
 * Renders FormError
 */
const FormError = hoc(useFormErrorProps, ({ content, visible }) => {
  if (!visible) return null;

  return (
    <div className={styles.formError}>
      <div className={styles.icon}>!</div>
      <div className={styles.error}>{i18next.t(content)}</div>
    </div>
  );
});

export { FormError };
