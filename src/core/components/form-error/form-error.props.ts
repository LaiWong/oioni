import { ReactNode } from 'react';

/**
 * Props
 */
type FormErrorProps = {
  /**
   * Errors
   */
  children?: ReactNode;
};

export { FormErrorProps };
