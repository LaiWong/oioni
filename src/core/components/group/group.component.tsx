import * as React from 'react';
import { GroupProps } from './group.props';
import * as styles from './group.scss';
import classNames from 'classnames';

/**
 * Renders FieldGroup
 */
const Group: React.FC<GroupProps> & {
  Row: React.FC;
} = ({ title, helper, children, className, titleClassName }) => (
  <div className={className}>
    {(title || helper) && (
      <div className={classNames(styles.header)}>
        {title && (
          <div className={classNames(styles.title, titleClassName)}>
            {title}
          </div>
        )}
        {helper && <div className={styles.helper}>{helper}</div>}
      </div>
    )}
    <div
      className={classNames(styles.fields, {
        [styles.fieldsMulti]: children && React.Children.count(children) > 1,
        [styles.fieldsSingle]: children && React.Children.count(children) == 1
      })}
    >
      {children}
    </div>
  </div>
);

/**
 * Merged fields
 */
Group.Row = ({ children }) => <div className={styles.row}>{children}</div>;

export { Group };
