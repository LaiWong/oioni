import { ReactNode } from 'react';

/**
 * Props
 */
type GroupProps = {
  title?: ReactNode;
  className?: string;
  titleClassName?: string;
  headerClassName?: string;
  helper?: ReactNode;
};

export { GroupProps };
