import * as React from 'react';
import { IconProps } from './icon.props';
import classNames from 'classnames';

/**
 * Renders Icon
 */
const Icon: React.FC<IconProps> = ({ className, name, ...props }) => (
  <span className={classNames(className, `icon-${name}`)} {...props} />
);

export { Icon };
