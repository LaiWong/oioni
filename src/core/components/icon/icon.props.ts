/**
 * Props
 */
type IconProps = React.HTMLAttributes<HTMLDivElement> & { name: string };

export { IconProps };
