import React, { useEffect, useRef, useState } from 'react';
import styles from './input-number.module.scss';
import classNames from 'classnames';
import { ControlProps, hoc } from '@core/utils';

import isNaN from 'lodash/isNaN';
import isNumber from 'lodash/isNumber';
import isString from 'lodash/isString';

type InputNumberProps = ControlProps<any> & {
  up: Function;
  down: Function;
  format: Function;
  handleChange: Function;
  text: string | number;
  name?: string;
};

export const useInputNumber = ({
  value,
  id,
  onChange,
  format
}: InputNumberProps) => {
  const [text, setText] = useState(value);

  useEffect(() => {
    setText(value);
  }, [value]);

  const parseText = text => {
    if (isNumber(text)) return text;

    if (isString(text)) {
      text = text.trim();

      if (!text) return '';
      const num = parseFloat(text);

      if (!isNaN(num)) {
        return num;
      }
    }

    return '';
  };

  const handleChange = e => {
    const value = parseText(+e.target.value);

    if (onChange) {
      onChange(format(value));
    }
  };

  const up = e => {
    if (onChange) {
      onChange(changeValue('+', text));
    }
    e.preventDefault();
  };

  const down = e => {
    if (onChange) {
      onChange(changeValue('-', text));
    }
    e.preventDefault();
  };

  const changeValue = (mod, value, step = 1) => {
    value = format(mod === '+' ? +value + step : +value - step);

    setText(value);
    return value;
  };

  return {
    id,
    text,
    up,
    down,
    handleChange
  };
};

export const InputNumber = hoc(
  useInputNumber,
  ({ id, text, up, down, handleChange }) => (
    <div className={classNames(styles.inputNumber)}>
      <input
        type='number'
        id={id}
        value={text}
        onChange={handleChange}
        className={classNames(styles.input)}
      />
      <button
        style={{ backgroundImage: `url(${require('img/time-range-up.svg')})` }}
        className={styles.up}
        onClick={e => {
          up(e);
        }}
      />
      <button
        style={{
          backgroundImage: `url(${require('img/time-range-down.svg')})`
        }}
        className={styles.down}
        onClick={e => {
          down(e);
        }}
      />
    </div>
  )
);
