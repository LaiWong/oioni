import * as React from 'react';
import { InputProps } from './input.props';
import * as styles from './input.scss';
import classNames from 'classnames';
import { hoc } from '@core/utils';
import { useState, useRef } from 'react';
import i18next from 'i18next';

import _Mask, * as InputMask from 'react-input-mask';

/**
 * !Typings retard alert!
 */
const Mask: typeof _Mask = InputMask as any;

/**
 * Use input
 */
const useInputProps = ({
  on,
  api,
  value,
  onFocus,
  disabled,
  onChange,
  suggestions,
  onSuggestionSelect,
  ...props
}: InputProps) => {
  const [focused, setFocused] = useState(false);
  const hasValue = Boolean(value) || value == '0';
  const input = useRef<HTMLInputElement>();

  const isSuggestionsVisible =
    suggestions?.length > 0 && value?.toString()?.length > 2 && focused;

  const _onFocus = (event: React.FocusEvent<HTMLInputElement>) => {
    setFocused(true);

    onFocus(event);
  };

  const _onBlur = () => {
    setFocused(false);
  };

  const onInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    if (!focused) setFocused(true);

    const { value } = event.target;

    onChange(value);

    on?.change(value);
  };

  const onSuggestionClick = (event, suggestion: typeof suggestions[number]) => {
    event.preventDefault();

    input.current.blur();

    onChange(typeof suggestion == 'string' ? suggestion : suggestion.name);

    if (onSuggestionSelect) onSuggestionSelect(suggestion);
  };

  api({
    focused,
    focus: () => input?.current?.focus()
  });

  return {
    input,
    focused,
    _onBlur,
    _onFocus,
    hasValue,
    onInputChange,
    onSuggestionClick,
    isSuggestionsVisible
  };
};

/**
 * Renders Input
 */
const Input = hoc(
  useInputProps,
  ({
    mask,
    type,
    input,
    value,
    label,
    focused,
    _onBlur,
    disabled,
    _onFocus,
    maskChar,
    hasValue,
    className,
    onKeyDown,
    suggestions,
    onInputChange,
    onSuggestionClick,
    isSuggestionsVisible,
    placeholder,
    ...props
  }) => (
    <div
      className={classNames(styles.input, className, {
        [styles.inputFocused]: focused,
        [styles.inputDisabled]: disabled,
        [styles.inputHasValue]: hasValue,
        [styles.inputHasLabel]: Boolean(label)
      })}
    >
      {label && <label className={styles.label}>{i18next.t(label)}</label>}

      <Mask
        className={styles.field}
        type={type}
        mask={mask}
        onBlur={_onBlur}
        onKeyDown={onKeyDown}
        inputRef={ref => (input.current = ref)}
        onFocus={_onFocus}
        disabled={disabled}
        value={value?.toString() ?? ''}
        maskChar={maskChar}
        onChange={onInputChange}
        placeholder={placeholder}
        // {...(props as any)}
      />
      {isSuggestionsVisible && (
        <div className={styles.suggestions}>
          {(suggestions as any[]).map((suggestion, index) => (
            <div
              key={index}
              className={styles.suggestion}
              onMouseDown={event => onSuggestionClick(event, suggestion)}
            >
              {typeof suggestion == 'string' ? suggestion : suggestion.name}
            </div>
          ))}
        </div>
      )}
    </div>
  )
);

Input.defaultProps = {
  value: '',
  mask: null,
  type: 'text',
  api: () => {},
  maskChar: '_',
  onFocus: () => {}
};

export { Input };
