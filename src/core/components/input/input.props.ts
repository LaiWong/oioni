import { ReactNode } from 'react';
import { ControlProps } from '../../utils/formik/control';

/**
 * Props
 */
type InputProps = ControlProps<string | number> & {
  /**
   * Html input type
   */
  type?: 'text' | 'number' | 'password';

  /**
   * Input mask
   */
  mask?: string;

  /**
   * Mask char
   */
  maskChar: string;

  /**
   * Focus handler
   */
  onFocus?: (event: React.FocusEvent<HTMLInputElement>) => any;

  /**
   * Suggest
   */
  suggest?: boolean;

  /**
   * Suggestions list
   */
  suggestions?: string[] | { name: string }[];

  /**
   * Suggestion select handle
   */
  onSuggestionSelect: (suggestion: string | { name: string }) => any;

  /**
   * Ref attr
   */
  api: (ref) => any;

  /**
   * Key down handler
   */
  onKeyDown?: (event: React.KeyboardEvent<HTMLInputElement>) => any;

  /**
   * Side handlers
   */
  on?: {
    change?: (value: string) => any;
  };
  placeholder?: string;
};

export { InputProps };
