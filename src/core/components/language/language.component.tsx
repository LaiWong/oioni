import * as React from 'react';
import { LanguageProps } from './language.props';
import * as styles from './language.scss';
import { Icon } from '../icon';
import classNames from 'classnames';
import { hoc } from '@core/utils';
import { useSelector } from 'react-redux';
import { State } from '@store';
import { useState, useRef } from 'react';
import { useClickOutside } from '@core/hooks';
import { useLanguagesList } from '@app/hooks/use-languages-list';

/**
 * Use language props
 */
const useLanguageProps = ({ onChange, value }: LanguageProps) => {
  const [opened, setOpened] = useState(false);
  const languages = useLanguagesList();
  const dropdown = useRef<HTMLDivElement>();

  const onClick = (lang: string) => {
    onChange(lang);

    setOpened(false);
  };

  const onDropClick = () => setOpened(!opened);

  const icon = languages.find(item => item.id == value)?.icon;

  useClickOutside(dropdown, () => {
    setOpened(false);
  });

  return {
    icon,
    opened,
    onClick,
    dropdown,
    languages,
    onDropClick
  };
};

/**
 * Renders Language
 */
const Language = hoc(
  useLanguageProps,
  ({
    icon,
    value,
    opened,
    onClick,
    dropdown,
    languages,
    className,
    boxClassName,
    onDropClick
  }) => (
    <div className={classNames(styles.language, className)} ref={dropdown}>
      <div className={styles.content} onClick={onDropClick}>
        <div className={styles.flag}>
          <img src={icon} alt='flag' />
        </div>
        <div className={styles.text}>
          <span className={styles.textName}>
            {languages.find(one => one.id == value)?.name}
          </span>
        </div>
        <Icon name='arrow-triangle' className={styles.icon} />
      </div>

      {opened && (
        <div className={classNames(styles.box, boxClassName)}>
          {languages.map((item, index) => (
            <div
              className={styles.option}
              key={item.id}
              onClick={() => onClick(item.id)}
            >
              <img className={styles.optionIcon} src={item.icon} />
              <div className={styles.optionName}>{item.name}</div>
            </div>
          ))}
        </div>
      )}
    </div>
  )
);

export { Language };
