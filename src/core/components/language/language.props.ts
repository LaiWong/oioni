/**
 * Props
 */
type LanguageProps = {
  /**
   * Value
   */
  value: string;

  /**
   * ClassName
   */
  className?: string;

  /**
   * On change
   */
  onChange: (lang: string) => any;

  /**
   * Box className
   */
  boxClassName?: string;
};

export { LanguageProps };
