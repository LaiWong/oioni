import * as React from 'react';
import classNames from 'classnames';
import { ModalProps } from './modal.props';
import * as styles from './modal.scss';

/**
 * Renders Modal
 */
const Modal: React.FC<ModalProps> = ({ children, className, ...props }) => (
  <div className={styles.modalContainer}>
    <div className={classNames(styles.modal, className)} {...(props as any)}>
      {children}
    </div>
  </div>
);

export { Modal };
