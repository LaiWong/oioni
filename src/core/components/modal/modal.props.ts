/**
 * Props
 */
type ModalProps = {
  /**
   * Class name
   */
  className?: string;
};

export { ModalProps };
