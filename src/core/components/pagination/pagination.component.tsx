import * as React from 'react';
import { PaginationProps } from '@core/components';
import * as styles from './pagination.scss';
import ReactPaginate from 'react-paginate';
import { useTranslation } from 'react-i18next';

/**
 * Renders Modal
 */
const Pagination: React.FC<PaginationProps> = ({ length, onPageChange }) => {
  const { t } = useTranslation();
  return (
    !!length &&
    length !== 1 && (
      <ReactPaginate
        containerClassName={styles.container}
        activeLinkClassName={styles.pageActive}
        pageLinkClassName={styles.page}
        previousLinkClassName={styles.buttonsLink}
        previousClassName={styles.buttons}
        nextClassName={styles.buttons}
        nextLinkClassName={styles.buttonsLink}
        disabledClassName={styles.buttonsDisabled}
        pageCount={length}
        onPageChange={onPageChange}
        marginPagesDisplayed={2}
        pageRangeDisplayed={2}
        breakLinkClassName={styles.page}
        nextLabel={t('merchantPanel.pagination.btn.next')}
        previousLabel={t('merchantPanel.pagination.btn.previous')}
      />
    )
  );
};
export { Pagination };
