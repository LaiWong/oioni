import { useState } from 'react';

/**
 * Props
 */
type PaginationProps = {
  /**
   * Pagination length
   */
  length: number;

  /**
   * On page number click
   */
  onPageChange: (page: { selected: number }) => void;
};

/**
 * Use pagination hook
 */
const usePagination = (cb?: () => void) => {
  const [selectedPage, setSelectedPage] = useState(1);

  const onPageChange = ({ selected }: { selected: number }) => {
    setSelectedPage(selected + 1);

    cb?.();
  };

  return { selectedPage, onPageChange };
};

export { usePagination, PaginationProps };
