import * as React from 'react';
import { SearchProps } from './search.props';
import * as styles from './search.scss';
import { Icon } from '../icon';
import classNames from 'classnames';

/**
 * Renders Select
 */
const Search: React.FC<SearchProps> = ({
  value,
  onChange,
  className,
  placeholder
}) => (
  <div className={classNames(styles.search, className)}>
    <input
      type='text'
      className={styles.field}
      placeholder={placeholder}
      value={value}
      onChange={event => onChange(event.target.value)}
    />
    <button className={styles.button}>
      <Icon name='search' />
    </button>
  </div>
);

export { Search };
