/**
 * Props
 */
type SearchProps = {
  value: string;
  onChange: (value: string) => any;
  className?: string;
  placeholder?: string;
};

export { SearchProps };
