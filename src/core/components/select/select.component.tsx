import { capitalize, hoc } from '@core/utils';
import classNames from 'classnames';
import * as React from 'react';
import { Icon } from '../icon';
import { useSelectProps } from './select.props';
import * as styles from './select.scss';
import i18next from 'i18next';

/**
 * Renders Select
 */
const Select = hoc(
  useSelectProps,
  ({
    label,
    theme,
    after,
    select,
    focused,
    options,
    onClick,
    selected,
    hasValue,
    disabled,
    className,
    selectors,
    reference,
    placeholder,
    onOptionClick
  }) => (
    <div
      className={classNames(styles.select, className, {
        [styles.selectFocused]: focused,
        [styles.selectHasValue]: hasValue,
        [styles.selectDisabled]: disabled,
        [styles.selectHasLabel]: Boolean(label),
        [styles['select' + capitalize(theme)]]: theme
      })}
      ref={reference}
    >
      {after && <div className={styles.after}>{after}</div>}
      {label && <label className={styles.label}>{i18next.t(label)}</label>}

      <div className={styles.field} onClick={onClick}>
        {selected ? select(selectors.name, selected) : placeholder || ''}
      </div>
      <Icon className={styles.icon} name='arrow-light' onClick={onClick} />
      {focused && (
        <div className={styles.box}>
          {options?.map((option, index) => {
            const id = select(selectors.id, option);
            const name = select(selectors.name, option);

            return (
              <div
                data-id={id}
                key={`${index}-${id}`}
                className={styles.option}
                onClick={() => onOptionClick(option)}
              >
                {name}
              </div>
            );
          })}
        </div>
      )}
    </div>
  )
);

Select.defaultProps = {
  shape: {},
  value: null,
  options: [],
  theme: 'default',
  placeholder: null
};

export { Select };
