import { useState, useMemo, useRef, ReactNode } from 'react';
import { get } from 'object-path';
import { useClickOutside } from '@core/hooks';
import { ControlProps } from '@core/utils';

/**
 * Field selector
 */
type Selector = string | ((option: unknown) => any);

/**
 * Props
 */
type SelectProps = ControlProps<any> & {
  /**
   * Options list
   */
  options: any[];

  /**
   * Control theme
   */
  theme?: 'default' | 'compact' | 'appointments';

  /**
   * Options shape
   */
  shape?: {
    id?: Selector;
    name?: Selector;
  };

  /**
   * Placeholder
   */
  placeholder?: string;

  /**
   * After select block
   */
  after?: ReactNode;
};

/**
 * Default selectors
 */
const defaultSelectors = {
  id: 'id',
  name: 'name'
};

/**
 * Use select
 */
const useSelectProps = ({
  shape,
  value,
  options,
  disabled,
  onChange,
  placeholder
}: SelectProps) => {
  const [focused, setFocused] = useState(false);
  const selectors = useMemo(
    () => ({
      ...defaultSelectors,
      ...shape
    }),
    [shape]
  );

  const reference = useRef<HTMLDivElement>(null);

  const hasValue = Boolean(value) || value == 0;

  const select = (selector, option) =>
    typeof selector == 'string' ? get(option, selector) : selector(option);

  const selected = options?.find(one => select(selectors.id, one) == value);

  const onClick = () => {
    if (disabled) return;

    setFocused(!focused);
  };

  const onBlur = () => {
    setFocused(false);
  };

  const onOptionClick = (option: SelectProps['options'][number]) => {
    onChange(select(selectors.id, option));

    setFocused(false);
  };

  useClickOutside(reference, () => {
    if (focused) setFocused(false);
  });

  return {
    select,
    onBlur,
    focused,
    onClick,
    selected,
    hasValue,
    reference,
    selectors,
    placeholder,
    onOptionClick
  };
};

export { SelectProps, useSelectProps };
