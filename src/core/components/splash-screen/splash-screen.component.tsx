import * as React from 'react';
import * as styles from './splash-screen.scss';
import { FC } from 'react';

/**
 * Renders SplashScreen
 */
const SplashScreen: FC = () => (
  <div className={styles.splashScreen}>
    <img className={styles.logo} src={require('img/logo-lg.svg')} alt='logo' />
    <div className={styles.backgroundGreen} />
    <div className={styles.backgroundOrange} />
    <div className={styles.backgroundBlue} />
    <div className={styles.backgroundPink} />
  </div>
);

export { SplashScreen };
