import * as React from 'react';
import { FC } from 'react';

/**
 * Renders Body
 */
const Body: FC = ({ children }) => <tbody>{children}</tbody>;

export { Body };
