/**
 * Props
 */
type BodyProps = {
  /**
   * Children
   */
  children: Element;
};

export { BodyProps };
