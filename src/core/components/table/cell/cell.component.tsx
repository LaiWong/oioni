import * as React from 'react';
import { CellProps } from './cell.props';
import * as styles from './cell.scss';
import { useContext } from 'react';
import { TableContext } from '../table.context';
import { HeadContext } from '../head';
import classNames from 'classnames';

/**
 * Renders Cell
 */
const Cell: React.FC<CellProps> = ({
  className,
  children,
  bold,
  atEnd,
  atCenter,
  ...props
}) => {
  const isHead = useContext(HeadContext);
  const { onSort } = useContext(TableContext);

  return React.createElement(isHead ? 'th' : 'td', {
    children,
    className: classNames(styles.cell, className, {
      [styles.cellHead]: isHead,
      [styles.cellBold]: bold,
      [styles.cellAtEnd]: atEnd,
      [styles.cellAtCenter]: atCenter
    }),
    ...props
  });
};

export { Cell };
