import * as React from 'react';
import { HeadProps } from './head.props';
import { HeadContext } from './head.context';

/**
 * Renders Head
 */
const Head: React.FC<HeadProps> = ({ children }) => (
  <thead>
    <HeadContext.Provider value>{children}</HeadContext.Provider>
  </thead>
);

export { Head };
