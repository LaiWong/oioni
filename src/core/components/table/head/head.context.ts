import { createContext } from 'react';

const HeadContext = createContext<boolean>(null);

export { HeadContext };
