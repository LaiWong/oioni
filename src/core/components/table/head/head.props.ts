/**
 * Props
 */
type HeadProps = {
  /**
   * Children
   */
  children: JSX.Element;
};

export { HeadProps };
