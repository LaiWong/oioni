import * as React from 'react';
import { RowProps } from './row.props';
import * as styles from './row.scss';
import classNames from 'classnames';
import { forwardRef } from 'react';

/**
 * <Row />
 */
const Row: React.FC<RowProps> = ({
  children,
  className,
  elementRef,
  ...props
}) => (
  <tr
    className={classNames(className, {
      [styles.clickable]: Boolean(props.onClick)
    })}
    ref={elementRef}
    {...props}
  >
    {children}
  </tr>
);

export { Row };
