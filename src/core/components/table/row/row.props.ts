import { Ref, HTMLAttributes } from 'react';

/**
 * Props
 */
type RowProps = HTMLAttributes<HTMLTableRowElement> & {
  /**
   * Class name
   */
  className?: string;

  /**
   * Element ref
   */
  elementRef?: Ref<any>;
};

export { RowProps };
