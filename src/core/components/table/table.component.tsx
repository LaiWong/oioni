import { TableContext } from './table.context';
import * as React from 'react';
import * as styles from './table.scss';
import { TableProps } from './table.props';

/**
 * Renders Grid
 */
const Table: React.FC<TableProps> = ({ children, onSort }) => (
  <TableContext.Provider value={{ onSort }}>
    <table className={styles.table}>{children}</table>
  </TableContext.Provider>
);

export { Table };
