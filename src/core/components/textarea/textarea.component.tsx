import * as React from 'react';
import { TextareaProps } from './textarea.props';
import * as styles from './textarea.scss';
import classNames from 'classnames';

/**
 * Renders Textarea
 */
const Textarea: React.FC<TextareaProps> = ({
  placeholder,
  className,
  value = '',
  onChange,
  onKeyDown
}) => (
  <div className={classNames(styles.textarea, className)}>
    <textarea
      onKeyDown={onKeyDown ? onKeyDown : null}
      className={styles.field}
      placeholder={placeholder}
      onChange={event => onChange(event.target.value)}
      value={value}
    />
  </div>
);

export { Textarea };
