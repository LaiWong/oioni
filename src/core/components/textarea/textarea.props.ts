import { ReactNode } from 'react';
import { ControlProps } from '@core/utils';

/**
 * Props
 */
type TextareaProps = ControlProps & {
  placeholder?: string;
};

export { TextareaProps };
