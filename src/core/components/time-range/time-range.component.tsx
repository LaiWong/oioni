import React from 'react';
import { useTimeRangeProps } from './time-range.props';
import styles from './time-range.scss';
import { hoc } from '@core/utils';
import line from './static/time-range-line.svg';

export const TimeRange = hoc(
  useTimeRangeProps,
  ({ input, from, to, formatHours, formatMinutes, onChangeTime }) => (
    <div className={styles.timeRange}>
      <div className={styles.inputs}>
        {input({
          id: 'from.h',
          value: from.h,
          format: formatHours,
          onChange: value => onChangeTime('from', 'h', value)
        })}
        {input({
          id: 'from.m',
          value: from.m,
          format: formatMinutes,
          onChange: value => onChangeTime('from', 'm', value)
        })}
        <div className={styles.line}>
          <img src={line} alt='line' />
        </div>
        {input({
          id: 'to.h',
          value: to.h,
          format: formatHours,
          onChange: value => onChangeTime('to', 'h', value)
        })}
        {input({
          id: 'to.m',
          value: to.m,
          format: formatMinutes,
          onChange: value => onChangeTime('to', 'm', value)
        })}
      </div>
    </div>
  )
);
