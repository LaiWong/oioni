import React from 'react';
import { format } from 'date-fns';
import { ControlProps } from '@core';

type TimeRangeProps = ControlProps<{ time_from: string; time_to: string }> & {
  name: string;
  input: Function;
};

const DEFAULT_TIME = '00';
const DEFAULT_SEPARATOR = ':';
const DEFAULT_HOURS_FORMAT = 'HH';
const DEFAULT_MINUTES_FORMAT = 'mm';

const formatters = {
  h: h => format(new Date().setHours(h), DEFAULT_HOURS_FORMAT),
  m: m => format(new Date().setMinutes(m), DEFAULT_MINUTES_FORMAT)
};

const formatHours = formatters.h;
const formatMinutes = formatters.m;

const parseTime = time => {
  const [h, m] = (time && time?.split(DEFAULT_SEPARATOR)) || [
    DEFAULT_TIME,
    DEFAULT_TIME
  ];
  return {
    h,
    m
  };
};

const getTimeFormat = ({ h, m }) =>
  `${formatHours(h)}${DEFAULT_SEPARATOR}${formatMinutes(m)}`;

export const useTimeRangeProps = ({
  name,
  input,
  value,
  onChange
}: TimeRangeProps) => {
  const from = parseTime(value.time_from);
  const to = parseTime(value.time_to);

  const settersTimeRange = {
    from: (key, value) => {
      from[key] = formatters[key](value);
    },
    to: (key, value) => {
      to[key] = formatters[key](value);
    }
  };

  const onChangeTime = (key, time, value) => {
    if (settersTimeRange[key] as Function) {
      settersTimeRange[key](time, value);
    }

    onChange({
      time_from: getTimeFormat(from),
      time_to: getTimeFormat(to)
    });
  };

  return {
    input,
    value,
    from,
    to,
    formatHours,
    formatMinutes,
    onChangeTime
  };
};
