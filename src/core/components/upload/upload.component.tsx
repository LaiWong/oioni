import * as React from 'react';
import { UploadProps, useUploadProps } from './upload.props';
import * as styles from './upload.scss';
import classNames from 'classnames';
import { hoc } from '@core/utils';

/**
 * Renders Upload
 */
const Upload = hoc(
  useUploadProps,
  ({
    t,
    label,
    multi,
    onClick,
    disabled,
    hasValue,
    inputRef,
    previews,
    className,
    onClearClick,
    onInputChange
  }) => (
    <React.Fragment>
      <div
        className={classNames(styles.upload, className, {
          [styles.uploadDisabled]: disabled,
          [styles.uploadHasValue]: hasValue,
          [styles.uploadHasLabel]: Boolean(label)
        })}
        onClick={onClick}
      >
        <div className={styles.field}>
          <div className={styles.value}>
            {t('common.uploadFile.placeholder')}
          </div>

          <img className={styles.icon} src={require('img/upload.svg')} />
        </div>

        <input
          type='file'
          ref={inputRef}
          multiple={multi}
          className={styles.input}
          onChange={onInputChange}
        />
      </div>
      {previews?.length > 0 && (
        <div className={styles.previews}>
          {previews.map((url, index) => (
            <div className={styles.preview} key={`${url}${index}`}>
              <div className={styles.clear} onClick={() => onClearClick(url)}>
                <div />
              </div>
              <img src={url} />
            </div>
          ))}
        </div>
      )}
    </React.Fragment>
  )
);

Upload.defaultProps = {
  multi: false
};

export { Upload };
