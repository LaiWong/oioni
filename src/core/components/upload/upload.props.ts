import { ControlProps } from '@core';
import { useRef } from 'react';
import { useDispatch } from 'react-redux';
import { upload } from '@store/general/actions';
import { snack } from '@store/snackbar';
import { useTranslation } from 'react-i18next';
import i18next from 'i18next';

/**
 * Props
 */
type UploadProps = ControlProps<string | string[]> & {
  /**
   * Can upload multiple files
   */
  multi?: boolean;

  /**
   * Accepted formats
   */
  accept: string[];
};

/**
 * <Upload /> props
 */
const useUploadProps = ({ value, accept, multi, onChange }: UploadProps) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const inputRef = useRef<HTMLInputElement>();

  const hasValue = multi ? value?.length > 0 : Boolean(value);

  const caption =
    hasValue && typeof value == 'string'
      ? value.substring(value.lastIndexOf('/') + 1)
      : '';

  const previews = Array.isArray(value) ? value : value ? [value] : [];

  const onClick = () => {
    inputRef.current.click();
  };

  const onInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    let files = Array.from(event.target.files);
    const totalFilesLength = value?.length + files?.length;

    if (files.length == 0) return onChange(null);

    if (totalFilesLength > 10) {
      dispatch(
        snack(
          !multi
            ? 'common.uploadFile.max10Files'.replace('0', '').replace('s', '')
            : 'common.uploadFile.max10Files'
        )
      );

      return;
    }

    if (accept) {
      const validFiles = files.filter(item =>
        accept.some(one => item.name.endsWith(one))
      );

      if (validFiles.length != files.length) {
        dispatch(
          snack(
            `common.uploadFile.someFilesAreInvalid, ${{
              extensions: accept.join(', ')
            }}`,
            'info'
          )
        );

        files = validFiles;
      }
    }

    if (!files.length) return;

    dispatch(
      upload(files, urls => {
        onChange(multi ? [...(value || []), ...urls] : urls[0]);
      })
    );

    event.target.value = '';
  };

  const onClearClick = (url: string) => {
    onChange(multi ? (value as string[]).filter(item => item != url) : null);
  };

  return {
    t,
    caption,
    onClick,
    previews,
    hasValue,
    inputRef,
    onClearClick,
    onInputChange
  };
};

export { UploadProps, useUploadProps };
