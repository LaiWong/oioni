import { useMounted } from './use-mounted';
import { useState } from 'react';
import { useUpdate } from './use-update';

const useInterval = (callback: () => any, interval: number) => {
  const [counter, setCounter] = useState(0);

  useMounted(() => {
    const intervalId = setInterval(() => {
      setCounter(counter => counter + 1);
    }, interval);

    return () => clearInterval(intervalId);
  });

  useUpdate(callback, [counter]);
};

export { useInterval };
