import { useTranslation } from 'react-i18next';

const useMonths = () => {
  const { t } = useTranslation();

  return [
    t('common.months.1'),
    t('common.months.2'),
    t('common.months.3'),
    t('common.months.4'),
    t('common.months.5'),
    t('common.months.6'),
    t('common.months.7'),
    t('common.months.8'),
    t('common.months.9'),
    t('common.months.10'),
    t('common.months.11'),
    t('common.months.12')
  ];
};

export { useMonths };
