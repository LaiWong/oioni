import { useState, useMemo } from 'react';
import { createPostfixer } from '@core/utils';
import { MLString } from '@api';

/**
 * Use ml value
 */
const useMultiLanguage = (lang: string) => {
  const [value, setValue] = useState(lang);
  const ml = createPostfixer(`.${value}`);

  return [value, setValue, ml] as [typeof value, typeof setValue, typeof ml];
};

/**
 * Use ml string
 */
const useMultiLanguageMapper = (lang: string, defaultLanguage?: string) =>
  useMemo(
    () => (value: MLString) => {
      if (value) {
        if (value[lang]) {
          return value[lang];
        } else if (defaultLanguage && value[defaultLanguage]) {
          return value[defaultLanguage];
        } else {
          return (
            value[Object.keys(value).find(el => !!value[el] === true)] || 'n/a'
          );
        }
      } else {
        return '';
      }
    },
    [lang]
  );

export { useMultiLanguage, useMultiLanguageMapper };
