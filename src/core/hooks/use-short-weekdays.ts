import { useTranslation } from 'react-i18next';

const useShortWeekdays = () => {
  const { t } = useTranslation();

  return [
    t('common.shortWeekdays.7'),
    t('common.shortWeekdays.1'),
    t('common.shortWeekdays.2'),
    t('common.shortWeekdays.3'),
    t('common.shortWeekdays.4'),
    t('common.shortWeekdays.5'),
    t('common.shortWeekdays.6')
  ];
};

export { useShortWeekdays };
