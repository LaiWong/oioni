import { useEffect, useState } from 'react';
import { getValueFromObjectByString } from '@core/utils';
import moment from 'moment';

const useSort = <T>(sortElements: T[]) => {
  const [sortBy, setSortBy] = useState('');
  const [direction, setDirection] = useState<'up' | 'down'>('up');
  const [sortedElements, setSortedElements] = useState<T[]>([]);

  const sort = (
    firstElement: T,
    secondElement: T,
    sortField: string,
    isParseToNumber: boolean = false
  ) => {
    let a = getValueFromObjectByString(sortField, firstElement);
    let b = getValueFromObjectByString(sortField, secondElement);

    if (isParseToNumber) {
      a = parseFloat(a);
      b = parseFloat(b);
    }

    return direction === 'up' ? (a < b ? 1 : -1) : a > b ? 1 : -1;
  };

  const sortByDate = (firstElement: T, secondElement: T, sortField: string) => {
    let a = getValueFromObjectByString(sortField, firstElement);
    let b = getValueFromObjectByString(sortField, secondElement);

    const isAfter = moment(a).isAfter(b);
    const isBefore = moment(a).isBefore(b);

    return direction === 'up' ? (isAfter ? 1 : -1) : isBefore ? 1 : -1;
  };

  useEffect(() => {
    if (!sortElements.length) {
      if (!sortedElements.length) return;

      setSortedElements([]);

      return;
    }

    setSortedElements([...sortElements]);
  }, [sortElements]);

  return {
    sortBy,
    direction,
    sortedElements,
    setSortBy,
    setDirection,
    setSortedElements,

    sort,
    sortByDate
  };
};

export { useSort };
