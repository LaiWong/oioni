import { useRef, useEffect } from 'react';

/**
 * Use update
 */
const useUpdate: typeof useEffect = (callback, deps) => {
  const mounted = useRef(false);

  useEffect(() => {
    if (!mounted.current) {
      mounted.current = true;

      return;
    }

    return callback();
  }, deps);
};

export { useUpdate };
