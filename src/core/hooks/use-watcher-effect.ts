import { useRef, useEffect } from 'react';

class Watcher {
  /**
   * Watcher active
   */
  public active = false;

  /**
   * Enable watcher
   */
  public start = () => {
    this.active = true;
  };

  /**
   * Disable watcher
   */
  public stop = () => {
    this.active = false;
  };
}
/**
 * Use activatable use effect
 */
const useWatcherEffect = (callback, deps) => {
  const watcher = useRef(new Watcher());

  useEffect(() => {
    const { active } = watcher.current;

    if (!active) return;

    return callback();
  }, [deps]);

  return watcher.current;
};

export { useWatcherEffect };
