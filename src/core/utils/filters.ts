import { stringify } from 'querystring';

/**
 * Exclude empty fields from object
 */
const excludeEmptyFields = (
  source: object,
  options: { emptyStrings?: boolean } = {}
) => {
  const { emptyStrings = true } = options;
  const result = {};

  for (let key in source) {
    const value = source[key];

    if (value == null || value == undefined) continue;
    if (!emptyStrings && value == '') continue;

    result[key] = value;
  }

  return result;
};

const omitEmptyQueryParams = (
  source: object,
  options?: { emptyStrings: boolean }
): string => stringify(excludeEmptyFields(source, options) as any);

export { excludeEmptyFields, omitEmptyQueryParams };
