const capitalize = (source: string) =>
  source.charAt(0).toUpperCase() + source.substring(1);

/**
 * Separate number by delimetr
 */
const separate = (x: number | string, separator: string = ' ') => {
  const parts = x.toString().split('.');

  parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, separator);

  return parts.join('.');
};

/**
 * Create postfixer func
 */
const createPostfixer = (postfix: string) => (
  value: string | TemplateStringsArray
) => `${value}${postfix}`;

/**
 * Format price
 */
const formatPrice = (x: number | string) =>
  new Intl.NumberFormat('ru-RU', {
    style: 'currency',
    currency: 'EUR'
  }).format(parseFloat(`${x || 0}`));

export { capitalize, separate, createPostfixer, formatPrice };
