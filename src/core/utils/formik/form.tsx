import * as React from 'react';
import { FormikProps, useFormik, FormikContext } from 'formik';
import { Fragment } from 'react';

import { Form as FormikForm } from 'formik';

/**
 * Form
 */
const Form: React.FC<React.HTMLAttributes<HTMLFormElement> & {
  html?: boolean;
  form: FormikProps<any>;
  formRef?: any;
}> = ({ html, form, formRef, children, ...props }) => (
  <FormikContext.Provider value={form}>
    {html ? (
      <FormikForm ref={formRef} {...props}>
        {children}
      </FormikForm>
    ) : (
      children
    )}
  </FormikContext.Provider>
);

Form.defaultProps = {
  html: true
};

export { Form };
