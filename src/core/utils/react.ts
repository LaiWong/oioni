import { ReactChild } from 'react';
import * as React from 'react';

/**
 * Children of type
 */
const childrenOfType = <
  T extends keyof JSX.IntrinsicElements | React.JSXElementConstructor<any>
>(
  children,
  type
) => {
  let results = [];

  React.Children.map(children, (item: React.ReactElement) => {
    if (item.type == type) results.push(item);
  });

  return results as React.ReactComponentElement<T>[];
};

export { childrenOfType };
