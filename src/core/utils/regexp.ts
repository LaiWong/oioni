/**
 * Reg exp for different string formats
 */
const formats = {
  date: {
    eu: (value: string) =>
      new RegExp(`[0-9]{2}\.[0-9]{2}\.[0-9]{4}`, 'gi').test(value)
  },
  phoneNumber: {
    check: (value: string) =>
      new RegExp(
        /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im
      ).test(value)
  },
  email: {
    check: (value: string) => new RegExp(/\S+@\S+\.\S+/).test(value)
  },
  photo: {
    check: (value: string) =>
      new RegExp(/^(ftp|http|https):\/\/[^ "]+$/).test(value)
  }
};

export { formats };
