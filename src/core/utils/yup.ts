import { Schema, object } from 'yup';
import { MLString } from '@api';

/**
 * Test functions for common stuff
 */
const testers = {
  /**
   * Ensure that mask is fullfilled like required to fill
   */
  mask: (chat: string = '_') => (value: string | number) =>
    value && !value.toString().includes('_')
};

const schemas = {
  ml: {
    string: (message: string) =>
      object<MLString>()
        .required(message)
        .nullable(true)
        .test('ml-required', message, value =>
          value ? Object.keys(value).length > 0 : false
        )
  }
};

export { testers, schemas };
