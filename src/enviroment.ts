/**
 * Build enviroment
 */
const enviroment = {
  api: (process.env.api as any) as {
    merchant: string;
    customer: string;
    order: string;
  },
  credentials: (process.env.credentials as any) as {
    authorization: {
      order: string;
      merchant: string;
      customer: string;
    };
  },

  stage: process.env.stage,
  development: process.env.NODE_ENV != 'production',
  sub_domain: process.env.sub_domain as string,
  widget_base_url: process.env.widget_base_url as string
};

export { enviroment };
