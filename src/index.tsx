import './styles/global.scss';
import * as React from 'react';
import { render } from 'react-dom';
import { createBrowserHistory } from 'history';
import { createStore } from './store';
import { Provider } from './provider';
import { App } from '@app';
import { init } from './localization';
import { Language } from '@api';
import { Route, Redirect, Switch } from 'react-router-dom';

const history = createBrowserHistory();
const store = createStore(history);

render(
  <Provider store={store} history={history}>
    <Switch>
      <Route path='/:language(et|en|ru|ua)' component={App} />
      <Redirect to={Language.et} />
    </Switch>
  </Provider>,
  document.getElementById('app')
);
