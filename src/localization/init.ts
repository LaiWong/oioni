import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';
import { setLocale } from './yup';
import { Language } from '@api';

/**
 * Setup localization
 */
const init = async (initialLanguage: Language, initialTranslations: Object) => {
  await i18n.use(initReactI18next).init({
    lng: initialLanguage,
    resources: {
      [initialLanguage]: {
        common: initialTranslations
      }
    },
    interpolation: {
      escapeValue: false
    },
    supportedLngs: [Language.et, Language.en, Language.ru, Language.ua],
    defaultNS: 'common',
    ns: ['common']
  } as any);

  setLocale();
};

export { init };
