import React, { Suspense } from 'react';
import { Redirect, Route, RouteChildrenProps, Switch } from 'react-router-dom';
import { MainPage, Snackbar } from './components';
import { useDispatch, useSelector } from 'react-redux';
import { hoc, useMounted } from '@core';
import { State } from '@store';
import { startup } from '@store/general';
import { Appointments } from '@appointments';
import { Services } from '@services';
import { Employees } from '@employees';
import { Customers } from '@customers';
import { Dashboard } from '@dashboard';
import { Auth } from '@auth';
import { Salon } from '@salon';
import { Merchant } from '@merchant';
import { SplashScreen } from '@core/components/splash-screen';
import { EmployeePanelTab, UserType } from '@api';
import { MyAppointments } from '@my-appointments';
import { NavigationMenu } from './components/navigation-menu';
import { EmployeeSettings } from '@employee-settings';
import { Profile } from '@profile';
import { RecoverPassword } from '@auth/pages/recover-password';
import { Finances } from '@finances';
import { Country } from '@country';
import { Languages } from '@languages';
import { Currencies } from '@currency';
import { Refund } from '@refund';
// import { Worktimes } from '../worktimes';

// const Auth = register('auth', () => import('@auth'));
// const Profile = register('profile', () => import('@profile'));
// const Dashboard = register('dashboard', () => import('@dashboard'));
// const Appointments = register('appointments', () => import('@appointments'));
// const Services = register('services', () => import('@services'));
// const Employees = register('employees', () => import('@employees'));
// const Salon = register('salon', () => import('@salon'));

/**
 * Use app
 */
const useApp = (props: RouteChildrenProps) => {
  const dispatch = useDispatch();
  const {
    ready,
    authorized,
    merchantUser,
    employeeUser,
    userType
  } = useSelector((state: State) => state.general);

  const isAdmin = (merchantUser || {}) && userType === UserType.admin;

  useMounted(() => {
    dispatch(startup());
  });

  return {
    ready,
    isAdmin,
    authorized: Boolean(authorized && (merchantUser || employeeUser)),
    userType
  };
};

/**
 * Renders App
 */
const App = hoc(
  useApp,
  ({ authorized, isAdmin, userType, ready, match: { url, path } }) => {
    const fallback = null;

    if (!ready) {
      return <SplashScreen />;
    }

    return (
      <div>
        <Switch>
          <Route
            exact
            path={`${path}/recover/password`}
            component={RecoverPassword}
          />
          <Route>
            {authorized ? (
              userType == UserType.employee ? (
                <Suspense fallback={fallback}>
                  <Switch>
                    <Switch>
                      <Route
                        path={`${path}/${EmployeePanelTab.myAppointments}`}
                        component={MyAppointments}
                      />
                      <Route
                        path={`${path}/${EmployeePanelTab.settings}`}
                        component={EmployeeSettings}
                      />
                      <Redirect
                        to={`${url}/${EmployeePanelTab.myAppointments}`}
                      />
                    </Switch>
                  </Switch>

                  <Route
                    path={`${path}/:currentTab`}
                    component={NavigationMenu}
                  />
                </Suspense>
              ) : (
                <MainPage>
                  <Suspense fallback={fallback}>
                    <Switch>
                      {isAdmin ? (
                        <Switch>
                          <Route path={`${path}/profile`} component={Profile} />
                          <Route
                            path={`${path}/merchants`}
                            component={Merchant}
                          />
                          <Route
                            path={`${path}/finances`}
                            component={Finances}
                          />
                          <Route path={`${path}/country`} component={Country} />
                          <Route
                            path={`${path}/languages`}
                            component={Languages}
                          />
                          <Route
                            path={`${path}/currency`}
                            component={Currencies}
                          />
                          <Route path={`${path}/refund`} component={Refund} />

                          <Redirect to={`${url}/finances`} />
                        </Switch>
                      ) : (
                        <Switch>
                          <Route path={`${path}/profile`} component={Profile} />
                          <Route
                            path={`${path}/appointments/:id?`}
                            component={Appointments}
                          />
                          <Route
                            path={`${path}/customers`}
                            component={Customers}
                          />
                          <Route
                            path={`${path}/services`}
                            component={Services}
                          />
                          <Route
                            path={`${path}/employee`}
                            component={Employees}
                          />
                          <Route path={`${path}/salon`} component={Salon} />
                          <Route
                            path={`${path}/dashboard`}
                            component={Dashboard}
                          />
                          <Redirect to={`${url}/dashboard`} />
                        </Switch>
                      )}
                    </Switch>
                  </Suspense>
                </MainPage>
              )
            ) : (
              <Suspense fallback={fallback}>
                <Switch>
                  <Route path={`${path}/auth`} component={Auth} />
                  <Redirect to={`${url}/auth`} />
                </Switch>
              </Suspense>
            )}

            <Snackbar />
          </Route>
        </Switch>
      </div>
    );
  }
);

export { App };
