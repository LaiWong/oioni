import { Button, hoc, Language as LanguagePicker } from '@core';
import { State } from '@store';
import classNames from 'classnames';
import * as React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { NavLink } from 'react-router-dom';
import * as styles from './main-page.scss';
import { User } from './user';
import { useTranslation } from 'react-i18next';
import { useNavLinks } from '@app/hooks';
import { Language, UserType } from '@api';
import { setLanguage } from '@store/general';
import { navigate } from '@store/router';

/**
 * Use main page
 */
const useMainPage = () => {
  const {
    t,
    i18n: { language }
  } = useTranslation();
  const dispatch = useDispatch();

  const { merchantUser } = useSelector((state: State) => state.general);
  const userType = useSelector((state: State) => state.general.userType);
  const isAdmin = merchantUser.isAdmin && userType === UserType.admin;

  const links = useNavLinks();

  const onLanguageChange = (language: Language) => {
    dispatch(setLanguage(language));
  };

  const onLogoClick = () => {
    dispatch(navigate('/'));
  };

  return {
    t,
    links,
    language,
    onLanguageChange,
    ready: Boolean(merchantUser),
    isAdmin,
    onLogoClick
  };
};

/**
 * Renders MainPage
 */
const MainPage = hoc(
  useMainPage,
  ({
    t,
    links,
    isAdmin,
    children,
    ready,
    language,
    onLanguageChange,
    onLogoClick
  }) => {
    if (!ready) return null;
    return (
      <div className={classNames(styles.mainPage, 'container')}>
        <aside className={styles.sidebar}>
          <div className={styles.logo}>
            <img
              onClick={onLogoClick}
              src={require('img/main-logo.svg')}
              alt='logo'
            />
          </div>

          <nav>
            {links.map((group, index) => {
              if (
                (!isAdmin && group.every(item => item.admin)) ||
                (isAdmin && group.every(item => item.merchant))
              )
                return null;

              return (
                <ul className={styles.siteNav} key={index}>
                  {group.map((link, index) => (
                    <li className={styles.item} key={index}>
                      <NavLink
                        className={classNames(styles.link)}
                        activeClassName={styles.linkActive}
                        to={link.to}
                      >
                        {link.name}
                      </NavLink>
                    </li>
                  ))}
                </ul>
              );
            })}
          </nav>
        </aside>
        <div className={styles.mainPart}>
          <header className={styles.mainHeader}>
            <LanguagePicker value={language} onChange={onLanguageChange} />
            <User />
          </header>

          <div className={styles.content}>{children}</div>
        </div>
      </div>
    );
  }
);

export { MainPage };
