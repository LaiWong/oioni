/**
 * Props
 */
type MainPageProps = {
  /**
   * Children
   */
  children: Element;
};

export { MainPageProps };
