import * as React from 'react';
import { SearchbarProps } from './searchbar.props';
import * as styles from './searchbar.scss';
import { Icon } from '@core/components/icon';
import classNames from 'classnames';

/**
 * Renders Searchbar
 */
const Searchbar: React.FC<SearchbarProps> = ({ children, className }) => (
  <div className={classNames(styles.searchbar, className)}>
    <input type='text' className={styles.field} />
    <label className={styles.label}>{children}</label>
    <button className={styles.button}>
      <Icon name='search' />
    </button>
  </div>
);

export { Searchbar };
