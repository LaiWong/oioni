import { ReactNode } from 'react';

/**
 * Props
 */
type SearchbarProps = {
  className?: string;
  children?: ReactNode;
};

export { SearchbarProps };
