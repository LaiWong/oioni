import * as React from 'react';
import * as styles from './user.scss';
import { useSelector, useDispatch } from 'react-redux';
import { State } from '@store';
import { logout } from '@store/general';
import { hoc, useClickOutside } from '@core';
import { useState, useRef } from 'react';
import { navigate } from '@store/router';
import { useTranslation } from 'react-i18next';

/**
 * User props
 */
const useUserProps = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const { merchant_name } = useSelector(
    (state: State) => state.general.merchantUser
  );
  const [opened, setOpened] = useState(false);
  const container = useRef();

  const onProfileClick = () => {
    dispatch(navigate('/profile'));

    setOpened(false);
  };

  useClickOutside(container, () => {
    if (!opened) return;

    setOpened(false);
  });

  return {
    t,
    opened,
    container,
    onProfileClick,
    name: merchant_name,
    status: t('merchantPanel.adminAccess'),
    onClick: () => {
      setOpened(!opened);
    },
    onLogoutClick: () => dispatch(logout())
  };
};

/**
 * Renders User
 */
const User = hoc(
  useUserProps,
  ({
    t,
    name,
    opened,
    status,
    onClick,
    container,
    onLogoutClick,
    onProfileClick
  }) => (
    <div className={styles.user} ref={container}>
      <div className={styles.avatar} onClick={onClick}>
        <img src={require('img/user-avatar-placeholder.svg')} alt='avatar' />
      </div>
      <div className={styles.info} onClick={onClick}>
        <span className={styles.name}>{name}</span>
        {status && <span className={styles.status}>{status}</span>}
      </div>

      {opened && (
        <div className={styles.box}>
          <div
            className={styles.boxItem}
            onClick={onProfileClick}
            key='profile'
          >
            {t('merchantPanel.tabs.profile')}
          </div>
          <div className={styles.boxItem} onClick={onLogoutClick} key='logout'>
            {t('merchantPanel.logout')}
          </div>
        </div>
      )}
    </div>
  )
);

export { User };
