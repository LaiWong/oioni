import * as React from 'react';
import * as styles from './navigation-menu.scss';
import { Icon } from '@core/components/icon';
import { useNavigationMenu } from './navigation-menu.props';
import { hoc } from '@core';
import classNames from 'classnames';

/**
 * Renders NavigationMenu
 */
const NavigationMenu = hoc(
  useNavigationMenu,
  ({ currentTab, navigationMenuTabs, onTabClick }) => (
    <div className={styles.navigationMenu}>
      {navigationMenuTabs.map(({ tab, iconName, disabled }) => (
        <Icon
          className={classNames(styles.item, {
            [styles.itemSelected]: tab === currentTab,
            [styles.itemDisabled]: disabled
          })}
          name={iconName}
          key={tab}
          onClick={e => {
            disabled ? e.preventDefault() : onTabClick(tab);
          }}
        />
      ))}
    </div>
  )
);

export { NavigationMenu };
