import { EmployeePanelTab } from '@api';

const navigationMenuTabs = [
  {
    tab: EmployeePanelTab.myAppointments,
    iconName: 'calendar',
    disabled: false
  },
  { tab: EmployeePanelTab.clients, iconName: 'clients', disabled: true },
  { tab: EmployeePanelTab.addAppointment, iconName: 'add', disabled: true },
  { tab: EmployeePanelTab.profile, iconName: 'person', disabled: true },
  { tab: EmployeePanelTab.settings, iconName: 'settings', disabled: false }
];

export { navigationMenuTabs };
