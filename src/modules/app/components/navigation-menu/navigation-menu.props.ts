import { navigationMenuTabs } from './navigation-menu.data';
import { useRouteMatch } from 'react-router';
import { useDispatch } from 'react-redux';
import { navigate } from '@store/router';
import { EmployeePanelTab } from '@api';
import { logout } from '@store/general';

const useNavigationMenu = () => {
  const dispatch = useDispatch();
  const {
    params: { currentTab }
  } = useRouteMatch<{ currentTab: string }>();
  const onTabClick = (tab: EmployeePanelTab) => {
    dispatch(navigate(`/${tab}`));
  };
  const onLogoutClick = () => {
    dispatch(logout());
  };

  return {
    currentTab,
    navigationMenuTabs,
    onTabClick,
    onLogoutClick
  };
};

export { useNavigationMenu };
