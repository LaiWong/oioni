import * as React from 'react';
import * as styles from './snackbar.scss';
import { useSelector } from 'react-redux';
import { State } from '@store';
import { hoc, capitalize } from '@core';
import classNames from 'classnames';
import i18next from 'i18next';

/**
 * Use snackbar props
 */
const useSnackbarProps = () => {
  const { type, content, options } = useSelector(
    (state: State) => state.snackbar
  );

  return {
    type,
    content,
    options
  };
};

const icons = {
  info: require('img/snackbar-info.svg'),
  error: require('img/snackbar-error.svg'),
  warning: require('img/snackbar-warning.svg')
};

/**
 * Renders Snackbar
 */
const Snackbar = hoc(useSnackbarProps, ({ type, content, options }) => {
  if (!content) return null;

  return (
    <div
      className={classNames(
        styles.snackbar,
        styles['snackbar' + capitalize(type)]
      )}
    >
      <img className={styles.icon} src={icons[type]} />
      <div className={styles.content}>
        {i18next.t(content as string, options)}
      </div>
    </div>
  );
});

export { Snackbar };
