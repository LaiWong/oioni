type ServiceCategoryColors = {
  color: string;
  bgColor: string;
};

const serviceCategoryColors: ServiceCategoryColors[] = [
  {
    color: '#41B3E3',
    bgColor: '#ECF9FF'
  },
  {
    color: '#EB7583',
    bgColor: '#FFEAEC'
  },
  {
    color: '#D677E2',
    bgColor: '#F7EFF8'
  },
  {
    color: '#1DC0C9',
    bgColor: '#E6F8F9'
  },
  {
    color: '#FF4C4D',
    bgColor: '#FFDEDE'
  },
  {
    color: '#F547A1',
    bgColor: '#FEECF5'
  },
  {
    color: '#F77B26',
    bgColor: '#FEF0E7'
  },
  {
    color: '#81BB78',
    bgColor: '#E6F1E4'
  },
  {
    color: '#465AEF',
    bgColor: '#E6E9F9'
  },
  {
    color: '#F4512F',
    bgColor: '#FEF3F1'
  },
  {
    color: '#2977FC',
    bgColor: '#7EADFE'
  },
  {
    color: '#E49903',
    bgColor: '#FBF4E7'
  },
  {
    color: '#16D1A4',
    bgColor: '#E7F9F4'
  },
  {
    color: '#A2507B',
    bgColor: '#FAF3F7'
  },
  {
    color: '#0993E1',
    bgColor: '#E8F3F9'
  },
  {
    color: '#5228FF',
    bgColor: '#EEEAFF'
  },
  {
    color: '#7D65DB',
    bgColor: '#F7F5FF'
  },
  {
    color: '#C3D129',
    bgColor: '#F0F2DB'
  }
];

const useAppointmentsWithColors = <AP extends { serviceCategoryId: number }>(
  appointments: AP[]
): (AP & { color: string; bgColor: string })[] =>
  appointments.map((appointment, index) => ({
    ...appointment,
    ...serviceCategoryColors[index]
  }));

export { useAppointmentsWithColors };
