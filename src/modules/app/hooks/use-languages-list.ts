import { useTranslation } from 'react-i18next';

const useLanguagesList = () => {
  const { t } = useTranslation();

  return [
    {
      id: 'en',
      name: t(`general.languages.en`),
      // icon: 'https://www.countryflags.io/gb/flat/64.png'
      icon: require('img/flags/eng.png')
    },
    {
      id: 'et',
      name: t(`general.languages.et`),
      // icon: 'https://www.countryflags.io/ee/flat/64.png'
      icon: require('img/flags/esti.png')
    },
    {
      id: 'ru',
      name: t(`general.languages.ru`),
      // icon: 'https://www.countryflags.io/ru/flat/64.png'
      icon: require('img/flags/ru.png')
    },
    {
      id: 'ua',
      name: t(`general.languages.ua`),
      // icon: 'https://www.countryflags.io/UA/shiny/64.png'
      icon: require('img/flags/ua.png')
    }
  ];
};

export { useLanguagesList };
