import { ReactNode } from 'react';
import { useTranslation } from 'react-i18next';
import { useRouteMatch } from 'react-router';

/**
 * Nav links
 */
const useNavLinks = (): {
  to: string;
  name: ReactNode;
  admin?: boolean;
  merchant?: boolean;
}[][] => {
  const { t } = useTranslation();
  const { url } = useRouteMatch();

  return [
    [
      {
        to: `${url}/dashboard`,
        name: t('merchantPanel.tabs.dashboard'),
        merchant: true
      },
      {
        to: `${url}/appointments`,
        name: t('merchantPanel.tabs.bookings'),
        merchant: true
      },
      {
        to: `${url}/customers`,
        name: t('merchantPanel.tabs.customers'),
        merchant: true
      },
      {
        to: `${url}/services`,
        name: t('merchantPanel.tabs.services'),
        merchant: true
      },
      {
        to: `${url}/employee`,
        name: t('merchantPanel.tabs.employees'),
        merchant: true
      },
      {
        to: `${url}/salon`,
        name: t('merchantPanel.tabs.salons'),
        merchant: true
      }
    ],
    [
      // {
      //   to: `${url}/dashboard`,
      //   name: t('merchantPanel.tabs.dashboard'),
      //   admin: true
      // },
      {
        to: `${url}/finances`,
        name: t('merchantPanel.tabs.finances'),
        admin: true
      },
      {
        to: `${url}/merchants`,
        name: t('merchantPanel.tabs.merchants'),
        admin: true
      },
      {
        to: `${url}/languages`,
        name: t('Languages'),
        admin: true
      },
      {
        to: `${url}/currency`,
        name: t('Currency'),
        admin: true
      },
      {
        to: `${url}/country`,
        name: 'Countries', //t('merchantPanel.tabs.country'),
        admin: true
      },
      {
        to: `${url}/refund`,
        name: t('Refund'),
        admin: true
      }
    ],
    [
      // {
      //   to: '/reports',
      //   name: 'Reports'
      // },
      {
        to: `${url}/profile`,
        name: t('merchantPanel.tabs.profile')
      }

      // {
      //   to: '/settings',
      //   name: 'Settings'
      // }
    ]
  ];
};

export { useNavLinks };
