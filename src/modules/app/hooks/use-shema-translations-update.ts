import { useUpdate } from '@core';
import { useTranslation } from 'react-i18next';

const useSchemaTranslationsUpdate = (form: {
  values: Object;
  setValues: (values: Object) => void;
}) => {
  const { i18n } = useTranslation();

  useUpdate(() => {
    form.setValues(form.values);
  }, [i18n.language]);
};

export { useSchemaTranslationsUpdate };
