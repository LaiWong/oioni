import { hoc, Select } from '@core';
import classNames from 'classnames';
import * as React from 'react';
import { useAppointmentsProps } from './appointments.props';
import * as styles from './appointments.scss';
import { AppointmentEditor } from './components';
import { AppointmentList } from '@appointments/components';

/**
 * Renders Appointments
 */
const Appointments = hoc(
  useAppointmentsProps,
  ({
    t,
    scrollRef,
    dispatchScrollPosition,
    selectedAppointmentId,
    onChangeSelectedAppointment,
    employeesSelectOptions,
    selectedEmployee,
    onChangeEmployee,
    dispatchOffset,
    filterDateLabel,
    filterDateDays,
    onChangeDate,
    appointments,
    timeSelectOptions,
    selectedTime,
    onChangeSelectedTime,
    selectedDate,
    isEditorDisabled
  }) => {
    const timeSelector = timeSelectOptions.map(x => (
      <button
        className={classNames(styles.timeSelectorItem, {
          [styles.selected]: selectedTime === x.id
        })}
        onClick={() => onChangeSelectedTime(x.id)}
        key={x.id}
      >
        <div className={styles.timeSelectorIcon}>
          <img
            className={styles.timeSelectorIcon}
            src={require('img/appointments-clock.svg')}
          />
        </div>
        <div className={styles.timeSelectorInfo}>
          <span>{x.name}</span>
        </div>
      </button>
    ));

    return (
      <div className={styles.appointments}>
        <div className={styles.filters}>
          <div className={styles.filtersEmployees}>
            <Select
              theme='appointments'
              className={styles.filtersEmployeesSelector}
              options={employeesSelectOptions}
              value={selectedEmployee}
              onChange={onChangeEmployee}
            />
          </div>
          <div className={styles.filtersDatepicker}>
            <div className={styles.filtersDatepickerMonth}>
              <div onClick={() => dispatchOffset({ type: 'previous' })}>
                <img
                  className={styles.filtersDatepickerMonthIcon}
                  src={require('img/appointments-arrow-left-gray.svg')}
                />
              </div>
              <div className={styles.filtersDatepickerMonthLabel}>
                {filterDateLabel}
              </div>
              <div onClick={() => dispatchOffset({ type: 'next' })}>
                <img
                  className={styles.filtersDatepickerMonthIcon}
                  src={require('img/appointments-arrow-right-gray.svg')}
                />
              </div>
            </div>
            <div className={styles.filtersDatepickerDays}>
              <div
                className={styles.filtersDatepickerDay}
                onClick={() => dispatchScrollPosition({ type: 'previous' })}
              >
                <img src={require('img/appointments-arrow-left.svg')} />
              </div>

              <div
                className={styles.filtersDatepickerDaysScroll}
                ref={scrollRef}
              >
                {filterDateDays.map(({ number, label, selected, inPast }) => (
                  <div
                    className={classNames(styles.filtersDatepickerDay, {
                      [styles.selected]: selected,
                      [styles.inPast]: inPast
                    })}
                    onClick={() => onChangeDate(number)}
                    key={number}
                  >
                    <span className={styles.filtersDatepickerDayNumber}>
                      {number}
                    </span>
                    <span className={styles.filtersDatepickerDayLabel}>
                      {label}
                    </span>
                  </div>
                ))}
              </div>

              <div
                className={styles.filtersDatepickerDay}
                onClick={() => dispatchScrollPosition({ type: 'next' })}
              >
                <img src={require('img/appointments-arrow-right.svg')} />
              </div>
            </div>
          </div>
        </div>
        <div className={styles.content}>
          <div className={styles.contentLeft}>
            <div className={styles.header}>
              <div className={styles.headerCaption}>
                <h2>{t('merchantPanel.booking.list.bookingsTitle')}</h2>
              </div>
              <div className={styles.timeSelector}>{timeSelector}</div>
            </div>
            <AppointmentList
              items={appointments}
              onChangeSelectedAppointment={onChangeSelectedAppointment}
              appointmentId={selectedAppointmentId}
            />
          </div>
          <div className={styles.contentRight}>
            <AppointmentEditor
              onChangeSelectedAppointment={onChangeSelectedAppointment}
              appointmentId={selectedAppointmentId}
              editorSelectedDate={selectedDate}
              formDisabled={isEditorDisabled}
            />
          </div>
        </div>
      </div>
    );
  }
);

export { Appointments };
