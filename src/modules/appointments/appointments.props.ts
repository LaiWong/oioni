import { RouteComponentProps } from 'react-router';
import { useMeta } from '@core';
import { useEffect, useMemo, useReducer, useRef, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { State } from '@store';
import { useTranslation } from 'react-i18next';
import {
  getAppointments,
  getCustomers,
  getEmployees,
  getServices
} from '@appointments/store';
import moment, { MomentSetObject } from 'moment';
import { getSalons } from '@salon';

/**
 * Props
 */
type AppointmentsProps = RouteComponentProps<{ id: string }> & {};

/**
 * Use appointments props
 */
const useAppointmentsProps = ({
  match: {
    params: { id }
  }
}: AppointmentsProps) => {
  useMeta({ title: 'Appointments - Oioni Administration Console' }, []);
  const { t, i18n } = useTranslation();
  const dispatch = useDispatch();

  const { _appointments, _employees } = useSelector(
    ({
      merchant: {
        appointments: { appointments, services, employees, customers }
      }
    }: State) => ({
      _appointments: appointments,
      _services: services,
      _employees: employees,
      _customers: customers
    })
  );

  useEffect(() => {
    dispatch(getAppointments());
    dispatch(getServices());
    dispatch(getEmployees());
    dispatch(getCustomers());
    dispatch(getSalons());
  }, []);

  const currentDate = new Date();
  const [selectedDate, setSelectedDate] = useState(new Date());
  const scrollRef = useRef(null);
  const [scrollPosition, dispatchScrollPosition] = useReducer(
    (state, action) => {
      const el = scrollRef.current;
      const scrollStep = el.offsetWidth / 3;
      const scrollMaxPosition = el.scrollWidth - el.offsetWidth;

      switch (action.type) {
        case 'next':
          if (state + scrollStep > scrollMaxPosition) {
            return scrollMaxPosition;
          } else {
            return state + scrollStep;
          }
        case 'previous':
          if (state - scrollStep < 0) {
            return 0;
          } else {
            return state - scrollStep;
          }
        case 'today':
          const dayOffset = el.firstChild.offsetWidth;
          const dayMargins = 4;
          const dayWidth = dayOffset + dayMargins;
          const dayNumber = new Date().getDate();
          const offset = dayNumber * dayWidth - dayWidth;

          return state + offset;
        case 'reset':
          return 0;
        default:
          throw new Error();
      }
    },
    0
  );

  const [offset, dispatchOffset] = useReducer((state, action) => {
    switch (action.type) {
      case 'next':
        dispatchScrollPosition({ type: 'reset' });
        return state + 1;
      case 'previous':
        dispatchScrollPosition({ type: 'reset' });
        return state - 1;
      case 'reset':
        return 0;
      default:
        throw new Error();
    }
  }, 0);

  const routeAppointmentId = id ? +id : 0;
  const [selectedAppointmentId, setSelectedAppointmentId] = useState(
    routeAppointmentId
  );
  const onChangeSelectedAppointment = (id: number) => {
    setSelectedAppointmentId(id);
  };

  const isEditorDisabled = useMemo(() => {
    if (selectedAppointmentId && new Date() > selectedDate) {
      return true;
    } else {
      return false;
    }
  }, [selectedAppointmentId, selectedDate]);

  useEffect(() => {
    if (selectedAppointmentId && _appointments) {
      const selectedAppointment = _appointments.find(
        x => x.id === selectedAppointmentId
      );
      if (selectedAppointment) {
        const appoinmentDate = new Date(selectedAppointment.booking_date_time);

        dispatchOffset({ type: 'reset' });
        setSelectedDate(
          new Date(
            Date.UTC(
              appoinmentDate.getUTCFullYear(),
              appoinmentDate.getUTCMonth(),
              appoinmentDate.getUTCDate(),
              0,
              0,
              0
            )
          )
        );
      }
    }
  }, [_appointments, selectedAppointmentId]);

  const getDateWithoutTime = (d: Date) => {
    return new Date(
      Date.UTC(d.getFullYear(), d.getMonth(), d.getDate(), 0, 0, 0, 0)
    );
  };

  const dateWithOffset = useMemo(() => {
    return new Date(
      Date.UTC(
        selectedDate.getFullYear(),
        selectedDate.getMonth() + offset,
        1,
        0,
        0,
        0
      )
    );
  }, [selectedDate, offset]);

  const filterDateDays = useMemo(() => {
    const result = [];
    const d = dateWithOffset;
    let i = new Date(d.getFullYear(), d.getMonth(), 1, 0, 0, 0, 0);

    while (i.getMonth() === d.getMonth()) {
      const isSelected =
        getDateWithoutTime(i).getTime() ===
        getDateWithoutTime(selectedDate).getTime();
      const isInPast =
        getDateWithoutTime(i).getTime() <
        getDateWithoutTime(currentDate).getTime();
      result.push({
        number: i.getDate(),
        label: t(`common.shortWeekdays.${i.getDay() === 0 ? 7 : i.getDay()}`),
        selected: isSelected,
        inPast: isInPast
      });

      i = new Date(i.getFullYear(), i.getMonth(), i.getDate() + 1, 0, 0, 0, 0);
    }

    return result;
  }, [offset, selectedDate, i18n.language]);

  const filterDateLabel = useMemo(() => {
    return `${t(
      `common.months.${dateWithOffset.getMonth() + 1}`
    )} ${dateWithOffset.getFullYear()}`;
  }, [offset, selectedDate, i18n.language]);

  const onChangeDate = (day: number) => {
    dispatchOffset({ type: 'reset' });
    setSelectedDate(
      new Date(
        Date.UTC(
          dateWithOffset.getFullYear(),
          dateWithOffset.getMonth(),
          day,
          0,
          0,
          0
        )
      )
    );
    setSelectedTime(null);
  };

  const [selectedEmployee, setSelectedEmployee] = useState<number>(null);

  const onChangeEmployee = (value: number) => {
    setSelectedEmployee(value);
  };

  const employeesSelectOptions = useMemo(
    () => [
      { id: null, name: t('merchantPanel.booking.specialistSelect.all') },
      ..._employees
    ],
    [_employees, i18n.language]
  );

  const timeSelectOptions = [
    {
      id: 1,
      name: '09:00 - 13:00'
    },
    {
      id: 2,
      name: '13:00 - 18:00'
    },
    {
      id: 3,
      name: '18:00 - 21:00'
    }
  ];
  const [selectedTime, setSelectedTime] = useState(0);

  const onChangeSelectedTime = (value: number) => {
    if (value === selectedTime) {
      setSelectedTime(0);
    } else {
      setSelectedTime(value);
    }
  };

  const appointments = useMemo(() => {
    let filtered = _appointments;

    filtered = filtered.filter(x => x.deleted !== 1);

    if (selectedEmployee) {
      filtered = filtered.filter(x => x.employee_id === selectedEmployee);
    }

    if (selectedDate) {
      filtered = filtered.filter(x => {
        const needleDate = selectedDate.toISOString().split('T')[0];
        const entryDate = x.booking_date_time.split('T')[0];

        if (needleDate === entryDate) {
          return x;
        }
      });
    }

    if (selectedTime) {
      filtered = filtered.filter(x => {
        const needleTimeSpan = timeSelectOptions.find(
          x => x.id === selectedTime
        );

        const inTimeRange = (
          date: string,
          start: MomentSetObject = {
            hours: 0,
            minutes: 0,
            seconds: 0
          },
          end: MomentSetObject = {
            hours: 0,
            minutes: 0,
            seconds: 0
          }
        ) => {
          const _start = moment.utc(date).set(start);
          const _end = moment.utc(date).set(end);

          return moment(moment.utc(date)).isBetween(
            _start,
            _end,
            undefined,
            '[)'
          );
        };

        switch (needleTimeSpan.id) {
          case 1:
            if (inTimeRange(x.booking_date_time, { hours: 9 }, { hours: 13 }))
              return x;
            break;
          case 2:
            if (inTimeRange(x.booking_date_time, { hours: 13 }, { hours: 18 }))
              return x;
            break;
          case 3:
            if (inTimeRange(x.booking_date_time, { hours: 18 }, { hours: 21 }))
              return x;
            break;
          default:
            return x;
        }
      });
    }
    return filtered;
  }, [_appointments, _employees, selectedEmployee, selectedDate, selectedTime]);

  // useEffect(() => {
  //   dispatchScrollPosition('reset');
  // }, [offset]);
  // useEffect(() => {
  //   if (scrollRef.current) {
  //     const day = getDateWithoutTime(selectedDate).getDate();
  //     console.log('day', day);

  //     let k = Math.round((day / 10) * 2);
  //     // let k = 1;
  //     console.log('k', k);

  //     while (k) {
  //       console.log('sad');

  //       dispatchScrollPosition({ type: 'next' });
  //       k = k - 1;
  //     }
  //   }
  // }, []);
  useEffect(() => {
    dispatchScrollPosition({ type: 'today' });
  }, []);

  useEffect(() => {
    if (scrollRef.current) {
      scrollRef.current.scrollTo({
        behavior: 'smooth',
        left: scrollPosition
      });
    }
  }, [scrollPosition]);

  return {
    t,
    scrollRef,
    dispatchScrollPosition,
    selectedAppointmentId,
    onChangeSelectedAppointment,
    appointments,
    filterDateLabel,
    filterDateDays,
    selectedEmployee,
    employeesSelectOptions,
    onChangeEmployee,
    dispatchOffset,
    onChangeDate,
    timeSelectOptions,
    selectedTime,
    onChangeSelectedTime,
    selectedDate,
    isEditorDisabled
  };
};

export { AppointmentsProps, useAppointmentsProps };
