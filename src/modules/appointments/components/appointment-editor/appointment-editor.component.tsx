import { Button, Group, hoc, Input, Card, Select } from '@core';
import * as React from 'react';
import * as styles from './appointment-editor.scss';
import { useAppointmentEditorProps } from './appointment-editor.props';
import classNames from 'classnames';

/**
 * Renders AppointmentEditor
 */
const AppointmentEditor = hoc(
  useAppointmentEditorProps,
  ({
    t,
    appointmentId,
    categories,
    selectedCategory,
    onChangeSelectedCategory,
    products,
    selectedProduct,
    onChangeSelectedProduct,
    eligibleEmployees,
    selectedEmployee,
    onChangeSelectedEmployee,
    eligibleDates,
    selectedDate,
    onChangeSelectedDate,
    eligibleEmployeeTime,
    selectedTime,
    onChangeSelectedTime,
    onClickCreate,
    onClickClose,
    selectedCustomer,
    onChangeSelectedCustomerLastName,
    onChangeSelectedCustomerFirstName,
    onChangeSelectedCustomerPhone,
    customerFieldsDeactivated,
    onAddAppointment,
    onUpdateAppointment,
    codeModal,
    dispatchCodeModal,
    code,
    codeRef,
    onCodeFocus,
    onCodeChange,
    onCodeKeyDown,
    onValidateCustomerCheck,
    formDisabled,
    eligibleSalons,
    selectedSalon,
    onChangeSelectedSalon
  }) => {
    const widget = (
      <div className={styles.widget}>
        <div>
          <img
            className={styles.widgetIcon}
            src={require('img/appointments-create.svg')}
          />
        </div>
        <div className={styles.widgetTitle}>
          {t('merchantPanel.booking.list.createTitle')}
        </div>
        <div className={styles.widgetLabel}>
          {t('merchantPanel.booking.list.createLabel')}
        </div>
        <Button className={styles.widgetButton} onClick={() => onClickCreate()}>
          {t('merchantPanel.booking.list.createButton')}
        </Button>
      </div>
    );

    const createAppointmentButtons = (
      <React.Fragment>
        <div
          className={classNames(styles.phoneValidation, {
            [styles.active]: true
          })}
        >
          <div className={styles.phoneValidationIcon}>
            <img src={require('img/appointments-info.svg')} />
          </div>
          <div className={styles.phoneValidationHelp}>
            <div className={styles.phoneValidationHelpTitle}>
              {t('merchantPanel.booking.form.phoneValidationHelpTitle')}
            </div>
            <div className={styles.phoneValidationHelpText}>
              {t('merchantPanel.booking.form.phoneValidationHelpText')}
            </div>
          </div>
        </div>
        <div className={styles.phoneValidationButtons}>
          <Button
            theme='primary'
            size='lg'
            {...{ disabled: false }}
            onClick={() => onAddAppointment(true)}
          >
            {t('merchantPanel.booking.form.addAndValidate')}
          </Button>
          <Button
            theme='primary'
            size='lg'
            onClick={() => onAddAppointment(false)}
          >
            {t('merchantPanel.booking.form.add')}
          </Button>
        </div>
      </React.Fragment>
    );

    const updateAppointmentButtons = (
      <Button theme='primary' size='lg' onClick={onUpdateAppointment}>
        {t('merchantPanel.booking.form.update')}
      </Button>
    );

    const codeModalForm = (
      <React.Fragment>
        <div
          className={styles.overlay}
          onClick={() => dispatchCodeModal({ type: 'hide' })}
        />
        <div className={styles.modalContainer}>
          <div className={styles.modal}>
            <h1 className={styles.modalHeading}>{t('auth.confirmNumber')}</h1>
            <div className={styles.modalCaption}>
              {t('auth.weHaveSentYouSms', { phone: selectedCustomer.phone })}:
            </div>
            <div className={styles.fields} ref={codeRef}>
              {code.map((value, index) => (
                <Input
                  key={index}
                  type='text'
                  className={styles.input}
                  value={value}
                  onChange={onCodeChange(index)}
                  onKeyDown={onCodeKeyDown(index)}
                  onFocus={onCodeFocus}
                />
              ))}
            </div>

            <div className={styles.wrapper}>
              <span className={styles.textSm}>
                {t('auth.didntReceiveSms')}{' '}
              </span>
              <span className={classNames(styles.textSm, styles.pointer)}>
                {t('auth.sentAgain')}
              </span>
            </div>

            <Button
              className={styles.button}
              type='submit'
              theme='teritary'
              size='md'
              onClick={onValidateCustomerCheck}
            >
              {t('auth.submitCodeButton')}
            </Button>
          </div>
        </div>
      </React.Fragment>
    );

    const form = (
      <Card className={styles.appointmentEditor}>
        <h2 className={styles.heading}>
          {t('merchantPanel.booking.form.addBooking')}
          <span>
            <img
              className={styles.headingIcon}
              src={require('img/appointments-close.svg')}
              onClick={() => onClickClose()}
            />
          </span>
        </h2>

        <Group
          className={styles.fieldGroup}
          title={t('merchantPanel.booking.form.serviceTitle')}
          titleClassName={styles.title}
        >
          <Select
            label={t('merchantPanel.booking.form.categoryLabel')}
            options={categories}
            value={selectedCategory}
            onChange={onChangeSelectedCategory}
          />
          <Select
            label={t('merchantPanel.booking.form.serviceLabel')}
            disabled={!selectedCategory}
            options={products}
            value={selectedProduct}
            onChange={onChangeSelectedProduct}
          />
        </Group>

        <Group
          className={styles.fieldGroup}
          title={t('merchantPanel.booking.form.employeeTitle')}
          titleClassName={styles.title}
        >
          <Select
            label={t('merchantPanel.booking.form.employee')}
            disabled={!selectedCategory || !selectedProduct}
            options={eligibleEmployees}
            value={selectedEmployee}
            onChange={onChangeSelectedEmployee}
          />
          <Select
            label={t('merchantPanel.booking.form.salon')}
            disabled={
              !selectedCategory || !selectedProduct || !selectedEmployee
            }
            options={eligibleSalons}
            value={selectedSalon}
            onChange={onChangeSelectedSalon}
          />
        </Group>
        <div className={styles.horizontalGroup}>
          <Select
            className={styles.horizontalGroupInput}
            label={t('merchantPanel.booking.form.employeeDate')}
            disabled={
              !selectedCategory ||
              !selectedProduct ||
              !selectedEmployee ||
              !selectedSalon
            }
            options={eligibleDates}
            value={selectedDate}
            onChange={onChangeSelectedDate}
          />
          <Select
            className={styles.horizontalGroupInput}
            label={t('merchantPanel.booking.form.employeeTime')}
            disabled={
              !selectedCategory ||
              !selectedProduct ||
              !selectedEmployee ||
              !selectedDate
            }
            options={eligibleEmployeeTime}
            value={selectedTime}
            onChange={onChangeSelectedTime}
          />
        </div>

        <Group
          className={styles.fieldGroup}
          title={t('merchantPanel.booking.form.clientTitle')}
          titleClassName={styles.title}
        >
          <Input
            className={styles.inputOnly}
            label={t('merchantPanel.booking.form.clientName.firstName')}
            value={selectedCustomer.name.firstName}
            onChange={onChangeSelectedCustomerFirstName}
            disabled={customerFieldsDeactivated}
          />
          <Input
            className={styles.inputOnly}
            label={t('merchantPanel.booking.form.clientName.lastName')}
            value={selectedCustomer.name.lastName}
            onChange={onChangeSelectedCustomerLastName}
            disabled={customerFieldsDeactivated}
          />
          <Input
            className={styles.inputOnly}
            label={t('merchantPanel.booking.form.clientPhone')}
            value={selectedCustomer.phone}
            onChange={onChangeSelectedCustomerPhone}
            mask='+99 999 999 99 99'
            disabled={customerFieldsDeactivated}
          />
        </Group>

        {appointmentId === -1
          ? createAppointmentButtons
          : updateAppointmentButtons}
        {codeModal === 'show' ? codeModalForm : <div />}
      </Card>
    );

    return appointmentId ? form : widget;
  }
);

export { AppointmentEditor };
