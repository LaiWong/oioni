import { useEffect, useMemo, useReducer, useRef, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
  createAppointment,
  getEligibleTimeSlots,
  updateAppointment,
  validateCustomerCheck,
  validateCustomerSend
} from '@appointments/store';
import { State } from '@store';
import { useTranslation } from 'react-i18next';
import { navigate } from '@store/router';
import { snack } from '@store/snackbar';

/**
 * Props
 */
type AppointmentEditorProps = {
  editorSelectedDate: Date;
  appointmentId: number;
  formDisabled: boolean;
  onChangeSelectedAppointment: (id: number) => void;
};

/**
 * Use appointment editor props
 */
const useAppointmentEditorProps = ({
  editorSelectedDate,
  appointmentId,
  onChangeSelectedAppointment,
  formDisabled
}: AppointmentEditorProps) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const codeRef = useRef(null);

  const {
    _appointments,
    _services,
    _employees,
    _customers,
    _eligibleTimeSlots,
    _fetching,
    _salons,
    merchant
  } = useSelector(
    ({
      merchant: {
        appointments: {
          appointments,
          services,
          employees,
          customers,
          eligibleTimeSlots,
          fetching
        },
        salon
      },
      general: { merchantUser }
    }: State) => ({
      _salons: salon.salons,
      _appointments: appointments,
      _services: services,
      _employees: employees,
      _customers: customers,
      _eligibleTimeSlots: eligibleTimeSlots,
      _fetching:
        fetching.appointments ||
        fetching.services ||
        fetching.employees ||
        fetching.customers,
      merchant: merchantUser
    })
  );

  const [codeModal, dispatchCodeModal] = useReducer((state, action) => {
    switch (action.type) {
      case 'show':
        return 'show';
      case 'hide':
        return 'hide';
      default:
        throw new Error();
    }
  }, 'hide');

  const [code, setCode] = useState<Array<string>>(['', '', '', '']);
  const [selectedSalon, setSelectedSalon] = useState<number>(null);
  const [selectedCategory, setSelectedCategory] = useState<number>(null);
  const [selectedEmployee, setSelectedEmployee] = useState<number>(null);
  const [selectedProduct, setSelectedProduct] = useState<number>(null);
  const [selectedDate, setSelectedDate] = useState<string>(null);
  const [selectedTime, setSelectedTime] = useState<string>(null);
  const [selectedCustomer, setSelectedCustomer] = useState<{
    name: {
      firstName: string;
      lastName: string;
    };
    phone: string;
  }>({
    name: {
      firstName: '',
      lastName: ''
    },
    phone: ''
  });

  const categories = useMemo(
    () =>
      _services
        .filter(
          (value, index, self) =>
            self.findIndex(v => v.category_id === value.category_id) === index
        )
        .map(x => ({ id: x.category_id, name: x.category.name })),
    [_services]
  );

  const products = useMemo(
    () =>
      _services
        .filter(x => x.category_id === selectedCategory)
        .map(x => ({
          id: x.id,
          name: x.name
        })),
    [_services, selectedCategory]
  );

  const eligibleEmployees = useMemo(
    () => _employees.filter(x => x.products.indexOf(selectedProduct) !== -1),
    [_services, selectedCategory, selectedProduct, _employees]
  );

  const eligibleSalons = useMemo(() => {
    const array = [];
    _salons.forEach(salon => {
      salon.services.forEach(({ productId, employees }) => {
        if (
          +productId === +selectedProduct &&
          employees.includes(selectedEmployee)
        ) {
          array.push(salon);
        }
      });
    });
    return array;
  }, [_salons, selectedEmployee, selectedProduct]);

  const eligibleDates = useMemo(() => {
    const listSize = 30;
    let k = 0;
    const result = [];
    const currentDate = new Date();

    while (k < listSize) {
      const i = new Date(
        Date.UTC(
          currentDate.getUTCFullYear(),
          currentDate.getUTCMonth(),
          currentDate.getUTCDate() + k,
          0,
          0,
          0
        )
      );

      result.push({
        id: i.toISOString(),
        name: `${i.getUTCDate()} ${t(
          `common.months.${i.getUTCMonth() + 1}`
        )} ${i.getUTCFullYear()}`
      });

      k = k + 1;
    }

    return result;
  }, []);

  const eligibleEmployeeTime = useMemo(
    () =>
      _eligibleTimeSlots
        .filter(x => x.available === true || `${x.time}.000Z` === selectedTime)
        .map(x => {
          const id = `${x.time}.000Z`;
          const date = new Date(`1970-01-01T${x.time}.000Z`);
          const name = `${date.getHours()}:${date.getMinutes()}`;

          return { id: id, name: x.time };
        }),
    [
      _services,
      _employees,
      _eligibleTimeSlots,
      selectedCategory,
      selectedProduct,
      selectedEmployee,
      selectedTime
    ]
  );

  const customerFieldsDeactivated = useMemo(() => {
    if (appointmentId !== -1) return true;
    const needle = _customers?.find(x => x.phone === selectedCustomer.phone);
    if (needle) return true;
  }, [appointmentId, selectedCustomer]);

  const onClickClose = () => {
    onChangeSelectedAppointment(null);
    dispatch(navigate('/appointments'));
  };

  const onChangeSelectedCategory = (id: number) => {
    setSelectedCategory(id);
    setSelectedProduct(null);
    setSelectedEmployee(null);
    setSelectedSalon(null);
    setSelectedDate(null);
    setSelectedTime(null);
  };

  const onChangeSelectedProduct = (id: number) => {
    setSelectedProduct(id);
    setSelectedEmployee(null);
    setSelectedSalon(null);
    setSelectedDate(null);
    setSelectedTime(null);
  };

  const onChangeSelectedDate = (date: string) => {
    setSelectedDate(date);
    setSelectedTime(null);
  };

  const onChangeSelectedTime = (time: string) => {
    setSelectedTime(time);
  };

  const onChangeSelectedEmployee = (id: number) => {
    setSelectedEmployee(id);
  };
  const onChangeSelectedSalon = (id: number) => {
    setSelectedSalon(id);
    if (selectedDate === null) {
      setSelectedDate(
        editorSelectedDate.toISOString().split('T')[0] + 'T00:00:00.000Z'
      );
    }
  };

  const onChangeSelectedCustomerFirstName = (value: string) => {
    setSelectedCustomer({
      ...selectedCustomer,
      name: { ...selectedCustomer.name, firstName: value }
    });
  };

  const onChangeSelectedCustomerLastName = (value: string) => {
    setSelectedCustomer({
      ...selectedCustomer,
      name: { ...selectedCustomer.name, lastName: value }
    });
  };

  const onChangeSelectedCustomerPhone = (value: string) => {
    setSelectedCustomer({
      name: selectedCustomer.name,
      phone: value
    });
  };

  const onClickCreate = () => {
    if (merchant.status !== 'active') {
      dispatch(snack('pleaseActivateMerchantAccount', 'error'));
      return;
    }

    onChangeSelectedAppointment(-1);
  };

  const onAddAppointment = (shouldValidate: boolean) => {
    if (!_services.length) return;

    if (shouldValidate) {
      dispatchCodeModal({ type: 'show' });
      dispatch(validateCustomerSend(selectedCustomer.phone));
      return;
    }

    const selectedService = _services.find(x => x.id === selectedProduct);
    const employee = selectedService.product_employees.find(
      x => x.employee_id === selectedEmployee
    );
    const selectedStore = employee ? employee.store_id : 0;
    const date = selectedDate.split('T')[0] + 'T' + selectedTime;

    dispatch(
      createAppointment(
        {
          first_name: selectedCustomer.name.firstName,
          last_name: selectedCustomer.name.lastName,
          customer_phone: selectedCustomer.phone,
          employee_id: selectedEmployee,
          product_id: selectedProduct,
          date: date,
          store_id: selectedStore,
          duration: selectedService.duration
        },
        (id: number) => {
          if (id) {
            if (shouldValidate) {
              dispatchCodeModal({ type: 'show' });
              dispatch(validateCustomerSend(selectedCustomer.phone));
            }
            onChangeSelectedAppointment(id);
          }
        }
      )
    );
  };

  const onUpdateAppointment = () => {
    const selectedService = _services.find(x => x.id === selectedProduct);
    const employee = selectedService.product_employees.find(
      x => x.employee_id === selectedEmployee
    );
    const selectedStore = employee ? employee.store_id : 0;
    const date = selectedDate.split('T')[0] + 'T' + selectedTime;

    dispatch(
      updateAppointment({
        id: appointmentId,
        employee_id: selectedEmployee,
        store_id: selectedStore,
        product_id: selectedProduct,
        duration: selectedService.duration,
        date: date
      })
    );
  };

  const onCodeChange = index => (value: string) => {
    const a = [...code];
    a[index] = value;
    setCode(a);

    codeRef.current.childNodes[index + 1]?.firstChild?.focus();
  };

  const onCodeFocus = (event: React.FocusEvent<HTMLInputElement>) => {
    event.currentTarget.select();
  };

  const onCodeKeyDown = (index: number) => (
    event: React.KeyboardEvent<HTMLInputElement>
  ) => {
    const value = code[index];
    const next = codeRef.current.childNodes[index + 1]?.firstChild;
    const previous = codeRef.current.childNodes[index - 1]?.firstChild;

    switch (event.which || event.keyCode) {
      // backspace
      case 8: {
        if (!value || value == '') {
          event.preventDefault();

          previous?.focus();
        }

        break;
      }

      // up | left
      case 37:
      case 38: {
        if (event.currentTarget.selectionStart == 0) {
          event.preventDefault();

          previous?.focus();
        }

        break;
      }

      // bottom | right
      case 39:
      case 40: {
        if (
          event.currentTarget.selectionEnd == event.currentTarget.value.length
        ) {
          event.preventDefault();

          next?.focus();
        }

        break;
      }
    }
  };

  const onValidateCustomerCheck = () => {
    dispatch(
      validateCustomerCheck(
        {
          phone: selectedCustomer.phone,
          code: code.join('')
        },
        () => {
          dispatchCodeModal({ type: 'hide' });
          onAddAppointment(false);
          onChangeSelectedAppointment(appointmentId);
          dispatch(navigate('/appointments/' + appointmentId));
          setCode(['', '', '', '']);
        }
      )
    );
  };

  useEffect(() => {
    if (
      !selectedProduct ||
      !selectedEmployee ||
      !selectedSalon ||
      !selectedDate
    )
      return;

    const needleDate = selectedDate.split('T')[0];
    const selectedService = _services.find(x => x.id === selectedProduct);

    dispatch(
      getEligibleTimeSlots({
        employee_id: selectedEmployee,
        store_id: +selectedSalon,
        duration: selectedService.duration,
        date: needleDate
      })
    );
  }, [selectedProduct, selectedSalon, selectedEmployee, selectedDate]);

  useEffect(() => {
    const appointment = _appointments.find(x => x.id === appointmentId);

    if (appointment) {
      setSelectedCategory(appointment.serviceCategoryId);
      setSelectedProduct(appointment.product_id);
      setSelectedEmployee(appointment.employee_id);
      setSelectedSalon(appointment.store_id);

      setSelectedCustomer({
        name: {
          firstName: appointment.customerName?.split(' ')[0],
          lastName: appointment.customerName?.split(' ')[1]
        },
        phone: appointment.customerPhone
      });

      const [
        appointmentDate,
        appointmentTime
      ] = appointment.booking_date_time.split('T');
      setSelectedDate(`${appointmentDate}T00:00:00.000Z`);
      setSelectedTime(appointmentTime);
    } else {
      setSelectedCategory(null);
      setSelectedProduct(null);
      setSelectedEmployee(null);
      setSelectedCustomer({
        name: {
          firstName: '',
          lastName: ''
        },
        phone: ''
      });
      setSelectedDate(null);
      setSelectedTime(null);
    }
  }, [_fetching, appointmentId]);

  return {
    t,
    appointmentId,
    categories,
    selectedCategory,
    onChangeSelectedCategory,
    products,
    selectedProduct,
    onChangeSelectedProduct,
    eligibleEmployees,
    selectedEmployee,
    onChangeSelectedEmployee,
    eligibleDates,
    selectedDate,
    onChangeSelectedDate,
    eligibleEmployeeTime,
    selectedTime,
    onChangeSelectedTime,
    onClickCreate,
    selectedCustomer,
    // onChangeSelectedCustomerName,

    onChangeSelectedCustomerLastName,
    onChangeSelectedCustomerFirstName,
    onChangeSelectedCustomerPhone,
    customerFieldsDeactivated,
    onClickClose,
    onAddAppointment,
    onUpdateAppointment,
    codeModal,
    dispatchCodeModal,
    code,
    codeRef,
    onCodeFocus,
    onCodeChange,
    onCodeKeyDown,
    onValidateCustomerCheck,
    formDisabled,
    eligibleSalons,
    selectedSalon,
    onChangeSelectedSalon
  };
};

export { AppointmentEditorProps, useAppointmentEditorProps };
