import * as React from 'react';
import { Button, formatPrice, hoc } from '@core';
import * as styles from './appointment-list.scss';
import { useAppointmentListProps } from './appointment-list.props';
import moment from 'moment';
import classNames from 'classnames';

/**
 * Renders AppointmentFilters
 */
const AppointmentList = hoc(
  useAppointmentListProps,
  ({
    t,
    isPast,
    itemsWithColor,
    onEditClick,
    onDeleteClick,
    selectedItemId
  }) => {
    const widget = (
      <div className={styles.widget}>
        <div>
          <img
            className={styles.widgetIcon}
            src={require('img/appointments-emoji.svg')}
          />
        </div>
        <div className={styles.widgetTitle}>
          {t('merchantPanel.booking.list.noBookingTitle')}
        </div>
        <div className={styles.widgetLabel}>
          {t('merchantPanel.booking.list.noBookingLabel')}
        </div>
      </div>
    );

    const list = (
      <div className={styles.list}>
        {itemsWithColor.map(
          ({
            id,
            bgColor,
            name,
            price,
            duration,
            booking_date_time,
            employee,
            customer,
            customerPhone,
            color,
            status
          }) => (
            <div
              key={id}
              className={classNames(styles.appointment, {
                [styles.appointmentSelected]: id === selectedItemId,
                [styles.appointmentPast]: isPast(booking_date_time)
              })}
            >
              <div
                className={styles.info}
                style={
                  isPast(booking_date_time)
                    ? { backgroundColor: '#eeeeee' }
                    : { backgroundColor: bgColor }
                }
              >
                <div className={styles.statusPay}>
                  {console.log('status', status)}
                  {status == 2
                    ? 'Paid'
                    : status == 3 && !isPast(booking_date_time)
                    ? 'Unpaid'
                    : null}
                </div>
                <div
                  className={styles.line}
                  style={{ backgroundColor: color }}
                />
                <div className={styles.top}>
                  <div className={styles.left}>
                    <h3 className={styles.title}>{name}</h3>
                    <span className={styles.description}>
                      {moment.utc(booking_date_time).format('HH:mm')}-
                      {moment
                        .utc(booking_date_time)
                        .add(duration, 'minutes')
                        .format('HH:mm')}
                    </span>
                  </div>

                  <div className={styles.right}>
                    <div className={styles.photo}>
                      <img src={employee.image_url} alt={employee.name} />
                    </div>
                    <h3 className={styles.title}>{employee.name}</h3>
                  </div>
                </div>
                <div className={styles.bottom}>
                  <div className={styles.left}>
                    <h3 className={styles.title}>
                      {customer?.first_name} {customer?.last_name}
                    </h3>
                    <span className={styles.description}>{customerPhone}</span>
                  </div>
                  <div className={styles.right}>
                    <h3 className={styles.price}>{formatPrice(price)}</h3>
                  </div>
                </div>
              </div>
              {!isPast(booking_date_time) && (
                <div className={styles.actions}>
                  <Button
                    className={styles.actionButton}
                    onClick={() => onEditClick(id)}
                  >
                    {t('merchantPanel.booking.btn.edit')}
                  </Button>
                  <Button
                    className={styles.actionButton}
                    theme='teritary'
                    onClick={() => onDeleteClick(id)}
                  >
                    {t('merchantPanel.booking.btn.delete')}
                  </Button>
                </div>
              )}
            </div>
          )
        )}
      </div>
    );

    return (
      <React.Fragment>
        {itemsWithColor.length === 0 ? widget : list}
      </React.Fragment>
    );
  }
);

export { AppointmentList };
