import { useDispatch } from 'react-redux';
import { navigate } from '@store/router';
import { useTranslation } from 'react-i18next';
import { deleteAppointment } from '@appointments/store';
import { useAppointmentsWithColors } from '@app/hooks';
import moment, { MomentInput } from 'moment';
/**
 * Props
 */
type AppointmentListProps = {
  appointmentId: number;
  items: Array<any>;
  onChangeSelectedAppointment: (id: number) => void;
};

const useAppointmentListProps = ({
  appointmentId,
  items,
  onChangeSelectedAppointment
}: AppointmentListProps) => {
  const { t, i18n } = useTranslation();
  const dispatch = useDispatch();

  const onEditClick = (id: number) => {
    onChangeSelectedAppointment(id);
    dispatch(navigate('/appointments/' + id));
  };

  const onDeleteClick = (id: number) => {
    dispatch(deleteAppointment(id));
  };

  const isPast = (booking_date_time: MomentInput) =>
    moment(
      moment.utc(booking_date_time).format('yyyy-MM-DD:HH:mm:ss')
    ).isBefore();

  const itemsWithColor = useAppointmentsWithColors([
    ...items
      .filter(({ booking_date_time }) => !isPast(booking_date_time))
      .sort(
        (a, b) =>
          +new Date(a.booking_date_time) - +new Date(b.booking_date_time)
      ),
    ...items
      .filter(({ booking_date_time }) => isPast(booking_date_time))
      .sort(
        (a, b) =>
          +new Date(b.booking_date_time) - +new Date(a.booking_date_time)
      )
  ]);

  return {
    t,
    selectedItemId: appointmentId,
    isPast,
    itemsWithColor,
    onEditClick,
    onDeleteClick
  };
};

export { useAppointmentListProps };
