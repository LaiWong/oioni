import { make } from 'redux-chill';
import {
  AppointmentCustomer,
  AppointmentEmployee,
  AppointmentItem,
  AppointmentService,
  AppointmentTimeSlot,
  RefundBookings
} from '@api';

const getAppointments = make('[appointments] get')
  .stage(
    'success',
    (payload: { appointments: AppointmentItem[]; total: number }) => payload
  )
  .stage('finish');

const getServices = make('[appointments] get services')
  .stage('success', (items: AppointmentService[]) => items)
  .stage('finish');

const getEmployees = make('[appointments] get employees')
  .stage('success', (items: AppointmentEmployee[]) => items)
  .stage('finish');

const getCustomers = make('[appointments] get customers')
  .stage('success', (items: AppointmentCustomer[]) => items)
  .stage('finish');

const getEligibleTimeSlots = make('[appointments] get eligible timeslots')
  .stage(
    (params: {
      date: string;
      store_id: number;
      duration: number;
      employee_id: number;
    }) => params
  )
  .stage('success', (items: AppointmentTimeSlot[]) => items)
  .stage('finish');

const createAppointment = make('[appointments] create appointment')
  .stage(
    (
      data: {
        first_name: string;
        last_name: string;
        customer_phone: string;
        employee_id: number;
        store_id: number;
        product_id: number;
        duration: number;
        date: string;
      },
      callback: (id: number) => void
    ) => ({ data, callback })
  )
  .stage('success')
  .stage('finish');

const updateAppointment = make('[appointments] update appointment')
  .stage(
    (params: {
      id: number;
      employee_id: number;
      store_id: number;
      product_id: number;
      duration: number;
      date: string;
    }) => params
  )
  .stage('success', (items: AppointmentItem[]) => items)
  .stage('finish');

const deleteAppointment = make('[appointments] delete appointment')
  .stage((id: number) => id)
  .stage('success', (items: AppointmentItem[]) => items)
  .stage('finish');

const validateCustomerSend = make('[appointments] validate customer send code')
  .stage((phone: string) => phone)
  .stage('success')
  .stage('finish');

const validateCustomerCheck = make(
  '[appointments] validate customer check code'
)
  .stage((params: { phone: string; code: string }, callback: () => any) => ({
    params,
    callback
  }))
  .stage('success')
  .stage('finish');

const getRefundBookings = make('[appointments] get refund bookings')
  .stage((params: { page?: number } = {}) => params)
  .stage(
    'success',
    (
      bookings: RefundBookings,
      totalPages: { totalCanceled: number; totalFailProcessing: number }
    ) => ({
      bookings,
      totalPages
    })
  );

const updateRefundBooking = make('[appointments] update refund booking').stage(
  (data: { order_id?: number; order_item_id?: number; mark: boolean }) => data
);

export {
  getAppointments,
  getServices,
  getEmployees,
  getCustomers,
  getEligibleTimeSlots,
  createAppointment,
  updateAppointment,
  deleteAppointment,
  validateCustomerSend,
  validateCustomerCheck,
  getRefundBookings,
  updateRefundBooking
};
