import * as moment from 'moment';
import { reducer } from 'redux-chill';
import {
  deleteAppointment,
  getAppointments,
  getCustomers,
  getEligibleTimeSlots,
  getEmployees,
  getRefundBookings,
  getServices
} from './actions';
import { AppointmentsState } from './state';

/**
 * General state
 */
const appointments = reducer(new AppointmentsState())
  // getAppointments reducers
  .on(getAppointments, state => {
    state.fetching.appointments = true;
  })
  .on(getAppointments.success, (state, { appointments, total }) => {
    state.appointments = appointments.filter(item => item.booking_date_time);
    state.total = total;

    state.appointments.sort(
      (a, b) =>
        moment(b.booking_date_time).valueOf() -
        moment(a.booking_date_time).valueOf()
    );
    state.appointments = appointments;
  })
  .on(getAppointments.finish, state => {
    state.fetching.appointments = false;
  })
  .on(getServices, state => {
    state.fetching.services = true;
  })
  .on(getServices.success, (state, items) => {
    state.services = items;
  })
  .on(getServices.finish, state => {
    state.fetching.services = false;
  })
  .on(getEmployees, state => {
    state.fetching.employees = true;
  })
  .on(getEmployees.success, (state, items) => {
    state.employees = items;
  })
  .on(getEmployees.finish, state => {
    state.fetching.employees = false;
  })
  .on(getCustomers, state => {
    state.fetching.customers = true;
  })
  .on(getCustomers.success, (state, items) => {
    state.customers = items;
  })
  .on(getCustomers.finish, state => {
    state.fetching.customers = false;
  })
  .on(getEligibleTimeSlots, state => {
    state.fetching.eligibleTimeSlots = true;
  })
  .on(getEligibleTimeSlots.success, (state, items) => {
    state.eligibleTimeSlots = items;
  })
  .on(getEligibleTimeSlots.finish, state => {
    state.fetching.eligibleTimeSlots = false;
  })
  .on(deleteAppointment.success, (state, items) => {
    const itemsIds = items.map(x => x.id);
    const _appointments = state.appointments.filter(
      x => itemsIds.indexOf(x.id) === -1
    );

    items.forEach(item => {
      const needle = state.appointments.find(x => x.id === item.id);
      _appointments.push({ ...needle, ...item });
    });

    state.appointments = _appointments;
  })
  .on(getRefundBookings.success, (state, { bookings, totalPages }) => {
    state.refundCanceledBookings = bookings.canceled;
    state.refundFailProcessingBookings = bookings.failProcessing;
    state.paginationLength = totalPages;
  });

export { appointments };
