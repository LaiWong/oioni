import { StoreContext } from '@store/context';
import { call, put, select } from 'redux-saga/effects';
import { State } from '@store';
import { Merchant } from '@api';
import { Payload, Saga } from 'redux-chill';
import {
  createAppointment,
  deleteAppointment,
  getAppointments,
  getCustomers,
  getEligibleTimeSlots,
  getEmployees,
  getRefundBookings,
  getServices,
  updateAppointment,
  updateRefundBooking,
  validateCustomerCheck,
  validateCustomerSend
} from './actions';
import { snack } from '@store/snackbar';

class AppointmentsSaga {
  private perPageCount = 9;

  @Saga(getAppointments)
  public *getAppointments(_, { appointments }: StoreContext) {
    try {
      const { id }: Merchant = yield select(
        (state: State) => state.general.merchantUser
      );

      const response = yield call(appointments.get, id, {
        order_statuses: '5,20'
      });

      yield put(
        getAppointments.success({
          appointments: response.data.data.appointments,
          total: response.data.data.total_appointments
        })
      );
    } catch (error) {
      yield put(snack('message.cannotGetAppointments', 'error'));
    } finally {
      yield put(getAppointments.finish());
    }
  }

  @Saga(getServices)
  public *getServices(_, { appointments }: StoreContext) {
    try {
      const { id }: Merchant = yield select(
        (state: State) => state.general.merchantUser
      );
      const response = yield call(appointments.fetchServices, id);

      yield put(getServices.success(response.data.products));
    } catch (error) {
      yield put(snack('message.cannotGetServices', 'warning'));
    } finally {
      yield put(getServices.finish());
    }
  }

  @Saga(getEmployees)
  public *getEmployees(_, { appointments }: StoreContext) {
    try {
      const { id }: Merchant = yield select(
        (state: State) => state.general.merchantUser
      );
      const response = yield call(appointments.fetchEmployees, id);

      yield put(getEmployees.success(response.data.result.employees));
    } catch (error) {
      yield put(snack('message.cannotGetEmployees', 'warning'));
    } finally {
      yield put(getEmployees.finish());
    }
  }

  @Saga(getCustomers)
  public *getCustomers(_, { appointments }: StoreContext) {
    try {
      const { id }: Merchant = yield select(
        (state: State) => state.general.merchantUser
      );
      const response = yield call(appointments.fetchCustomers, id);

      yield put(getCustomers.success(response.data.result));
    } catch (error) {
      yield put(snack('message.cannotGetCustomers', 'warning'));
    } finally {
      yield put(getCustomers.finish());
    }
  }

  @Saga(getEligibleTimeSlots)
  public *getEligibleTimeSlots(
    params: {
      employee_id: number;
      store_id: number;
      duration: number;
      date: string;
    },
    { appointments }: StoreContext
  ) {
    try {
      const response = yield call(appointments.fetchEligibleTimeSlots, params);

      yield put(getEligibleTimeSlots.success(response.data.result.timeslots));
    } catch (error) {
      yield put(snack('message.cannotGetEligibleTimeSlots', 'warning'));
    } finally {
      yield put(getEligibleTimeSlots.finish());
    }
  }

  @Saga(createAppointment)
  public *createAppointment(
    { data: _data, callback }: Payload<typeof createAppointment>,
    { appointments }: StoreContext
  ) {
    let response_id;
    try {
      const { id }: Merchant = yield select(
        (state: State) => state.general.merchantUser
      );

      const {
        data: { data }
      } = yield call(appointments.createAppointment, {
        ..._data,
        merchant_id: id
      });

      if (data.success === false) {
        throw new Error(data.message);
      }
      response_id = data[0]?.id;
      yield put(snack('message.bookingWasCreated', 'info'));
      yield put(getAppointments());
    } catch (error) {
      yield put(snack('message.cannotCreateAppointment', 'warning'));
    } finally {
      yield call(callback, response_id);
    }
  }

  @Saga(updateAppointment)
  public *updateAppointment(
    params: {
      id: number;
      employee_id: number;
      store_id: number;
      product_id: number;
      duration: number;
      date: string;
    },
    { appointments }: StoreContext
  ) {
    try {
      const response = yield call(appointments.updateAppointment, params);

      if (response.data.success === false) {
        throw new Error(response.data.message);
      }

      yield put(getAppointments());
    } catch (error) {
      yield put(snack('message.cannotUpdateAppointment', 'warning'));
    } finally {
      yield put(updateAppointment.finish());
    }
  }

  @Saga(deleteAppointment)
  public *deleteAppointment(id: number, { appointments }: StoreContext) {
    try {
      const response = yield call(appointments.deleteAppointment, id);

      yield put(
        deleteAppointment.success(response.data.data.result.order_items)
      );
    } catch (error) {
      yield put(snack('message.cannotDeleteAppointment', 'warning'));
    } finally {
      yield put(deleteAppointment.finish());
    }
  }

  @Saga(validateCustomerSend)
  public *validateCustomerSend(
    phone: Payload<typeof validateCustomerSend>,
    { appointments }: StoreContext
  ) {
    try {
      yield call(appointments.validateCustomerSend, phone);
      yield put(validateCustomerSend.success());
    } catch (error) {
      yield put(snack('message.cannotValidateAppointment', 'warning'));
    } finally {
      yield put(validateCustomerSend.finish());
    }
  }

  @Saga(validateCustomerCheck)
  public *validateCustomerCheck(
    { params, callback },
    { appointments }: StoreContext
  ) {
    try {
      yield call(appointments.validateCustomerCheck, params.phone, params.code);
      yield put(validateCustomerCheck.success());
      yield call(callback);
    } catch (error) {
      yield put(snack('message.cannotValidateCustomer', 'warning'));
    } finally {
      yield put(validateCustomerCheck.finish());
    }
  }

  @Saga(getRefundBookings)
  public *getRefundBookings(
    { page }: Payload<typeof getRefundBookings>,
    { appointments }: StoreContext
  ) {
    try {
      const {
        data: { canceled, failProcessing, totalCanceled, totalFailProcessing }
      } = yield call(appointments.getRefundBookings, {
        page,
        perPage: this.perPageCount
      });

      yield put(
        getRefundBookings.success(
          { canceled, failProcessing },
          { totalCanceled, totalFailProcessing }
        )
      );
    } catch (error) {
      yield put(snack('Cannot update booking!', 'error'));
    }
  }

  @Saga(updateRefundBooking)
  public *updateRefundBooking(
    data: Payload<typeof updateRefundBooking>,
    { appointments }: StoreContext
  ) {
    try {
      const response = yield call(appointments.updateRefundBookings, data);
    } catch (error) {
      yield put(snack('Cannot update booking!', 'error'));
    }
  }
}

const sagas = [new AppointmentsSaga()];

export { AppointmentsSaga, sagas };
