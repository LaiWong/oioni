import {
  AppointmentCustomer,
  AppointmentEmployee,
  AppointmentItem,
  AppointmentService,
  AppointmentTimeSlot,
  RefundBookings,
  RefundCanceledBookings,
  RefundFailProcessingBookings
} from '@api';

class AppointmentsState {
  public total: number;

  public appointments: AppointmentItem[] = [];

  public services: AppointmentService[] = [];

  public employees: AppointmentEmployee[] = [];

  public customers: AppointmentCustomer[] = [];

  public eligibleTimeSlots: AppointmentTimeSlot[] = [];

  public refundBookings: RefundBookings | [] = [];

  public refundCanceledBookings: RefundCanceledBookings = [];

  public refundFailProcessingBookings: RefundFailProcessingBookings = [];

  /**
   * Pagination length
   */
  public paginationLength = {
    totalCanceled: null,
    totalFailProcessing: null
  };

  /**
   * Errors
   */
  public errors = {
    update: null
  };

  /**
   * Fetching state
   */
  public fetching = {
    appointments: false,
    services: false,
    employees: false,
    customers: false,
    eligibleTimeSlots: false
  };
}

export { AppointmentsState };
