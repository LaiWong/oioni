import { object, string } from 'yup';
import { testers } from '@core';

/**
 * Appointment editor schema
 */
const appointmentSchema = object<{
  date: string;
  time: string;
}>({
  date: string()
    .required()
    .nullable(true)
    .test('mask', 'Date is required', testers.mask())
    .label('Date'),
  time: string()
    .required()
    .nullable(true)
    .label('Time')
});

export { appointmentSchema };
