import { hoc, Language as LanguagePicker } from '@core';
import * as React from 'react';
import { Link, Redirect, Route, Switch } from 'react-router-dom';
import { useAuth } from './auth.props';
import * as styles from './auth.scss';
import { Code, Login, Registration } from './pages';
import { Password } from '@auth/pages/password';
import { Recover } from '@auth/pages/recover';
import { RecoverComplete } from '@auth/pages/recover-complete';
import { Profile } from '@profile';

/**
 * <Auth />
 */
const Auth = hoc(
  useAuth,
  ({
    t,
    userType,
    match: { path, url, params },
    language,
    onLanguageChange,
    onBackClick,
    history: {
      location: { pathname }
    },
    isLogin,
    isPassword,
    onLogoClick
  }) => (
    <div className={styles.auth}>
      <div className='container'>
        <div className={styles.header}>
          <Route
            exact
            path={`${path}/code`}
            render={() => (
              <div className={styles.back}>
                <img
                  src={require('img/arrow-left.png')}
                  alt='Arrow left'
                  onClick={onBackClick}
                />
                <h1>{t('auth.navigation.arrow.back')}</h1>
              </div>
            )}
          />
          <Route
            exact
            path={[
              `${path}/login`,
              `${path}/register`,
              `${path}/password`,
              `${path}/recover`,
              `${path}/recover/complete`,
              `${path}/complete`
            ]}
            render={() => (
              <img
                src={require('img/main-logo.svg')}
                onClick={onLogoClick}
                alt='logo'
              />
            )}
          />
          <LanguagePicker
            boxClassName={styles.languagesBox}
            value={language}
            onChange={onLanguageChange}
          />
        </div>
        <div className={styles.content}>
          <Switch>
            <Route path={`${path}/login`} component={Login} />
            <Route path={`${path}/password`} component={Password} />
            <Route path={`${path}/code`} component={Code} />
            <Route exact path={`${path}/register`} component={Registration} />
            <Route exact path={`${path}/recover`} component={Recover} />
            <Route
              exact
              path={`${path}/recover/complete`}
              component={RecoverComplete}
            />
            <Route path={`${path}/complete`} component={Profile} />
            <Redirect to={`${url}/login`} />
          </Switch>
          <Route
            exact
            path={[`${path}/login`, `${path}/password`, `${path}/register`]}
            render={() => (
              <div className={styles.registerText}>
                <h4>
                  {isLogin || isPassword
                    ? t('auth.registration.notHaveAccount')
                    : t('auth.registration.accountExist')}
                </h4>
                <Link
                  to={`/${params.language}/auth/${
                    isLogin || isPassword ? 'register' : 'login'
                  }`}
                  className={styles.link}
                >
                  {isLogin || isPassword
                    ? t('auth.registration.form.submit')
                    : t('auth.loginButton')}
                </Link>
              </div>
            )}
          />
          <Route
            exact
            path={`${path}/recover`}
            render={() => (
              <div className={styles.recover}>
                <Link
                  to={`/${params.language}/auth/login`}
                  className={styles.link}
                >
                  {t('auth.registration.returnToLogin')}
                </Link>
              </div>
            )}
          />
        </div>
      </div>
    </div>
  )
);

export { Auth };
