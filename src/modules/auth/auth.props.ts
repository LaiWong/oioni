import { useDispatch, useSelector } from 'react-redux';
import { State } from '@store';
import { matchPath, useHistory, useRouteMatch } from 'react-router';
import { Language } from '@api';
import { setLanguage } from '@store/general';
import { useTranslation } from 'react-i18next';
import { useMemo } from 'react';
import { navigate } from '@store/router';

/**
 * Use auth
 */
const useAuth = () => {
  const {
    i18n: { language },
    t
  } = useTranslation();
  const dispatch = useDispatch();
  const match = useRouteMatch<{ language: string }>();
  const history = useHistory();
  const { userType } = useSelector((state: State) => state.general);

  const onLanguageChange = (language: Language) => {
    dispatch(setLanguage(language));
  };

  const { isLogin, isPassword } = useMemo(
    () => ({
      isLogin: !!matchPath(`/${match.params.language}/auth/login`, {
        exact: true,
        path: history.location.pathname
      }),
      isPassword: !!matchPath(`/${match.params.language}/auth/password`, {
        exact: true,
        path: history.location.pathname
      })
    }),
    [match]
  );

  const onBackClick = () => {
    history.goBack();
  };

  const onLogoClick = () => dispatch(navigate('/'));

  return {
    t,
    userType,
    language,
    onLanguageChange,
    onBackClick,
    match,
    history,
    isLogin,
    isPassword,
    onLogoClick
  };
};

export { useAuth };
