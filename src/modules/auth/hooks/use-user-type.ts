import { UserType } from '@api';
import { useSelector } from 'react-redux';
import { State } from '@store';

const useUserType = () => {
  const { isEmployee, isMerchant, isSuperAdmin } = useSelector(
    (state: State) => state.auth
  );

  let defaultUserType;

  const previousUserType = localStorage.userType;

  switch (true) {
    case previousUserType == UserType.admin && isSuperAdmin: {
      defaultUserType = UserType.admin;
      break;
    }
    case previousUserType == UserType.merchant && isMerchant: {
      defaultUserType = UserType.merchant;
      break;
    }
    case previousUserType == UserType.employee && isEmployee: {
      defaultUserType = UserType.employee;
      break;
    }
    case !isEmployee && !isMerchant: {
      defaultUserType = UserType.merchant;
      break;
    }
    default: {
      defaultUserType = isMerchant ? UserType.merchant : UserType.employee;
      break;
    }
  }

  return { defaultUserType };
};

export { useUserType };
