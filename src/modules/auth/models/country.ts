type Country = {
  id: number;
  name: string;
  names: {
    code: string;
    name: string;
  }[];
  code: string;
  validations: string;
  phone_length: number;
  created_at: string;
  updated_at: string;
  validation_regex: string;
};

// type Country = {
//   id: number;
//   name: string;
//   names: {
//     code: string;
//     name: string;
//   }[];
//   code: string;
//   business_info: {
//     params: {
//       name: string;
//       length: number;
//     }[];
//   };
//   active: boolean;
//   validations: string;
//   validation_regex: string;
//   phone_length: number;
//   created_at: string;
//   updated_at: string;
//   currency: {
//     id: number;
//     currency: string;
//     active: boolean;
//     settings: {
//       usd: string;
//     };
//   };
//   language: any;
//   finance_models_countries: {
//     id: number;
//     active: boolean;
//     finance_model: {
//       id: number;
//       name: string;
//       active: boolean;
//       settings: {
//         привіт: string;
//       };
//     };
//   }[];
//   countries_languages: {
//     id: number;
//     active: boolean;
//     language: {
//       id: number;
//       language: string;
//       active: boolean;
//       settings: {
//         [x: string]: string;
//       };
//     };
//   }[];
// };

export { Country };
