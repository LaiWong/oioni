import { Form, FormError, Group, hoc, Input } from '@core';
import classNames from 'classnames';
import * as React from 'react';
import { Fragment } from 'react';
import { Trans } from 'react-i18next';
import { useCodeProps } from './code.props';
import * as styles from './code.scss';

/**
 * Static list for fields mapping
 */
const fields = new Array(4).fill(1);

/**
 * Renders Code
 */
const Code = hoc(
  useCodeProps,
  ({
    t,
    form,
    login,
    inputs,
    timer,
    errors,
    onFocus,
    onChange,
    onKeyDown,
    onSendClick
  }) => (
    <Form className={styles.form} form={form}>
      <div className={styles.code}>
        <h1 className={styles.heading}>{t('auth.confirmNumber')}</h1>

        <div className={styles.wrapper}>
          <span className={styles.textMd}>
            <Trans
              i18nKey='auth.registration.text.sentSMS'
              values={{ login }}
              components={[<br key='1' />]}
            />
          </span>
        </div>

        <div className={styles.fields}>
          {fields.map((item, index) => (
            <Group key={index}>
              <Input
                type='text'
                api={ref => {
                  inputs.current[index] = ref;
                }}
                onFocus={onFocus}
                value={form.values.otp[index]}
                className={styles.input}
                onChange={onChange(index)}
                onKeyDown={onKeyDown(index)}
              />
            </Group>
          ))}
        </div>

        <div className={styles.errorContainer}>
          <FormError visible={!!errors.code}>{errors.code}</FormError>
        </div>

        <div className={styles.wrapper}>
          {timer > 0 ? (
            <Fragment>
              <p className={classNames(styles.textSm, styles.wait)}>
                {t('auth.registration.code.resend')} <span>0:{timer}</span>
              </p>
            </Fragment>
          ) : (
            <p
              className={classNames(styles.textSm, styles.pointer)}
              onClick={onSendClick}
            >
              {t('auth.registration.code.btn.resend')}
            </p>
          )}
        </div>
      </div>
    </Form>
  )
);

export { Code };
