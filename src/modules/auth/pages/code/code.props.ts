import { useDispatch, useSelector } from 'react-redux';
import { useFormik } from 'formik';
import { otpSchema } from '@auth/validation';
import { checkOtp, sendOtp } from '@auth/store';
import { useEffect, useRef, useState } from 'react';
import { useMeta } from '@core';
import { State } from '@store';
import { useTranslation } from 'react-i18next';
import { useSchemaTranslationsUpdate } from '@app/hooks';
import { lazy } from 'yup';
import { navigate } from '@store/router';
import { useUserType } from '@auth/hooks';

/**
 * Use code
 */
const useCodeProps = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const { login, sent, errors } = useSelector((state: State) => state.auth);
  const { defaultUserType } = useUserType();

  const [timer, setTimer] = useState(60);

  const form = useFormik({
    initialValues: {
      otp: [],
      userType: defaultUserType
    },
    validationSchema: lazy(otpSchema),
    onSubmit: ({ otp, userType }, { setSubmitting }) => {
      dispatch(
        checkOtp({ phone: login, otp: otp.join(''), userType }, () =>
          setSubmitting(false)
        )
      );
    }
  });

  useSchemaTranslationsUpdate(form);

  const {
    values: { userType }
  } = form;

  const onSendClick = () => {
    dispatch(
      sendOtp(
        {
          phone: '+' + login.replace(/[^0-9]/g, '')
        },
        () => form.setSubmitting(false),
        true
      )
    );

    setTimer(60);
  };

  const onFocus = (event: React.FocusEvent<HTMLInputElement>) => {
    event.currentTarget.select();
  };

  const onChange = index => value => {
    const patch = [...form.values.otp];

    switch (true) {
      case !value: {
        patch[index] = '';

        form.setFieldValue('otp', patch);

        break;
      }

      case value.length == 1: {
        patch[index] = value;

        form.setFieldValue('otp', patch);

        inputs.current[index + 1]?.focus();

        break;
      }

      case value.length > 1: {
        const limited = value
          .slice(0, form.values.otp.length - index)
          .split('');
        const start = index;
        const end = start + limited.length - 1;

        limited.map((char, index) => {
          patch[start + index] = char;
        });

        form.setFieldValue('otp', patch);

        inputs.current[end]?.focus();

        break;
      }
    }

    const isFull = patch.every(el => !!el);

    if (!isFull || patch.length !== 4) return;

    dispatch(
      checkOtp(
        { phone: login, otp: patch.join(''), userType, isRegistration: true },
        () => form.setSubmitting(false)
      )
    );
  };

  const onKeyDown = (index: number) => (
    event: React.KeyboardEvent<HTMLInputElement>
  ) => {
    const value = form.values.otp[index];
    const next = inputs.current[index + 1];
    const previous = inputs.current[index - 1];

    switch (event.which || event.keyCode) {
      // backspace
      case 8: {
        if (!value || value == '') {
          event.preventDefault();

          previous?.focus();
        }

        break;
      }

      // up | left
      case 37:
      case 38: {
        if (event.currentTarget.selectionStart == 0) {
          event.preventDefault();

          previous?.focus();
        }

        break;
      }

      // bottom | right
      case 39:
      case 40: {
        if (
          event.currentTarget.selectionEnd == event.currentTarget.value.length
        ) {
          event.preventDefault();

          next?.focus();
        }

        break;
      }
    }
  };

  const inputs = useRef<{ focus: () => any }[]>([]);

  useEffect(() => {
    const first = inputs.current[0];

    first?.focus();
  }, []);

  useEffect(() => {
    if (sent) return;
    dispatch(navigate('/auth/login'));
  }, [sent]);

  useEffect(() => {
    timer > 0 && setTimeout(() => setTimer(timer - 1), 1000);
  }, [timer]);

  useMeta({ title: t('general.meta.title') }, []);

  return {
    t,
    form,
    sent,
    login,
    inputs,
    errors,
    timer,
    onFocus,
    onChange,
    onKeyDown,
    onSendClick
  };
};

export { useCodeProps };
