import { Button, Field, Form, FormError, Group, hoc } from '@core';
import React from 'react';
import { useLoginProps } from './login.props';
import * as styles from './login.scss';
import classNames from 'classnames';
import { Trans } from 'react-i18next';

/**
 * Renders Login
 */
const Login = hoc(useLoginProps, ({ t, form, errors }) => (
  <Form className={styles.form} form={form}>
    <div className={styles.login}>
      <h1 className={styles.heading}>{t('auth.login.text.title')}</h1>
      <h3 className={classNames(styles.heading, styles.textAlign)}>
        <Trans i18nKey='auth.login.text.main' components={[<br key='0' />]} />
      </h3>
      <Group
        className={classNames(styles.fields, {
          [styles.error]: errors
        })}
      >
        <Field.Input
          type='text'
          name='login'
          label={t('auth.login.phoneOrEmail')}
        />
      </Group>

      <div className={styles.error}>
        <FormError>{errors.login}</FormError>
      </div>

      <Button
        className={styles.button}
        type='submit'
        theme={!!form.values.login ? 'primary' : 'secondary'}
        size='md'
      >
        {t('auth.submitCodeButton')}
      </Button>
    </div>
  </Form>
));

export { Login };
