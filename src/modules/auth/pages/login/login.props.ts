import { useDispatch, useSelector } from 'react-redux';
import { State } from '@store';
import { useFormik } from 'formik';
import { getCountries, loginStart } from '@auth/store';
import { useEffect } from 'react';
import { formats, useMeta } from '@core';
import { object, string } from 'yup';
import { useTranslation } from 'react-i18next';
import { useSchemaTranslationsUpdate } from '@app/hooks';

/**
 * Use login
 */
const useLoginProps = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const { errors } = useSelector((state: State) => state.auth);

  const form = useFormik({
    initialValues: {
      login: ''
    },
    onSubmit: ({ login }, { setSubmitting }) => {
      dispatch(loginStart({ login }, () => setSubmitting(false)));
    },
    validationSchema: object({
      login: string()
        .required('message.phoneOrEmailRequired')
        .test(
          'login',
          'message.invalidCredentials',
          value =>
            formats.phoneNumber.check(value) || formats.email.check(value)
        )
    }),
    validateOnChange: false
  });

  useSchemaTranslationsUpdate(form);

  useEffect(() => {
    dispatch(getCountries());
  }, []);

  useMeta({ title: t('general.meta.title') }, []);

  return {
    t,
    form,
    errors
  };
};

export { useLoginProps };
