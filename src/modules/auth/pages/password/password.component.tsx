import { Button, Field, Form, FormError, Group, hoc, Icon } from '@core';
import React, { Fragment } from 'react';
import { usePasswordProps } from './password.props';
import * as styles from './password.scss';
import classNames from 'classnames';
import { UserCaption, UserType } from '@api';
import { Trans } from 'react-i18next';

/**
 * Renders Password
 */
const Password = hoc(
  usePasswordProps,
  ({
    t,
    form,
    errors,
    isEmail,
    isPhone,
    onSendClick,
    timer,
    onRecoverClick,
    isUserTypeSelectAvailable,
    onToggleUserTypeClick,
    userTypeAvatar,
    isMerchant,
    isEmployee,
    isSuperAdmin
  }) => (
    <Form className={styles.form} form={form}>
      <div className={styles.login}>
        <h1 className={styles.heading}>{t('auth.login.text.title')}</h1>
        <h3 className={classNames(styles.heading, styles.textAlign)}>
          <Trans
            i18nKey='auth.password.text.title'
            components={[<br key='0' />]}
          />
        </h3>

        <Group
          className={classNames(styles.fields, {
            [styles.error]: errors
          })}
        >
          <Field.Input
            disabled={false}
            type='text'
            name='login'
            label='Phone or email'
          />
        </Group>

        <Group
          className={classNames(styles.fields, {
            [styles.error]: errors
          })}
        >
          <Field.Input
            disabled={false}
            type={isPhone ? 'number' : isEmail ? 'password' : 'number'}
            name='password'
            label={`Enter ${isPhone ? 'code' : ''}${isEmail ? 'password' : ''}`}
          />
        </Group>

        <FormError>{errors.login || errors.code}</FormError>

        {isPhone && (
          <div className={styles.wrapper}>
            {timer > 0 ? (
              <Fragment>
                <p className={styles.textSm}>
                  {t('auth.password.sentSMSCode')}
                </p>
                <p className={classNames(styles.textSm, styles.wait)}>
                  <Trans
                    i18nKey='auth.password.resend'
                    values={{ timer }}
                    components={[
                      <span className={styles.timer} key='1'>
                        0:{timer}
                      </span>
                    ]}
                  />
                </p>
              </Fragment>
            ) : (
              <p
                className={classNames(styles.textSm, styles.pointer)}
                onClick={onSendClick}
              >
                {t('auth.password.sendCode')}
              </p>
            )}
          </div>
        )}

        {isEmail && (
          <div className={styles.wrapper}>
            <p
              className={classNames(styles.textSm, styles.pointer)}
              onClick={onRecoverClick}
            >
              {t('auth.password.forgotPassword')}
            </p>
          </div>
        )}

        <Button
          className={styles.button}
          type='submit'
          theme={
            !!form.values.password && !!form.values.login
              ? 'primary'
              : 'secondary'
          }
          size='md'
        >
          {t('auth.submitCodeButton')}
        </Button>
      </div>

      {isUserTypeSelectAvailable && (
        <div className={styles.popup}>
          <Icon name={userTypeAvatar} className={styles.icon} />
          <div>
            <span className={styles.userType}>
              You are logged in as {form.values.userType}
            </span>
            <br />

            {isSuperAdmin && (
              <>
                <span
                  className={styles.linkTrigger}
                  onClick={() => onToggleUserTypeClick(UserType.admin)}
                >
                  {/* {t('auth.userRoleSwitcher.admin')} */}
                  Click here to enter as admin
                </span>
                <br />
              </>
            )}
            {isMerchant && (
              <>
                <span
                  className={styles.linkTrigger}
                  onClick={() => onToggleUserTypeClick(UserType.merchant)}
                >
                  Click here to enter as merchant
                </span>
                <br />
              </>
            )}
            {isEmployee && (
              <>
                <span
                  className={styles.linkTrigger}
                  onClick={() => onToggleUserTypeClick(UserType.employee)}
                >
                  Click here to enter as employee
                </span>
              </>
            )}
          </div>
        </div>
      )}
    </Form>
  )
);

export { Password };
