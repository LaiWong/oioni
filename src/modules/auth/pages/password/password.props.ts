import { useDispatch, useSelector } from 'react-redux';
import { State } from '@store';
import { useFormik } from 'formik';
import { checkOtp, login as signin, loginStart } from '@auth/store';
import { formats, useMeta } from '@core';
import { object, string } from 'yup';
import { useTranslation } from 'react-i18next';
import { useSchemaTranslationsUpdate } from '@app/hooks';
import { useEffect, useMemo, useState } from 'react';
import { navigate } from '@store/router';
import { useUserType } from '@auth/hooks';
import { UserCaption, UserType } from '@api';

/**
 * Use password
 */
const usePasswordProps = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const {
    errors,
    login,
    sent,
    isEmployee,
    isMerchant,
    isSuperAdmin
  } = useSelector((state: State) => state.auth);
  const { defaultUserType } = useUserType();

  const [timer, setTimer] = useState(60);

  const isUserTypeSelectAvailable =
    [isEmployee, isMerchant, isSuperAdmin].filter(Boolean).length > 1;

  const form = useFormik({
    initialValues: {
      login: login,
      password: '',
      userType: defaultUserType
    },
    onSubmit: ({ login, password, userType }, { setSubmitting }) => {
      if (isPhone) {
        dispatch(
          checkOtp(
            {
              phone: '+' + login.replace(/[^0-9]/g, ''),
              otp: password,
              userType
            },
            () => setSubmitting(false)
          )
        );
        return;
      }

      if (isEmail) {
        dispatch(
          signin({ email: login, password, userType }, () =>
            setSubmitting(false)
          )
        );
        return;
      }
    },
    validationSchema: object({
      login: string()
        .required('Phone or email is required')
        .test(
          'login',
          'Invalid credentials',
          value =>
            formats.phoneNumber.check(value) || formats.email.check(value)
        ),
      password: string().required('message.fillAllGaps')
    }),
    validateOnChange: false
  });

  const isEmail = useMemo(() => formats.email.check(form.values.login), [
    form.values.login
  ]);
  const isPhone = useMemo(() => formats.phoneNumber.check(form.values.login), [
    form.values.login
  ]);

  const isMerchantSelected = form.values.userType == UserType.merchant;

  const userTypeAvatar = useMemo(() => {
    switch (form.values.userType) {
      case UserType.admin:
        return UserCaption.admin;
      case UserType.merchant:
        return UserCaption.manager;
      case UserType.employee:
        return UserCaption.employee;
      default:
        break;
    }
  }, [form.values.userType]);

  const onSendClick = () => {
    dispatch(
      loginStart({ login: form.values.login }, () => form.setSubmitting(false))
    );

    setTimer(60);
  };

  const onRecoverClick = () => {
    dispatch(navigate('/auth/recover'));
  };

  const onToggleUserTypeClick = (type: UserType) => {
    form.setFieldValue('userType', type);
  };

  useEffect(() => {
    if (sent) return;

    dispatch(navigate('/auth/login'));
  }, []);

  useEffect(() => {
    timer > 0 && setTimeout(() => setTimer(timer - 1), 1000);
  }, [timer]);

  useSchemaTranslationsUpdate(form);

  useMeta({ title: t('general.meta.title') }, []);

  return {
    t,
    form,
    timer,
    errors,
    isEmail,
    isPhone,
    onSendClick,
    onRecoverClick,
    isUserTypeSelectAvailable,
    onToggleUserTypeClick,
    isMerchantSelected,
    userTypeAvatar,
    isMerchant,
    isEmployee,
    isSuperAdmin
  };
};

export { usePasswordProps };
