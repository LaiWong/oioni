import { Button, hoc } from '@core';
import React from 'react';
import { useRecoverCompleteProps } from './recover-complete.props';
import styles from './recover-complete.scss';
import classNames from 'classnames';

/**
 * Recover complete
 */
const RecoverComplete = hoc(
  useRecoverCompleteProps,
  ({ t, login, onReturnLoginClick }) => (
    <div className={styles.login}>
      <h1 className={styles.heading}>Done And Done!</h1>
      <h3 className={classNames(styles.heading, styles.textAlign)}>
        Password reset link is sent to <br />
        {login}. Not your inbox?
        <br />
        Check spam
      </h3>

      <Button
        className={styles.button}
        theme='primary'
        size='md'
        onClick={onReturnLoginClick}
      >
        Return to Login
      </Button>
    </div>
  )
);

export { RecoverComplete };
