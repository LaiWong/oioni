import { useDispatch, useSelector } from 'react-redux';
import { useMeta } from '@core';
import { useTranslation } from 'react-i18next';
import { State } from '@store';
import { useEffect } from 'react';
import { recover as recoverAccount } from '@auth/store';
import { navigate } from '@store/router';

/**
 * Recover complete props
 */
const useRecoverCompleteProps = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();

  const { login, recover } = useSelector((state: State) => state.auth);

  const onReturnLoginClick = () => {
    dispatch(navigate('/auth/login'));
  };

  useMeta({ title: t('auth.registration.meta.title') }, []);

  useEffect(() => {
    if (recover) return;

    dispatch(navigate('/navigate/login'));

    return () => {
      dispatch(recoverAccount.success());
    };
  }, []);

  return {
    t,
    login,
    onReturnLoginClick
  };
};

export { useRecoverCompleteProps };
