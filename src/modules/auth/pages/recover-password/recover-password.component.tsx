import { hoc } from '@core';
import React from 'react';
import styles from './recover-password.scss';
import { useRecoverPasswordProps } from './recover-password.props';
import { ChangeModal } from '@profile/components';

/**
 * Recover password
 */
const RecoverPassword = hoc(
  useRecoverPasswordProps,
  ({ onClose, onSave, token, merchant_id }) => (
    <div className={styles.container}>
      <ChangeModal
        changing={'password'}
        onClose={onClose}
        onSave={onSave}
        merchantId={merchant_id as string}
        token={token as string}
      />
    </div>
  )
);

export { RecoverPassword };
