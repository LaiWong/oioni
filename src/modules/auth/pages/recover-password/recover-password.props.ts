import { useDispatch } from 'react-redux';
import { useQuery } from '@core';
import { useEffect } from 'react';
import { logout } from '@store/general';
import { navigate } from '@store/router';

/**
 * Recover password props
 */
const useRecoverPasswordProps = () => {
  const { merchant_id, token } = useQuery();
  const dispatch = useDispatch();

  const onSave = () => {
    dispatch(logout());
  };

  const onClose = () => {
    dispatch(navigate('/'));
  };

  useEffect(() => {
    if (merchant_id && token) return;

    onClose();
  }, [merchant_id, token]);

  return { onClose, onSave, merchant_id, token };
};

export { useRecoverPasswordProps };
