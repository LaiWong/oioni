import { Button, Field, Form, FormError, Group, hoc } from '@core';
import React from 'react';
import { useRecoverProps } from './recover.props';
import styles from './recover.scss';
import classNames from 'classnames';

/**
 * Recover
 */
const Recover = hoc(useRecoverProps, ({ t, form, errors }) => (
  <Form className={styles.form} form={form}>
    <div className={styles.recover}>
      <h1 className={styles.heading}>Recover your account</h1>
      <h3 className={classNames(styles.heading, styles.textAlign)}>
        Having an identity crisis? You can recover <br />
        your account using the email address
      </h3>

      <Group
        className={classNames(styles.fields, {
          [styles.error]: errors
        })}
      >
        <Field.Input type='text' name='login' label='Email' disabled={false} />
      </Group>

      <div className={styles.errorContainer}>
        <FormError>{errors.recover}</FormError>
      </div>

      <Button
        className={styles.button}
        type='submit'
        theme={!!form.values.login ? 'primary' : 'secondary'}
        size='md'
      >
        Recover account
      </Button>
    </div>
  </Form>
));

export { Recover };
