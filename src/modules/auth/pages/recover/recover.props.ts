import { useDispatch, useSelector } from 'react-redux';
import { State } from '@store';
import { useFormik } from 'formik';
import { formats, useMeta } from '@core';
import { useTranslation } from 'react-i18next';
import { object, string } from 'yup';
import { recover } from '@auth/store';

/**
 * Recover props
 */
const useRecoverProps = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();

  const { errors } = useSelector((state: State) => state.auth);

  const form = useFormik({
    initialValues: {
      login: ''
    },
    onSubmit: ({ login }, { setSubmitting }) => {
      dispatch(recover({ login }, () => setSubmitting(true)));
    },
    validationSchema: object({
      login: string()
        .required('Email is required')
        .test('login', 'Invalid email', (value) => formats.email.check(value))
    })
  });

  useMeta({ title: t('auth.registration.meta.title') }, []);

  return {
    t,
    form,
    errors
  };
};

export { useRecoverProps };
