import { Button, Checkbox, Field, Form, FormError, Group, hoc } from '@core';
import React from 'react';
import { useRegistrationProps } from './registration.props';
import styles from './registration.scss';
import classNames from 'classnames';
import { Trans } from 'react-i18next';

/**
 * <Registration />
 */
const Registration = hoc(
  useRegistrationProps,
  ({
    form,
    errors,
    t,
    shape,
    countries,
    onAgreedChange,
    mask,
    onPhoneChange
  }) => (
    <Form className={styles.form} form={form}>
      <div className={styles.login}>
        <h1 className={styles.heading}>
          {t('auth.registration.registerToOioni')}
        </h1>
        <h3 className={classNames(styles.heading, styles.textAlign)}>
          {t('auth.registration.enterPhone')}
          <br />
          {t('auth.registration.businessAccount')}
        </h3>
        <Group
          className={classNames(styles.fields, {
            [styles.error]: errors
          })}
        >
          <Field.Input
            type='text'
            name='merchantName'
            label={t('auth.registration.form.merchantName')}
            disabled={false}
          />
        </Group>

        <Group
          className={classNames(styles.fields, {
            [styles.error]: errors
          })}
        >
          <Field.Select
            name='country'
            options={countries}
            label={t('auth.login.form.country')}
            shape={shape}
            disabled={false}
          />
          <Field.Input
            onChange={onPhoneChange}
            type='text'
            name='phoneNumber'
            mask={mask}
            maskChar='_'
            label={t('auth.login.form.phone')}
            disabled={false}
          />
        </Group>

        <FormError>{errors.registration}</FormError>

        <div className={styles.agreement}>
          <Checkbox value={form.values.agreed} onChange={onAgreedChange} />
          <h3 className={styles.textAgreement}>
            <Trans
              i18nKey='auth.registration.checkbox.label'
              components={[<br key='0' />]}
            />
            <a target='_blank' href='https://oioni.com/terms/'>
              <span>{t('auth.registration.privacyPolicy')}</span>
            </a>
          </h3>
        </div>

        <Button
          className={styles.button}
          type='submit'
          theme={form.isValid && form.values.agreed ? 'primary' : 'secondary'}
          size='md'
        >
          {t('auth.submitCodeButton')}
        </Button>
      </div>
    </Form>
  )
);

export { Registration };
