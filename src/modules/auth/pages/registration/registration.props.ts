import { useDispatch, useSelector } from 'react-redux';
import { State } from '@store';
import { useFormik } from 'formik';
import { useMeta, useMultiLanguage } from '@core';
import { Country, Language } from '@api';
import { useTranslation } from 'react-i18next';
import { useEffect, useMemo } from 'react';
import { getCountries, register } from '@auth/store/actions';
import { registrationSchema } from '@auth/validation';
import { lazy } from 'yup';

/**
 * Registration props
 */
const useRegistrationProps = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();

  const [language, onLanguageChange, ml] = useMultiLanguage(Language.et);

  const { countries, errors } = useSelector((state: State) => state.auth);

  const form = useFormik({
    initialValues: {
      country: undefined,
      merchantName: '',
      phoneNumber: '',
      agreed: false
    },
    onSubmit: ({ country, merchantName, phoneNumber }, { setSubmitting }) => {
      const { code } = countries.find(one => one.id == country);
      dispatch(
        register(
          {
            country: countries.find(one => one.id === country).id,
            phoneNumber: '+' + (code + phoneNumber).replace(/[^0-9]/g, ''),
            merchantName
          },
          () => setSubmitting(true)
        )
      );
    },
    validationSchema: lazy(({ country }) =>
      registrationSchema(
        countries.find(one => one.id === country)?.validation_regex
      )
    )
  });

  const shape = {
    name: (option: Country) =>
      t(`general.countries.${option.name}`) + ` (${option.code})`
  };

  const onAgreedChange = (value: boolean) => {
    form.setFieldValue('agreed', value);
  };

  useMeta({ title: t('auth.registration.meta.title') }, []);

  useEffect(() => {
    dispatch(getCountries());
  }, []);

  const mask = useMemo(() => {
    if (form.values.country) {
      form.setFieldValue('phoneNumber', '');
      const { code, validations } = countries.find(
        one => one.id == form.values.country
      );
      // const codeISO = code.replace('+', '');
      // const regx = new RegExp(`^.{${codeISO.length}}`);
      // const m = `${validations.replace(regx, codeISO)}`.replace(/_/g, '9');
      return validations ? `${validations.replace(/_/g, '9')}` : '';
    }
  }, [countries, form.values.country]);

  const onPhoneChange = (value: string) => {
    form.setFieldValue('phoneNumber', value.replace(/-/g, ''));
  };

  return {
    t,
    ml,
    form,
    errors,
    language,
    onLanguageChange,
    onAgreedChange,
    shape,
    countries,
    mask,
    onPhoneChange
  };
};

export { useRegistrationProps };
