import { make } from 'redux-chill';
import { Country, GetOtpSuccess, UserType } from '@api';
import { Register } from '../../../api/models/auth/register';

/**
 * Log in start
 */
const loginStart = make('[auth] login start')
  .stage((data: { login: string }, callback) => ({ data, callback }))
  .stage('success', (payload: string) => payload)
  .stage('failure', (error: string) => error)
  .stage('finish');

/**
 * Log in
 */
const login = make('[auth] login')
  .stage(
    (
      data: { email: string; password: string; userType: UserType },
      callback
    ) => ({
      data,
      callback
    })
  )
  .stage('success')
  .stage('failure', (error: string) => error)
  .stage('finish');

/**
 * Register
 */
const register = make('[auth] register')
  .stage((data: Register, callback) => ({
    data,
    callback
  }))
  .stage('success')
  .stage('failure', (error: string) => error)
  .stage('finish');

/**
 * Recover
 */
const recover = make('[auth] recover')
  .stage((data: { login: string }, callback) => ({ data, callback }))
  .stage('success')
  .stage('failure', (error: string) => error)
  .stage('finish');

/**
 * Send otp
 */
const sendOtp = make('[auth] send otp')
  .stage(
    (
      data: { phone: string },
      callback: () => any,
      isRegistration?: boolean
    ) => ({
      data,
      callback,
      isRegistration
    })
  )
  .stage('success', (payload: GetOtpSuccess) => payload)
  .stage('failure', (error: string, isRegistration: boolean) => ({
    error,
    isRegistration
  }))
  .stage('finish');

/**
 * Check otp
 */
const checkOtp = make('[auth] check otp')
  .stage(
    (
      data: {
        phone: string;
        otp: string;
        userType: UserType;
        isRegistration?: boolean;
      },
      callback: () => any
    ) => ({
      data,
      callback
    })
  )
  .stage('success')
  .stage('failure', (error: string) => error)
  .stage('finish');

/**
 * Get countries
 */
const getCountries = make('[auth] get countries')
  .stage('success', (countries: Country[]) => countries)
  .stage('failure')
  .stage('finish');

export {
  checkOtp,
  sendOtp,
  getCountries,
  loginStart,
  login,
  register,
  recover
};
