import { reducer } from 'redux-chill';
import { AuthState } from './state';
import {
  checkOtp,
  getCountries,
  login,
  loginStart,
  recover,
  register,
  sendOtp
} from './actions';

/**
 * Auth state
 */
const auth = reducer(new AuthState())
  .on(loginStart, state => {
    state.errors.login = null;
    state.errors.code = null;
  })
  .on(loginStart.success, (state, payload) => {
    state.sent = true;
    state.login = payload;
    state.errors.login = null;
  })
  .on(loginStart.failure, (state, error) => {
    state.errors.login = error;
  })
  .on(login.failure, (state, error) => {
    state.errors.login = error;
  })
  .on([login, login.success], state => {
    state.errors.login = null;
  })
  .on(register, (state, { data: { phoneNumber, country, merchantName } }) => {
    state.login = phoneNumber;
    state.countryId = country;
    state.merchantName = merchantName;
    state.errors.registration = null;
  })
  .on(register.failure, (state, error) => {
    state.errors.registration = error;
    state.login = null;
    state.countryId = null;
    state.merchantName = null;
  })
  .on(recover, (state, { data: { login } }) => {
    state.recover = true;
    state.login = login;
  })
  .on(recover.failure, (state, error) => {
    state.errors.recover = error;
    state.recover = false;
  })
  .on(recover.success, state => {
    state.errors.recover = null;
    state.recover = false;
  })
  .on(
    sendOtp.success,
    (state, { isEmployee, isMerchant, isSuperAdmin, phone }) => {
      state.sent = true;
      state.login = phone;
      state.isEmployee = isEmployee;
      state.isMerchant = isMerchant;
      state.isSuperAdmin = isSuperAdmin;
    }
  )
  .on(sendOtp.failure, (state, { error, isRegistration }) => {
    isRegistration
      ? (state.errors.registration = error)
      : (state.errors.login = error);
  })
  .on(getCountries.success, (state, countries) => {
    state.countries = countries;
  })
  .on(checkOtp, state => {
    state.errors.code = null;
  })
  .on(checkOtp.failure, (state, error) => {
    state.errors.code = error;
  });

export { auth };
