import { StoreContext } from '@store/context';
import { authorize } from '@store/general';
import { navigate } from '@store/router';
import { Payload, Saga } from 'redux-chill';
import { call, delay, put } from 'redux-saga/effects';
import {
  checkOtp,
  getCountries,
  login,
  loginStart,
  recover,
  register,
  sendOtp
} from './actions';
import { snack } from '@store/snackbar';
import i18next from 'i18next';
import { formats } from '@core';

class AuthSaga {
  /**
   * Get countries list
   */
  @Saga(getCountries)
  public *getCountries(_, { auth }: StoreContext) {
    try {
      const {
        data: { result }
      } = yield call(auth.countries);

      yield put(getCountries.success(result));
    } catch (error) {
      yield put(snack('message.cannotGetCountries', 'error'));
    }
  }

  /**
   * Log in start
   */
  @Saga(loginStart)
  public *loginStart(
    { data: { login }, callback }: Payload<typeof loginStart>,
    storeContext: StoreContext
  ) {
    localStorage.login = login;

    const isEmail = formats.email.check(login);

    if (isEmail) {
      yield put(loginStart.success(login));
      yield call(callback);
      yield put(navigate('/auth/password'));
      return;
    }

    const isPhone = formats.phoneNumber.check(login);

    if (isPhone) {
      yield this.sendOtp(
        {
          data: { phone: '+' + login.replace(/[^0-9]/g, '') },
          callback,
          isRegistration: false
        },
        storeContext
      );

      return;
    }

    yield put(loginStart.failure('message.invalidCredentials'));
  }

  /**
   * Log in
   */
  @Saga(login)
  public *login(
    { data: { email, password, userType }, callback }: Payload<typeof login>,
    { auth }: StoreContext
  ) {
    try {
      const {
        data: { employee, merchant },
        headers
      } = yield call(auth.login, { email, password });

      const token = headers.idtoken;
      localStorage.setItem('idtoken', token);

      yield put(authorize({ employee, merchant, userType }));
      yield put(login.success());

      yield put(navigate('/'));
    } catch (error) {
      yield put(login.failure('message.invalidCredentials'));
    } finally {
      yield call(callback);
    }
  }

  /**
   * Register
   */
  @Saga(register)
  public *register(
    {
      data: { country, merchantName, phoneNumber },
      callback
    }: Payload<typeof register>,
    storeContext: StoreContext
  ) {
    try {
      yield this.sendOtp(
        { data: { phone: phoneNumber }, callback, isRegistration: true },
        storeContext
      );
      localStorage.countryId = country;
      localStorage.merchantName = merchantName;

      yield put(register.success());
    } catch (error) {
      yield put(register.failure('message.somethingWentWrong'));
    } finally {
      yield call(callback);
    }
  }

  /**
   * Recover
   */
  @Saga(recover)
  public *recover(
    { data: { login }, callback }: Payload<typeof recover>,
    storeContext: StoreContext
  ) {
    try {
      yield call(storeContext.auth.recover, { email: login });
      yield put(navigate('/auth/recover/complete'));
    } catch (error) {
      yield put(recover.failure('message.somethingWentWrong'));
    } finally {
      yield call(callback);
    }
  }

  /**
   * Test init
   */
  @Saga(sendOtp)
  public *sendOtp(
    { data: { phone }, callback, isRegistration }: Payload<typeof sendOtp>,
    { auth }: StoreContext
  ) {
    try {
      const {
        data: { isEmployee, isMerchant, isSuperAdmin }
      } = yield call(auth.sendOtp, { phone, isRegistration });

      localStorage.login = phone;
      yield put(
        sendOtp.success({ isEmployee, isMerchant, isSuperAdmin, phone })
      );

      if (!isRegistration && (isEmployee || isMerchant || isSuperAdmin)) {
        yield put(navigate('/auth/password'));
      } else if (isRegistration) {
        yield put(navigate('/auth/code'));
      } else {
        yield put(loginStart.failure('message.notRegistered'));
      }
    } catch (error) {
      if (isRegistration && error.response.status === 403) {
        yield put(sendOtp.failure('message.userExist', isRegistration));
      } else {
        yield put(
          sendOtp.failure('message.invalidPhoneNumber', isRegistration)
        );
      }
    } finally {
      yield call(callback);
    }
  }

  /**
   * Login
   */
  @Saga(checkOtp)
  public *checkOtp(
    {
      data: { phone, otp, userType, isRegistration },
      callback
    }: Payload<typeof checkOtp>,
    { auth }: StoreContext
  ) {
    try {
      const {
        data: { employee, merchant }
      } = yield call(auth.checkOtp, { phone, otp });

      if (isRegistration) {
        yield put(navigate('/auth/complete'));
        return;
      }

      yield put(authorize({ employee, merchant, userType }));

      yield put(navigate('/'));
    } catch (error) {
      yield put(checkOtp.failure('message.wrongCode'));
    } finally {
      yield call(callback);
    }
  }
}

const sagas = [new AuthSaga()];

export { AuthSaga, sagas };
