// import { Country } from '@auth/models';
import { Country } from '@api';

const { countryId, login, merchantName } = localStorage;

class AuthState {
  /**
   * Login
   */
  public login: string = login || null;

  /**
   * Merchant name
   */
  public merchantName: string = merchantName || null;

  /**
   * Country id
   */
  public countryId = countryId ? Number(countryId) : null;

  /**
   * Send otp
   */
  public sent = false;

  /**
   * Is in recover
   */
  public recover = false;

  /**
   * Countries list
   */
  public countries: Country[] = [];

  /**
   * Errors
   */
  public errors = {
    code: null,
    login: null,
    registration: null,
    recover: null
  };

  /**
   * Is employee
   */
  public isEmployee: boolean;

  /**
   * Is merchant
   */
  public isMerchant: boolean;

  /**
   * Is Admin
   */
  public isSuperAdmin: boolean;
}

export { AuthState };
