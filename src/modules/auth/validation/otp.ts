import { array, object } from 'yup';
import i18next from 'i18next';

/**
 * Login form schema
 */
const otpSchema = () =>
  object({
    otp: array()
      .nullable(true)
      .test(
        'auth.code.form.otpCode',
        'auth.code.errors.otpCodeRequired',
        (value: string[]) => value && /[0-9]{4}/gi.test(value.join(''))
      )
  });

export { otpSchema };
