import { object, string } from 'yup';
import i18next from 'i18next';

/**
 * Phone number form schema
 */
const phoneNumberSchema = ({
  phoneValidationRegex
}: {
  phoneValidationRegex: string;
}) =>
  object({
    country: string()
      .required()
      .nullable(true)
      .label('auth.login.form.country'),
    phone: string()
      .nullable(true)
      .matches(
        new RegExp(`^${phoneValidationRegex}$`, 'g'),
        'auth.login.errors.phoneNumberNotValid'
      )
      .label('auth.login.form.phone')
  });

export { phoneNumberSchema };
