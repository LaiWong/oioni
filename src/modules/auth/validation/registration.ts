import { boolean, number, object, string } from 'yup';
import i18next from 'i18next';

/**
 * Registration form schema
 */
const registrationSchema = phoneRegex =>
  object({
    merchantName: string().required('auth.registration.merchantNameRequired'),
    country: number().required('auth.registration.countryRequired'),
    phoneNumber: string()
      .nullable(true)
      .required('auth.registration.errors.phoneNumberRequired')
      .matches(
        new RegExp(`^${phoneRegex}$`, 'g'),
        'auth.login.errors.phoneNumberNotValid'
      ),
    agreed: boolean().oneOf([true], 'auth.login.errors.agreePolicy')
  });

export { registrationSchema };
