import React from 'react';
import { hoc } from '@core';
import { useCountry } from './country.props';
import * as styles from './country.scss';
import { Redirect, Route, Switch } from 'react-router';
import { Countries } from './pages/countries';
import { CountryEditor } from './pages/country';

const Comp = () => {
  return <div>Compwefwefwefwefwef</div>;
};

const Country = hoc(useCountry, ({ match: { path, url, params } }) => {
  return (
    <div className={styles.country}>
      <Switch>
        <Route exact path={`${path}/countries`} component={Countries} />
        <Route path={`${path}/add`} component={CountryEditor} />
        <Route path={`${path}/:countryId`} component={CountryEditor} />
        <Redirect to={`${url}/countries`} />
      </Switch>
    </div>
  );
});

export { Country };
