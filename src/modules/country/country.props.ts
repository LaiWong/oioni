import { RouteComponentProps } from 'react-router';

const useCountry = (props: RouteComponentProps) => {
  return { ...props };
};

export { useCountry };
