import { useTranslation } from 'react-i18next';
import { useMemo } from 'react';

/**
 * Titles
 */
const useTitles = (
  // Key is equal to component name
  key
): {
  name: string;
  atEnd?: boolean;
  bold?: boolean;
  atCenter?: boolean;
}[] => {
  const { t } = useTranslation();

  const titles = useMemo(
    () => ({
      countries: [
        {
          name: t('Name')
        },
        {
          name: t('iso code'),
          atCenter: true
        },
        {
          name: t('language by default'),
          atCenter: true
        },
        {
          name: t('list of languages'),
          atCenter: true
        },
        {
          name: t('Currency'),
          atCenter: true
        },
        {
          name: ''
        }
      ],
      country: [
        {
          name: t('ISO code')
          //   atCenter: true
        },
        {
          name: t('Name')
        },
        {
          name: t('Currency')
          //   atCenter: true
        },
        {
          name: t('language by default')
          //   atCenter: true
        },
        {
          name: t('list of languages')
          //   atCenter: true
        },
        {
          name: t('list of ISO codes for clients')
          //   atCenter: true
        },
        {
          name: t('Financial model')
          //   atCenter: true
        },
        {
          name: t('Business info')
          //   atCenter: true
        }
      ]
    }),
    [t]
  );

  return titles[key.toLowerCase()];
};

export { useTitles };
