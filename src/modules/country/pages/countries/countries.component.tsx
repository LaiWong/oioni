import { Body, Button, Cell, Head, hoc, Pagination, Row, Table } from '@core';
import React from 'react';
import { useCountriesProps } from './countries.props';
import * as styles from './countries.scss';

const Countries = hoc(
  useCountriesProps,
  ({
    countriesTitles,
    onAddCountryClick,
    onOpenCountryClick,
    onPageChange,
    paginationLength,
    countries
  }) => (
    <div className={styles.countries}>
      <div className={styles.header}>
        <h1 className={styles.title}>Country managment</h1>
        <Button onClick={onAddCountryClick}>Add country</Button>
      </div>
      <Table>
        <Head>
          <Row>
            {countriesTitles?.map(({ atCenter, atEnd, bold, name }, index) => (
              <Cell key={index} atCenter={atCenter} atEnd={atEnd} bold={bold}>
                {name}
              </Cell>
            ))}
          </Row>
        </Head>
        <Body>
          {countries?.map(
            (
              { id, name, code, language, countries_languages = [], currency },
              index
            ) => (
              <Row key={index}>
                <Cell>{name}</Cell>
                <Cell atCenter>{code}</Cell>
                <Cell atCenter>{language?.language}</Cell>
                <Cell atCenter>
                  {countries_languages.length &&
                    countries_languages.map(({ language, id }) => (
                      <span key={id}>{language.language}, </span>
                    ))}
                </Cell>
                <Cell atCenter>{currency?.currency}</Cell>
                <Cell atEnd>
                  <Button onClick={() => onOpenCountryClick(id)}>Open</Button>
                </Cell>
              </Row>
            )
          )}
        </Body>
      </Table>
      <Pagination length={paginationLength} onPageChange={onPageChange} />
    </div>
  )
);

export { Countries };
