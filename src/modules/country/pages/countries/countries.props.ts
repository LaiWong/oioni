import { useTitles } from '@country/hooks/use-title';
import { getCountries } from '@country/store';
import { State } from '@store';
import { navigate } from '@store/router';
import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { RouteComponentProps } from 'react-router';
import { usePagination } from '@core/components/pagination/pagination.props';

const useCountriesProps = (props: RouteComponentProps) => {
  const dispatch = useDispatch();

  const { countries, paginationLength } = useSelector(
    (state: State) => state.country
  );

  const { selectedPage, onPageChange } = usePagination();

  const countriesTitles = useTitles('countries');

  const onAddCountryClick = () => {
    dispatch(navigate('/country/add'));
  };

  const onOpenClick = () => {
    dispatch(navigate('/country/open'));
  };

  const onOpenCountryClick = (id: number) => {
    dispatch(navigate(`/country/${id}`));
  };

  useEffect(() => {
    dispatch(getCountries({ page: selectedPage }));
  }, [selectedPage]);

  return {
    countriesTitles,
    onAddCountryClick,
    onOpenClick,
    onPageChange,
    paginationLength,
    onOpenCountryClick,
    countries,
    ...props
  };
};

export { useCountriesProps };
