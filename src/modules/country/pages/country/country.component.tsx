import { Button, Checkbox, Field, Form, FormError, Group, hoc } from '@core';
import React from 'react';
import { useCountryProps } from './country.props';
import styles from './country.scss';
import Select from 'react-select';
import { FieldArray } from 'formik';
import classNames from 'classnames';

const CountryEditor = hoc(
  useCountryProps,
  ({
    t,
    form,
    countryError,
    params,
    countryName,
    currencies,
    onChangeCurrency,
    currency,
    languages,
    main_lang,
    onChangeMainLanguage,
    second_lang,
    onChangeSecondLanguage,
    state,
    setstate,
    disabledSelectors
  }) => {
    const colourStyles = {
      control: styles => ({ ...styles, height: '4rem', borderColor: '#aaa' }),
      multiValueLabel: styles => ({ ...styles, color: '#fff' }),
      singleValue: styles => ({ ...styles, color: '#000' }),
      multiValue: styles => ({
        ...styles,
        background:
          'linear-gradient(154.85deg, #cc3791 15.56%, #e08e2e 84.16%)',
        color: '#fff'
      })
    };
    return (
      <div className={styles.container}>
        <Form className={styles.country} form={form}>
          <div className={styles.header}>
            <h1>Country managment{countryName && `/${countryName}`}</h1>
            <Button
              className={styles.save}
              theme={form.isValid ? 'primary' : 'secondary'}
              size='md'
              type='submit'
            >
              {t('auth.registration.form.btn.save')}
            </Button>
          </div>

          <div className={styles.fields}>
            <Group
              className={styles.group}
              title={'Phone Code'}
              titleClassName={styles.groupTitle}
            >
              <Field.Input
                className={styles.field}
                name='code'
                onChange={value => {
                  form.setFieldValue(
                    'code',
                    value.toString().replace(/\D/g, '')
                  );
                }}
              />
            </Group>
            <Group
              className={styles.group}
              title={'ISO'}
              titleClassName={styles.groupTitle}
            >
              <Field.Input
                className={styles.field}
                name='iso'
                onChange={value => {
                  form.setFieldValue(
                    'iso',
                    value
                      .toString()
                      .toUpperCase()
                      .replace(/[^A-Za-z]/g, '')
                  );
                }}
              />
            </Group>
            <Group
              className={styles.group}
              title={'Name'}
              titleClassName={styles.groupTitle}
            >
              <Field.Input className={styles.field} type='text' name='name' />
            </Group>
            <Group
              className={styles.group}
              title={'Currency'}
              titleClassName={styles.groupTitle}
            >
              <Select
                isDisabled={disabledSelectors}
                name='currency'
                className={styles.select}
                value={currency}
                onChange={onChangeCurrency}
                options={currencies}
                styles={colourStyles}
                getOptionLabel={({ currency }) => currency}
                getOptionValue={currency => currency}
              />
            </Group>
            <Group
              className={styles.group}
              title={'Language by default'}
              titleClassName={styles.groupTitle}
            >
              <Select
                isDisabled={disabledSelectors}
                // defaultValue={languages[0]}
                name='main_lang'
                className={styles.select}
                value={main_lang}
                onChange={onChangeMainLanguage}
                options={languages}
                styles={colourStyles}
                getOptionLabel={({ language }) => language}
                getOptionValue={language => language}
              />
            </Group>
            <Group
              className={styles.group}
              title={'List of languages'}
              titleClassName={styles.groupTitle}
            >
              <Select
                // defaultValue={[second_lang[1], second_lang[2]]}
                isMulti
                isDisabled={disabledSelectors}
                name='second_lang'
                className={styles.select}
                value={second_lang}
                onChange={onChangeSecondLanguage}
                options={languages}
                styles={colourStyles}
                closeMenuOnSelect={false}
                getOptionLabel={({ language }) => language}
                getOptionValue={language => language.id}
              />
            </Group>
            <Group
              className={styles.group}
              title={'Phone Length'}
              titleClassName={styles.groupTitle}
            >
              <Field.Input
                placeholder='Type number'
                className={styles.field}
                name='phone_length'
                onChange={value => {
                  form.setFieldValue(
                    'phone_length',
                    value.toString().replace(/\D/g, '')
                  );
                }}
              />
            </Group>
            <Group
              className={styles.group}
              title={'Finance model'}
              titleClassName={styles.groupTitle}
            >
              {state.map((model, index) => {
                const { id, name, active } = model;
                return (
                  <Checkbox
                    key={id}
                    onChange={value => {
                      let a = [];
                      for (let i = 0; i < state.length; i++) {
                        const element = state[i];
                        a.push(element);
                      }
                      a[index].active = value;
                      setstate(a);
                    }}
                    value={active}
                    label={name}
                  />
                );
              })}
            </Group>
            <Group
              className={`${styles.group} ${styles.b}`}
              title={'Business info'}
              titleClassName={styles.businessTitle}
            >
              <FieldArray
                name='params'
                render={({ remove, push }) => {
                  return (
                    <div>
                      {params.map((param, index) => (
                        <div key={index} className={styles.param}>
                          <Group
                            className={styles.name}
                            title={'Name'}
                            titleClassName={styles.groupTitle}
                          >
                            <Field.Input
                              className={styles.field}
                              name={`params.${index}.name`}
                            />
                          </Group>
                          <Group
                            className={styles.name}
                            title={'Count'}
                            titleClassName={styles.groupTitle}
                          >
                            <Field.Input
                              type='number'
                              {...({ min: 1 } as any)}
                              className={classNames(
                                styles.field,
                                styles.inputNumber
                              )}
                              name={`params.${index}.length`}
                            />
                          </Group>
                          <Button
                            type='button'
                            className={styles.buttonAddParam}
                            onClick={() => push({ name: '', length: '' })}
                          >
                            +
                          </Button>
                          <Button
                            type='button'
                            className={styles.buttonAddParam}
                            onClick={() => params.length > 1 && remove(index)}
                          >
                            -
                          </Button>
                        </div>
                      ))}
                    </div>
                  );
                }}
              />
            </Group>
          </div>
          <FormError>{countryError}</FormError>
        </Form>
      </div>
    );
  }
);

export { CountryEditor };
