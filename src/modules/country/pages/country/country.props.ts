import { useTitles } from '@country/hooks/use-title';
import {
  createCountry,
  getCountries,
  getFinanceModels,
  updateCountry
} from '@country/store';
import { countrySchema } from '@country/validations/country';
import { getCurrencies } from '@currency';
import { getLanguages } from '@languages';
import { State } from '@store';
import { useFormik } from 'formik';
import { useEffect, useMemo, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';
import { useRouteMatch } from 'react-router';
import { FinanceModel } from 'src/api/models/finance-model';

const useCountryProps = () => {
  const { t } = useTranslation();
  const coutryTitles = useTitles('country');
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getCountries());
    dispatch(getCurrencies());
    dispatch(getLanguages());
    dispatch(getFinanceModels());
  }, []);
  const {
    params: { countryId }
  } = useRouteMatch<{ countryId: string }>();

  const { countries, currencies, languages, financeModels } = useSelector(
    (state: State) => ({
      countries: state.country.countries,
      currencies: state.currency.currencies,
      languages: state.language.languages,
      financeModels: state.country.financeModels
    })
  );

  const country = useMemo(
    () => countries.find(country => country.id === +countryId),
    [countryId, countries]
  );
  const countryName = country?.name;
  const [state, setstate] = useState<FinanceModel[]>([]);

  useEffect(() => {
    let a = [];
    for (let i = 0; i < financeModels.length; i++) {
      const { id, name, settings } = financeModels[i];
      a.push({ id, active: false, name, settings });
    }
    if (country?.finance_models_countries?.length) {
      const activeFinanceModel = country.finance_models_countries;
      for (let j = 0; j < activeFinanceModel.length; j++) {
        const element = activeFinanceModel[j].finance_model;
        a = a.map(item =>
          item.id == element.id ? { ...item, active: true } : item
        );
      }
    }
    setstate(a);
  }, [financeModels, country]);
  const form = useFormik({
    initialValues: {
      name: country?.name,
      code: country?.code.toString().replace(/\D/g, ''),
      main_lang: country?.language,
      second_lang: country?.countries_languages.map(el => el.language),
      currency: country?.currency,
      finance_models: '',
      params: country?.business_info?.params || [{ name: '', length: 0 }],
      phone_length: country?.phone_length || '',
      iso: country?.iso_code_alpha_2 || ''
    },
    enableReinitialize: true,
    onSubmit: (values, { setSubmitting }) => {
      const {
        name,
        code,
        iso,
        currency,
        main_lang,
        second_lang,
        params,
        finance_models,
        phone_length
      } = values;

      const data = {
        id: country?.id,
        name,
        iso_code_alpha_2: iso,
        code: `+${code}`,
        currency_id: currency?.id,
        main_lang_id: main_lang?.id,
        phone_length: +phone_length,
        second_langs: second_lang?.map(lang => lang?.id),
        business_info: {
          params
        },
        finance_models: state
          .filter((model: FinanceModel) => {
            return model?.active === true;
          })
          .map(model => model?.id)
      };

      country?.id
        ? dispatch(updateCountry(data, () => setSubmitting(false)))
        : dispatch(createCountry(data, () => setSubmitting(false)));
    },
    validationSchema: countrySchema(),
    validateOnChange: true
  });

  const {
    setFieldValue,
    values: { iso, currency, main_lang, second_lang, finance_models, params }
  } = form;
  const countryError = form.errors[0];

  const selectChange = option => {
    setFieldValue('second_lang', option);
  };

  const selectedOption = form.values.second_lang;

  const onChangeCurrency = cuurency => {
    setFieldValue('currency', cuurency);
  };

  const onChangeMainLanguage = language => {
    setFieldValue('main_lang', language);
  };

  const onChangeSecondLanguage = language => {
    setFieldValue('second_lang', language);
  };

  const disabledSelectors = form.isSubmitting;

  return {
    t,
    form,
    setFieldValue,
    countryError,
    coutryTitles,
    selectChange,
    selectedOption,
    countryName,
    params,
    currencies,
    onChangeCurrency,
    currency,
    languages,
    main_lang,
    onChangeMainLanguage,
    second_lang,
    onChangeSecondLanguage,
    financeModels,
    finance_models,
    state,
    setstate,
    disabledSelectors
  };
};

export { useCountryProps };
