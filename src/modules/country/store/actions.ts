import { make } from 'redux-chill';
import { Country, CreateOrUpdateCountry } from 'src/api/models/country';
import { FinanceModel } from 'src/api/models/finance-model';

const getCountries = make('[country] get countries')
  .stage(
    (params: { page?: number; id?: number; callback?: () => void } = {}) =>
      params
  )
  .stage(
    'success',
    (data: { totalPages: number; countries: Country[] }) => data
  )
  .stage('failure', (error: string) => error)
  .stage('finish');

const createCountry = make('[country] create country')
  .stage((data: CreateOrUpdateCountry, callback: () => void) => ({
    data,
    callback
  }))
  .stage('success')
  .stage('failure', (error: string) => error)
  .stage('finish');

const updateCountry = make('[country] update country')
  .stage((data: CreateOrUpdateCountry, callback: () => void) => ({
    data,
    callback
  }))
  .stage('success')
  .stage('failure', (error: string) => error)
  .stage('finish');

const getFinanceModels = make('[country] get finance models')
  .stage((data?: { id?: number; name?: string }, callback?: () => void) => ({
    data,
    callback
  }))
  .stage('success', (models: FinanceModel[]) => models)
  .stage('failure', (error: string) => error)
  .stage('finish');

export { getCountries, createCountry, updateCountry, getFinanceModels };
