import { reducer } from 'redux-chill';
import {
  createCountry,
  getCountries,
  getFinanceModels,
  updateCountry
} from './actions';
import { CountryState } from './state';

const country = reducer(new CountryState())
  .on(getCountries.success, (state, { countries, totalPages }) => {
    state.countries = countries;
    state.paginationLength = totalPages;
  })
  .on(getCountries.failure, (state, error) => {
    state.errors = error;
    state.paginationLength = 0;
  })
  .on(createCountry.failure, (state, error) => {
    state.errors = error;
  })
  .on(updateCountry.failure, (state, error) => {
    state.errors = error;
  })
  .on(getFinanceModels.success, (state, models) => {
    state.financeModels = models;
  });

export { country };
