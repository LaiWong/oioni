import { StoreContext } from '@store/context';
import { snack } from '@store/snackbar';
import { Payload, Saga } from 'redux-chill';
import { call, put } from 'redux-saga/effects';
import {
  createCountry,
  getCountries,
  getFinanceModels,
  updateCountry
} from './actions';

class CountrySaga {
  private perPageCount = 9;

  /**
   * Get countries list
   */
  @Saga(getCountries)
  public *getCountries(
    { id, page }: Payload<typeof getCountries>,
    { country }: StoreContext
  ) {
    try {
      const {
        data: { result, totalPages }
      } = yield call(country.getCountries, {
        id,
        page,
        perPage: this.perPageCount
      });

      yield put(getCountries.success({ countries: result, totalPages }));
    } catch (error) {
      yield put(snack('get countries error', 'error'));
    }
  }

  @Saga(createCountry)
  public *createCountry(
    { data, callback }: Payload<typeof createCountry>,
    { country }: StoreContext
  ) {
    try {
      const response = yield call(country.createCountry, data);
      yield put(createCountry.success(response.data));
      yield put(snack('Country was created!'));
    } catch (error) {
      yield put(snack('Cannot create country!', 'error'));
    } finally {
      callback();
    }
  }

  @Saga(updateCountry)
  public *updateCountry(
    { data, callback }: Payload<typeof updateCountry>,
    { country }: StoreContext
  ) {
    try {
      const response = yield call(country.updateCountry, data);
      yield put(updateCountry.success(response));
      yield put(snack('Country was updated!'));
    } catch (error) {
      yield put(snack('Cannot update Country!', 'error'));
    } finally {
      callback();
    }
  }

  @Saga(getFinanceModels)
  public *getFinanceModels(
    { data = {} }: Payload<typeof getFinanceModels>,
    { financeModel }: StoreContext
  ) {
    try {
      const {
        data: { result }
      } = yield call(financeModel.getFinanceModels, data);
      yield put(getFinanceModels.success(result));
    } catch (error) {
      yield put(snack('Get finance model error!', 'error'));
    }
  }
}

export { CountrySaga };
