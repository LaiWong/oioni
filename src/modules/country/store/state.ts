import { Country, FinanceModel } from '@api';

class CountryState {
  /**
   * Countries
   */
  public countries: Array<Partial<Country>> = [];

  /**
   * Pagination length
   */
  public paginationLength: number;

  /**
   * Finance Models
   */
  public financeModels: FinanceModel[] = [];

  /**
   * Errors
   */
  public errors: string = null;
}

export { CountryState };
