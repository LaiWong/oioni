import { array, object, string } from 'yup';

/**
 * Country form schema
 */

const countrySchema = () =>
  object({
    code: string().required('Phone code is required!'),
    iso: string().required('Iso code is required'),
    name: string().required('Country name is required!'),
    currency: object().shape({
      currency: string().required('Currency is required!')
    }),
    main_lang: object().shape({
      language: string().required('Default language is required!')
    }),
    second_lang: array()
      .of(
        object().shape({
          language: string().required('Second languages is required!')
        })
      )
      .required('Second languages is required!'),
    phone_length: string().required('Phone length is required!'),
    // finance_models: array().required('Finance models are required!'),
    params: array()
      .of<{ name: string; length: string }>(
        object().shape({
          name: string().required('Info are required!'),
          length: string().required('Info are required!')
        })
      )
      .required('Business info are required!')
  });

export { countrySchema };
