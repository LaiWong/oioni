import {
  Body,
  Button,
  Cell,
  Field as CustomField,
  Form,
  Group,
  Head,
  hoc,
  Modal,
  Row,
  Table
} from '@core';
import classNames from 'classnames';
import React from 'react';
import { useCurrenciesProps } from './currencies.props';
import styles from './currencies.scss';
import { deleteCurrency } from './store';

const Currencies = hoc(
  useCurrenciesProps,
  ({
    t,
    form,
    dispatch,
    currencies,
    currenciesTitle,
    isVisible,
    setIsVisible,
    isActive,
    currencyDispatch,
    switcherChange,
    onCreateCurrency
  }) => (
    <div className={styles.currencies}>
      <Form form={form} className={styles.form}>
        {isVisible && (
          <Modal className={styles.modal}>
            <span
              className={styles.modalClose}
              onClick={() => setIsVisible(false)}
            />
            <h3 className={styles.modalTitle}>
              {form.values?.id ? 'Edit' : 'Create'} Currency
            </h3>
            <Group>
              <div className={styles.modalContent}>
                <CustomField.Input label={'Currency Name'} name='currency' />
                <div className={styles.modalSwitcher}>
                  <p className={isActive ? styles.active : ''}>disable</p>
                  <div
                    className={classNames(
                      styles.switcher,
                      styles[`switcher${isActive ? 'Active' : 'NotActive'}`]
                    )}
                    onClick={switcherChange}
                  >
                    <div
                      className={classNames(
                        styles.toggle,
                        styles[`toggle${isActive ? 'Active' : 'NotActive'}`]
                      )}
                    />
                  </div>
                  <p className={!isActive ? styles.active : ''}>activate</p>
                </div>
              </div>
            </Group>
            <div className={styles.modalButtons}>
              <Button type='submit'>Save</Button>
              <Button type='reset'>Cancel</Button>
            </div>
          </Modal>
        )}
        <div className={styles.container}>
          <div className={styles.header}>
            <h1>Currency Manager</h1>
            <Button type='button' onClick={onCreateCurrency}>
              Add Currency
            </Button>
          </div>
          <div className={styles.tableWrapper}>
            <Table>
              <Head>
                <Row>
                  {currenciesTitle.map(
                    ({ title, atCenter, atEnd }, ReactIndex) => (
                      <Cell
                        key={ReactIndex}
                        bold
                        atCenter={atCenter}
                        atEnd={atEnd}
                      >
                        {title}
                      </Cell>
                    )
                  )}
                </Row>
              </Head>
              <Body>
                {currencies.map(({ id, currency, active }, index) => (
                  <Row key={index}>
                    <Cell>{id}</Cell>
                    <Cell atCenter>{currency}</Cell>
                    <Cell>
                      <div className={styles.currencyStatus}>
                        {active.toString()}
                      </div>
                    </Cell>
                    <Cell className={styles.buttons}>
                      <Button
                        type='button'
                        onClick={() => {
                          currencyDispatch({
                            type: 'update',
                            payload: { id, currency, active }
                          });
                          setIsVisible(true);
                        }}
                      >
                        Edit
                      </Button>
                      <Button
                        type='button'
                        theme='secondary'
                        onClick={() => {
                          dispatch(deleteCurrency(id));
                        }}
                      >
                        Delete
                      </Button>
                    </Cell>
                  </Row>
                ))}
              </Body>
            </Table>
          </div>
        </div>
      </Form>
    </div>
  )
);
export { Currencies };
