import { Currency } from '@api';
import { State } from '@store';
import { useFormik } from 'formik';
import { useEffect, useMemo, useReducer, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';
import { RouteComponentProps } from 'react-router';
import { createCurrency, getCurrencies, updateCurrency } from './store';

const useCurrenciesProps = (props: RouteComponentProps) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getCurrencies());
  }, []);

  const currenciesTitle = [
    {
      title: 'ID',
      atCenter: false,
      atEnd: false
    },
    {
      title: 'Name',
      atCenter: true,
      atEnd: false
    },
    {
      title: 'Active',
      atCenter: true,
      atEnd: false
    },
    {
      title: '',
      atCenter: false,
      atEnd: false
    }
  ];
  let _currencies = useSelector((state: State) => state.currency.currencies);

  const [isVisible, setIsVisible] = useState(false);

  const [currency, currencyDispatch] = useReducer((state, action) => {
    switch (action.type) {
      case 'create':
        return (state = {} as Currency);
      case 'update':
        return (state = action.payload);
      default:
        return (state = {} as Currency);
    }
  }, 'create');

  const form = useFormik<Currency>({
    initialValues: currency,
    onSubmit: values => {
      values?.id
        ? dispatch(updateCurrency(values))
        : dispatch(createCurrency(values));

      setIsVisible(false);
    },
    enableReinitialize: true
    // validate: {}
  });

  const { active: isActive } = form.values;

  const currencies = useMemo(
    () =>
      _currencies.length
        ? [..._currencies].sort((a, b) => a.id - b.id)
        : _currencies,
    [_currencies]
  );

  const switcherChange = () => {
    form.setFieldValue('active', !form.values?.active);
  };

  const onCreateCurrency = () => {
    currencyDispatch({ type: 'create' });
    setIsVisible(true);
  };

  return {
    t,
    form,
    dispatch,
    currenciesTitle,
    currencies,
    isVisible,
    setIsVisible,
    isActive,
    currencyDispatch,
    switcherChange,
    onCreateCurrency,
    ...props
  };
};

export { useCurrenciesProps };
