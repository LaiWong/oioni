import { Currency } from '@api';
import { make } from 'redux-chill';

const getCurrencies = make('[currency] get currency')
  .stage((id?: number, currency?: string, active?: boolean) => ({
    id,
    currency,
    active
  }))
  .stage('success', (currencies: Currency[]) => currencies)
  .stage('failure')
  .stage('finish');

const createCurrency = make('[currency] create currency')
  .stage((data: Omit<Currency, 'id'>) => data)
  .stage('success')
  .stage('failure')
  .stage('finish');

const updateCurrency = make('[currency] update currency')
  .stage((currency: Currency) => currency)
  .stage('success')
  .stage('failure')
  .stage('finish');

const deleteCurrency = make('[currency] delete currency')
  .stage((id: number) => id)
  .stage('success')
  .stage('failure')
  .stage('finish');

export { getCurrencies, createCurrency, updateCurrency, deleteCurrency };
