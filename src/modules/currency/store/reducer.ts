import { reducer } from 'redux-chill';
import { getCurrencies } from './actions';
import { CurrencyState } from './state';

const currency = reducer(new CurrencyState()).on(
  getCurrencies.success,
  (state, currencies) => (state.currencies = currencies)
);

export { currency };
