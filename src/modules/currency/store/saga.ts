import { StoreContext } from '@store/context';
import { snack } from '@store/snackbar';
import { Saga, Payload } from 'redux-chill';
import { call, put } from 'redux-saga/effects';
import {
  getCurrencies,
  createCurrency,
  updateCurrency,
  deleteCurrency
} from './actions';

class CurrencySaga {
  @Saga(getCurrencies)
  public *getLanguages(
    data: Payload<typeof getCurrencies>,
    { currency }: StoreContext
  ) {
    try {
      const {
        data: { result }
      } = yield call(currency.getCurrencies, data);
      yield put(getCurrencies.success(result));
    } catch (error) {
      yield put(snack('Cannot get Currency!', 'error'));
    }
  }

  @Saga(createCurrency)
  public *createCurrency(
    data: Payload<typeof createCurrency>,
    { currency }: StoreContext
  ) {
    try {
      const {
        data: { result }
      } = yield call(currency.createCurrency, data);
      yield put(snack('Currency was created successfully!'));
    } catch (error) {
      yield put(snack('Cannot create Currency!', 'error'));
    } finally {
      yield put(getCurrencies());
    }
  }

  @Saga(updateCurrency)
  public *updateLanguage(
    data: Payload<typeof updateCurrency>,
    { currency }: StoreContext
  ) {
    try {
      const {
        data: { result }
      } = yield call(currency.updateCurrency, data);
      yield put(snack('Currency was updated successfully!'));
    } catch (error) {
      yield put(snack('Cannot update currency!', 'error'));
    } finally {
      yield put(getCurrencies());
    }
  }

  @Saga(deleteCurrency)
  public *deleteCurrency(
    id: Payload<typeof deleteCurrency>,
    { currency }: StoreContext
  ) {
    try {
      const {
        data: { result }
      } = yield call(currency.deleteCurrency, id);
      yield put(snack('Currency was deleted successfully!'));
    } catch (error) {
      yield put(snack('Cannot delete currency!', 'error'));
    } finally {
      yield put(getCurrencies());
    }
  }
}

export { CurrencySaga };
