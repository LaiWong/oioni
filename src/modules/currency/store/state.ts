import { Currency } from '@api';

class CurrencyState {
  public currencies: Currency[] = [];
}

export { CurrencyState };
