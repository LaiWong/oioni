import * as React from 'react';
import { CustomersProps } from './customers.props';
import * as styles from './customers.scss';
import { Route, Switch } from 'react-router-dom';
import { CustomersList } from './pages';

/**
 * Renders Customers
 */
const Customers: React.FC<CustomersProps> = ({ match }) => (
  <div className={styles.customers}>
    <Switch>
      <Route path={match.path} component={CustomersList} />
    </Switch>
  </div>
);

export { Customers };
