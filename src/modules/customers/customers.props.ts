import { RouteComponentProps } from 'react-router';

/**
 * Props
 */
type CustomersProps = RouteComponentProps;

export { CustomersProps };
