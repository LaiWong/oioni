import { useSort } from '@core';
import { Customer } from '@api';

const sortByElements = [
  'name',
  'income',
  'last_booking_date',
  'email',
  'phone'
];

const useSortCustomers = (customers: Customer[] = []) => {
  const {
    sortBy,
    direction,
    sortedElements: sortedCustomers,
    setSortBy,
    setDirection,
    setSortedElements: setSortedCustomers,
    sort
  } = useSort<Customer>(customers);

  const onSortClick = (sortBy: string) => {
    const sortByIndex = sortByElements.indexOf(sortBy);
    const servicesToSort = [...sortedCustomers];

    switch (sortByIndex) {
      case 1: {
        servicesToSort.sort((a, b) => sort(a, b, 'income', true));

        break;
      }
    }

    setDirection(direction === 'up' ? 'down' : 'up');
    setSortedCustomers(servicesToSort);
    setSortBy(sortBy);
  };

  return { sortBy, direction, sortedCustomers, onSortClick, setSortBy };
};

export { sortByElements, useSortCustomers };
