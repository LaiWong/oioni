import { useTranslation } from 'react-i18next';
import { useMemo } from 'react';

/**
 * Titles
 */
const useTitles = (
  // Key is equal to component name
  key
): {
  name: string;
  atEnd?: boolean;
  bold?: boolean;
  atCenter?: boolean;
}[] => {
  const { t } = useTranslation();

  const titles = useMemo(
    () => ({
      customers: [
        {
          name: t('merchantPanel.customers.list.name')
        },
        {
          name: t('merchantPanel.customers.list.income').replace(', €', '')
        },
        {
          name: t('merchantPanel.customers.list.last_booking_date')
        },
        {
          name: t('merchantPanel.customers.list.email')
        },
        {
          name: t('merchantPanel.customers.list.phone')
        }
      ]
    }),
    [t]
  );

  return titles[key.toLowerCase()];
};

export { useTitles };
