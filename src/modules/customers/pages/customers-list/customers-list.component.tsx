import * as React from 'react';
import * as moment from 'moment';
import { useCustomersList } from './customers-list.props';
import * as styles from './customers-list.scss';
import {
  Body,
  Cell,
  formatPrice,
  Head,
  hoc,
  Pagination,
  Row,
  Search,
  Table
} from '@core';
import classNames from 'classnames';
import { sortByElements } from '@customers/hooks';

const getLastBookingTime = date =>
  date ? moment(date).format('MM.DD.YYYY [at] HH:mm') : '';

export const getFormatPhonNumber = phone =>
  phone?.replace(/\+(\d{2})(\d{3})(\d{3})(\d{4})/, '+$1 $2 $3 $4');

/**
 * Renders CustomersList
 */
const CustomersList = hoc(
  useCustomersList,
  ({
    t,
    query,
    paginationLength,
    customersTitles,
    sortedCustomers,
    sortBy,
    direction,
    onSortClick,
    onQueryChange,
    onPageChange
  }) => (
    <div className={styles.customersList}>
      <div className={styles.header}>
        <h2 className={styles.title}>
          {t('merchantPanel.customers.list.title')}
        </h2>
        <Search
          value={query}
          onChange={onQueryChange}
          className={styles.search}
          placeholder={t('merchantPanel.customers.list.searchPlaceholder')}
        />
      </div>
      <Table>
        <Head>
          <Row>
            {customersTitles?.map(({ name, atCenter, atEnd, bold }, index) => (
              <Cell
                key={index}
                className={classNames({ [styles.sortElement]: index === 1 })}
                atEnd={atEnd}
                atCenter={atCenter}
                bold={bold}
                onClick={() =>
                  index === 1 && onSortClick(sortByElements[index])
                }
              >
                {name}
                {index === 1 && index === sortByElements.indexOf(sortBy) && (
                  <img
                    className={styles.sortDirection}
                    style={{
                      transform: `rotate(${
                        direction === 'up' ? '90' : '-90'
                      }deg)`
                    }}
                    src={require('img/arrow-left.png')}
                    alt={direction}
                  />
                )}
              </Cell>
            ))}
          </Row>
        </Head>
        <Body>
          {sortedCustomers.map(customer => (
            <Row key={customer.id}>
              <Cell>
                <div className={styles.customer}>
                  {customer.image_url ? (
                    <img className={styles.avatar} src={customer.image_url} />
                  ) : (
                    <div
                      className={classNames(
                        styles.avatar,
                        styles.avatarNoAvatar
                      )}
                    />
                  )}
                  <div className={styles.customerName}>
                    {customer.first_name} {customer.last_name}
                  </div>
                </div>
              </Cell>
              <Cell>
                <div className={styles.name}>
                  {formatPrice(customer.income)}
                </div>
              </Cell>
              <Cell>
                <div className={styles.customerInfo}>
                  {getLastBookingTime(customer.last_booking_date)}
                </div>
              </Cell>
              <Cell>
                <div className={styles.customerInfo}>{customer.email} </div>
              </Cell>
              <Cell>
                <div className={styles.customerInfo}>
                  {getFormatPhonNumber(customer.phone)}
                </div>
              </Cell>
            </Row>
          ))}
        </Body>
      </Table>
      <Pagination length={paginationLength} onPageChange={onPageChange} />
    </div>
  )
);

export { CustomersList };
