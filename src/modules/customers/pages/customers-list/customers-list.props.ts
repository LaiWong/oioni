import { useMeta, usePagination } from '@core';
import { getCustomers } from '@customers/store';
import { State } from '@store';
import { useEffect, useRef, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { useSortCustomers, useTitles } from '@customers/hooks';

const MIN_SEARCH_SYMBOLS = 3;

/**
 * Use customers list
 */
const useCustomersList = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const { paginationLength, customers } = useSelector(
    (state: State) => state.merchant.customers
  );

  const {
    sortBy,
    sortedCustomers,
    direction,
    onSortClick,
    setSortBy
  } = useSortCustomers(customers);
  const { selectedPage, onPageChange } = usePagination(() => {
    setSortBy('');
  });

  const customersTitles = useTitles('customers');

  const [query, setQuery] = useState('');
  const interval = useRef<any>();

  const onQueryChange = (search: string) => {
    setQuery(search);

    clearTimeout(interval.current);

    interval.current = setTimeout(() => {
      if (!search || search.length >= MIN_SEARCH_SYMBOLS) {
        dispatch(getCustomers({ search }));
      }
    }, 300);
  };

  useEffect(() => {
    dispatch(getCustomers({ page: selectedPage }));
  }, [selectedPage]);

  useMeta({ title: 'Customers - Oioni Administration Console' }, []);

  return {
    t,
    query,
    customersTitles,
    paginationLength,
    sortBy,
    sortedCustomers,
    direction,
    onSortClick,
    onQueryChange,

    onPageChange
  };
};

export { useCustomersList };
