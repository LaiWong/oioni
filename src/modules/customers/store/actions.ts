import { make } from 'redux-chill';
import { Customer } from '@api';

/**
 * Get customers
 */
const getCustomers = make('[client] get')
  .stage((params: { page?: number; search?: string } = {}) => params)
  .stage(
    'success',
    (payload: { totalPages: number; customers: Customer[] }) => payload
  )
  .stage('finish');

export { getCustomers };
