import { reducer } from 'redux-chill';
import { CustomersState } from './state';
import { getCustomers } from './actions';

/**
 * General state
 */
const customers = reducer(new CustomersState()).on(
  getCustomers.success,
  (state, { customers, totalPages }) => {
    state.customers = customers;
    state.paginationLength = totalPages;
  }
);

export { customers };
