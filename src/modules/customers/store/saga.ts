import { getCustomers } from './actions';
import { Payload, Saga } from 'redux-chill';
import { StoreContext } from '@store/context';
import { State } from '@store';
import { call, put, select } from 'redux-saga/effects';
import { snack } from '@store/snackbar';

class CustomersSaga {
  private perPageCount = 9;

  /**
   * Get customers list
   */
  @Saga(getCustomers)
  public *getCustomers(
    { search, page }: Payload<typeof getCustomers>,
    { customer }: StoreContext
  ) {
    try {
      const {
        general: { merchantUser }
      }: State = yield select();

      const response = yield call(customer.get, {
        search,
        page,
        perPage: this.perPageCount,
        merchant: merchantUser.id
      });

      yield put(
        getCustomers.success({
          customers: response.data.result,
          totalPages: response.data.totalPages
        })
      );
    } catch (error) {
      yield put(snack('message.cannotGetCustomers', 'error'));
    }
  }
}

const sagas = [new CustomersSaga()];

export { CustomersSaga, sagas };
