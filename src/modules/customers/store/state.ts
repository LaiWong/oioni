import { Customer } from '@api';

class CustomersState {
  /**
   * Customers list
   */
  public customers: Customer[] = [];
  /**
   * Pagination Length
   */
  public paginationLength: number;
}

export { CustomersState };
