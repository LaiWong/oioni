import * as React from 'react';
import { AppointmentProps } from './appointment.props';
import * as styles from './appointment.scss';
import classNames from 'classnames';
import * as moment from 'moment-timezone';
import { EmployeeImageUrl } from '@api';
import { formatPrice } from '@core';

/**
 * Renders Appointment
 */
const Appointment: React.FC<AppointmentProps> = ({
  name,
  price,
  employee,
  stylistName,
  customerName,
  customerPhone,
  booking_date_time,
  store_name,
  color,
  bgColor
}) => (
  <div className={styles.appointment} style={{ background: bgColor }}>
    <div className={styles.leftLine} style={{ background: color }} />
    <div className={styles.top}>
      <div className={styles.content}>
        <span className={classNames(styles.salon)}>{store_name}</span>
        <span className={classNames(styles.service)} style={{ color }}>
          {name}
        </span>
        <span className={styles.time}>
          {moment(booking_date_time).tz('UTC').format('HH:mm DD.MM.YYYY')}
        </span>
      </div>
      <div className={classNames(styles.content, styles.specialist)}>
        <div className={styles.avatar}>
          <img
            src={employee?.image_url || EmployeeImageUrl.default}
            alt='avatar'
          />
        </div>
        <span className={styles.name}>{stylistName}</span>
      </div>
    </div>
    <div className={styles.bottom}>
      <div className={styles.content}>
        <span className={styles.customer}>{customerName}</span>
        <span className={styles.phone}>{customerPhone}</span>
      </div>
      <div className={styles.content}>
        <span className={styles.price}>{formatPrice(price)}</span>
      </div>
    </div>
  </div>
);

export { Appointment };
