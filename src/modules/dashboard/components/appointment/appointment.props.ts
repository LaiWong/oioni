import { Appointment } from '@api';

/**
 * Props
 */
type AppointmentProps = Appointment & {
  /**
   * Color
   */
  color: string;

  /**
   * Background color
   */
  bgColor: string;
};

export { AppointmentProps };
