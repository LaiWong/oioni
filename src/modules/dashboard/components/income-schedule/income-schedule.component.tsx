import * as React from 'react';
import { useIncomeSchedule } from './income-schedule.props';
import * as styles from './income-schedule.scss';
import { formatPrice, hoc } from '@core';
import moment from 'moment';

/**
 * Renders IncomeSchedule
 */
const IncomeSchedule = hoc(
  useIncomeSchedule,
  ({
    t,
    last10DaysStatistic,
    rowsPrices,
    amountOnlinePayment,
    amountPayInSalon
  }) => (
    <div className={styles.appointmentsTimeList}>
      <div className={styles.time}>
        {rowsPrices?.map((price, index) => (
          <div className={styles.timeItem} key={index}>
            {formatPrice(price)}
          </div>
        ))}
      </div>
      <div>
        <div className={styles.appointmentGrid}>
          <div>
            {rowsPrices.map((time, index) => (
              <div className={styles.appointmentGridItem} key={index} />
            ))}
          </div>

          {last10DaysStatistic.map(
            ({ height, left }, index) =>
              height.height > 0 && (
                <div
                  className={styles.column}
                  key={index}
                  style={{ height: height.height, left }}
                >
                  <div
                    style={{
                      backgroundColor: '#ebfcf2',
                      border: height.status5 > 0 ? '2px solid #aef3cb' : 'none',
                      borderRadius: '10px 10px 0px 0px',
                      height: height.status5,
                      width: '100%'
                    }}
                  />
                  <div
                    style={{
                      backgroundColor: 'rgb(236, 249, 255)',
                      border:
                        height.status20 > 0 ? '2px solid #b7daf5' : 'none',
                      borderRadius:
                        height.status5 === 0
                          ? '10px 10px 0px 0px'
                          : '2px 2px 0px 0px',
                      marginTop: height.status5 > 0 ? '2px' : '0',
                      height:
                        height.status5 > 0
                          ? height.status20 - 2
                          : height.status20,
                      width: '100%'
                    }}
                  />
                </div>
              )
          )}
        </div>

        <div className={styles.days}>
          {last10DaysStatistic.map(({ date }, index) => (
            <div className={styles.day} key={index}>
              {moment(date).format('DD.MM')}
            </div>
          ))}
        </div>
        <div className={styles.description}>
          <div className={styles.status5}>
            <div className={styles.point}>
              {t('merchantPanel.dashboard.schedule.onlinePayments')}{' '}
              {amountOnlinePayment}
            </div>
          </div>
          <div className={styles.status20}>
            <div className={styles.point}>
              {t('merchantPanel.dashboard.schedule.payInSalon')}{' '}
              {amountPayInSalon}
            </div>
          </div>
        </div>
      </div>
    </div>
  )
);

export { IncomeSchedule };
