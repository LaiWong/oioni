import { BalanceStatistics } from '@api';
import { formatPrice } from '@core';
import moment from 'moment';
import { useTranslation } from 'react-i18next';

const sizes = {
  quarterHeight: 75,
  columnWidth: 40,
  columnPadding: 20
};

/**
 * Props
 */
type IncomeScheduleProps = {
  /**
   * Balance statistics
   */
  balanceStatistics: BalanceStatistics;
};

const useIncomeSchedule = ({ balanceStatistics }: IncomeScheduleProps) => {
  const { t } = useTranslation();
  const rowPrice = balanceStatistics.maxAmount.price / 4;
  const rowsPrices = [rowPrice * 4, rowPrice * 3, rowPrice * 2, rowPrice];

  const last10DaysStatistic = Array(10)
    .fill(0)
    .map((_, index) => {
      const date = moment()
        .subtract(9 - index, 'd')
        .format('YYYY-MM-DD');

      const hasStats = Boolean(balanceStatistics.stats[date]);
      if (!hasStats) {
        return {
          height: {
            height: 0,
            status20: 0,
            status5: 0
          },
          date
        };
      }

      const height = {
        height:
          (balanceStatistics.stats[date].totalPrice / rowPrice) *
          sizes.quarterHeight,
        status20:
          (balanceStatistics.stats[date].status20Price / rowPrice) *
          sizes.quarterHeight,
        status5:
          (balanceStatistics.stats[date].status5Price / rowPrice) *
          sizes.quarterHeight
      };

      const left = index * (sizes.columnWidth + sizes.columnPadding);

      return {
        height,
        left,
        date
      };
    });
  const amountOnlinePayment = formatPrice(balanceStatistics.amountStatus5);
  const amountPayInSalon = formatPrice(balanceStatistics.amountStatus20);

  return {
    t,
    last10DaysStatistic,
    rowsPrices,
    amountOnlinePayment,
    amountPayInSalon
  };
};

export { useIncomeSchedule };
