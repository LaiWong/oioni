import * as React from 'react';
import { Fragment, useEffect } from 'react';
import * as styles from './dashboard.scss';
import { Route, RouteChildrenProps, Switch } from 'react-router-dom';
import { Balances } from './pages';
import { Button } from '@core/components/button/button.component';
import { Appointment } from '@dashboard/components';
import { useDispatch, useSelector } from 'react-redux';
import { getBalanceStatistics, getDashboardAppointments } from './store';
import { State } from '@store';
import { formatPrice, hoc, Icon, useMeta } from '@core';
import { navigate } from '@store/router';
import { DashboardAppointmentType } from '@api';
import { useTranslation } from 'react-i18next';
import { IncomeSchedule } from './components';
import classNames from 'classnames';
import { useAppointmentsWithColors } from '@app/hooks';

/**
 * Use dashboard props
 */
const useDashboardProps = (props: RouteChildrenProps) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const {
    balanceStatistics,
    appointments: {
      upcoming: { appointments }
    }
  } = useSelector((state: State) => state.merchant.dashboard);

  const isAmountIncrease = balanceStatistics?.amountIncrease > 0;
  const isAmountDecrease = balanceStatistics?.amountIncrease < 0;

  // const filteredAppointments = appointments.filter(x => x.deleted !== 1);

  const formattedAppointments = useAppointmentsWithColors(appointments).filter(
    ({ deleted }) => deleted !== 1
  );

  const onMoreClick = () => {
    dispatch(navigate('/appointments'));
  };

  const onDetailsClick = () => {
    dispatch(navigate('/dashboard/balance'));
  };

  useEffect(() => {
    dispatch(getDashboardAppointments(DashboardAppointmentType.upcoming));
    dispatch(getBalanceStatistics());
  }, []);

  useMeta({ title: 'Dashboard - Oioni Administration Console' }, []);

  return {
    t,
    formattedAppointments,
    balanceStatistics,
    isAmountIncrease,
    isAmountDecrease,
    onMoreClick,
    onDetailsClick
  };
};

/**
 * Renders Dashboard
 */
const Dashboard = hoc(
  useDashboardProps,
  ({
    t,
    formattedAppointments,
    balanceStatistics,
    isAmountIncrease,
    isAmountDecrease,
    match: { path },
    onDetailsClick,
    onMoreClick
  }) => (
    <Switch>
      <Route path={`${path}/balance`} component={Balances} />
      <Route
        path={path}
        render={() => (
          <div className={styles.dashboard}>
            <div className={styles.left}>
              {balanceStatistics && (
                <Fragment>
                  <div className={styles.columns}>
                    <div className={styles.column}>
                      <div className={styles.balanceName}>
                        {t('merchantPanel.dashboard.totalBalance')}
                      </div>
                      <div className={styles.balance}>
                        {formatPrice(balanceStatistics.balance)}
                      </div>
                      <Button
                        theme='teritary'
                        onClick={onDetailsClick}
                        className={styles.buttonSeeDetails}
                      >
                        {t('merchantPanel.dashboard.seeDetails')}
                      </Button>
                    </div>
                    <div className={styles.column}>
                      <div className={styles.balanceName}>
                        {t('merchantPanel.dashboard.fulfilledBalance')}
                      </div>
                      <div className={styles.fulfilledBalance}>
                        {formatPrice(balanceStatistics.finished)}
                      </div>
                      <div className={styles.balanceDescription}>
                        {t(
                          'merchantPanel.dashboard.fulfilledBalanceDescription'
                        )}
                      </div>
                    </div>
                    <div className={styles.column}>
                      <div className={styles.balanceName}>
                        {t('merchantPanel.dashboard.notFulfilledBalance')}
                      </div>
                      <div className={styles.notFulfilledBalance}>
                        {formatPrice(balanceStatistics.pending)}
                      </div>
                      <div className={styles.balanceDescription}>
                        {t(
                          'merchantPanel.dashboard.notFulfilledBalanceDescription'
                        )}
                      </div>
                    </div>
                  </div>
                  <div className={styles.revenueOverview}>
                    {t('merchantPanel.dashboard.revenueOverview')}
                  </div>
                  <div className={styles.statsForLast10Days}>
                    {t('merchantPanel.dashboard.statsForLast10Days')}
                  </div>
                  <div
                    className={classNames(styles.amountIncrease, {
                      [styles.amountIncreaseSuccess]: isAmountIncrease,
                      [styles.amountIncreaseFail]: isAmountDecrease
                    })}
                  >
                    {balanceStatistics.amountIncrease !== 0 && (
                      <div
                        className={classNames(styles.iconContainer, {
                          [styles.iconSuccessContainer]: isAmountIncrease,
                          [styles.iconFailContainer]: isAmountDecrease
                        })}
                      >
                        <Icon className={styles.icon} name='arrow-light' />
                      </div>
                    )}
                    {isAmountIncrease && '+'}{' '}
                    {formatPrice(balanceStatistics?.amountIncrease)}
                  </div>
                  <IncomeSchedule balanceStatistics={balanceStatistics} />
                </Fragment>
              )}
            </div>

            <div className={styles.right}>
              <div className={styles.rightTop}>
                <h2 className={styles.title}>
                  {t('merchantPanel.dashboard.upcomingBookings')}
                </h2>

                <div className={styles.amount}>
                  {formattedAppointments?.length}
                </div>
              </div>
              <div className={styles.wrapper}>
                {formattedAppointments?.map((appointment, index) => (
                  <Appointment key={index} {...appointment} />
                ))}
              </div>
              <div className={styles.blur} />

              <Button
                className={styles.buttonAppointments}
                theme='teritary'
                size='sm'
                onClick={onMoreClick}
              >
                {t('merchantPanel.dashboard.seeAllBookings', {
                  total: formattedAppointments?.length
                })}
              </Button>
            </div>
          </div>
        )}
      />
    </Switch>
  )
);

export { Dashboard };
