import { Appointment } from '@api';
import { useSort } from '@core';

const sortByElements = [
  'bookingId',
  'stylistName',
  'appointmentDate',
  'clientName',
  'price'
];

const useSortDashboardAppointments = (appointments: Appointment[] = []) => {
  const {
    sortBy,
    direction,
    sortedElements: sortedAppointments,
    setSortBy,
    setDirection,
    setSortedElements: setSortedAppointments,
    sort,
    sortByDate
  } = useSort<Appointment>(appointments);

  const onSortClick = (sortBy: string) => {
    const sortByIndex = sortByElements.indexOf(sortBy);
    const servicesToSort = [...sortedAppointments];

    switch (sortByIndex) {
      case 0: {
        servicesToSort.sort((a, b) => sort(a, b, 'id'));

        break;
      }
      case 1: {
        servicesToSort.sort((a, b) => sort(a, b, 'stylistName'));

        break;
      }
      case 2: {
        servicesToSort.sort((a, b) => sortByDate(a, b, 'booking_date_time'));

        break;
      }
      case 3: {
        servicesToSort.sort((a, b) => sort(a, b, 'customerName'));

        break;
      }
      case 4: {
        servicesToSort.sort((a, b) => sort(a, b, 'price', true));

        break;
      }
    }

    setDirection(direction === 'up' ? 'down' : 'up');
    setSortedAppointments(servicesToSort);
    setSortBy(sortBy);
  };

  return { sortedAppointments, sortBy, direction, onSortClick };
};

export { sortByElements, useSortDashboardAppointments };
