import { useTranslation } from 'react-i18next';
import { useMemo } from 'react';

/**
 * Titles
 */
const useTitles = (
  // Key is equal to component name
  key
): {
  name: string;
  atEnd?: boolean;
  bold?: boolean;
  atCenter?: boolean;
}[] => {
  const { t } = useTranslation();

  const titles = useMemo(
    () => ({
      balances: [
        {
          name: t('merchantPanel.dashboard.balanceDetails.bookingId')
        },
        {
          name: t('merchantPanel.dashboard.balanceDetails.stylistName')
        },
        {
          name: t('merchantPanel.dashboard.balanceDetails.appointmentDate')
        },
        {
          name: t('merchantPanel.dashboard.balanceDetails.clientName')
        },
        {
          name: t('merchantPanel.dashboard.balanceDetails.price').replace(
            ', EUR',
            ''
          )
        }
      ]
    }),
    [t]
  );

  return titles[key.toLowerCase()];
};

export { useTitles };
