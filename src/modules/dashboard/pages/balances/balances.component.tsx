import * as React from 'react';
import { useBalancesProps } from './balances.props';
import * as styles from './balances.scss';
import { Body, Cell, formatPrice, Head, hoc, Icon, Row, Table } from '@core';
import * as moment from 'moment-timezone';
import classNames from 'classnames';
import { EmployeeImageUrl } from '@api';
import { sortByElements } from '@dashboard/hooks';

/**
 * <Balances />
 */
const Balances = hoc(
  useBalancesProps,
  ({
    t,
    currentTab,
    balancesTitles,
    tabs,
    onTabClick,
    sortedAppointments,
    onBackClick,
    onSortClick,
    sortBy,
    direction
  }) => (
    <div className={styles.balances}>
      <div className={styles.header}>
        <div className={styles.back} onClick={onBackClick}>
          <Icon className={styles.icon} name='arrow-back' />
        </div>
        <div className={styles.tabs}>
          {tabs.map(({ balance, id, title }) => (
            <div
              className={classNames(styles.tab, {
                [styles.tabSelected]: id === currentTab
              })}
              key={id}
              onClick={() => onTabClick(id)}
            >
              <h2 className={styles.title}>{title}</h2>
              <span className={styles.balance}>{formatPrice(balance)}</span>
            </div>
          ))}
        </div>
      </div>

      <Table>
        <Head>
          <Row>
            {balancesTitles?.map(({ name, atCenter, atEnd, bold }, index) => (
              <Cell
                key={index}
                className={styles.sortElement}
                atEnd={atEnd}
                atCenter={atCenter}
                bold={bold}
                onClick={() => onSortClick(sortByElements[index])}
              >
                {name}
                {index === sortByElements.indexOf(sortBy) && (
                  <img
                    className={styles.sortDirection}
                    style={{
                      transform: `rotate(${
                        direction === 'up' ? '90' : '-90'
                      }deg)`
                    }}
                    src={require('img/arrow-left.png')}
                    alt={direction}
                  />
                )}
              </Cell>
            ))}
          </Row>
        </Head>
        <Body>
          {sortedAppointments.map((appointment, index) => (
            <Row key={index}>
              <Cell>{appointment.id}</Cell>
              <Cell>
                <div className={styles.stylist}>
                  <img
                    className={styles.stylistAvatar}
                    src={
                      appointment.employee?.image_url ||
                      EmployeeImageUrl.default
                    }
                  />
                  <div className={styles.stylistName}>
                    {appointment.stylistName ||
                      t(
                        'merchantPanel.dashboard.balanceDetails.stylistNamePlaceholder'
                      )}
                  </div>
                </div>
              </Cell>
              <Cell className={styles.date}>
                {appointment.completed_on
                  ? moment(appointment.booking_date_time)
                      .tz('UTC')
                      .format('HH:mm DD.MM.YYYY')
                  : '-'}
              </Cell>
              <Cell>
                {appointment.customerName ? (
                  <div className={styles.customer}>
                    <div className={styles.customerName}>
                      {appointment.customerName}
                    </div>
                    <div className={styles.customerPhone}>
                      {appointment.customerPhone}
                    </div>
                  </div>
                ) : (
                  '-'
                )}
              </Cell>
              <Cell className={styles.priceValue}>
                {formatPrice(appointment.price)}
              </Cell>
            </Row>
          ))}
        </Body>
      </Table>
    </div>
  )
);

export { Balances };
