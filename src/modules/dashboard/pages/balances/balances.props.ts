import { useDispatch, useSelector } from 'react-redux';
import { State } from '@store';
import { useMemo, useState } from 'react';
import { navigate } from '@store/router';
import { useTranslation } from 'react-i18next';
import { DashboardAppointmentType, DashboardBalanceTab } from '@api';
import { getDashboardAppointments } from '@dashboard/store';
import { useMounted } from '@core';
import { useSortDashboardAppointments, useTitles } from '@dashboard/hooks';

/**
 * Use balances props
 */
const useBalancesProps = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const { balanceStatistics, appointments } = useSelector(
    (state: State) => state.merchant.dashboard
  );

  const [currentTab, setCurrentTab] = useState(DashboardBalanceTab.total);
  const tabs = [
    {
      title: t('merchantPanel.dashboard.balanceDetails.tabs.totalBalance'),
      balance: balanceStatistics?.balance,
      id: DashboardBalanceTab.total
    },
    {
      title: t('merchantPanel.dashboard.balanceDetails.tabs.completed'),
      balance: balanceStatistics?.finished,
      id: DashboardBalanceTab.completed
    },
    {
      title: t('merchantPanel.dashboard.balanceDetails.tabs.notFulfilled'),
      balance: balanceStatistics?.pending,
      id: DashboardBalanceTab.notFulfilled
    }
  ];

  const balancesTitles = useTitles('balances');

  const visibleAppointments = useMemo(() => {
    switch (currentTab) {
      case DashboardBalanceTab.total: {
        return appointments.total.appointments.filter(
          ({ deleted }) => deleted !== 1
        );
      }

      case DashboardBalanceTab.completed:
        return appointments.completed.appointments.filter(
          ({ deleted }) => deleted !== 1
        );

      case DashboardBalanceTab.notFulfilled:
        return appointments.upcoming.appointments.filter(
          ({ deleted }) => deleted !== 1
        );
    }
  }, [currentTab, appointments]);

  const onBackClick = () => {
    dispatch(navigate('/dashboard'));
  };

  const onTabClick = (tabId: DashboardBalanceTab) => {
    setCurrentTab(tabId);
  };

  useMounted(() => {
    dispatch(getDashboardAppointments(DashboardAppointmentType.total));
    dispatch(getDashboardAppointments(DashboardAppointmentType.completed));
    dispatch(getDashboardAppointments(DashboardAppointmentType.upcoming));
  });

  const {
    sortedAppointments,
    direction,
    sortBy,
    onSortClick
  } = useSortDashboardAppointments(visibleAppointments);

  return {
    t,
    currentTab,
    balancesTitles,
    tabs,
    onTabClick,
    sortedAppointments,
    onBackClick,
    onSortClick,
    sortBy,
    direction
  };
};

export { useBalancesProps };
