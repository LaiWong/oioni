import { make } from 'redux-chill';
import { Appointment, BalanceStatistics, DashboardAppointmentType } from '@api';

/**
 * Get dashboard appointments
 */
const getDashboardAppointments = make('[dashboard] get dashboard appointments')
  .stage((type: DashboardAppointmentType) => type)
  .stage(
    'success',
    (payload: {
      appointments: Appointment[];
      type: DashboardAppointmentType;
      total: number;
    }) => payload
  );

/**
 * Get balance statistics
 */
const getBalanceStatistics = make('[dashboard] get balance statistics').stage(
  'success',
  (balanceStatistics: BalanceStatistics) => balanceStatistics
);

export { getBalanceStatistics, getDashboardAppointments };
