import { reducer } from 'redux-chill';
import { DashboardState } from './state';
import { getBalanceStatistics, getDashboardAppointments } from './actions';

/**
 * General state
 */
const dashboard = reducer(new DashboardState())
  .on(
    getDashboardAppointments.success,
    (state, { appointments, total, type }) => {
      state.appointments[type].appointments = appointments; //appointments
      state.appointments[type].total = total;
    }
  )
  .on(getBalanceStatistics.success, (state, balanceStatistics) => {
    state.balanceStatistics = balanceStatistics;
  });

export { dashboard };
