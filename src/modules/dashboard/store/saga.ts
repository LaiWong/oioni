import { Saga, Payload } from 'redux-chill';
import { getBalanceStatistics, getDashboardAppointments } from './actions';
import { StoreContext } from '@store/context';
import { call, select, put, takeEvery } from 'redux-saga/effects';
import { State } from '@store';
import { Merchant, DashboardAppointmentType } from '@api';
import { snack } from '@store/snackbar';
import i18next from 'i18next';
import { number } from 'yup';

class DashboardSaga {
  /**
   * Get all appointments
   */
  @Saga(takeEvery, getDashboardAppointments)
  public *getDashboardAppointments(
    type: Payload<typeof getDashboardAppointments>,
    { appointments: appointmentsAPI }: StoreContext
  ) {
    try {
      const { id }: Merchant = yield select(
        (state: State) => state.general.merchantUser
      );

      const params: any = {
        page: 1,
        perPage: 9999,
        order_statuses: '5,20'
      };

      switch (type) {
        case DashboardAppointmentType.completed:
          params.previous = true;

          break;
        case DashboardAppointmentType.upcoming:
          params.future = true;

          break;
      }

      const {
        data: {
          data: { total_appointments, appointments }
        }
      } = yield call(appointmentsAPI.get, id, params);

      yield put(
        getDashboardAppointments.success({
          total: total_appointments,
          appointments,
          type
        })
      );
    } catch (error) {
      yield put(snack('message.cannotGetAllBookings', 'error'));
    }
  }

  /**
   * Get balance statistics
   */
  @Saga(getBalanceStatistics)
  public *getBalanceStatistics(_, { appointments }: StoreContext) {
    try {
      const { data } = yield call(appointments.balanceStatistics);

      yield put(getBalanceStatistics.success(data));
    } catch (e) {
      yield put(snack('message.cannotGetBalanceStatistics', 'error'));
    }
  }
}

const sagas = [new DashboardSaga()];

export { DashboardSaga, sagas };
