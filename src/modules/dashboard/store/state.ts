import { BalanceStatistics, DashboardAppointments } from '@api';

class DashboardState {
  /**
   * Appointments
   */
  public appointments: DashboardAppointments = {
    total: { appointments: [], total: 0 },
    completed: { appointments: [], total: 0 },
    upcoming: {
      appointments: [],
      total: 0
    }
  };

  /**
   * Balance statistics
   */
  public balanceStatistics: BalanceStatistics;
}

export { DashboardState };
