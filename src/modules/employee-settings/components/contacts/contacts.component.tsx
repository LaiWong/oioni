import * as React from 'react';
import * as styles from './contacts.scss';
import { ContactsProps } from './contacts.props';
import { Icon } from '@core/components/icon';
import classNames from 'classnames';
import { Button } from '@core';
import 'core.scss';
import { useTranslation } from 'react-i18next';

/**
 * Renders Contacts
 */
const Contacts: React.FC<ContactsProps> = ({ onClick }) => {
  const { t } = useTranslation();
  return (
    <div className={styles.container} onClick={onClick}>
      <div className={styles.contacts}>
        <h1 className={styles.title}>
          {t('employeePanel.settings.page.contact.title')}
        </h1>
        <p className={styles.textMain}>
          {t('employeePanel.settings.page.contact.mainText')}
        </p>
        <div className={styles.phone}>
          <Icon
            name='phone'
            className={classNames(styles.iconPhone, styles.icon)}
          />
          +372 56822602
        </div>
        <div className={styles.email}>
          <Icon
            name='mail'
            className={classNames(styles.iconMail, styles.icon)}
          />
          <div>hello@oioni.com</div>
        </div>
        <div className={styles.buttons}>
          <Button className={styles.button} size='md' theme='teritary'>
            {t('employeePanel.settings.page.contact.telegram')}
          </Button>
          <Button className={styles.button} size='md' theme='teritary'>
            {t('employeePanel.settings.page.contact.messanger')}
          </Button>
        </div>
        <div>
          <p className={styles.textDescription}>
            {t('employeePanel.settings.page.contact.description')}
          </p>
          <span>&#169; oioni, 2020</span>
        </div>
      </div>
    </div>
  );
};

export { Contacts };
