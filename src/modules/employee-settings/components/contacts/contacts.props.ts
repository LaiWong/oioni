import { RouteChildrenProps } from 'react-router';

/**
 * Props
 */
type ContactsProps = {
  settingName: string;
  iconName: string;
  arrow?: boolean;
  className?: string;
  onClick?: () => void;
} & RouteChildrenProps;

export { ContactsProps };
