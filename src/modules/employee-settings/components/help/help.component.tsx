import React from 'react';
import * as styles from './help.scss';
import { hoc } from '@core';
import { useHelpProps } from './help.props';
import classNames from 'classnames';
import { Question } from './question/question.component';

const Help = hoc(useHelpProps, ({ questions, t }) => (
  <div className={styles.container}>
    <div className={styles.help}>
      <h1 className={styles.title}>
        {t('employeePanel.settings.page.help.title')}
      </h1>
      <ul>
        {questions.map((element, index) => (
          <li key={index}>
            <Question item={element} />
          </li>
        ))}
      </ul>
    </div>
  </div>
));

export { Help };
