import { useTranslation } from 'react-i18next';

const useHelpProps = () => {
  const { t } = useTranslation();
  const questions = [
    {
      question: t('employeePanel.settings.page.help.question.1.title'),
      answer: t('employeePanel.settings.page.help.question.1.text')
    },
    {
      question: t('employeePanel.settings.page.help.question.2.title'),
      answer: t('employeePanel.settings.page.help.question.2.text')
    },
    {
      question: t('employeePanel.settings.page.help.question.3.title'),
      answer: t('employeePanel.settings.page.help.question.3.text')
    },
    {
      question: t('employeePanel.settings.page.help.question.4.title'),
      answer: t('employeePanel.settings.page.help.question.4.text')
    },
    {
      question: t('employeePanel.settings.page.help.question.5.title'),
      answer: t('employeePanel.settings.page.help.question.5.text')
    },
    {
      question: t('employeePanel.settings.page.help.question.6.title'),
      answer: t('employeePanel.settings.page.help.question.6.text')
    },
    {
      question: t('employeePanel.settings.page.help.question.7.title'),
      answer: t('employeePanel.settings.page.help.question.7.text')
    },
    {
      question: t('employeePanel.settings.page.help.question.8.title'),
      answer: t('employeePanel.settings.page.help.question.8.text')
    },
    {
      question: t('employeePanel.settings.page.help.question.9.title'),
      answer: t('employeePanel.settings.page.help.question.9.text')
    },
    {
      question: t('employeePanel.settings.page.help.question.10.title'),
      answer: t('employeePanel.settings.page.help.question.10.text')
    },
    {
      question: t('employeePanel.settings.page.help.question.11.title'),
      answer: t('employeePanel.settings.page.help.question.11.text')
    },
    {
      question: t('employeePanel.settings.page.help.question.12.title'),
      answer: t('employeePanel.settings.page.help.question.12.text')
    },
    {
      question: t('employeePanel.settings.page.help.question.13.title'),
      answer: t('employeePanel.settings.page.help.question.13.text')
    }
  ];
  return {
    questions,
    t
  };
};
export { useHelpProps };
