import React from 'react';
import { hoc, Icon } from '@core';
import { useQuestionProps } from './question.props';
import classNames from 'classnames';
import styles from './question.scss';

const Question = hoc(
  useQuestionProps,
  ({ handleVisibleAnswer, visible, item }) => (
    <div className={styles.item}>
      <div onClick={handleVisibleAnswer} className={styles.question}>
        {item.question}
        <Icon
          className={classNames(styles.arrow, {
            [styles.arrowOpen]: visible
          })}
          name='arrow-light'
        />
      </div>
      <div
        className={classNames(styles.answer, {
          [styles.answerVisible]: visible
        })}
      >
        {item.answer}
      </div>
    </div>
  )
);
export { Question };
