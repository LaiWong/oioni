import { useState } from 'react';

const useQuestionProps = ({ item }) => {
  const [visible, setVisible] = useState(false);
  const handleVisibleAnswer = () => {
    setVisible(!visible);
  };
  return { handleVisibleAnswer, visible, item };
};
export { useQuestionProps };
