import * as React from 'react';
import * as styles from './settings-item.scss';
import { SettingsItemProps } from './settings-item.props';
import { Icon } from '@core/components/icon';
import classNames from 'classnames';

/**
 * Renders SettingsItem
 */
const SettingsItem: React.FC<SettingsItemProps> = ({
  settingName,
  iconName,
  arrow,
  children,
  className,
  onClick
}) => (
  <div className={styles.settingsItem} onClick={onClick}>
    <div className={classNames(className, styles.settingTrigger, styles.open)}>
      <div className={styles.iconWrapper}>
        <Icon name={iconName} className={styles.icon} />
      </div>
      <span className={styles.settingName}>{settingName}</span>
      {arrow && <Icon name='arrow-light' className={styles.arrowIcon} />}
    </div>
    {true && children && (
      <div className={styles.settingDetails}>{children}</div>
    )}
  </div>
);

export { SettingsItem };
