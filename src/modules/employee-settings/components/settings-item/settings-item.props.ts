/**
 * Props
 */
type SettingsItemProps = {
  settingName: string;
  iconName: string;
  arrow?: boolean;
  className?: string;
  onClick?: () => void;
};

export { SettingsItemProps };
