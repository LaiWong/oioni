import * as React from 'react';
import { useEmployeeSettings } from './employee-settings.props';
import * as styles from './employee-settings.scss';
import { SettingsItem } from './components/settings-item';
import { Select } from '@core/components/select';
import { hoc, Button, Input } from '@core';
import { Icon } from '@core/components/icon';
import { Switch, Route, Redirect } from 'react-router';
import { Contacts } from './components/contacts/contacts.component';
import { Help } from './components/help/help.component';

/**
 * Renders EmployeeSettings
 */
const EmployeeSettings = hoc(
  useEmployeeSettings,
  ({
    t,
    language,
    languages,
    onLogoutClick,
    onLanguageChange,
    onContactsClick,
    onHelpClick,
    match: { path, url }
  }) => (
    <Switch>
      <Route path={path + '/contacts'} component={Contacts} />
      <Route path={path + '/help'} component={Help} />
      <Route
        path={`${path}`}
        render={() => (
          <div className={styles.employeeSettings}>
            <div className={styles.settingsBlock}>
              <SettingsItem
                settingName={t('employeePanel.settings.title')}
                iconName='planet'
                arrow
              >
                <Select
                  value={language}
                  label={t('employeePanel.settings.language')}
                  options={languages}
                  onChange={onLanguageChange}
                />
              </SettingsItem>

              <SettingsItem
                settingName={t('employeePanel.settings.notification.title')}
                iconName='speaker'
                arrow
              >
                <label className={styles.notification} htmlFor='switch1'>
                  <div className={styles.notificationText}>
                    <div>
                      {t('employeePanel.settings.notification.sms.allow')}
                    </div>
                    <div className={styles.notificationDescription}>
                      {t('employeePanel.settings.notification.sms.about')}
                    </div>
                  </div>
                  <div className={styles.checkboxSwitch}>
                    <input
                      disabled
                      id='switch1'
                      className={styles.checkboxItem}
                      type='checkbox'
                    />
                    <span className={styles.slider} />
                  </div>
                </label>

                <label className={styles.notification} htmlFor='switch2'>
                  <div className={styles.notificationText}>
                    <div>
                      {t('employeePanel.settings.notification.email.allow')}
                    </div>
                    <div className={styles.notificationDescription}>
                      {t('employeePanel.settings.notification.email.about')}
                    </div>
                  </div>
                  <div className={styles.checkboxSwitch}>
                    <input
                      disabled
                      id='switch2'
                      className={styles.checkboxItem}
                      type='checkbox'
                    />
                    <span className={styles.slider} />
                  </div>
                </label>
              </SettingsItem>

              <SettingsItem
                onClick={onHelpClick}
                settingName={t('employeePanel.settings.help')}
                iconName='question'
              />
              <SettingsItem
                onClick={onContactsClick}
                settingName={t('employeePanel.settings.contact')}
                iconName='contact-us'
              />
              <SettingsItem
                settingName={t('employeePanel.settings.logout')}
                iconName='sign-out'
                onClick={onLogoutClick}
              />
            </div>
          </div>
        )}
      />
      <Redirect to={`${url}/settings`} />
    </Switch>
  )
);

export { EmployeeSettings };
