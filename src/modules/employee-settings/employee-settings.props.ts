import { useTranslation } from 'react-i18next';
import { useDispatch } from 'react-redux';
import { setLanguage, logout } from '@store/general';
import { Language } from 'src/api/models/common';
import { useLanguagesList } from '@app/hooks';
import { RouteChildrenProps, useRouteMatch } from 'react-router';
import { navigate } from '@store/router';

const useEmployeeSettings = (props: RouteChildrenProps) => {
  const {
    t,
    i18n: { language }
  } = useTranslation();
  const dispatch = useDispatch();
  const languages = useLanguagesList();
  const { url, path } = useRouteMatch();

  const onLanguageChange = (language: Language) => {
    dispatch(setLanguage(language));
  };

  const onLogoutClick = () => {
    dispatch(logout());
  };
  const onContactsClick = () => {
    dispatch(navigate('/settings/contacts'));
  };
  const onHelpClick = () => {
    dispatch(navigate('/settings/help'));
  };

  return {
    t,
    language,
    languages,
    onLanguageChange,
    onLogoutClick,
    onContactsClick,
    onHelpClick
  };
};

export { useEmployeeSettings };
