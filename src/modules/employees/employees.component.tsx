import * as React from 'react';
import { EmployeesProps } from './employees.props';
import * as styles from './employees.scss';
import { Route, Switch } from 'react-router-dom';
import { Employee, EmployeesList } from './pages';
import { Worktimes } from '@worktimes';

/**
 * Renders Employees
 */
const Employees: React.FC<EmployeesProps> = ({ match }) => (
  <div className={styles.employees}>
    <Route path={match.path + '/edit/:id'} component={Employee} />
    <Switch>
      <Route path={match.path + '/add'} component={Employee} exact />
      <Route path={match.path + '/worktime/:id'} component={Worktimes} />
      <Route path={match.path} component={EmployeesList} />
    </Switch>
  </div>
);

export { Employees };
