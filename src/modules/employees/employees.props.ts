import { RouteComponentProps } from 'react-router';

/**
 * Props
 */
type EmployeesProps = RouteComponentProps;

export { EmployeesProps };
