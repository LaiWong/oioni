import { Button, Field, Form, FormError, Group, hoc, Language } from '@core';
import * as React from 'react';
import { Fragment } from 'react';
import { useEmployeeProps } from './employee.props';
import * as styles from './employee.scss';
import classNames from 'classnames';
import { Product } from 'src/api/models/product';

/**
 * Renders Employee
 */
const Employee = hoc(
  useEmployeeProps,
  ({
    t,
    form,
    isNew,
    formRef,
    lang,
    uml,
    onLanguageChange,
    canAddService,
    isRightColumnErrorVisible,
    rightColumnError,
    isInfoErrorVisible,
    infoError,
    onRemoveClick,
    onCancelClick,
    productOptions,
    onAddServiceClick,
    filterByAvailability,
    onAddAllServicesClick,
    error
  }) => {
    const {
      values: { products }
    } = form;

    return (
      <Fragment>
        {!isNew && <div className={styles.overlay} />}

        <Form form={form} formRef={formRef}>
          <div
            className={classNames(styles.employee, {
              [styles.employeeEdit]: !isNew
            })}
          >
            <div className={styles.header}>
              <h2 className={styles.title}>
                <div className={styles.titleCaption}>
                  {isNew
                    ? t('merchantPanel.employee.add.title')
                    : t('merchantPanel.employee.edit.title')}
                </div>
                <Language value={lang} onChange={onLanguageChange} />
              </h2>
              <div className={styles.buttons}>
                <Button
                  className={classNames(styles.button, styles.buttonColored)}
                  type='submit'
                  theme='primary'
                >
                  {t('merchantPanel.employee.form.save')}
                </Button>
                <Button
                  className={classNames(styles.button, styles.buttonSecondary)}
                  onClick={onCancelClick}
                  theme='secondary'
                >
                  {t('merchantPanel.employee.form.cancel')}
                </Button>
              </div>
            </div>
            <div className={styles.content}>
              <div className={styles.column}>
                <Group
                  className={styles.info}
                  title={t('merchantPanel.employee.form.info')}
                >
                  <Field.Input
                    name='name'
                    label={t('merchantPanel.employee.form.name')}
                  />
                  <Field.Input
                    name='email'
                    label={t('merchantPanel.employee.form.email')}
                  />
                  <Field.Input
                    name='phone'
                    label={t('merchantPanel.employee.form.phone')}
                    disabled={!isNew}
                  />
                </Group>

                <FormError>{error}</FormError>

                {/* <FormError visible={isInfoErrorVisible} content={infoError} /> */}

                {products && products?.length !== 0 && (
                  <Group
                    className={styles.services}
                    title={t('merchantPanel.employee.form.services')}
                  >
                    {products.map((product, index) => (
                      <Field.Select
                        name={`products.${index}`}
                        options={filterByAvailability(product, productOptions)}
                        placeholder='Select service...'
                        key={index}
                        shape={{
                          name: (option: Product) => uml(option.settings.name)
                        }}
                        after={
                          <div
                            className={styles.remove}
                            onClick={onRemoveClick(index)}
                          />
                        }
                      />
                    ))}
                  </Group>
                )}

                {canAddService && (
                  <div className={styles.controls}>
                    <Button type='button' onClick={onAddServiceClick}>
                      {t('merchantPanel.employee.form.addService')}
                    </Button>
                    <Button
                      type='button'
                      onClick={onAddAllServicesClick}
                      theme='secondary'
                    >
                      {t('merchantPanel.employee.form.addAllServices')}
                    </Button>
                  </div>
                )}
              </div>

              <div className={styles.column}>
                <Group title={t('merchantPanel.employee.form.photo')}>
                  <Field.Upload
                    name='image_url'
                    accept={['png', 'jpeg', 'jpg', 'webp']}
                  />
                </Group>

                <div className={styles.photoDescription}>
                  {t('common.uploadFile.validFileDescription')}
                </div>

                {/* <FormError
                  visible={isRightColumnErrorVisible}
                  content={rightColumnError}
                /> */}
              </div>

              {/* <ServicesSelect /> */}
            </div>
          </div>
        </Form>
      </Fragment>
    );
  }
);

export { Employee };
