import { Employee, Language } from '@api';
import {
  useClickOutside,
  useMeta,
  useMultiLanguage,
  useMultiLanguageMapper
} from '@core';
import {
  getEmployee,
  getEmployeeProducts,
  saveEmployee
} from '@employees/store';
import { State } from '@store';
import { navigate } from '@store/router';
import { useFormik } from 'formik';
import { useEffect, useRef } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useRouteMatch } from 'react-router-dom';
import { lazy, object, string } from 'yup';
import { useTranslation } from 'react-i18next';
import i18next from 'i18next';
import { useSchemaTranslationsUpdate } from '@app/hooks';

const defaultValues = {} as Employee;
const phoneRegExp = /^((\+[1-9]{1,4}[ -]?)|(\([0-9]{2,3}\)[ -]?)|([0-9]{2,4})[ -]?)*?[0-9]{3,4}[ -]?[0-9]{3,4}$/;

/**
 * Validation schema
 */
const schema = () =>
  object({
    name: string()
      .nullable(true)
      .required('merchantPanel.employee.form.employeeNameRequired')
      .max(40, 'merchantPanel.employee.form.errors.employeeNameTooLong'),
    email: string()
      .required('merchantPanel.employee.form.emailRequired')
      .email('merchantPanel.employee.form.errors.employeeEmailNotValid'),
    phone: string()
      .required('merchantPanel.employee.form.phoneRequired')
      .matches(
        phoneRegExp,
        'merchantPanel.employee.form.errors.phoneNumberNotValid'
      )
      .matches(
        /^\+/,
        'merchantPanel.employee.form.errors.phoneNumberShouldStartWithPlus'
      )
      .max(15, 'merchantPanel.employee.form.errors.phoneNumberTooLong')
      .nullable(true),

    image_url: string()
      .required('merchantPanel.employee.form.bannerRequired')
      .nullable(true)
  });

/**
 * <Employee /> props
 */
const useEmployeeProps = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();

  const {
    params: { id, language }
  } = useRouteMatch<{ id: string; language: Language }>();

  const formRef = useRef();

  const [lang, setLang, ml] = useMultiLanguage(language);
  const uml = useMultiLanguageMapper(lang);

  const { employee, products, error } = useSelector(
    (state: State) => state.merchant.employees
  );

  const isNew = !Boolean(id);

  const form = useFormik({
    onSubmit: (values, { setSubmitting }) => {
      dispatch(saveEmployee(values, () => setSubmitting(false)));
    },
    validationSchema: lazy(schema),
    enableReinitialize: true,
    initialValues: employee || defaultValues
  });

  useSchemaTranslationsUpdate(form);

  const { values, errors } = form;

  const canAddService =
    products.length > 0 && (values?.products?.length || 0) != products.length;

  const isRightColumnErrorVisible = Boolean(errors.image_url);
  const rightColumnError = errors.image_url;
  const isInfoErrorVisible = Boolean(
    errors.name || errors.email || errors.phone
  );
  const infoError = errors.name || errors.email || errors.phone;

  const onCancelClick = () => {
    dispatch(navigate('/employee'));
  };

  const onAddServiceClick = () => {
    const { products } = form.values;

    form.setFieldValue('products', products ? [...products, null] : [null]);
  };

  const onRemoveClick = (position: number) => () => {
    form.setFieldValue(
      'products',
      values.products.filter((_, index) => index != position)
    );
  };

  const filterByAvailability = (id: number, options: typeof products) =>
    options.filter(option => {
      if (option.id == id) return true;

      if (!values?.products?.length) return true;

      return values.products.every(one => one != option.id);
    });

  const onAddAllServicesClick = () => {
    const { products: current = [] } = form.values;
    const rest = products
      .filter(product => current.every(one => one != product.id))
      .map(item => item.id);

    form.setFieldValue('products', [...current, ...rest]);
  };

  useClickOutside(formRef, () => {
    if (!id) return;

    dispatch(navigate('/employee'));
  });

  useEffect(() => {
    dispatch(getEmployeeProducts());
    dispatch(getEmployee(id ? Number(id) : null));
  }, []);

  useMeta(
    {
      title: isNew
        ? t('merchantPanel.employee.add.meta.title')
        : t('merchantPanel.employee.edit.meta.title')
    },
    []
  );

  return {
    t,
    ml,
    uml,
    lang,
    form,
    isNew,
    formRef,
    canAddService,
    isRightColumnErrorVisible,
    rightColumnError,
    isInfoErrorVisible,
    infoError,
    onRemoveClick,
    onCancelClick,
    onAddServiceClick,
    filterByAvailability,
    onAddAllServicesClick,
    productOptions: products,
    onLanguageChange: setLang,
    error
  };
};

export { useEmployeeProps };
