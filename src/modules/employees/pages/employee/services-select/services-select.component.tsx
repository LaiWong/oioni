import * as React from 'react';
import { ServicesSelectProps } from './services-select.props';
import * as styles from './services-select.scss';
import { Checkbox } from '@core';

/**
 * Renders ServicesSelect
 */
const ServicesSelect: React.FC<ServicesSelectProps> = ({}) => (
  <div className={styles.servicesSelect}>
    <div className={styles.header}>
      <h2 className={styles.title}>Services</h2>

      <div className={styles.description}>Select services for a specialist</div>
    </div>

    <div className={styles.servicesGroup}>
      <h2 className={styles.groupName}>Woman Haircut</h2>

      <div className={styles.checkboxesWrapper}>
        <Checkbox
          className={styles.checkbox}
          value={false}
          label='Select all'
        />
        <Checkbox className={styles.checkbox} value label='HG Haircut' />
        <Checkbox
          className={styles.checkbox}
          value={false}
          label='Hot scissor cut'
        />
        <Checkbox
          className={styles.checkbox}
          value={false}
          label='EVA Professional ritualities haicare'
        />
        <Checkbox
          className={styles.checkbox}
          value={false}
          label='Hot scissors cut'
        />
        <Checkbox
          className={styles.checkbox}
          value={false}
          label='Regular Haircut'
        />
        <Checkbox
          className={styles.checkbox}
          value={false}
          label='Hair color'
        />
      </div>
    </div>

    <div className={styles.servicesGroup}>
      <h2 className={styles.groupName}>Barber services</h2>

      <div className={styles.checkboxesWrapper}>
        <Checkbox
          className={styles.checkbox}
          value={false}
          label='Select all'
        />
        <Checkbox className={styles.checkbox} value label='Beard cut' />
        <Checkbox className={styles.checkbox} value={false} label='Styling' />
        <Checkbox
          className={styles.checkbox}
          value={false}
          label='Beard and mustache trims'
        />
        <Checkbox className={styles.checkbox} value={false} label='Shaving' />
        <Checkbox
          className={styles.checkbox}
          value={false}
          label='Straight razor shave'
        />
        <Checkbox
          className={styles.checkbox}
          value={false}
          label='Hair coloring'
        />
      </div>
    </div>

    <div className={styles.servicesGroup}>
      <h2 className={styles.groupName}>Face massage</h2>

      <div className={styles.checkboxesWrapper}>
        <Checkbox
          className={styles.checkbox}
          value={false}
          label='Select all'
        />
        <Checkbox className={styles.checkbox} value label='Beard cut' />
        <Checkbox className={styles.checkbox} value={false} label='Styling' />
        <Checkbox
          className={styles.checkbox}
          value={false}
          label='Beard and mustache trims'
        />
        <Checkbox className={styles.checkbox} value={false} label='Shaving' />
        <Checkbox
          className={styles.checkbox}
          value={false}
          label='Straight razor shave'
        />
        <Checkbox
          className={styles.checkbox}
          value={false}
          label='Hair coloring'
        />
      </div>
    </div>
  </div>
);

export { ServicesSelect };
