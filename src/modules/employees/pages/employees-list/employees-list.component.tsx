import * as React from 'react';
import { useEmployeesList } from './employees-list.props';
import * as styles from './employees-list.scss';
import {
  Body,
  Button,
  Cell,
  Confirm,
  Head,
  hoc,
  Pagination,
  Row,
  Search,
  Table
} from '@core';

/**
 * Renders EmployeesList
 */
const EmployeesList = hoc(
  useEmployeesList,
  ({
    t,
    query,
    toDelete,
    employees,
    paginationLength,
    onAddClick,
    onEditClick,
    onQueryChange,
    onCloseDelete,
    onDeleteClick,
    onConfirmDeleteClick,
    onAddWorktimeClick,
    onPageChange
  }) => (
    <div className={styles.employeesList}>
      <div className={styles.header}>
        <h2 className={styles.title}>
          {t('merchantPanel.employee.list.title')}
        </h2>
        <Search
          value={query}
          onChange={onQueryChange}
          className={styles.search}
          placeholder={t('merchantPanel.employee.list.searchPlaceholder')}
        />
        <Button className={styles.button} theme='primary' onClick={onAddClick}>
          {t('merchantPanel.employee.list.add')}
        </Button>
      </div>
      <Table>
        <Head>
          <Row>
            <Cell>{t('merchantPanel.employee.list.name')}</Cell>
            <Cell>{t('merchantPanel.employee.list.bookingsCount')}</Cell>
            <Cell>{t('merchantPanel.employee.list.servicesCount')}</Cell>
            <Cell />
          </Row>
        </Head>
        <Body>
          {employees.map(employee => (
            <Row key={employee.id}>
              <Cell>
                <div className={styles.stylist}>
                  <img className={styles.avatar} src={employee.image_url} />
                  <div className={styles.name}>{employee.name} </div>
                </div>
              </Cell>
              <Cell>{employee.appointmentsInQue}</Cell>
              <Cell>{employee.supportingServices}</Cell>
              <Cell>
                <div className={styles.actions}>
                  <Button
                    size='xs'
                    theme='teritary'
                    onClick={() => onAddWorktimeClick(employee)}
                  >
                    {t('merchantPanel.employee.list.worktime')}
                  </Button>
                  <Button
                    size='xs'
                    theme='teritary'
                    onClick={() => onEditClick(employee)}
                  >
                    {t('merchantPanel.employee.list.edit')}
                  </Button>
                  <Button
                    size='xs'
                    theme='teritary'
                    onClick={() => onDeleteClick(employee.id)}
                  >
                    {t('merchantPanel.employee.list.delete')}
                  </Button>
                </div>
              </Cell>
            </Row>
          ))}
        </Body>
      </Table>
      <Pagination length={paginationLength} onPageChange={onPageChange} />
      {toDelete && (
        <Confirm onClose={onCloseDelete}>
          <div className={styles.deleteDisclaimer}>
            <img
              className={styles.deleteDisclaimerLogo}
              src={require('img/compact-logo.svg')}
            />
            <div className={styles.deleteDisclaimerTitle}>
              {t('merchantPanel.employee.deleteDisclaimer.titile')}
            </div>
            <div className={styles.deleteDisclaimerText}>
              {t('merchantPanel.employee.deleteDisclaimer.text')}
            </div>
            <div className={styles.deleteDisclaimerControls}>
              <Button size='md' onClick={onConfirmDeleteClick}>
                {t('merchantPanel.employee.deleteDisclaimer.delete')}
              </Button>
              <Button size='md' theme='teritary' onClick={onCloseDelete}>
                {t('merchantPanel.employee.deleteDisclaimer.cancel')}
              </Button>
            </div>
          </div>
        </Confirm>
      )}
    </div>
  )
);

export { EmployeesList };
