import { Employee } from '@api';
import { useMeta, usePagination } from '@core';
import { deleteEmployee, getEmployees } from '@employees/store';
import { State } from '@store';
import { navigate } from '@store/router';
import { useEffect, useRef, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';

/**
 * Use employees list
 */
const useEmployeesList = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const { paginationLength, employees } = useSelector(
    (state: State) => state.merchant.employees
  );

  const { selectedPage, onPageChange } = usePagination();

  const [query, setQuery] = useState('');
  const [toDelete, setToDelete] = useState<Employee['id']>();
  const interval = useRef<any>();

  const onAddClick = () => {
    dispatch(navigate('/employee/add'));
  };

  const onEditClick = (employee: Employee) => {
    dispatch(navigate('/employee/edit/' + employee.id));
  };
  const onAddWorktimeClick = (employee: Employee) => {
    dispatch(navigate('/employee/worktime/' + employee.id));
  };

  const onQueryChange = (search: string) => {
    setQuery(search);

    clearTimeout(interval.current);

    interval.current = setTimeout(() => {
      dispatch(getEmployees({ search }));
    }, 300);
  };

  const onDeleteClick = (employee: Employee['id']) => {
    setToDelete(employee);
  };

  const onCloseDelete = () => {
    setToDelete(null);
  };

  const onConfirmDeleteClick = () => {
    dispatch(deleteEmployee(toDelete));

    setToDelete(null);
  };

  useEffect(() => {
    dispatch(getEmployees({ page: selectedPage }));
  }, [selectedPage]);

  useMeta({ title: 'Employees - Oioni Administration Console' }, []);

  return {
    t,
    query,
    toDelete,
    employees,
    paginationLength,
    onAddClick,
    onEditClick,
    onCloseDelete,
    onQueryChange,
    onDeleteClick,
    onConfirmDeleteClick,

    onAddWorktimeClick,
    onPageChange
  };
};

export { useEmployeesList };
