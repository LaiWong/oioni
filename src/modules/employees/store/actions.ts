import { make } from 'redux-chill';
import { Employee } from '@api';
import { EmployeesState } from './state';

/**
 * Get employee
 */
const getEmployee = make('[employees] get employee')
  .stage((id?: number) => id)
  .stage('success', (employee: Employee) => employee)
  .stage('finish');

/**
 * Save employee
 */
const saveEmployee = make('[employee] save')
  .stage((data: Employee, callback) => ({
    data,
    callback
  }))
  .stage('success')
  .stage('failure', (error: string) => error)
  .stage('finish');

/**
 * Get employees
 */
const getEmployees = make('[employee] get')
  .stage((params: { search?: string; page?: number } = {}) => params)
  .stage(
    'success',
    (payload: { totalPages: number; employees: Employee[] }) => payload
  )
  .stage('finish');

/**
 * Get employee products
 */
const getEmployeeProducts = make('[employee] get products')
  .stage('success', (products: EmployeesState['products']) => products)
  .stage('failure');

/**
 * Delete empl
 */
const deleteEmployee = make('[employee] delete').stage((id: number) => ({
  id
}));

export {
  getEmployee,
  saveEmployee,
  getEmployees,
  deleteEmployee,
  getEmployeeProducts
};
