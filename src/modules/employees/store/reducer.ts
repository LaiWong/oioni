import { reducer } from 'redux-chill';
import { EmployeesState } from './state';
import {
  getEmployee,
  getEmployeeProducts,
  getEmployees,
  saveEmployee
} from './actions';

/**
 * General state
 */
const employees = reducer(new EmployeesState())
  .on(getEmployee.success, (state, employee) => {
    state.employee = employee;
  })
  .on(getEmployees.success, (state, { employees, totalPages }) => {
    state.employees = employees;
    state.paginationLength = totalPages;
  })
  .on(getEmployeeProducts.success, (state, products) => {
    state.products = products;
  })
  .on(saveEmployee.failure, (state, error) => {
    state.error = error;
  })
  .on([saveEmployee, saveEmployee.success], state => {
    state.error = null;
  });

export { employees };
