import {
  deleteEmployee,
  getEmployee,
  getEmployeeProducts,
  getEmployees,
  saveEmployee
} from './actions';
import { Payload, Saga } from 'redux-chill';
import { StoreContext } from '@store/context';
import { State } from '@store';
import { call, put, select } from 'redux-saga/effects';
import { snack } from '@store/snackbar';
import { Employee } from '@api';
import { navigate } from '@store/router';

class EmployeesSaga {
  private perPageCount = 9;

  /**
   * Get employees list
   */
  @Saga(getEmployees)
  public *getEmployees(
    { page, search }: Payload<typeof getEmployees>,
    { employee }: StoreContext
  ) {
    try {
      const {
        general: { merchantUser }
      }: State = yield select();

      const response = yield call(employee.get, {
        search,
        page,
        perPage: this.perPageCount,
        merchant: merchantUser.id
      });

      yield put(
        getEmployees.success({
          employees: response.data.result.employees,
          totalPages: response.data.result.totalPages
        })
      );
    } catch (error) {
      yield put(snack('message.cannotGetEmployees', 'error'));
    }
  }

  @Saga(getEmployeeProducts)
  public *getEmployeeProducts(_, { product }: StoreContext) {
    try {
      const {
        general: { merchantUser }
      }: State = yield select();

      const response = yield call(product.get, {
        merchant: merchantUser.id
      });

      yield put(getEmployeeProducts.success(response.data.products));
    } catch (error) {
      yield put(getEmployeeProducts.failure());
    }
  }

  /**
   * Get employee
   */
  @Saga(getEmployee)
  public *getEmployee(
    id: Payload<typeof getEmployee>,
    { employee }: StoreContext
  ) {
    try {
      const {
        general: { merchantUser }
      }: State = yield select();

      if (!id) {
        yield put(getEmployee.success({} as Employee));

        return;
      }

      const response = yield call(employee.getOne, merchantUser.id, id);

      yield put(getEmployee.success(response.data.result));
    } catch (error) {
      yield put(snack('message.cannotGetEmployee', 'error'));
    }
  }

  /**
   * Save employee
   */
  @Saga(saveEmployee)
  public *saveEmployee(
    { data, callback }: Payload<typeof saveEmployee>,
    { employee }: StoreContext
  ) {
    try {
      const {
        general: { merchantUser }
      }: State = yield select();

      const { name, phone, products, image_url, email } = data;

      const payload = {
        name,
        phone,
        email,
        products,
        image_url,
        merchant_id: merchantUser.id
      };

      const isEdit = data?.id;

      yield isEdit
        ? call(employee.put, data.id, payload)
        : call(employee.post, payload);

      yield put(snack('message.employeeSaved'));

      if (isEdit) {
        yield put(getEmployees());
      }

      yield put(navigate('/employee'));
    } catch (error) {
      const errors = error.response.data?.message?.errors;

      if (errors && errors[0]?.path === 'email') {
        yield put(saveEmployee.failure('User with this email already exists'));
      } else {
        yield put(saveEmployee.failure('message.somethingWentWrong'));
      }

      yield call(callback);
    }
  }

  /**
   * Delete employee
   */
  @Saga(deleteEmployee)
  public *deleteEmployee(
    { id }: Payload<typeof deleteEmployee>,
    { employee }: StoreContext
  ) {
    try {
      const {
        general: { merchantUser }
      }: State = yield select();

      yield call(employee.delete, id, merchantUser.id);

      yield put(snack('message.employeeDeleted'));

      yield put(getEmployees());
    } catch (error) {
      yield put(snack('message.cannotDeleteEmployee', 'error'));
    }
  }
}

const sagas = [new EmployeesSaga()];

export { EmployeesSaga, sagas };
