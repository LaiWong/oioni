import { Employee } from '@api';

class EmployeesState {
  /**
   * Employees list
   */
  public employees: Employee[] = [];

  /**
   * Pagination length
   */
  public paginationLength: number;

  /**
   * Opened employee
   */
  public employee: Employee = null;

  /**
   * Products to select
   */
  public products: { id: number; name: string }[] = [];

  /**
   * Errors
   */
  public error: string | null;
}

export { EmployeesState };
