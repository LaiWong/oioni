import * as React from 'react';
import * as styles from './copy-icon.scss';
import { CopyIconProps } from './copy-icon.props';

/**
 * Renders Copy icon
 */
const CopyIcon: React.FC<CopyIconProps> = ({ onClick }) => (
  <div className={styles.container}>
    <img onClick={onClick} src={require('img/copy-icon.png')} />
  </div>
);

export { CopyIcon };
