/**
 * Copy icon props
 */
type CopyIconProps = {
  /**
   * On click
   */
  onClick: () => void;
};

export { CopyIconProps };
