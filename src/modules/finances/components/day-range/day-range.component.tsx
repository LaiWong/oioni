import React from 'react';
import { useDayRangeProps } from './day-range.props';
import { Button, Field, Form, hoc } from '@core';
import styles from './day-range.scss';
import arrowLeft from '@worktimes/static/worktime-arrowLeft.svg';
import arrowRight from '@worktimes/static/worktime-arrowRight.svg';
import classNames from 'classnames';
import { TimeRangeTypes } from '@api';
import moment from 'moment';

/**
 * Renders Day Range
 */
const DayRange = hoc(
  useDayRangeProps,
  ({
    t,
    form,
    divRef,
    selectedMonth,
    monthDays,
    weekDays,
    timeRanges,
    onNextMonthClick,
    onPrevMonthClick,
    getDayOfDate,
    onDayClick,
    selectedTimeRange,
    setTimeRange,
    onCancelClick,
    onSaveClick,
    checkIsCurrentMonthsDay,
    checkIsSelectedDay,
    checkIsBetweenSelectedDay,
    checkIsStartOfSelectedDay,
    checkIsEndOfSelectedDay,
    checkBothSelectedDayIsOneDay
  }) => (
    <div className={styles.timeSelector} ref={divRef}>
      <div className={styles.periodContainer}>
        <div className={styles.periods}>
          {timeRanges?.map(({ label, type }, index) => (
            <div
              key={index}
              className={styles.period}
              onClick={() => setTimeRange(type)}
            >
              <div className={styles.timeRange}>
                <div
                  className={classNames({
                    [styles.timeRangeSelected]: type === selectedTimeRange
                  })}
                />
              </div>
              <div className={styles.periodLabel}>{label}</div>
            </div>
          ))}
        </div>
        <div className={styles.selectPeriod}>
          <div
            className={styles.period}
            onClick={() => setTimeRange(TimeRangeTypes.selectPeriod)}
          >
            <div className={styles.timeRange}>
              <div
                className={classNames({
                  [styles.timeRangeSelected]:
                    TimeRangeTypes.selectPeriod === selectedTimeRange
                })}
              />
            </div>
            <div className={styles.periodLabel}>
              {t('superAdmin.finances.timeSelector.selectPeriod')}
            </div>
            <img src={arrowRight} alt={'arrow right'} />
          </div>
        </div>
      </div>
      <div className={styles.container}>
        <div>
          <h4 className={styles.formTitle}>
            {t('superAdmin.finances.timeSelector.selectedPeriod')}
          </h4>
          <Form form={form} className={styles.form}>
            <Field.Input
              className={styles.inputContainer}
              name={'start'}
              disabled
            />
            <p>&#8212;</p>
            <Field.Input
              className={styles.inputContainer}
              name={'end'}
              disabled
            />
          </Form>
        </div>
        <div className={styles.header}>
          <div onClick={onPrevMonthClick}>
            <img className={styles.icon} src={arrowLeft} alt={'arrow left'} />
          </div>
          <p>
            {t(`common.months.${moment(selectedMonth).format('M')}`)}{' '}
            {moment(selectedMonth).format('YYYY')}
          </p>
          <div onClick={onNextMonthClick}>
            <img className={styles.icon} src={arrowRight} alt={'arrow right'} />
          </div>
        </div>
        <div className={styles.weekDays}>
          {weekDays?.map((day, index) => (
            <p key={index} className={styles.weekDaysWeek}>
              {day}
            </p>
          ))}
          {monthDays?.map(({ day, month }, index) => (
            <p
              key={index}
              className={classNames(styles.weekDaysDay, {
                [styles.weekDaysDayNotThisMonth]: checkIsCurrentMonthsDay(
                  month
                ),
                [styles.weekDaysDaySelected]: checkIsSelectedDay(day, month),
                [styles.weekDaysDayBetweenDays]: checkIsBetweenSelectedDay(
                  day,
                  month
                ),
                [styles.weekDaysDaySelectedWithBorderStart]: checkIsStartOfSelectedDay(
                  day,
                  month
                ),
                [styles.weekDaysDaySelectedWithBorderEnd]: checkIsEndOfSelectedDay(
                  day,
                  month
                ),
                [styles.weekDaysDaySelectedWithBorderBoth]: checkBothSelectedDayIsOneDay(
                  day,
                  month
                )
              })}
              style={{
                gridColumn: `${
                  getDayOfDate(day, month) + 1 === 8
                    ? 1
                    : getDayOfDate(day, month) + 1
                }`
              }}
              onClick={() => onDayClick({ day, month })}
            >
              {day}
            </p>
          ))}
        </div>
        <div className={styles.buttonsContainer}>
          <div className={styles.buttons}>
            <Button
              onClick={onCancelClick}
              className={styles.button}
              theme={'teritary'}
            >
              {t('superAdmin.finances.timeSelector.button.cancel')}
            </Button>
            <Button
              onClick={onSaveClick}
              className={styles.button}
              theme={'primary'}
            >
              {t('superAdmin.finances.timeSelector.button.save')}
            </Button>
          </div>
        </div>
      </div>
    </div>
  )
);

export { DayRange };
