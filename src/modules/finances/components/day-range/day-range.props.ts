import { useTranslation } from 'react-i18next';
import React, {
  ReactElement,
  useEffect,
  useMemo,
  useRef,
  useState
} from 'react';
import moment from 'moment';
import { useFormik } from 'formik';
import { useDispatch, useSelector } from 'react-redux';
import { TimeRangeTypes } from '@api';
import {
  setSelectedStartAndEndDate,
  setSelectedTimeRange
} from '@finances/store';
import { State } from '@store';
import { useTimeRangeTypes } from './hooks';
import { snack } from '@store/snackbar';
import { useClickOutside } from '@core';

type DayRangeProps = {
  onClose: () => void;
};

const monthFormat = 'MMMM YYYY';
const fullFormat = 'D MMMM YYYY';
const inputFormat = 'DD.MM.YYYY';

/**
 * Use day range props
 */
const useDayRangeProps = ({ onClose }: DayRangeProps) => {
  const {
    t,
    i18n: { language }
  } = useTranslation();
  const dispatch = useDispatch();

  const { selectedStartAndEndDate, selectedTimeRange } = useSelector(
    (state: State) => state.finances
  );

  const divRef = useRef();

  const [toggle, setToggle] = useState(false);
  const [selectedMonth, setSelectedMonth] = useState(
    moment().format(monthFormat)
  );
  const [selectedDates, setSelectedDates] = useState({
    start: selectedStartAndEndDate?.start,
    end: selectedStartAndEndDate?.end
  });

  const form = useFormik({
    initialValues: {
      start: null,
      end: null
    },
    onSubmit: () => {}
  });

  const getCurrentDate = (date: number | string, month: string) =>
    `${date} ${month}`;

  const getMonthDays = (days: number, month: string) =>
    Array.from(Array(days), (_, i) => ({ day: i + 1, month }));

  const changeMonth = (month: string, count: number, isAdd: boolean) =>
    isAdd
      ? moment(month)
          .add(count, 'month')
          .format(monthFormat)
      : moment(month)
          .subtract(count, 'month')
          .format(monthFormat);

  const getDayOfDate = (date, month) =>
    moment(getCurrentDate(date, month), fullFormat).isoWeekday();

  const monthDays = useMemo(() => {
    const prevMonthDaysCount = moment(
      changeMonth(selectedMonth, 1, false)
    ).daysInMonth();
    const prevMonth = changeMonth(selectedMonth, 1, false);
    const prevMonthDays = getMonthDays(prevMonthDaysCount, prevMonth);

    const nextMonthDaysCount = moment(
      changeMonth(selectedMonth, 1, true)
    ).daysInMonth();
    const nextMonth = changeMonth(selectedMonth, 1, true);
    const nextMonthDays = getMonthDays(nextMonthDaysCount, nextMonth);

    const monthDaysCount = moment(selectedMonth).daysInMonth();
    const monthDays = getMonthDays(monthDaysCount, selectedMonth);

    const firstDayOfMonth = getDayOfDate(monthDays[0].day, selectedMonth);
    const lastDayOfMonth = getDayOfDate(
      monthDays[monthDays.length - 1].day,
      selectedMonth
    );

    if (firstDayOfMonth !== 7) {
      monthDays.unshift(
        ...prevMonthDays.filter(
          (value, index) => index >= prevMonthDays.length - firstDayOfMonth
        )
      );
    }

    if (lastDayOfMonth !== 6) {
      monthDays.push(
        ...nextMonthDays.filter((value, index) => index < 6 - lastDayOfMonth)
      );
    }

    if (lastDayOfMonth === 7) {
      monthDays.push(...nextMonthDays.filter((value, index) => index < 6));
    }

    return monthDays;
  }, [selectedMonth]);

  const weekDays = useMemo(() => {
    const weekdaysNumbers = [7, 1, 2, 3, 4, 5, 6];

    return weekdaysNumbers.map(value => t(`common.shortWeekdays.${value}`));
  }, [language]);

  const timeRanges = useTimeRangeTypes();

  const setTimeRange = (type: TimeRangeTypes) => {
    dispatch(setSelectedTimeRange(type));
  };

  const onNextMonthClick = () => {
    setSelectedMonth(changeMonth(selectedMonth, 1, true));
  };

  const onPrevMonthClick = () => {
    setSelectedMonth(changeMonth(selectedMonth, 1, false));
  };

  const checkIsBefore = (dateToCheck, date) =>
    date ? moment(dateToCheck).isBefore(date) : true;

  const onDayClick = ({ day, month }: { day: number; month: string }) => {
    if (month !== selectedMonth) return;

    setTimeRange(TimeRangeTypes.selectPeriod);

    const { start, end } = selectedDates;

    const currentDate = getCurrentDate(day, month);

    switch (true) {
      case checkIsBefore(currentDate, start): {
        setSelectedDates({ start: currentDate, end });
        break;
      }
      case !checkIsBefore(currentDate, end): {
        setSelectedDates({
          start,
          end: currentDate
        });
        break;
      }
      case !checkIsBefore(currentDate, start) &&
        checkIsBefore(currentDate, end): {
        if (!toggle) {
          setSelectedDates({
            start,
            end: currentDate
          });
        } else {
          setSelectedDates({
            start: currentDate,
            end
          });
        }

        setToggle(!toggle);
        break;
      }
    }
  };

  const onCancelClick = () => {
    onClose();

    dispatch(setSelectedStartAndEndDate({ start: null, end: null }));
    setTimeRange(TimeRangeTypes.allTime);
  };

  const onSaveClick = () => {
    onClose();

    dispatch(setSelectedStartAndEndDate(selectedDates));
    dispatch(snack('message.saved'));
  };

  const onTimeRangeChange = () => {
    switch (selectedTimeRange) {
      case TimeRangeTypes.allTime: {
        setSelectedDates({ start: null, end: null });
        setSelectedMonth(moment().format(monthFormat));
        break;
      }
      case TimeRangeTypes.currentDay: {
        const today = moment().format(fullFormat);
        setSelectedMonth(moment().format(monthFormat));

        setSelectedDates({
          start: today,
          end: today
        });
        break;
      }
      case TimeRangeTypes.previousDay: {
        const previousDay = moment()
          .subtract(1, 'days')
          .format(fullFormat);
        const month = moment(previousDay).format(monthFormat);

        if (month !== selectedMonth) {
          setSelectedMonth(changeMonth(selectedMonth, 1, false));
        }

        setSelectedDates({
          start: previousDay,
          end: previousDay
        });
        break;
      }
      case TimeRangeTypes.currentWeek: {
        const today = moment().format('D');
        const month = moment().format(monthFormat);
        const dayOfWeek = getDayOfDate(today, month);
        const startOfWeek = moment(getCurrentDate(today, month), fullFormat)
          .subtract(dayOfWeek - 1, 'days')
          .format(fullFormat);
        const endOfWeek = moment(startOfWeek, fullFormat)
          .add('6', 'days')
          .format(fullFormat);

        setSelectedDates({ start: startOfWeek, end: endOfWeek });
        setSelectedMonth(moment().format(monthFormat));
        break;
      }
      case TimeRangeTypes.currentMonth: {
        const month = moment().format(monthFormat);
        const monthDaysCount = moment(month, monthFormat).daysInMonth();
        const start = getCurrentDate(1, month);
        const end = getCurrentDate(monthDaysCount, month);

        setSelectedDates({ start, end });
        setSelectedMonth(moment().format(monthFormat));
        break;
      }
      case TimeRangeTypes.previousMonth: {
        const month = moment()
          .subtract(1, 'months')
          .format(monthFormat);
        const monthDaysCount = moment(month, monthFormat).daysInMonth();
        const start = getCurrentDate(1, month);
        const end = getCurrentDate(monthDaysCount, month);

        setSelectedDates({ start, end });
        setSelectedMonth(moment(month).format(monthFormat));
        break;
      }
      case TimeRangeTypes.currentQuarter: {
        const startMonthOfQuarter = moment()
          .startOf('quarter')
          .format(monthFormat);
        const endMonthOfQuarter = moment()
          .endOf('quarter')
          .format(monthFormat);
        const daysCountOfEndMonthOfQuarter = moment(
          endMonthOfQuarter,
          monthFormat
        ).daysInMonth();
        const start = getCurrentDate(1, startMonthOfQuarter);
        const end = getCurrentDate(
          daysCountOfEndMonthOfQuarter,
          endMonthOfQuarter
        );

        setSelectedDates({ start, end });
        setSelectedMonth(startMonthOfQuarter);
        break;
      }
    }
  };

  useClickOutside(divRef, onClose);

  useEffect(() => {
    const { start, end } = selectedDates;

    const getDate = (date: string) =>
      date ? moment(date).format(inputFormat) : '';

    form.setFieldValue('start', getDate(start));
    form.setFieldValue('end', getDate(end));
  }, [selectedDates]);

  useEffect(() => {
    onTimeRangeChange();
  }, [selectedTimeRange]);

  const { start, end } = selectedDates;
  const checkIsCurrentMonthsDay = (month: string) => month !== selectedMonth;
  const checkIsSelectedDay = (day: number, month: string) => {
    const currentDate = getCurrentDate(day, month);
    return start === currentDate || end === currentDate;
  };
  const checkIsBetweenSelectedDay = (day: number, month: string) => {
    const currentDate = getCurrentDate(day, month);
    return (
      start &&
      start !== currentDate &&
      end &&
      end !== currentDate &&
      checkIsBefore(currentDate, end) &&
      !checkIsBefore(currentDate, start)
    );
  };
  const checkIsStartOfSelectedDay = (day: number, month: string) =>
    start && end && start === getCurrentDate(day, month);
  const checkIsEndOfSelectedDay = (day: number, month: string) =>
    start && end && end === getCurrentDate(day, month);
  const checkBothSelectedDayIsOneDay = (day: number, month: string) =>
    start && end && start === `${day} ${month}` && end === `${day} ${month}`;

  return {
    t,
    form,
    divRef,
    selectedMonth,
    selectedDates,
    timeRanges,
    weekDays,
    monthDays,
    onNextMonthClick,
    onPrevMonthClick,
    getDayOfDate,
    onDayClick,
    checkIsBefore,
    selectedTimeRange,
    setTimeRange,
    onCancelClick,
    onSaveClick,
    checkIsCurrentMonthsDay,
    checkIsSelectedDay,
    checkIsBetweenSelectedDay,
    checkIsStartOfSelectedDay,
    checkIsEndOfSelectedDay,
    checkBothSelectedDayIsOneDay
  };
};

export { useDayRangeProps };
