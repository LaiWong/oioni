import { TimeRangeTypes } from '@api';
import { useTranslation } from 'react-i18next';

/**
 * Hook that returns all type of time range
 */
const useTimeRangeTypes = (): { label: string; type: TimeRangeTypes }[] => {
  const { t } = useTranslation();

  return [
    {
      label: t('superAdmin.finances.timeSelector.allTime'),
      type: TimeRangeTypes.allTime
    },
    {
      label: t('superAdmin.finances.timeSelector.currentDay'),
      type: TimeRangeTypes.currentDay
    },
    {
      label: t('superAdmin.finances.timeSelector.previousDay'),
      type: TimeRangeTypes.previousDay
    },
    {
      label: t('superAdmin.finances.timeSelector.currentWeek'),
      type: TimeRangeTypes.currentWeek
    },
    {
      label: t('superAdmin.finances.timeSelector.currentMonth'),
      type: TimeRangeTypes.currentMonth
    },
    {
      label: t('superAdmin.finances.timeSelector.previousMonth'),
      type: TimeRangeTypes.previousMonth
    },
    {
      label: t('superAdmin.finances.timeSelector.currentQuarter'),
      type: TimeRangeTypes.currentQuarter
    }
  ];
};

export { useTimeRangeTypes };
