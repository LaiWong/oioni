import * as React from 'react';
import { useFinancesHeaderProps } from './finances-header.props';
import * as styles from './finances-header.scss';
import { Button, formatPrice, hoc } from '@core';
import classNames from 'classnames';
import { DayRange } from '../day-range';
import moment from 'moment';

/**
 * Renders Finances Header
 */
const FinancesHeader = hoc(
  useFinancesHeaderProps,
  ({
    t,
    language,
    revenue,
    pending,
    needToPay,
    paidOut,
    isGlobalMerchants,
    isGlobalHistory,
    onTimeSelectorClick,
    isTimeSelectorOpen,
    selected,
    lastUpdateTime,
    setIsTimeSelectorOpen
  }) => (
    <div>
      <div className={styles.financesHeader}>
        <h2>
          {t('superAdmin.finances.header.title')}{' '}
          <span className={styles.greyText}>
            Updated {moment(lastUpdateTime).format('MMMM DD, YYYY, HH:mm')}
          </span>
        </h2>

        <div className={styles.dateSelectorContainer}>
          <h3>{t('superAdmin.finances.header.select.title')}</h3>
          <div
            className={styles.dateSelector}
            onClick={e => {
              e.persist();
              e.nativeEvent.stopImmediatePropagation();
              e.stopPropagation();
              setIsTimeSelectorOpen(true);
            }}
          >
            <p>{selected?.label?.toUpperCase() || 'Selected Period'}</p>
            <div className={styles.arrowBottomContainer}>
              <img src={require('img/arrow-bottom.png')} alt='Arrow bottom' />
            </div>
            {isTimeSelectorOpen && <DayRange onClose={onTimeSelectorClick} />}
          </div>
        </div>
      </div>
      <div className={styles.header}>
        <div className={classNames(styles.info, styles.infoWithBorder)}>
          <h4>
            {isGlobalMerchants || isGlobalHistory ? 'Oioni' : ''}{' '}
            {t('superAdmin.finances.header.Revenue')}
          </h4>
          <p className={classNames(styles.price, styles.priceBig)}>
            {formatPrice(revenue)}
          </p>
          <Button
            className={classNames(styles.button, 'not-active-link')}
            theme='teritary'
            size='xs'
          >
            {t('superAdmin.finances.header.button.details')}
          </Button>
        </div>
        <div className={styles.infoContainer}>
          <div className={styles.info}>
            <h4>{t('superAdmin.finances.header.needToPay.title')}</h4>
            <p className={styles.price}>{formatPrice(needToPay)}</p>
            <p className={styles.greyText}>
              {t('superAdmin.finances.header.needToPay.description.amount')}
              <br /> {isGlobalMerchants || isGlobalHistory ? 'Oioni' : ''}{' '}
              {t('superAdmin.finances.header.needToPay.description.needtoPay')}
            </p>
          </div>
          <div className={styles.info}>
            <h4>{t('superAdmin.finances.header.pending.title')}</h4>
            <p className={styles.price}>{formatPrice(pending)}</p>
            <p className={styles.greyText}>
              {t('superAdmin.finances.header.merchantAccrued')}
            </p>
          </div>
          <div className={styles.info}>
            <h4>{t('superAdmin.finances.header.totalPaid')}</h4>
            <p className={styles.price}>{formatPrice(paidOut)}</p>
            <p className={styles.greyText}>
              {t(`superAdmin.finances.header.totalAmountDescription`)}
              {(isGlobalMerchants || isGlobalHistory) && language === 'en'
                ? 's'
                : ''}
            </p>
          </div>
        </div>
      </div>
    </div>
  )
);

export { FinancesHeader };
