import { useTranslation } from 'react-i18next';
import { matchPath, useRouteMatch } from 'react-router';
import { useMemo, useRef, useState } from 'react';
import { useSelector } from 'react-redux';
import { State } from '@store';
import { useTimeRangeTypes } from '@finances/components/day-range/hooks';

type FinancesHeaderProps = {
  /**
   * Revenue
   */
  revenue: number;

  /**
   * Need to pay
   */
  needToPay: number;

  /**
   * Services performed
   */
  pending: number;

  /**
   * Unfulfilled services
   */
  paidOut: number;
};

/**
 * Use finances header props
 */
const useFinancesHeaderProps = (props: FinancesHeaderProps) => {
  const {
    t,
    i18n: { language }
  } = useTranslation();
  const match = useRouteMatch<{ language: string }>();

  const { selectedTimeRange, lastUpdateTime } = useSelector(
    (state: State) => state.finances
  );

  const [isTimeSelectorOpen, setIsTimeSelectorOpen] = useState(false);

  const { isGlobalMerchants, isGlobalHistory } = useMemo(
    () => ({
      isGlobalMerchants: !!matchPath(match.url, {
        exact: true,
        path: `/${match.params.language}/finances/merchants`
      }),
      isGlobalHistory: !!matchPath(match.url, {
        exact: true,
        path: `/${match.params.language}/finances/history`
      })
    }),
    [match]
  );

  const timeRanges = useTimeRangeTypes();

  const selected = timeRanges.find(({ type }) => type === selectedTimeRange);

  const onTimeSelectorClick = () => {
    setIsTimeSelectorOpen(!isTimeSelectorOpen);
  };

  return {
    t,
    language,
    isTimeSelectorOpen,
    isGlobalMerchants,
    isGlobalHistory,
    onTimeSelectorClick,
    selected,
    lastUpdateTime,
    setIsTimeSelectorOpen,
    ...props
  };
};

export { useFinancesHeaderProps };
