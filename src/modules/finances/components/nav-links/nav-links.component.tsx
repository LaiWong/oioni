import React from 'react';
import { NavLinksProps } from './nav-links.props';
import * as styles from './nav-links.scss';
import { NavLink } from 'react-router-dom';
import { Button } from '@core';

const NavLinks: React.FC<NavLinksProps> = ({ links }) => (
  <div className={styles.nav}>
    {links?.map(({ text, url }, index) => (
      <NavLink key={index} activeClassName={styles.active} to={url}>
        <Button className={styles.links} theme='teritary' size='md'>
          {text}
        </Button>
      </NavLink>
    ))}
  </div>
);

export { NavLinks };
