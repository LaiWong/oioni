/**
 * Props
 */
type NavLinksProps = {
  /**
   * Links
   */
  links: {
    text: string;
    url: string;
  }[];
};

export { NavLinksProps };
