import * as React from 'react';
import { Fragment } from 'react';
import { NavLink, Redirect, Route, Switch } from 'react-router-dom';
import { useFinancesProps } from './finances.props';
import * as styles from './finances.scss';
import { Button, formatPrice, hoc, Search } from '@core';
import { FinancesHeader, NavLinks } from '@finances/components';
import { History, Merchant, Merchants, Payment } from './pages';

/**
 * Renders Finances
 */
const Finances = hoc(
  useFinancesProps,
  ({
    t,
    match: { path, url },
    links,
    isPayment,
    query,
    onQueryChange,
    onBackClick,
    selectedMerchant,
    financesDetails,
    totalPayment
  }) => (
    <div className={styles.finances}>
      <Route
        path={[`${path}/payment`, `${path}/merchants/:id`]}
        render={() => (
          <div className={styles.backContainer}>
            <div className={styles.back}>
              <img
                onClick={onBackClick}
                src={require('img/arrow-left.png')}
                alt='Back'
              />
              <p>
                {isPayment
                  ? t('superAdmin.merchants.button.paymentPage')
                  : selectedMerchant?.name || 'No name'}
              </p>
            </div>
          </div>
        )}
      />
      <Route
        exact
        path={[`${path}/history`, `${path}/merchants`]}
        render={() => (
          <Fragment>
            <FinancesHeader
              revenue={financesDetails?.revenue}
              needToPay={financesDetails?.need_to_pay}
              paidOut={financesDetails?.paid}
              pending={financesDetails?.pending}
            />
            <div className={styles.financesNav}>
              <NavLinks links={links} />
              <Route
                path={`${path}/merchants`}
                render={() => (
                  <div className={styles.header}>
                    <div>
                      <p>{t('superAdmin.merchants.overallPay')}</p>
                      <h4>{formatPrice(totalPayment)}</h4>
                    </div>
                    <div>
                      <NavLink to={`${url}/payment`}>
                        <Button theme='primary' size='md'>
                          {t('superAdmin.merchants.button.paymentPage')}
                        </Button>
                      </NavLink>
                    </div>
                  </div>
                )}
              />
              <Route
                path={`${path}/history`}
                render={() => (
                  <div className={styles.search}>
                    <Search
                      value={query}
                      placeholder={t('superAdmin.merchants.search')}
                      onChange={onQueryChange}
                    />
                  </div>
                )}
              />
            </div>
          </Fragment>
        )}
      />
      <Switch>
        <Route exact path={`${path}/payment`} component={Payment} />
        <Route exact path={`${path}/merchants`} component={Merchants} />
        <Route
          exact
          path={`${path}/history`}
          render={props => <History {...props} search={query} />}
        />
        <Route path={`${path}/merchants/:id`} component={Merchant} />
        <Redirect to={`${url}/merchants`} />
      </Switch>
    </div>
  )
);

export { Finances };
