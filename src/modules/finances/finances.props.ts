import { useFinancesLinks } from '@finances/hooks';
import { getFinancesDetails } from '@finances/store';
import { State } from '@store';
import { navigate } from '@store/router';
import { useEffect, useMemo, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';
import { matchPath } from 'react-router';
import { RouteChildrenProps } from 'react-router-dom';

/**
 * Use finances props
 */
const useFinancesProps = (props: RouteChildrenProps) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();

  const {
    selectedMerchant,
    financesDetails,
    selectedMerchants,
    selectedStartAndEndDate
  } = useSelector((state: State) => state.finances);

  const [query, setQuery] = useState('');

  const { isPayment } = useMemo(() => {
    const pathname = props.history.location.pathname;
    const url = props.match.url;

    return {
      isPayment: !!matchPath(`${url}/payment`, {
        exact: true,
        path: pathname
      })
    };
  }, [props.history.location.pathname]);

  const links = useFinancesLinks();

  const totalPayment = useMemo(
    () =>
      selectedMerchants.reduce(
        (accQuantity, { need_to_pay }) => accQuantity + need_to_pay,
        0
      ),
    [selectedMerchants]
  );

  const onQueryChange = (query: string) => {
    setQuery(query);
  };

  const onBackClick = () => {
    dispatch(navigate('/finances/merchants'));
  };

  useEffect(() => {
    dispatch(getFinancesDetails(selectedStartAndEndDate));
  }, [selectedStartAndEndDate]);

  return {
    t,
    links,
    query,
    onQueryChange,
    onBackClick,
    selectedMerchant,
    totalPayment,
    financesDetails,
    isPayment,
    ...props
  };
};

export { useFinancesProps };
