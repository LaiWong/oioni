import { useTranslation } from 'react-i18next';
import { useRouteMatch } from 'react-router';

/**
 * Use finances links
 */
const useFinancesLinks = () => {
  const { t } = useTranslation();
  const { url } = useRouteMatch();

  return [
    {
      text: t('superAdmin.finances.navlink.Merchants'),
      url: `${url}/merchants`
    },
    {
      text: t('superAdmin.finances.navlink.History'),
      url: `${url}/history`
    }
  ];
};

export { useFinancesLinks };
