import { useTranslation } from 'react-i18next';
import { useRouteMatch } from 'react-router';

/**
 * Use merchant links
 */
const useMerchantLinks = () => {
  const { t } = useTranslation();
  const { url } = useRouteMatch();

  return [
    {
      text: t('superAdmin.merchant.navlink.Bookings'),
      url: `${url}/bookings`
    },
    {
      text: t('superAdmin.merchant.navlink.History'),
      url: `${url}/history`
    }
  ];
};

export { useMerchantLinks };
