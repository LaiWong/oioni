import { useTranslation } from 'react-i18next';
import { useMemo } from 'react';

/**
 * Titles
 */
const useTitles = (
  // Key is equal to component name
  key
): {
  name: string;
  atEnd?: boolean;
  bold?: boolean;
}[] => {
  const { t } = useTranslation();

  const titles = useMemo(
    () => ({
      merchants: [
        {
          name: t('superAdmin.merchants.table.titles.merchantName')
        },
        {
          name: t('superAdmin.merchants.table.titles.needToPay')
        },
        {
          name: t('superAdmin.merchants.table.titles.pendingServices')
        },
        {
          name: t('superAdmin.merchants.table.titles.paidOut'),
          atEnd: true
        }
      ],
      history: [
        {
          name: t('superAdmin.history.table.titles.merchantName')
        },
        {
          name: t('superAdmin.history.table.titles.lastPayment')
        },
        {
          name: t('superAdmin.history.table.titles.paidOut')
        },
        {
          name: t('superAdmin.history.table.titles.revenue'),
          atEnd: true
        },
        {}
      ],
      payment: [
        {
          name: t('superAdmin.payment.table.titles.name')
        },
        {
          name: t('superAdmin.payment.table.titles.VAT')
        },
        {
          name: t('superAdmin.payment.table.titles.IBAN')
        },
        {
          name: t('superAdmin.payment.table.titles.payment')
        },
        {
          name: t('superAdmin.payment.table.titles.purpose')
        },
        {
          name: t('superAdmin.payment.table.titles.status')
        }
      ],
      merchanthistory: [
        {
          name: t('superAdmin.merchanthistory.table.titles.merchantName')
        },
        {
          name: 'Date'
        },
        {
          name: t('superAdmin.merchanthistory.table.titles.paidOut')
        },
        {}
      ]
    }),
    [t]
  );

  return titles[key.toLowerCase()];
};

export { useTitles };
