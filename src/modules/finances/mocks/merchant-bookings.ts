const bookings = [
  {
    booking_date_time: '14:30 05.03.2020',
    employee_name: 'Valentino Korshenko',
    product_name: 'Depilation',
    status: 1,
    price: '240.99'
  },
  {
    booking_date_time: '14:30 05.03.2020',
    employee_name: 'Valentino Korshenko',
    product_name: 'Depilation',
    status: 0,
    price: '240.99'
  },
  {
    booking_date_time: '14:30 05.03.2020',
    employee_name: 'Valentino Korshenko',
    product_name: 'Depilation',
    status: 0,
    price: '240.99'
  },
  {
    booking_date_time: '14:30 05.03.2020',
    employee_name: 'Valentino Korshenko',
    product_name: 'Depilation',
    status: 1,
    price: '240.99'
  }
];

export { bookings };
