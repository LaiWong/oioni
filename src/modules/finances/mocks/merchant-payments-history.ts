const merchantPaymentsHistory = [
  {
    merchant_name: 'Tuuletuka Salon',
    avatar: '',
    last_payment_price: '1500',
    last_payment_date: '26 September 2021',
    total_balance: '3700',
    total_paid: '125500',
    revenue: '15650'
  },
  {
    merchant_name: 'Tuuletuka Salon',
    avatar: '',
    last_payment_price: '1500',
    last_payment_date: '26 September 2021',
    total_balance: '3700',
    total_paid: '125500',
    revenue: '15650'
  }
];

export { merchantPaymentsHistory };
