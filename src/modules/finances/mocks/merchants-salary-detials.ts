const merchantsSalary = [
  {
    id: 1,
    merchant_name: 'Tuuletuka Salon',
    total_balance: '3700',
    service_unfulfilled: '2900',
    service_performed: '900',
    total_paid: '125500',
    avatar: '',
    vat: '123456789',
    iban: '123456789',
    payment: '450',
    purpose: 'Oioni',
    status: 1
  },
  {
    id: 2,
    merchant_name: 'Tuuletuka Salon',
    total_balance: '3700',
    service_unfulfilled: '2900',
    service_performed: '900',
    total_paid: '125500',
    avatar: '',
    vat: '123456789',
    iban: '123456789',
    payment: '450',
    purpose: 'Oioni',
    status: 0
  },
  {
    id: 3,
    merchant_name: 'Tuuletuka Salon',
    total_balance: '3700',
    service_unfulfilled: '2900',
    service_performed: '900',
    total_paid: '125500',
    avatar: '',
    vat: '123456789',
    iban: '123456789',
    payment: '450',
    purpose: 'Oioni',
    status: 0
  },
  {
    id: 4,
    merchant_name: 'Tuuletuka Salon',
    total_balance: '3700',
    service_unfulfilled: '2900',
    service_performed: '900',
    total_paid: '125500',
    avatar: '',
    vat: '123456789',
    iban: '123456789',
    payment: '450',
    purpose: 'Oioni',
    status: 1
  }
];

export { merchantsSalary };
