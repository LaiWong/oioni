import * as React from 'react';
import { useHistoryProps } from './history.props';
import * as styles from './history.scss';
import {
  Body,
  Cell,
  EmptyPage,
  formatPrice,
  Head,
  hoc,
  Pagination,
  Row,
  Table
} from '@core';
import * as moment from 'moment';
import { UserAvatarAndName } from '@merchant/pages/merchant/components';
import classNames from 'classnames';

/**
 * Renders History
 */
const History = hoc(
  useHistoryProps,
  ({ t, historyTitles, financesHistory, paginationLength, onPageChange }) => (
    <div className={styles.history}>
      <Table>
        <Head>
          <Row>
            {historyTitles?.map((title, index) => (
              <Cell
                className={styles.cell}
                key={index}
                atEnd={title.atEnd}
                bold={title.bold}
              >
                {title.name}
              </Cell>
            ))}
          </Row>
        </Head>
        <Body>
          {financesHistory?.map(
            (
              {
                merchantId,
                merchantAvatar,
                merchantName,
                lastPayment,
                revenue,
                paid
              },
              index
            ) => (
              <Row key={index}>
                <Cell className={styles.cell} bold>
                  <div className={styles.name}>
                    <UserAvatarAndName
                      name={merchantName || 'No name'}
                      imgUrl={merchantAvatar?.image}
                    />
                  </div>
                </Cell>
                <Cell className={styles.cell}>
                  {lastPayment?.date &&
                    `${moment(lastPayment?.date).format('DD MMMM YYYY')}, `}
                  <span className={styles.bold}>
                    {lastPayment?.amount && formatPrice(lastPayment?.amount)}
                  </span>
                </Cell>
                <Cell className={styles.cell}>{formatPrice(paid)}</Cell>
                <Cell className={styles.cell} atEnd bold>
                  {formatPrice(revenue)}
                </Cell>
                <Cell className={styles.cell}>
                  <p
                    className={classNames(styles.threeDots, 'not-active-link')}
                  >
                    &#8942;
                  </p>
                </Cell>
              </Row>
            )
          )}
        </Body>
      </Table>
      {paginationLength ? (
        <Pagination length={paginationLength} onPageChange={onPageChange} />
      ) : (
        <EmptyPage title={t('superAdmin.emptyPage.history')} />
      )}
    </div>
  )
);

export { History };
