import { useTranslation } from 'react-i18next';
import { useTitles } from '../../hooks';
import { useEffect, useRef } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { getHistoryOfPayments } from '@finances/store';
import { State } from '@store';
import { RouteChildrenProps } from 'react-router';
import { usePagination } from '@core';

/**
 * Use history props
 */
const useHistoryProps = ({
  search,
  props
}: {
  search: string;
  props: RouteChildrenProps;
}) => {
  // const search = props.search;
  const { t } = useTranslation();
  const dispatch = useDispatch();

  const {
    financesHistory,
    paginationLength,
    selectedStartAndEndDate
  } = useSelector((state: State) => state.finances);

  const { selectedPage, onPageChange } = usePagination();

  const historyTitles = useTitles('history');

  const interval = useRef<any>();

  useEffect(() => {
    clearTimeout(interval.current);
    interval.current = setTimeout(() => {
      dispatch(
        getHistoryOfPayments({
          search,
          selectedPage,
          selectedDates: selectedStartAndEndDate
        })
      );
    }, 300);
  }, [selectedPage, selectedStartAndEndDate, search]);

  return {
    t,
    historyTitles,
    financesHistory,
    paginationLength,
    onPageChange
  };
};

export { useHistoryProps };
