import * as React from 'react';
import { useMerchantProps } from './merchant.props';
import * as styles from './merchant.scss';
import { hoc, Search } from '@core';
import { FinancesHeader } from '@finances/components';
import { NavLinks } from '@finances/components/nav-links';
import { Redirect, Route, Switch } from 'react-router-dom';
import { Bookings, History } from './pages';

/**
 * Renders Merchant
 */
const Merchant = hoc(
  useMerchantProps,
  ({
    match: { path, url },
    links,
    query,
    onQueryChange,
    isBookings,
    merchantFinanceDetails
  }) => (
    <div>
      <FinancesHeader
        revenue={merchantFinanceDetails?.revenue}
        needToPay={merchantFinanceDetails?.need_to_pay}
        paidOut={merchantFinanceDetails?.paid}
        pending={merchantFinanceDetails?.pending}
      />
      <div className={styles.merchantNav}>
        <NavLinks links={links} />
        <div className={styles.search}>
          <Search
            value={query}
            placeholder={`Search${isBookings ? '' : ' service'}`}
            onChange={onQueryChange}
          />
        </div>
      </div>
      <Switch>
        <Route path={`${path}/bookings`} component={Bookings} />
        <Route path={`${path}/history`} component={History} />
        <Redirect to={`${url}/bookings`} />
      </Switch>
    </div>
  )
);

export { Merchant };
