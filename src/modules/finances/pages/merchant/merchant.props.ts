import { useTranslation } from 'react-i18next';
import { RouteChildrenProps } from 'react-router-dom';
import { useMerchantLinks } from '@finances/hooks';
import { useEffect, useMemo, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { getMerchantFinancesDetails } from '@finances/store';
import { matchPath, useRouteMatch } from 'react-router';
import { State } from '@store';

/**
 * Use merchant props
 */
const useMerchantProps = (props: RouteChildrenProps) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const {
    params: { id },
    url
  } = useRouteMatch<{ id: string }>();

  const { merchantFinanceDetails, selectedStartAndEndDate } = useSelector(
    (state: State) => state.finances
  );

  const [query, setQuery] = useState('');

  const isBookings = useMemo(
    () =>
      !!matchPath(`${url}/bookings`, {
        exact: true,
        path: props.history.location.pathname
      }),
    [props.history.location.pathname, url]
  );

  const links = useMerchantLinks();

  const onQueryChange = (query: string) => {
    setQuery(query);
  };

  useEffect(() => {
    if (!id) return;

    dispatch(getMerchantFinancesDetails(+id, selectedStartAndEndDate));
  }, [selectedStartAndEndDate]);

  return {
    t,
    query,
    links,
    onQueryChange,
    isBookings,
    merchantFinanceDetails,
    ...props
  };
};

export { useMerchantProps };
