import * as React from 'react';
import {
  Body,
  Button,
  Cell,
  EmptyPage,
  formatPrice,
  Head,
  hoc,
  Pagination,
  Row,
  Table
} from '@core';
import { useBookingsProps } from './bookings.props';
import * as styles from './bookings.scss';
import * as moment from 'moment';
import { UserAvatarAndName } from '@merchant/pages/merchant/components';
import classNames from 'classnames';

/**
 * Renders Bookings
 */
const Bookings = hoc(
  useBookingsProps,
  ({
    t,
    bookingTitles,
    bookings,
    paginationLength,
    onPageChange,
    checkIsCompleted
  }) => (
    <div className={styles.bookings}>
      <Table>
        <Head>
          <Row>
            {bookingTitles?.map((title, index) => (
              <Cell
                className={styles.cell}
                key={index}
                atEnd={title.atEnd}
                bold={title.bold}
              >
                {title.name}
              </Cell>
            ))}
          </Row>
        </Head>
        <Body>
          {bookings?.map(
            (
              {
                booking_date_time,
                employee_name,
                product_name,
                status,
                price,
                revenue
              },
              index
            ) => (
              <Row key={index}>
                <Cell className={styles.cell}>
                  {moment(booking_date_time)
                    .utc(false)
                    .format('HH:mm YYYY-MM-DD')}
                </Cell>
                <Cell className={styles.cell} bold>
                  <UserAvatarAndName
                    name={employee_name}
                    imgUrl={require('img/avatar.png')}
                  />
                </Cell>
                <Cell className={styles.cell}>{product_name}</Cell>
                <Cell className={styles.cell}>
                  <Button
                    className={
                      styles[
                        `button${
                          checkIsCompleted(booking_date_time)
                            ? 'Completed'
                            : 'Upcoming'
                        }`
                      ]
                    }
                    size='xs'
                  >
                    {checkIsCompleted(booking_date_time)
                      ? t('merchantPanel.booking.tabs.previous').toUpperCase()
                      : t('merchantPanel.booking.tabs.upcoming').toUpperCase()}
                  </Button>
                </Cell>
                <Cell className={styles.cell} atEnd>
                  {formatPrice(price)}
                </Cell>
                <Cell
                  className={classNames(
                    styles.cell,
                    styles[`price${status ? 'Completed' : 'Upcoming'}`]
                  )}
                  atEnd
                  bold
                >
                  {formatPrice(revenue)}
                </Cell>
              </Row>
            )
          )}
        </Body>
      </Table>
      {paginationLength.othersLength ? (
        <Pagination
          length={paginationLength.othersLength}
          onPageChange={onPageChange}
        />
      ) : (
        <EmptyPage title={t('superAdmin.merchant.bookings.emptyPage.title')} />
      )}
    </div>
  )
);

export { Bookings };
