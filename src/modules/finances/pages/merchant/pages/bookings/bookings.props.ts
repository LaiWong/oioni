import { useTranslation } from 'react-i18next';
import { useTitles } from '@merchant/hooks';
import { bookings } from '@finances/mocks';
import { useEffect } from 'react';
import { useRouteMatch } from 'react-router';
import { useDispatch, useSelector } from 'react-redux';
import { getMerchantDetails, unmount } from '@merchant';
import { State } from '@store';
import moment from 'moment';
import { usePagination } from '@core';

/**
 * <Bookings /> props
 */
const useBookingsProps = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const {
    params: { id }
  } = useRouteMatch<{ id: string }>();

  const {
    merchant: {
      merchantDetails: { bookings },
      paginationLength
    },
    finances: { selectedStartAndEndDate }
  } = useSelector((state: State) => ({
    merchant: state.merchant.merchant,
    finances: state.finances
  }));

  const { selectedPage, onPageChange } = usePagination();

  const bookingTitles = useTitles('bookings');

  const checkIsCompleted = (date: string) =>
    moment(moment.utc(date).format('yyyy-MM-DD:HH:mm:ss')).isBefore();

  useEffect(
    () => () => {
      dispatch(unmount('bookings'));
    },
    []
  );

  useEffect(() => {
    if (!id) return;

    dispatch(
      getMerchantDetails.bookings(id, selectedPage, selectedStartAndEndDate)
    );
  }, [selectedPage, selectedStartAndEndDate]);

  return {
    t,
    bookingTitles,
    bookings,
    paginationLength,
    onPageChange,
    checkIsCompleted
  };
};

export { useBookingsProps };
