import * as React from 'react';
import {
  Body,
  Cell,
  EmptyPage,
  formatPrice,
  Head,
  hoc,
  Pagination,
  Row,
  Table
} from '@core';
import { useHistoryProps } from './history.props';
import * as styles from './history.scss';
import { UserAvatarAndName } from '@merchant/pages/merchant/components';
import classNames from 'classnames';
import moment from 'moment';

/**
 * Renders History
 */
const History = hoc(
  useHistoryProps,
  ({
    t,
    historyTitles,
    merchantHistoryOfPayment,
    paginationLength,
    onPageChange,
    selectedMerchant
  }) => (
    <div className={styles.history}>
      <Table>
        <Head>
          <Row>
            {historyTitles?.map((title, index) => (
              <Cell
                className={styles.cell}
                key={index}
                atEnd={title.atEnd}
                bold={title.bold}
              >
                {title.name}
              </Cell>
            ))}
          </Row>
        </Head>
        <Body>
          {merchantHistoryOfPayment?.map(
            ({ amount_EUR, createdAt, id, merchant_id }, index) => (
              <Row key={index}>
                <Cell className={styles.cell} bold>
                  <div className={styles.name}>
                    <UserAvatarAndName
                      name={selectedMerchant?.name}
                      imgUrl={selectedMerchant?.image}
                    />
                  </div>
                </Cell>
                <Cell className={styles.cell}>
                  {moment(createdAt).format('HH:MM YYYY-MM-DD')}
                </Cell>
                <Cell className={styles.cell}>{formatPrice(amount_EUR)}</Cell>
                <Cell className={styles.cell}>
                  <p
                    className={classNames(styles.threeDots, 'not-active-link')}
                  >
                    &#8942;
                  </p>
                </Cell>
              </Row>
            )
          )}
        </Body>
      </Table>
      {paginationLength ? (
        <Pagination length={paginationLength} onPageChange={onPageChange} />
      ) : (
        <EmptyPage title={t('superAdmin.merchant.history.emptyPage.title')} />
      )}
    </div>
  )
);

export { History };
