import { useTranslation } from 'react-i18next';
import { useTitles } from '@finances/hooks';
import { useEffect } from 'react';
import { getMerchantFinancesDetails } from '@finances/store';
import { useDispatch, useSelector } from 'react-redux';
import { useRouteMatch } from 'react-router';
import { unmount } from '@merchant';
import { State } from '@store';

/**
 * <History /> props
 */
const useHistoryProps = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const {
    params: { id }
  } = useRouteMatch<{ id: string }>();

  const {
    merchantHistoryOfPayment,
    paginationLength,
    selectedMerchant,
    selectedStartAndEndDate
  } = useSelector((state: State) => state.finances);

  const historyTitles = useTitles('merchanthistory');

  const onPageChange = () => {};

  useEffect(
    () => () => {
      dispatch(unmount('merchantHistoryOfPayment'));
    },
    []
  );

  useEffect(() => {
    if (!id) return;

    dispatch(getMerchantFinancesDetails.history(+id, selectedStartAndEndDate));
  }, [selectedStartAndEndDate]);

  return {
    t,
    historyTitles,
    merchantHistoryOfPayment,
    paginationLength,
    selectedMerchant,
    onPageChange
  };
};

export { useHistoryProps };
