import * as React from 'react';
import { useMerchantsProps } from './merchants.props';
import * as styles from './merchants.scss';
import {
  Body,
  Cell,
  Checkbox,
  EmptyPage,
  formatPrice,
  Head,
  hoc,
  Pagination,
  Row,
  Table
} from '@core';
import { UserAvatarAndName } from '@merchant/pages/merchant/components';
import classNames from 'classnames';

/**
 * Renders Merchants
 */
const Merchants = hoc(
  useMerchantsProps,
  ({
    t,
    match: { url },
    merchantsTitles,
    merchants,
    onMerchantCheckboxChange,
    onSelectAllClick,
    onMerchantClick,
    checkIsIncludesMerchant,
    selectedMerchants,
    paginationLength,
    onPageChange
  }) => (
    <div className={styles.merchants}>
      <Table>
        <Head>
          <Row>
            <Cell className={classNames(styles.cell, styles.checkboxContainer)}>
              <Checkbox
                className={styles.checkbox}
                value={selectedMerchants.length === 9}
                onChange={onSelectAllClick}
              />
            </Cell>
            {merchantsTitles?.map((title, index) => (
              <Cell
                className={styles.cell}
                key={index}
                atEnd={title.atEnd}
                bold={title.bold}
                dangerouslySetInnerHTML={{ __html: title.name }}
              />
            ))}
          </Row>
        </Head>
        <Body>
          {merchants?.map((merchant, index) => {
            const { id, name, need_to_pay, image, paid, pending } = merchant;

            return (
              <Row key={index}>
                <Cell
                  className={classNames(styles.cell, styles.checkboxContainer)}
                >
                  <Checkbox
                    className={styles.checkbox}
                    onChange={() => onMerchantCheckboxChange(merchant)}
                    value={checkIsIncludesMerchant(id)}
                  />
                </Cell>
                <Cell className={styles.cell} bold>
                  <div className={styles.name}>
                    <UserAvatarAndName
                      name={name || t('superAdmin.merchants.user.noName')}
                      imgUrl={image}
                      isNavLink
                      linkUrl={`${url}/${id}`}
                      onLinkClick={() => onMerchantClick(merchant)}
                    />
                  </div>
                </Cell>
                <Cell className={styles.cell} bold>
                  {formatPrice(need_to_pay)}
                </Cell>
                <Cell className={styles.cell}>{formatPrice(pending)}</Cell>
                <Cell className={styles.cell} atEnd>
                  {formatPrice(paid)}
                </Cell>
              </Row>
            );
          })}
        </Body>
      </Table>
      {paginationLength ? (
        <Pagination length={paginationLength} onPageChange={onPageChange} />
      ) : (
        <EmptyPage title={t('superAdmin.merchants.emptyPage.title')} />
      )}
    </div>
  )
);

export { Merchants };
