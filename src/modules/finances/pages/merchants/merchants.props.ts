import { useTranslation } from 'react-i18next';
import { RouteChildrenProps } from 'react-router-dom';
import { useTitles } from '../../hooks';
import { useDispatch, useSelector } from 'react-redux';
import { State } from '@store';
import {
  getMerchantsSalaryDetails,
  merchantToPayment,
  setSelectedMerchant
} from '@finances/store';
import { MerchantsSalary } from '@api';
import { useEffect } from 'react';
import { usePagination } from '@core';

/**
 * Use merchants props
 */
const useMerchantsProps = (props: RouteChildrenProps) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();

  const {
    selectedMerchants,
    merchants,
    paginationLength,
    selectedStartAndEndDate
  } = useSelector((state: State) => state.finances);

  const { selectedPage, onPageChange } = usePagination(() => {
    dispatch(merchantToPayment.clear());
  });

  const merchantsTitles = useTitles('merchants');

  const onMerchantCheckboxChange = (data: MerchantsSalary) => {
    const find = selectedMerchants.find(merchant => merchant.id === data.id);

    if (!find) {
      dispatch(merchantToPayment.add([data]));

      return;
    }

    dispatch(merchantToPayment.remove(data.id));
  };

  const onSelectAllClick = (value: boolean) => {
    if (value) {
      const merchantsToAdd = merchants.filter(
        merchant => !selectedMerchants.some(({ id }) => id === merchant.id)
      );

      dispatch(merchantToPayment.add(merchantsToAdd));

      return;
    }

    dispatch(merchantToPayment.clear());
  };

  const onMerchantClick = (merchant: MerchantsSalary) => {
    dispatch(setSelectedMerchant(merchant));
  };

  const checkIsIncludesMerchant = (id: number) =>
    selectedMerchants.some(merchant => merchant.id === id);

  useEffect(() => {
    dispatch(getMerchantsSalaryDetails(selectedPage, selectedStartAndEndDate));
  }, [selectedPage, selectedStartAndEndDate]);

  return {
    t,
    merchantsTitles,
    merchants,
    onMerchantCheckboxChange,
    onSelectAllClick,
    onMerchantClick,
    checkIsIncludesMerchant,
    selectedMerchants,
    paginationLength,
    selectedPage,
    onPageChange,
    ...props
  };
};

export { useMerchantsProps };
