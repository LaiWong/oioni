import * as React from 'react';
import { usePaymentProps } from './payment.props';
import * as styles from './payment.scss';
import { Body, Button, Cell, formatPrice, Head, hoc, Row, Table } from '@core';
import { UserAvatarAndName } from '@merchant/pages/merchant/components';
import { CopyIcon } from '@finances/components';
import classNames from 'classnames';

/**
 * Renders Payment
 */
const Payment = hoc(
  usePaymentProps,
  ({
    t,
    paymentTitles,
    merchantsSalary,
    selectedMerchants,
    onCopyClick,
    onPayClick
  }) => (
    <div className={styles.payment}>
      <Table>
        <Head>
          <Row>
            {paymentTitles?.map((title, index) => (
              <Cell
                className={styles.cell}
                key={index}
                atEnd={title.atEnd}
                bold={title.bold}
                dangerouslySetInnerHTML={{ __html: title.name }}
              />
            ))}
          </Row>
        </Head>
        <Body>
          {selectedMerchants?.map(
            ({ id, name, need_to_pay, iban, vat, ...rest }, index) => (
              <Row key={index}>
                <Cell
                  className={classNames(styles.cell, styles.cellWithBorder)}
                  bold
                >
                  <div className={styles.name}>
                    <UserAvatarAndName
                      name={name || 'No name'}
                      imgUrl={require('img/avatar.png')}
                    />
                  </div>
                </Cell>
                <Cell
                  className={classNames(styles.cell, styles.cellWithBorder)}
                  bold
                >
                  <div className={styles.withIcon}>
                    <p id='vat'>{vat}</p>
                    {!!vat && <CopyIcon onClick={() => onCopyClick('vat')} />}
                  </div>
                </Cell>
                <Cell
                  className={classNames(styles.cell, styles.cellWithBorder)}
                  bold
                >
                  <div className={styles.withIcon}>
                    <p id='iban'>{iban}</p>
                    {!!iban && <CopyIcon onClick={() => onCopyClick('iban')} />}
                  </div>
                </Cell>
                <Cell
                  className={classNames(styles.cell, styles.cellWithBorder)}
                  bold
                >
                  <div className={styles.withIcon}>
                    <p id='payment'>{formatPrice(need_to_pay)}</p>
                    {!!need_to_pay && (
                      <CopyIcon onClick={() => onCopyClick('payment')} />
                    )}
                  </div>
                </Cell>
                <Cell
                  className={classNames(styles.cell, styles.cellWithBorder)}
                  bold
                  atEnd
                >
                  <div className={styles.withIcon}>
                    <p id='purpose'>{'Oioni'}</p>
                    <CopyIcon onClick={() => onCopyClick('purpose')} />
                  </div>
                </Cell>
                <Cell
                  className={classNames(styles.cell, styles.cellWithBorder)}
                  bold
                >
                  <div className={styles.withIcon}>
                    <Button
                      className={classNames(styles.button)}
                      size='sm'
                      theme='teritary'
                      {...{ disabled: !need_to_pay }}
                      onClick={() =>
                        onPayClick({
                          id,
                          name,
                          iban,
                          vat,
                          need_to_pay,
                          ...rest
                        })
                      }
                    >
                      {need_to_pay === 0
                        ? t('superAdmin.payment.button.PAID')
                        : t('superAdmin.payment.button.UNPAID')}
                    </Button>
                    <img
                      className={classNames(
                        styles.arrowBottom,
                        'not-active-link'
                      )}
                      src={require('img/arrow-bottom.png')}
                      alt='Arrow bottom'
                    />
                  </div>
                </Cell>
              </Row>
            )
          )}
        </Body>
      </Table>
    </div>
  )
);

export { Payment };
