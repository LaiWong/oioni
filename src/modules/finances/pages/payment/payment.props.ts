import { useTranslation } from 'react-i18next';
import { useTitles } from '../../hooks';
import { merchantsSalary } from '@finances/mocks';
import { useDispatch, useSelector } from 'react-redux';
import { State } from '@store';
import { MerchantsSalary } from '@api';
import { payToMerchant } from '@finances/store';
import { snack } from '@store/snackbar';
import { useEffect } from 'react';

/**
 * Use payment props
 */
const usePaymentProps = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();

  const { selectedMerchants } = useSelector((state: State) => state.finances);

  const paymentTitles = useTitles('payment');

  const onCopyClick = (key: string) => {
    const element = document.getElementById(key);

    const el = document.createElement('textarea');
    el.value = element.innerText;
    document.body.appendChild(el);

    el.select();

    document.execCommand('copy');

    document.body.removeChild(el);
    dispatch(snack('Copied to clipboard'));
  };

  const onPayClick = ({ id, need_to_pay }: MerchantsSalary) => {
    dispatch(payToMerchant({ merchant_id: id, amount_EUR: need_to_pay }));
  };

  return {
    t,
    paymentTitles,
    merchantsSalary,
    onCopyClick,
    onPayClick,
    selectedMerchants
  };
};

export { usePaymentProps };
