import { make } from 'redux-chill';
import {
  FinancesDetails,
  HistoryOfPayments,
  MerchantFinanceDetails,
  MerchantHistoryOfPayment,
  MerchantsSalary,
  PayToMerchant,
  SelectedDates,
  TimeRangeTypes
} from '@api';

/**
 * Add or remove merchant from payment
 */
const merchantToPayment = make('[finances] merchant to payment')
  .stage('add', (payload: MerchantsSalary[]) => payload)
  .stage('remove', (id: number) => id)
  .stage('clear');

/**
 * Set selected merchant
 */
const setSelectedMerchant = make('[finances] set selected merchant').stage(
  (merchant: MerchantsSalary) => merchant
);

/**
 * Get merchants salary details
 */
const getMerchantsSalaryDetails = make(
  '[finances] get merchants salary details'
)
  .stage((selectedPage: number, selectedDates: SelectedDates) => ({
    selectedPage,
    selectedDates
  }))
  .stage('success', (data: MerchantsSalary[], paginationLength) => ({
    data,
    paginationLength
  }))
  .stage('finish');

/**
 * Get finances details
 */
const getFinancesDetails = make('[finances] get details')
  .stage((selectedStartAndEndDate: SelectedDates) => ({
    selectedStartAndEndDate
  }))
  .stage('success', (payload: FinancesDetails) => payload)
  .stage('finish');

/**
 * Get history of payment
 */
const getHistoryOfPayments = make('[finances] get history')
  .stage(
    ({
      search,
      selectedPage,
      selectedDates
    }: {
      search?: string;
      selectedPage?: number;
      selectedDates: SelectedDates;
    }) => ({
      search,
      selectedPage,
      selectedDates
    })
  )
  .stage('success', (data: HistoryOfPayments[], totalPages: number) => ({
    data,
    totalPages
  }))
  .stage('finish');

/**
 * Get merchant finances details
 */
const getMerchantFinancesDetails = make('[finances] get merchant finances')
  .stage((id: number, selectedDates: SelectedDates) => ({ id, selectedDates }))
  .stage('success', (payload: MerchantFinanceDetails) => payload)
  .stage('finish')
  .stage('history', (id: number, selectedDates: SelectedDates) => ({
    id,
    selectedDates
  }))
  .stage(
    'historysuccess',
    (data: MerchantHistoryOfPayment[], totalPages: number) => ({
      data,
      totalPages
    })
  );

/**
 * Pay to merchant
 */
const payToMerchant = make('[finances] pay to merchant')
  .stage((data: PayToMerchant) => ({ data }))
  .stage('success')
  .stage('failure', (error: string) => error)
  .stage('finish');

/**
 * Set selected start and end date
 */
const setSelectedStartAndEndDate = make(
  '[finances] set selected dates'
).stage((dates: SelectedDates) => ({ dates }));

/**
 * Set selected time range
 */
const setSelectedTimeRange = make(
  '[finances] set selected time range'
).stage((timeRange: TimeRangeTypes) => ({ timeRange }));

export {
  getMerchantsSalaryDetails,
  merchantToPayment,
  setSelectedMerchant,
  getFinancesDetails,
  getHistoryOfPayments,
  getMerchantFinancesDetails,
  payToMerchant,
  setSelectedStartAndEndDate,
  setSelectedTimeRange
};
