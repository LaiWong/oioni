import { reducer } from 'redux-chill';
import { FinancesState } from './state';
import {
  getFinancesDetails,
  getHistoryOfPayments,
  getMerchantFinancesDetails,
  getMerchantsSalaryDetails,
  merchantToPayment,
  payToMerchant,
  setSelectedMerchant,
  setSelectedStartAndEndDate,
  setSelectedTimeRange
} from '@finances/store/actions';
import { unmount } from '@merchant';
import moment from 'moment';

/**
 * Finances state
 */
const finances = reducer(new FinancesState())
  .on(
    getMerchantsSalaryDetails.success,
    (state, { data, paginationLength }) => {
      state.merchants = data;
      state.paginationLength = paginationLength;
    }
  )
  .on(merchantToPayment.add, (state, payload) => {
    state.selectedMerchants.push(...payload);

    localStorage.selectedMerchants = JSON.stringify(state.selectedMerchants);
  })
  .on(merchantToPayment.remove, (state, id) => {
    state.selectedMerchants = state.selectedMerchants.filter(
      (merchant) => merchant.id !== id
    );

    localStorage.selectedMerchants = JSON.stringify(state.selectedMerchants);
  })
  .on(merchantToPayment.clear, (state) => {
    state.selectedMerchants = [];

    localStorage.selectedMerchants = JSON.stringify(state.selectedMerchants);
  })
  .on(setSelectedMerchant, (state, merchant) => {
    state.selectedMerchant = merchant;

    localStorage.selectedMerchant = JSON.stringify(state.selectedMerchant);
  })
  .on(getFinancesDetails.success, (state, payload) => {
    state.financesDetails = payload;
    state.lastUpdateTime = moment().format();
  })
  .on(getHistoryOfPayments.success, (state, { data, totalPages }) => {
    state.financesHistory = data;
    state.paginationLength = totalPages;
  })
  .on(getMerchantFinancesDetails.success, (state, payload) => {
    state.merchantFinanceDetails = payload;
    state.lastUpdateTime = moment().format();
  })
  .on(
    getMerchantFinancesDetails.historysuccess,
    (state, { data, totalPages }) => {
      state.merchantHistoryOfPayment = data;
      state.paginationLength = totalPages;
    }
  )
  .on(payToMerchant.failure, (state, error) => {
    state.error = error;
  })
  .on([payToMerchant, payToMerchant.success, payToMerchant.finish], (state) => {
    state.error = null;
  })
  .on(setSelectedStartAndEndDate, (state, { dates }) => {
    state.selectedStartAndEndDate = dates;
  })
  .on(setSelectedTimeRange, (state, { timeRange }) => {
    state.selectedTimeRange = timeRange;
  })
  .on(unmount, (state, key) => {
    state[key] = null;
    state.paginationLength = 0;
    state.selectedStartAndEndDate = {
      start: null,
      end: null
    };
  });

export { finances };
