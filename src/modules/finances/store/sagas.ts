import { StoreContext } from '@store/context';
import { Payload, Saga } from 'redux-chill';
import { call, put, select } from 'redux-saga/effects';
import {
  getFinancesDetails,
  getHistoryOfPayments,
  getMerchantFinancesDetails,
  getMerchantsSalaryDetails,
  payToMerchant
} from './actions';
import { snack } from '@store/snackbar';
import { State } from '@store';
import { navigate } from '@store/router';

class FinancesSaga {
  private perPageCount = 9;

  /**
   * Get finances details
   */
  @Saga(getFinancesDetails)
  public *getFinances(
    { selectedStartAndEndDate }: Payload<typeof getFinancesDetails>,
    { finances }: StoreContext
  ) {
    try {
      const {
        data: { statistic }
      } = yield call(finances.getFinancesData, selectedStartAndEndDate);

      yield put(getFinancesDetails.success(statistic));
    } catch (err) {
      yield put(snack('message.cannotGetFinancesDetails', 'error'));
    }
  }

  /**
   * Get merchants salary details
   */
  @Saga(getMerchantsSalaryDetails)
  public *getCountries(
    { selectedPage, selectedDates }: Payload<typeof getMerchantsSalaryDetails>,
    { finances }: StoreContext
  ) {
    try {
      const {
        data: {
          finances: { data, totalPages }
        }
      } = yield call(finances.getMerchantsSalaryData, {
        perPage: this.perPageCount,
        page: selectedPage,
        selectedDates
      });

      yield put(getMerchantsSalaryDetails.success(data, totalPages));
    } catch (error) {
      yield put(snack('Cannot get salary details', 'error'));
    }
  }

  /**
   * Get finances history
   */
  @Saga(getHistoryOfPayments)
  public *getFinancesHistory(
    {
      selectedPage,
      selectedDates,
      search
    }: Payload<typeof getHistoryOfPayments>,
    { finances }: StoreContext
  ) {
    try {
      const {
        data: { merchants, totalPages }
      } = yield call(finances.getFinancesHistory, {
        page: selectedPage,
        perPage: this.perPageCount,
        selectedDates,
        search
      });

      yield put(getHistoryOfPayments.success(merchants, totalPages));
    } catch (err) {
      yield put(snack('Cannot get finances history', 'error'));
    }
  }

  /**
   * Get merchant finance details
   */
  @Saga(getMerchantFinancesDetails)
  public *getMerchantFinances(
    { id, selectedDates }: Payload<typeof getMerchantFinancesDetails>,
    { finances }: StoreContext
  ) {
    try {
      const {
        data: {
          finances: { data }
        }
      } = yield call(finances.getMerchantFinancesDetails, {
        id,
        selectedDates
      });

      yield put(getMerchantFinancesDetails.success(data[0]));
    } catch (err) {
      yield put(snack('Cannot get merchant finances details', 'error'));
    }
  }

  /**
   * Get merchant history of payment
   */
  @Saga(getMerchantFinancesDetails.history)
  public *getMerchantHistoryOfPayment(
    { id, selectedDates }: Payload<typeof getMerchantFinancesDetails.history>,
    { finances }: StoreContext
  ) {
    try {
      const {
        data: { data, totalPages }
      } = yield call(finances.getMerchantHistoryOfPayment, {
        id,
        selectedDates
      });

      yield put(getMerchantFinancesDetails.historysuccess(data, totalPages));
    } catch (err) {
      yield put(snack('Cannot get merchant history of payment', 'error'));
    }
  }

  /**
   * Pay to merchant
   */
  @Saga(payToMerchant)
  public *payToMerchant(
    { data }: Payload<typeof payToMerchant>,
    { finances }: StoreContext
  ) {
    try {
      yield call(finances.payToMerchant, data);

      yield put(payToMerchant.success());

      const selectedStartAndEndDate = yield select(
        (state: State) => state.finances
      );
      yield put(
        getMerchantFinancesDetails(data.merchant_id, selectedStartAndEndDate)
      );

      yield put(navigate('/finances/merchants'));

      yield put(snack('Successfully Paid', 'info'));
    } catch (err) {
      yield put(snack('Cannot pay for merchant', 'error'));
    }
  }
}

const sagas = [new FinancesSaga()];

export { FinancesSaga, sagas };
