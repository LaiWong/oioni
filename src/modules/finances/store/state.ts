import {
  FinancesDetails,
  HistoryOfPayments,
  MerchantFinanceDetails,
  MerchantHistoryOfPayment,
  MerchantsSalary,
  SelectedDates,
  TimeRangeTypes
} from '@api';

const { selectedMerchants, selectedMerchant } = localStorage;

class FinancesState {
  /**
   * Merchants
   */
  public merchants: MerchantsSalary[] = [];

  /**
   * Selected merchants
   */
  public selectedMerchants: MerchantsSalary[] = selectedMerchants
    ? JSON.parse(selectedMerchants)
    : [];

  /**
   * Selected merchant
   */
  public selectedMerchant: MerchantsSalary = selectedMerchant
    ? JSON.parse(selectedMerchant)
    : null;

  /**
   * Finances details
   */
  public financesDetails: FinancesDetails;

  /**
   * Finances history
   */
  public financesHistory: HistoryOfPayments[];

  /**
   * Merchant finance details
   */
  public merchantFinanceDetails: MerchantFinanceDetails;

  /**
   * Merchant history of payment
   */
  public merchantHistoryOfPayment: MerchantHistoryOfPayment[];

  /**
   * Error
   */
  public error: string = null;

  /**
   * Pagination length
   */
  public paginationLength: number = 0;

  /**
   * Selected start and end date
   */
  public selectedStartAndEndDate: SelectedDates;

  /**
   * Selected time range
   */
  public selectedTimeRange: TimeRangeTypes = TimeRangeTypes.allTime;

  /**
   * Last update time
   */
  public lastUpdateTime: string;
}

export { FinancesState };
