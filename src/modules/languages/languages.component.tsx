import React from 'react';
import {
  Body,
  Button,
  Cell,
  Checkbox,
  Form,
  Head,
  hoc,
  Modal,
  Row,
  Table,
  Field as CustomField,
  Group
} from '@core';
import { useLanguagesProps } from './languages.props';
import styles from './languages.scss';
import { Redirect, Route, Switch } from 'react-router';
import { Field, FieldArray } from 'formik';
import { CountryLanguage } from '@api';
import classNames from 'classnames';
import { deleteLanguage } from './store';

const Languages = hoc(
  useLanguagesProps,
  ({
    form,
    match: { path, url },
    dispatch,
    languages,
    languagesTitle,
    isVisible,
    setIsVisible,
    isActive,
    // setIsActive,
    languageDispatch,
    switcherChange,
    onCreateLanguage
  }) => {
    return (
      <div className={styles.languages}>
        <Form form={form} className={styles.form}>
          {isVisible && (
            <Modal className={styles.modal}>
              <span
                className={styles.modalClose}
                onClick={() => setIsVisible(false)}
              />
              <h3 className={styles.modalTitle}>
                {form.values?.id ? 'Edit' : 'Create'} Language
              </h3>
              <Group>
                <div className={styles.modalContent}>
                  <CustomField.Input label={'Language Name'} name='language' />
                  <div className={styles.modalSwitcher}>
                    <p className={isActive ? styles.active : ''}>disable</p>
                    <div
                      className={classNames(
                        styles.switcher,
                        styles[`switcher${isActive ? 'Active' : 'NotActive'}`]
                      )}
                      onClick={switcherChange}
                    >
                      <div
                        className={classNames(
                          styles.toggle,
                          styles[`toggle${isActive ? 'Active' : 'NotActive'}`]
                        )}
                      />
                    </div>
                    <p className={!isActive ? styles.active : ''}>activate</p>
                  </div>
                </div>
              </Group>
              <div className={styles.modalButtons}>
                <Button type='submit'>Save</Button>
                <Button type='reset'>Cancel</Button>
              </div>
            </Modal>
          )}
          <div className={styles.container}>
            <div className={styles.header}>
              <h1>Language Manager</h1>
              <Button type='button' onClick={onCreateLanguage}>
                Add Language
              </Button>
            </div>
            <div className={styles.tableWrapper}>
              <Table>
                <Head>
                  <Row>
                    {languagesTitle.map(
                      ({ title, atCenter, atEnd }, ReactIndex) => (
                        <Cell
                          key={ReactIndex}
                          bold
                          atCenter={atCenter}
                          atEnd={atEnd}
                        >
                          {title}
                        </Cell>
                      )
                    )}
                  </Row>
                </Head>
                <Body>
                  {languages.map(({ id, language, active }, index) => {
                    return (
                      <Row key={index}>
                        <Cell>{id}</Cell>
                        <Cell atCenter>{language}</Cell>
                        <Cell>
                          <div className={styles.languageStatus}>
                            {active.toString()}
                          </div>
                        </Cell>
                        <Cell className={styles.buttons}>
                          <Button
                            type='button'
                            onClick={() => {
                              languageDispatch({
                                type: 'update',
                                payload: { id, language, active }
                              });
                              setIsVisible(true);
                            }}
                          >
                            Edit
                          </Button>
                          <Button
                            type='button'
                            theme='secondary'
                            onClick={() => {
                              console.log('delete language');
                              dispatch(deleteLanguage(id));
                            }}
                          >
                            Delete
                          </Button>
                        </Cell>
                      </Row>
                    );
                  })}
                </Body>
              </Table>
            </div>
          </div>
        </Form>
      </div>
    );
  }
);
export { Languages };
