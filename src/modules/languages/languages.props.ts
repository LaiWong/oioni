import { CountryLanguage } from '@api';
import { State } from '@store';
import { useFormik } from 'formik';
import { useEffect, useMemo, useReducer, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { RouteComponentProps } from 'react-router';
import { createLanguage, getLanguages, updateLanguage } from './store';

const useLanguagesProps = (props: RouteComponentProps) => {
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getLanguages());
  }, []);

  const languagesTitle = [
    {
      title: 'ID',
      atCenter: false,
      atEnd: false
    },
    {
      title: 'Name',
      atCenter: true,
      atEnd: false
    },
    {
      title: 'Active',
      atCenter: true,
      atEnd: false
    },
    // {
    //   title: 'Settings',
    //   atCenter: true,
    //   atEnd: false
    // },
    {
      title: '',
      atCenter: false,
      atEnd: false
    }
  ];
  let _languages = useSelector((state: State) => state.language.languages);

  const [isVisible, setIsVisible] = useState(false);

  const [language, languageDispatch] = useReducer((state, action) => {
    switch (action.type) {
      case 'create':
        return (state = {} as CountryLanguage);
      case 'update':
        return (state = action.payload);
      default:
        return (state = {} as CountryLanguage);
    }
  }, 'create');

  const form = useFormik<CountryLanguage>({
    initialValues: language,
    onSubmit: values => {
      values?.id
        ? dispatch(updateLanguage(values))
        : dispatch(createLanguage(values));

      setIsVisible(false);
    },
    enableReinitialize: true
  });

  const { active: isActive } = form.values;

  const languages = useMemo(
    () =>
      _languages.length
        ? [..._languages].sort((a, b) => a.id - b.id)
        : _languages,
    [_languages]
  );

  const switcherChange = () => {
    form.setFieldValue('active', !form.values?.active);
  };

  const onCreateLanguage = () => {
    languageDispatch({ type: 'create' });
    setIsVisible(true);
  };

  return {
    form,
    dispatch,
    languagesTitle,
    languages,
    isVisible,
    setIsVisible,
    isActive,
    // setIsActive,
    languageDispatch,
    switcherChange,
    onCreateLanguage,
    ...props
  };
};

export { useLanguagesProps };
