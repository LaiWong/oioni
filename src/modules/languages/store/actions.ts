import { CountryLanguage } from '@api';
import { make } from 'redux-chill';

const getLanguages = make('[language] get languages')
  .stage((id?: number, language?: string, active?: boolean) => ({
    id,
    language,
    active
  }))
  .stage('success', (languages: CountryLanguage[]) => languages)
  .stage('failure')
  .stage('finish');

const createLanguage = make('[language] create language')
  .stage((data: Omit<CountryLanguage, 'id'>) => data)
  .stage('success')
  .stage('failure')
  .stage('finish');

const updateLanguage = make('[language] update language')
  .stage((language: CountryLanguage) => language)
  .stage('success')
  .stage('failure')
  .stage('finish');

const deleteLanguage = make('[language] delete language')
  .stage((id: number) => id)
  .stage('success')
  .stage('failure')
  .stage('finish');

export { getLanguages, createLanguage, updateLanguage, deleteLanguage };
