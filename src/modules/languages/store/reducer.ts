import { reducer } from 'redux-chill';
import { getLanguages } from './actions';
import { LanguageState } from './state';

const language = reducer(new LanguageState()).on(
  getLanguages.success,
  (state, languages) => (state.languages = languages)
);

export { language };
