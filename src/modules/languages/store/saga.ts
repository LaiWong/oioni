import { StoreContext } from '@store/context';
import { snack } from '@store/snackbar';
import { Saga, Payload } from 'redux-chill';
import { call, put } from 'redux-saga/effects';
import {
  createLanguage,
  deleteLanguage,
  getLanguages,
  updateLanguage
} from './actions';

class LanguageSaga {
  @Saga(getLanguages)
  public *getLanguages(
    data: Payload<typeof getLanguages>,
    { language }: StoreContext
  ) {
    try {
      const {
        data: { result }
      } = yield call(language.getLanguages, data);
      yield put(getLanguages.success(result));
      // yield put(snack('Get Languages success!'));
    } catch (error) {
      yield put(snack('Cannot get language!'));
    }
  }

  @Saga(createLanguage)
  public *createLanguage(
    data: Payload<typeof createLanguage>,
    { language }: StoreContext
  ) {
    try {
      const {
        data: { result }
      } = yield call(language.createLanguage, data);
      console.log('result', result);
      yield put(snack('Language was created successfully!'));
    } catch (error) {
      yield put(snack('Cannot create language!'));
    } finally {
      yield put(getLanguages());
    }
  }

  @Saga(updateLanguage)
  public *updateLanguage(
    data: Payload<typeof updateLanguage>,
    { language }: StoreContext
  ) {
    try {
      const {
        data: { result }
      } = yield call(language.updateLanguage, data);
      console.log('result', result);
      yield put(snack('Language was updated successfully!'));
    } catch (error) {
      yield put(snack('Cannot update language!'));
    } finally {
      yield put(getLanguages());
    }
  }

  @Saga(deleteLanguage)
  public *deleteLanguage(
    id: Payload<typeof deleteLanguage>,
    { language }: StoreContext
  ) {
    try {
      const {
        data: { result }
      } = yield call(language.deleteLanguage, id);
      console.log('result', result);
      yield put(snack('Language was deleted successfully!'));
    } catch (error) {
      yield put(snack('Cannot delete language!'));
    } finally {
      yield put(getLanguages());
    }
  }
}

export { LanguageSaga };
