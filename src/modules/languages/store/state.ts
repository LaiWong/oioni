import { CountryLanguage } from '@api';

class LanguageState {
  public languages: CountryLanguage[] = [];
}

export { LanguageState };
