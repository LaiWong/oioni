import * as React from 'react';
import * as styles from './merchant-action-modal.scss';
import { MerchantActionModalProps } from './merchant-action-modal.props';

/**
 * Renders Merchant Action Modal
 */
const MerchantActionModal: React.FC<MerchantActionModalProps> = ({
  onMouseLeave,
  onDeleteMerchant,
  t
}) => (
  <div className={styles.container} onMouseLeave={onMouseLeave}>
    <h3 className={'not-active-link'}>
      {t('superAdmin.merchants.actionModal.requestMoreInfo')}
    </h3>
    <h3 className={'not-active-link'}>
      {t('superAdmin.merchants.actionModal.loginMerchantAccess')}
    </h3>
    <h3 className={styles.delete} onClick={onDeleteMerchant}>
      {t('superAdmin.merchants.actionModal.deleteMerchant')}
    </h3>
  </div>
);

export { MerchantActionModal };
