import { MouseEventHandler } from 'react';
import { TFunction } from 'i18next';

/**
 * Props
 */
type MerchantActionModalProps = {
  /**
   * On Mouse Leave Event
   */
  onMouseLeave?: MouseEventHandler<HTMLDivElement>;

  /**
   * On Delete Merchant Click
   */
  onDeleteMerchant?: () => void;
  /**
   * On Translate Function
   */
  t: TFunction;
};

export { MerchantActionModalProps };
