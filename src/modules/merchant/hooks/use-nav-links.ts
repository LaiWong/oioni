import { ReactNode } from 'react';
import { useTranslation } from 'react-i18next';
import { useRouteMatch } from 'react-router';

/**
 * Nav links
 */
const useNavLinks = (): {
  to: string;
  name: ReactNode;
  key: string;
}[] => {
  const { t } = useTranslation();
  const { url } = useRouteMatch();

  return [
    {
      to: `${url}/bookings`,
      name: t('merchantPanel.tabs.bookings'),
      key: 'merchantPanel.tabs.bookings'
    },
    {
      to: `${url}/services`,
      name: t('merchantPanel.tabs.services'),
      key: 'merchantPanel.tabs.services'
    },
    {
      to: `${url}/employees`,
      name: t('merchantPanel.tabs.employees'),
      key: 'merchantPanel.tabs.employees'
    },
    {
      to: `${url}/salons`,
      name: t('merchantPanel.tabs.salons'),
      key: 'merchantPanel.tabs.salons'
    },
    {
      to: `${url}/profile`,
      name: t('merchantPanel.tabs.profile'),
      key: 'merchantPanel.tabs.profile'
    }
  ];
};

export { useNavLinks };
