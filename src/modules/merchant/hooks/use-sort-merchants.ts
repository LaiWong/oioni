import { Merchant } from '@api';
import { useSort } from '@core';

const sortByElements = ['registerDate', 'name', 'serviceProvider', 'сlients'];

const useSortMerchants = (merchants: Merchant[]) => {
  const {
    sortBy,
    direction,
    sortedElements: sortedMerchants,
    setSortBy,
    setSortedElements: setSortedMerchants,
    setDirection,
    sort,
    sortByDate
  } = useSort<Merchant>(merchants);

  const onSortClick = (sortBy: string) => {
    const sortByIndex = sortByElements.indexOf(sortBy);
    const servicesToSort = [...sortedMerchants];

    switch (sortByIndex) {
      case 0: {
        servicesToSort.sort((a, b) => sortByDate(a, b, 'created_at'));

        break;
      }
      case 1: {
        servicesToSort.sort((a, b) => sort(a, b, 'merchant_name'));

        break;
      }
      case 2: {
        servicesToSort.sort((a, b) => sort(a, b, 'services_count'));

        break;
      }
      case 3: {
        servicesToSort.sort((a, b) => sort(a, b, 'users_count'));

        break;
      }
    }

    setDirection(direction === 'up' ? 'down' : 'up');
    setSortedMerchants(servicesToSort);
    setSortBy(sortBy);
  };

  return { sortedMerchants, sortBy, direction, onSortClick, setSortBy };
};

export { sortByElements, useSortMerchants };
