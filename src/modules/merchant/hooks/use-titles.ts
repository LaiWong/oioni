import { useTranslation } from 'react-i18next';
import { useMemo } from 'react';

/**
 * Titles
 */
const useTitles = (
  // Key is equal to component name
  key
): {
  name: string;
  atEnd?: boolean;
  bold?: boolean;
  atCenter?: boolean;
}[] => {
  const { t } = useTranslation();

  const titles = useMemo(
    () => ({
      merchants: [
        {
          name: t('superAdmin.merchants.titles.registerDate')
        },
        {
          name: t('superAdmin.merchants.titles.name')
        },
        {
          name: t('superAdmin.merchants.titles.serviceProvider')
        },
        {
          name: t('superAdmin.merchants.titles.сlients'),
          atEnd: true
        },
        {}
      ],
      bookings: [
        {
          name: t('superAdmin.bookings.titles.date')
        },
        {
          name: t('superAdmin.bookings.titles.stylistName')
        },
        {
          name: t('superAdmin.bookings.titles.service')
        },
        {
          name: t('superAdmin.bookings.titles.status')
        },
        {
          name: t('superAdmin.bookings.titles.amount'),
          atEnd: true
        },
        {
          name: t('superAdmin.bookings.titles.revenue'),
          atEnd: true
        }
      ],
      refundCanceled: [
        {
          name: t('order_item_id')
          // atEnd: true
        },
        {
          name: t('refund_percent')
          // atEnd: true
        },
        {
          name: t('price')
          // atEnd: true
        },
        {
          name: t('date')
          // atEnd: true
        }
      ],
      refundFailProcessing: [
        {
          name: t('order_id')
          // atEnd: true
        },
        {
          name: t('price')
          // atEnd: true
        },
        {
          name: t('info')
          // atEnd: true
        }
      ],
      services: [
        {
          name: t('merchantPanel.service.list.serviceName'),
          bold: true
        },
        {
          name: t('merchantPanel.service.list.serviceCategory')
        },
        {
          name: t('merchantPanel.service.list.duration')
        },
        {
          name: t('merchantPanel.service.list.price')
        }
      ],
      salons: [
        {
          name: t('merchantPanel.salon.list.name')
        },
        {
          name: t('merchantPanel.salon.list.address')
        },
        {
          name: t('merchantPanel.salon.list.contacts')
        }
      ],
      profile: [
        {
          name: t('superAdmin.merchant.profile.titles.venueName')
        },
        {
          name: t('superAdmin.merchant.profile.titles.contactInfo')
        },
        {
          name: t('superAdmin.merchant.profile.titles.businessInfo')
        },
        {
          name: t('superAdmin.merchant.profile.titles.otherInfo')
        }
      ],
      employees: [
        {
          name: t('superAdmin.merchant.employee.titles.stylistName')
        },
        { name: t('superAdmin.merchant.employee.titles.role'), atCenter: true },
        {
          name: t('superAdmin.merchant.employee.titles.services'),
          atCenter: true
        },
        {}
      ]
    }),
    [t]
  );

  return titles[key.toLowerCase()];
};

export { useTitles };
