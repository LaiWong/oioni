import * as React from 'react';
import { RouteComponentProps } from 'react-router';
import { Route, Switch } from 'react-router-dom';

import { Merchant as SingleMerchant, Merchants } from './pages';

/**
 * <Merchant />
 */
const Merchant: React.FC<RouteComponentProps> = ({ match }) => (
  <div>
    <Switch>
      <Route exact path={match.path} component={Merchants} />
      <Route path={`${match.path}/:id`} component={SingleMerchant} />
    </Switch>
  </div>
);

export { Merchant };
