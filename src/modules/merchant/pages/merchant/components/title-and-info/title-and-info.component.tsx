import * as React from 'react';
import * as styles from './title-and-info.scss';
import { TitleAndInfoProps } from './title-and-info.props';

/**
 * Renders TitleAndInfo
 */
const TitleAndInfo: React.FC<TitleAndInfoProps> = ({ title, info }) => (
  <div className={styles.container}>
    <h6>{title}</h6>
    {info && <h4 dangerouslySetInnerHTML={{ __html: `${info}` }} />}
  </div>
);

export { TitleAndInfo };
