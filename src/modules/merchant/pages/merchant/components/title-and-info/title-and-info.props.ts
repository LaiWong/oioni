/**
 * Props
 */
type TitleAndInfoProps = {
  /**
   * Title
   */
  title: string;

  /**
   * Info
   */
  info: string;
};

export { TitleAndInfoProps };
