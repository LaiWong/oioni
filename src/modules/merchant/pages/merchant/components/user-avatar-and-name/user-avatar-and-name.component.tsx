import * as React from 'react';
import { NavLink } from 'react-router-dom';
import * as styles from './user-avatar-and-name.scss';
import { UserAvatarAndNameProps } from '@merchant/pages/merchant/components/user-avatar-and-name/user-avatar-and-name.props';
import classNames from 'classnames';
import { formats } from '@core';

/**
 * Renders UserAvatarAndName
 */
const UserAvatarAndName: React.FC<UserAvatarAndNameProps> = ({
  name,
  imgUrl,
  isNavLink,
  linkUrl,
  onLinkClick
}) => (
  <div className={styles.userContainer}>
    {imgUrl && formats.photo.check(imgUrl) ? (
      <img className={styles.userAvatar} src={imgUrl} />
    ) : (
      <div
        className={classNames(styles.userAvatar, styles.userAvatarNoAvatar)}
      />
    )}
    <p>
      {isNavLink ? (
        <NavLink to={linkUrl} onClick={onLinkClick}>
          {name}
        </NavLink>
      ) : (
        name
      )}
    </p>
  </div>
);

export { UserAvatarAndName };
