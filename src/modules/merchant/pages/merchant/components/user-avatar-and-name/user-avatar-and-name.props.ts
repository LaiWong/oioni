/**
 * Props
 */
type UserAvatarAndNameProps = {
  /**
   * User Name
   */
  name: string;

  /**
   * Avatar Image Url
   */
  imgUrl: string;

  /**
   * Name is nav link
   */
  isNavLink?: boolean;

  /**
   * Nav link url
   */
  linkUrl?: string;

  /**
   * On link click function
   */
  onLinkClick?: () => void;
};

export { UserAvatarAndNameProps };
