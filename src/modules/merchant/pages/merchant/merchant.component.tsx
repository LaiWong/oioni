import * as React from 'react';
import classNames from 'classnames';
import { useMerchantProps } from './merchant.props';
import * as styles from './merchant.scss';
import { hoc } from '@core';
import { NavLink, Route, Switch } from 'react-router-dom';
import { Bookings, Employees, Profile, Salons, Services } from './pages';
import { Redirect } from 'react-router';

/**
 * Renders Merchant
 */
const Merchant = hoc(
  useMerchantProps,
  ({
    t,
    match: { url, path, params },
    links,
    selectedCategory,
    setSelectedCategory,
    selectedMerchant,
    isActive,
    setStatus
  }) => (
    <div className={styles.merchant}>
      <div className={styles.header}>
        <div className={styles.header1}>
          <h2 className={styles.title}>
            <NavLink
              //@ts-ignore
              to={`/${params.language}/merchants`}
            >
              <span>{t('superAdmin.merchants.allMerchants')}</span>
            </NavLink>{' '}
            / {selectedMerchant?.merchant_name}
          </h2>
        </div>
        <div className={styles.header2}>
          <p className={isActive ? styles.active : ''}>
            {t('merchantPanel.merchant.list.disable')}
          </p>
          <div
            className={classNames(
              styles.switcher,
              styles[`switcher${isActive ? 'Active' : 'NotActive'}`]
            )}
            onClick={setStatus}
          >
            <div
              className={classNames(
                styles.toggle,
                styles[`toggle${isActive ? 'Active' : 'NotActive'}`]
              )}
            />
          </div>
          <p className={!isActive ? styles.active : ''}>
            {t('merchantPanel.merchant.list.activate')}
          </p>
        </div>
      </div>
      <div className={styles.navContainer}>
        <ul className={styles.nav}>
          {links.map(({ name, to, key }, index) => (
            <li key={index}>
              <NavLink
                onClick={() => setSelectedCategory({ name, to, key })}
                className={styles.navLink}
                activeClassName={styles.active}
                to={to}
              >
                {name}
                <div className={styles.dot} />
              </NavLink>
            </li>
          ))}
        </ul>
      </div>
      <div className={styles.contentHeader}>
        <h2>{t(`${selectedCategory.key}`)}</h2>
      </div>
      <div>
        <Switch>
          <Route exact path={`${path}/bookings`} component={Bookings} />
          <Route path={`${path}/services`} component={Services} />
          <Route path={`${path}/salons`} component={Salons} />
          <Route path={`${path}/profile`} component={Profile} />
          <Route path={`${path}/employees`} component={Employees} />
          <Redirect to={`${url}/bookings`} />
        </Switch>
      </div>
    </div>
  )
);

export { Merchant };
