import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { ReactNode, useEffect, useState } from 'react';
import { useRouteMatch } from 'react-router';
import { useNavLinks } from '../../hooks';
import { State } from '@store';
import { getMerchants, setMerchantStatus } from '@merchant/store/actions';

/**
 * <Merchant /> props
 */
const useMerchantProps = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const match = useRouteMatch();

  const { selectedMerchant } = useSelector(
    (state: State) => state.merchant.merchant
  );

  const [selectedCategory, setSelectedCategory] = useState<{
    name: ReactNode;
    to: string;
    key: string;
  }>({ name: '', to: '', key: '' });

  const isActive = selectedMerchant?.status === 'active';

  const links = useNavLinks();

  const setStatus = () => {
    const { status, id } = selectedMerchant;

    dispatch(setMerchantStatus(id, status == 'active' ? 'disabled' : 'active'));
  };

  useEffect(() => {
    const link = links[0];

    setSelectedCategory(link);

    dispatch(getMerchants());
  }, []);

  return {
    isActive,
    t,
    selectedCategory,
    setSelectedCategory,
    setStatus,
    links,
    match,
    selectedMerchant
  };
};

export { useMerchantProps };
