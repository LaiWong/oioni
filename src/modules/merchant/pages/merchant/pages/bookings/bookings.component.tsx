import * as React from 'react';
import {
  Body,
  Button,
  Cell,
  EmptyPage,
  formatPrice,
  Head,
  hoc,
  Pagination,
  Row,
  Table
} from '@core';
import { useBookingsProps } from './bookings.props';
import * as styles from './bookings.scss';
import { UserAvatarAndName } from '../../components';
import * as moment from 'moment';

/**
 * Renders Bookings
 */
const Bookings = hoc(
  useBookingsProps,
  ({
    t,
    bookingTitles,
    bookings,
    onPageChange,
    paginationLength,
    checkIsCompleted
  }) => (
    <div>
      <Table>
        <Head>
          <Row>
            {bookingTitles?.map((title, index) => (
              <Cell key={index} atEnd={title.atEnd} bold={title.bold}>
                {title.name}
              </Cell>
            ))}
          </Row>
        </Head>
        <Body>
          {bookings?.map(
            (
              {
                booking_date_time,
                employee_name,
                product_name,
                status,
                price,
                employee_image_url,
                revenue
              },
              index
            ) => (
              <Row key={index}>
                <Cell>
                  {moment(booking_date_time)
                    .utc(false)
                    .format('HH:mm YYYY-MM-DD')}
                </Cell>
                <Cell bold>
                  <UserAvatarAndName
                    name={employee_name}
                    imgUrl={employee_image_url}
                  />
                </Cell>
                <Cell>{product_name}</Cell>
                <Cell>
                  <Button
                    className={
                      styles[
                        `button${
                          checkIsCompleted(booking_date_time)
                            ? 'Completed'
                            : 'Upcoming'
                        }`
                      ]
                    }
                    size='xs'
                  >
                    {checkIsCompleted(booking_date_time)
                      ? t('merchantPanel.booking.tabs.previous').toUpperCase()
                      : t('merchantPanel.booking.tabs.upcoming').toUpperCase()}
                  </Button>
                </Cell>
                <Cell atEnd>{formatPrice(price)}</Cell>
                <Cell atEnd bold>
                  {formatPrice(revenue)}
                </Cell>
              </Row>
            )
          )}
        </Body>
      </Table>
      {paginationLength ? (
        <Pagination length={paginationLength} onPageChange={onPageChange} />
      ) : (
        <EmptyPage title={t('superAdmin.merchant.bookings.emptyPage.title')} />
      )}
    </div>
  )
);

export { Bookings };
