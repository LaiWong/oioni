import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { useTitles } from '@merchant/hooks';
import { useEffect, useMemo } from 'react';
import { getMerchantDetails, unmount } from '@merchant/store/actions';
import { useRouteMatch } from 'react-router';
import { State } from '@store';
import moment, { MomentInput } from 'moment';
import { usePagination } from '@core';

/**
 * <Bookings /> props
 */
const useBookingsProps = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const match = useRouteMatch<{ id: string }>();

  const {
    merchantDetails: { bookings: _bookings },
    paginationLength: { othersLength }
  } = useSelector((state: State) => state.merchant.merchant);

  const { selectedPage, onPageChange } = usePagination();

  const bookings = useMemo(() => {
    if (_bookings?.length) {
      const isPast = (booking_date_time: MomentInput) =>
        moment(
          moment.utc(booking_date_time).format('yyyy-MM-DD:HH:mm:ss')
        ).isBefore();
      const noDeletedBookings = _bookings.filter(
        ({ deleted }) => deleted !== 1
      );
      return [
        ...noDeletedBookings
          .filter(({ booking_date_time }) => !isPast(booking_date_time))
          .sort(
            (a, b) =>
              +new Date(a.booking_date_time) - +new Date(b.booking_date_time)
          ),
        ...noDeletedBookings
          .filter(({ booking_date_time }) => isPast(booking_date_time))
          .sort(
            (a, b) =>
              +new Date(b.booking_date_time) - +new Date(a.booking_date_time)
          )
      ];
    }
  }, [_bookings]);

  const bookingTitles = useTitles('bookings');

  const checkIsCompleted = (date: string) =>
    moment(moment.utc(date).format('yyyy-MM-DD:HH:mm:ss')).isBefore();

  useEffect(
    () => () => {
      dispatch(unmount('bookings'));
    },
    []
  );

  useEffect(() => {
    dispatch(getMerchantDetails.bookings(match.params.id, selectedPage));
  }, [selectedPage, match.params.id]);

  return {
    t,
    bookingTitles,
    bookings,
    paginationLength: othersLength,
    onPageChange,
    checkIsCompleted
  };
};

export { useBookingsProps };
