import * as React from 'react';
import {
  Body,
  Button,
  Cell,
  EmptyPage,
  Head,
  hoc,
  Pagination,
  Row,
  Table
} from '@core';
import { useEmployeesProps } from './employees.props';
import * as styles from './employees.scss';
import { UserAvatarAndName } from '../../components';

/**
 * Renders Employees
 */
const Employees = hoc(
  useEmployeesProps,
  ({ employeesTitles, employees, onPageChange, t, paginationLength }) => (
    <div>
      <Table>
        <Head>
          <Row>
            {employeesTitles?.map((title, index) => (
              <Cell
                key={index}
                bold={title.bold}
                atCenter={title.atCenter}
                atEnd={title.atEnd}
              >
                {title.name}
              </Cell>
            ))}
          </Row>
        </Head>
        <Body>
          {employees?.map(
            ({ name, services_count, image_url, jobs }, index) => (
              <Row key={index}>
                <Cell>
                  <UserAvatarAndName name={name} imgUrl={image_url} />
                </Cell>
                <Cell atCenter>{jobs[0]?.name}</Cell>
                <Cell bold atCenter>
                  {+services_count !== 0 ? services_count : 0}
                </Cell>
                <Cell atEnd>
                  <div className={styles.container}>
                    <Button
                      className={'not-active-link'}
                      theme='teritary'
                      size='xs'
                    >
                      {t('superAdmin.merchants.specialist.button.addWorkTime')}
                    </Button>
                    <Button
                      className={'not-active-link'}
                      theme='teritary'
                      size='xs'
                    >
                      {t('superAdmin.merchants.specialist.button.EditProfile')}
                    </Button>
                  </div>
                </Cell>
              </Row>
            )
          )}
        </Body>
      </Table>
      {paginationLength ? (
        <Pagination length={paginationLength} onPageChange={onPageChange} />
      ) : (
        <EmptyPage title={t('superAdmin.emptyPage.employees')} />
      )}
    </div>
  )
);

export { Employees };
