import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { useTitles } from '@merchant/hooks';
import { useEffect } from 'react';
import { getMerchantDetails, unmount } from '@merchant/store/actions';
import { useRouteMatch } from 'react-router';
import { State } from '@store';
import { usePagination } from '@core';

/**
 * <Employees /> props
 */
const useEmployeesProps = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const match = useRouteMatch<{ id: string }>();

  const {
    merchantDetails: { employees },
    paginationLength: { othersLength }
  } = useSelector((state: State) => state.merchant.merchant);

  const { selectedPage, onPageChange } = usePagination();

  const employeesTitles = useTitles('employees');

  useEffect(
    () => () => {
      dispatch(unmount('employees'));
    },
    []
  );

  useEffect(() => {
    dispatch(getMerchantDetails.employees(match.params.id, selectedPage));
  }, [selectedPage, match.params.id]);

  return {
    t,
    employeesTitles,
    employees,
    onPageChange,
    paginationLength: othersLength
  };
};

export { useEmployeesProps };
