import * as React from 'react';
import { Body, Cell, Head, hoc, Row, Table } from '@core';
import { useProfileProps } from './profile.props';
import * as styles from './profile.scss';
import { TitleAndInfo } from '../../components';
import { getFormatPhonNumber } from '@customers/pages';

/**
 * Renders Profile
 */
const Profile = hoc(
  useProfileProps,
  ({ t, uml, iban, profileTitles, vat, contact, venue, email }) => (
    <div>
      <Table>
        <Head>
          <Row>
            {profileTitles?.map((title, index) => (
              <Cell key={index} bold={title.bold} atEnd={title.atEnd}>
                {title.name}
              </Cell>
            ))}
          </Row>
        </Head>
        <Body>
          <Row key={1} className={styles.row}>
            <Cell>
              <TitleAndInfo
                title={t('merchantPanel.profile.form.legalName')}
                info={uml(venue?.legalName)}
              />
            </Cell>
            <Cell>
              <TitleAndInfo
                title={t('merchantPanel.profile.form.contactPersonName')}
                info={uml(contact?.name)}
              />
            </Cell>
            <Cell>
              <TitleAndInfo
                title={t('merchantPanel.profile.form.email')}
                info={email}
              />
            </Cell>
            <Cell>
              <TitleAndInfo
                title={t('merchantPanel.profile.form.vat')}
                info={vat}
              />
            </Cell>
          </Row>
          <Row key={2} className={styles.row}>
            <Cell>
              <TitleAndInfo
                title={t('merchantPanel.profile.form.registrationCode')}
                info={venue?.registrationCode}
              />
            </Cell>
            <Cell>
              <TitleAndInfo
                title={t('merchantPanel.profile.form.contactPersonPhone')}
                info={getFormatPhonNumber(contact?.phone)}
              />
            </Cell>
            <Cell>
              <TitleAndInfo
                title={t('merchantPanel.profile.form.iban')}
                info={iban}
              />
            </Cell>
          </Row>
        </Body>
      </Table>
    </div>
  )
);

export { Profile };
