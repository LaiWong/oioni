import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { useTitles } from '@merchant/hooks';
import { useEffect, useMemo } from 'react';
import { getMerchantDetails, unmount } from '@merchant/store/actions';
import { useRouteMatch } from 'react-router';
import { State } from '@store';
import { useMultiLanguageMapper } from '@core';

/**
 * <Profile /> props
 */
const useProfileProps = () => {
  const { t } = useTranslation();
  const {
    i18n: { language }
  } = useTranslation();
  const dispatch = useDispatch();
  const match = useRouteMatch<{ id: string }>();
  const uml = useMultiLanguageMapper(language, 'en');

  const profileTitles = useTitles('profile');

  const { profile } = useSelector(
    (state: State) => state.merchant.merchant.merchantDetails
  );
  const { venue, contact, business, vat, iban, email } = useMemo(
    () => ({
      venue: profile?.settings.venue,
      contact: profile?.settings.contact,
      business: profile?.settings.business,
      vat: profile?.settings.vat,
      iban: profile?.settings.iban,
      email: profile?.email
    }),
    [match.params.id, profile, language]
  );

  useEffect(
    () => () => {
      dispatch(unmount('profile'));
    },
    []
  );

  useEffect(() => {
    dispatch(getMerchantDetails.profile(match.params.id));
  }, []);

  return {
    t,
    uml,
    profileTitles,
    profile,
    venue,
    contact,
    business,
    vat,
    iban,
    email,
    language
  };
};

export { useProfileProps };
