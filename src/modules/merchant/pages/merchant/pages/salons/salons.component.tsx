import * as React from 'react';
import {
  Body,
  Cell,
  Head,
  hoc,
  Pagination,
  Row,
  Table,
  EmptyPage
} from '@core';
import { useSalonsProps } from './salons.props';
import { UserAvatarAndName } from '../../components';

/**
 * Renders Salons
 */
const Salons = hoc(
  useSalonsProps,
  ({ t, salonsTitles, salons, onPageChange, paginationLength }) => (
    <div>
      <Table>
        <Head>
          <Row>
            {salonsTitles?.map((title, index) => (
              <Cell key={index} bold={title.bold} atEnd={title.atEnd}>
                {title.name}
              </Cell>
            ))}
          </Row>
        </Head>
        <Body>
          {salons?.map(({ name, address, contacts, image_url }, index) => (
            <Row key={index}>
              <Cell bold>
                <UserAvatarAndName name={name} imgUrl={image_url} />
              </Cell>
              <Cell>
                {address?.split(',').slice(0, 1)} <br />{' '}
                {address?.split(',').slice(2, address?.split(',').length)}
              </Cell>
              <Cell>{contacts}</Cell>
            </Row>
          ))}
        </Body>
      </Table>
      {paginationLength ? (
        <Pagination length={paginationLength} onPageChange={onPageChange} />
      ) : (
        <EmptyPage title={t('superAdmin.emptyPage.salons')} />
      )}
    </div>
  )
);

export { Salons };
