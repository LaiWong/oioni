import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { useRouteMatch } from 'react-router';
import { useTitles } from '@merchant/hooks';
import { useEffect } from 'react';
import { getMerchantDetails, unmount } from '@merchant/store/actions';
import { State } from '@store';
import { usePagination } from '@core';

/**
 * <Salons /> props
 */
const useSalonsProps = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const match = useRouteMatch<{ id: string }>();

  const {
    merchantDetails: { salons },
    paginationLength: { othersLength }
  } = useSelector((state: State) => state.merchant.merchant);

  const { selectedPage, onPageChange } = usePagination();

  const salonsTitles = useTitles('salons');

  useEffect(
    () => () => {
      dispatch(unmount('salons'));
    },
    []
  );

  useEffect(() => {
    dispatch(getMerchantDetails.salons(match.params.id, selectedPage));
  }, [selectedPage, match.params.id]);

  return {
    t,
    salonsTitles,
    salons,
    onPageChange,
    paginationLength: othersLength
  };
};

export { useSalonsProps };
