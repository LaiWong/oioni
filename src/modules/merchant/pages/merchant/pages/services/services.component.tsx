import * as React from 'react';
import {
  Body,
  Cell,
  EmptyPage,
  formatPrice,
  Head,
  hoc,
  Pagination,
  Row,
  Table
} from '@core';
import { useServicesProps } from './services.props';

/**
 * Renders Services
 */
const Services = hoc(
  useServicesProps,
  ({ t, servicesTitles, services, match, onPageChange, paginationLength }) => (
    <div>
      <Table>
        <Head>
          <Row>
            {servicesTitles?.map((title, index) => (
              <Cell key={index} atEnd={title.atEnd}>
                {title.name}
              </Cell>
            ))}
          </Row>
        </Head>
        <Body>
          {services?.map(
            (
              { name, settings, category, duration, price, old_price },
              index
            ) => (
              <Row key={index}>
                <Cell bold>
                  {settings?.name[match.params.language] || name}
                </Cell>
                <Cell>
                  {category?.settings?.name[match.params.language] ||
                    category?.name}
                </Cell>
                <Cell>
                  {duration / 60 >= 1 &&
                    `${(duration / 60).toFixed(0)}${t(
                      'merchantPanel.service.time.duration.hour.title'
                    )}`}{' '}
                  {duration % 60 !== 0 &&
                    ` ${duration % 60}${t(
                      'merchantPanel.service.time.duration.shortMinutes'
                    )}`}
                </Cell>
                <Cell bold>
                  {formatPrice(price)}{' '}
                  {old_price !== price && (
                    <span style={{ color: '#f00' }}>%</span>
                  )}
                </Cell>
              </Row>
            )
          )}
        </Body>
      </Table>
      {paginationLength ? (
        <Pagination length={paginationLength} onPageChange={onPageChange} />
      ) : (
        <EmptyPage title={t('superAdmin.emptyPage.services')} />
      )}
    </div>
  )
);

export { Services };
