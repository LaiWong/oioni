import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { useRouteMatch } from 'react-router';
import { useTitles } from '@merchant/hooks';
import { useEffect } from 'react';
import { getMerchantDetails, unmount } from '@merchant/store/actions';
import { State } from '@store';
import { usePagination } from '@core';

/**
 * <Services /> props
 */
const useServicesProps = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const match = useRouteMatch<{ id: string; language: string }>();

  const {
    merchantDetails: { services },
    paginationLength: { othersLength }
  } = useSelector((state: State) => state.merchant.merchant);

  const { selectedPage, onPageChange } = usePagination();

  const servicesTitles = useTitles('services');

  useEffect(
    () => () => {
      dispatch(unmount('services'));
    },
    []
  );

  useEffect(() => {
    dispatch(getMerchantDetails.services(match.params.id, selectedPage));
  }, [selectedPage, match.params.id]);

  return {
    t,
    servicesTitles,
    services,
    match,
    onPageChange,
    paginationLength: othersLength
  };
};

export { useServicesProps };
