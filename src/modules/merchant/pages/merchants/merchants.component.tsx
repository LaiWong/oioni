import * as React from 'react';
import { NavLink } from 'react-router-dom';
import { useMerchantsProps } from './merchants.props';
import * as styles from './merchants.scss';
import {
  Body,
  Button,
  Cell,
  Head,
  hoc,
  Modal,
  Pagination,
  Row,
  Table
} from '@core';
import { MerchantActionModal } from '../../components';
import * as moment from 'moment';
import { sortByElements } from '@merchant/hooks';
import classNames from 'classnames';

/**
 * Renders Merchants
 */
const Merchants = hoc(
  useMerchantsProps,
  ({
    t,
    sortedMerchants,
    sortBy,
    direction,
    onSortClick,
    match,
    merchantsTitles,
    openMerchant,
    setOpenMerchant,
    merchantToDelete,
    setMerchantToDelete,
    submitMerchantDelete,
    onPageChange,
    paginationLength,
    onLinkClick
  }) => (
    <div className={styles.merchants}>
      {merchantToDelete !== -1 && (
        <Modal className={styles.deleteMerchantModal}>
          <h1>Delete merchant?</h1>
          <p>
            Are you sure you want to delete merchant? <br /> You will not be
            able to reverse this action
          </p>
          <div className={styles.buttonsContainer}>
            <Button
              onClick={() => setMerchantToDelete(-1)}
              className={styles.button1}
              theme='teritary'
              size='sm'
            >
              Cancel
            </Button>
            <Button
              onClick={submitMerchantDelete}
              className={styles.button2}
              size='sm'
            >
              Delete
            </Button>
          </div>
        </Modal>
      )}
      <div className={styles.header}>
        <h2 className={styles.title}>
          {t('merchantPanel.merchant.list.title')}
        </h2>
      </div>
      <Table>
        <Head>
          <Row>
            {merchantsTitles?.map((title, index) => (
              <Cell
                key={index}
                className={classNames({ [styles.sortElement]: !!title.name })}
                atEnd={title.atEnd}
                atCenter={title.atCenter}
                bold={title.bold}
                onClick={() => onSortClick(sortByElements[index])}
              >
                {title.name}
                {index === sortByElements.indexOf(sortBy) && (
                  <img
                    className={styles.sortDirection}
                    style={{
                      transform: `rotate(${
                        direction === 'up' ? '90' : '-90'
                      }deg)`
                    }}
                    src={require('img/arrow-left.png')}
                    alt={direction}
                  />
                )}
              </Cell>
            ))}
          </Row>
        </Head>
        <Body>
          {sortedMerchants?.map(
            (
              {
                id,
                created_at,
                users_count,
                services_count,
                merchant_name,
                status
              },
              index
            ) => (
              <Row key={index}>
                <Cell>{moment(created_at).format('HH:MM YYYY-MM-DD')}</Cell>
                <Cell bold>
                  <NavLink
                    to={`${match.url}/${id}/bookings`}
                    onClick={() =>
                      onLinkClick({
                        id,
                        created_at,
                        users_count,
                        services_count,
                        merchant_name,
                        status
                      })
                    }
                  >
                    {merchant_name || 'No name'}
                  </NavLink>
                </Cell>
                <Cell>
                  {services_count && services_count === 0
                    ? 'No'
                    : services_count}{' '}
                  {/* {t('merchantPanel.tabs.services').toLowerCase()} */}
                </Cell>
                <Cell atEnd>{users_count}</Cell>
                <Cell atEnd>
                  <div className={styles.detailButtonContainer}>
                    <Button
                      className={styles.detailButton}
                      onClick={() =>
                        setOpenMerchant(openMerchant !== id ? id : -1)
                      }
                      theme='teritary'
                      size='xs'
                    >
                      &#8230;
                    </Button>
                  </div>
                  {openMerchant === id && (
                    <MerchantActionModal
                      t={t}
                      onMouseLeave={() => setOpenMerchant(-1)}
                      onDeleteMerchant={() => setMerchantToDelete(id)}
                    />
                  )}
                </Cell>
              </Row>
            )
          )}
        </Body>
      </Table>
      <Pagination length={paginationLength} onPageChange={onPageChange} />
    </div>
  )
);

export { Merchants };
