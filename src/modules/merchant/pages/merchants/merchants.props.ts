import { useDispatch, useSelector } from 'react-redux';
import { useMeta, useMultiLanguageMapper, usePagination } from '@core';
import { useEffect, useState } from 'react';
import { State } from '@store';
import {
  getMerchants,
  setMerchantStatus,
  setSelectedMerchant,
  unmount
} from '@merchant/store';
import { useTranslation } from 'react-i18next';
import { useRouteMatch } from 'react-router';
import { useSortMerchants, useTitles } from '@merchant/hooks';
import { Merchant } from '@api';
import { snack } from '@store/snackbar';

/**
 * <Merchants /> props
 */
const useMerchantsProps = () => {
  const { t } = useTranslation();
  const {
    i18n: { language }
  } = useTranslation();
  const dispatch = useDispatch();
  const match = useRouteMatch();

  const {
    merchants,
    paginationLength: { merchantsLength }
  } = useSelector((state: State) => state.merchant.merchant);

  const {
    sortBy,
    sortedMerchants,
    direction,
    onSortClick,
    setSortBy
  } = useSortMerchants(merchants);
  const { selectedPage, onPageChange } = usePagination(() => {
    setSortBy('');
  });

  const [openMerchant, setOpenMerchant] = useState(-1);
  const [merchantToDelete, setMerchantToDelete] = useState(-1);
  const uml = useMultiLanguageMapper(language);

  const merchantsTitles = useTitles('merchants');

  const submitMerchantDelete = () => {
    const { id, status } = merchants.find(({ id }) => id === merchantToDelete);

    setMerchantToDelete(-1);

    if (status === 'disabled') {
      dispatch(snack('Merchant is already disabled'));

      return;
    }

    dispatch(setMerchantStatus(id, 'disabled'));
  };

  const onLinkClick = (merchant: Merchant) => {
    dispatch(setSelectedMerchant(merchant));
  };

  useEffect(
    () => () => {
      dispatch(unmount('merchants'));
    },
    []
  );

  useEffect(() => {
    dispatch(getMerchants({ page: selectedPage }));
  }, [selectedPage]);

  useMeta({ title: t('merchantPanel.merchant.list.meta.title') }, []);

  return {
    t,
    uml,
    openMerchant,
    setOpenMerchant,
    merchantToDelete,
    setMerchantToDelete,
    submitMerchantDelete,
    onLinkClick,
    match,
    merchantsTitles,
    onPageChange,
    paginationLength: merchantsLength,
    sortBy,
    sortedMerchants,
    direction,
    onSortClick
  };
};

export { useMerchantsProps };
