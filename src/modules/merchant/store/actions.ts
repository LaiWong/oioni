import { make } from 'redux-chill';
import { Merchant, MerchantDetails, MerchantFull, SelectedDates } from '@api';

/**
 * Get merchants
 */
const getMerchants = make('[merchant] get')
  .stage((params: { search?: string; page?: number } = {}) => params)
  .stage(
    'success',
    (payload: { total: number; length: number; merchants: Merchant[] }) =>
      payload
  )
  .stage('finish');

/**
 * Get merchant details
 */
const getMerchantDetails = make('[merchant details] get')
  .stage(
    'bookings',
    (
      selectedMerchantId: string,
      page: number,
      selectedDates?: SelectedDates
    ) => ({
      selectedMerchantId,
      page,
      selectedDates
    })
  )
  .stage('services', (selectedMerchantId: string, page: number) => ({
    selectedMerchantId,
    page
  }))
  .stage('salons', (selectedMerchantId: string, page: number) => ({
    selectedMerchantId,
    page
  }))
  .stage('profile', (selectedMerchantId: string) => selectedMerchantId)
  .stage('employees', (selectedMerchantId: string, page: number) => ({
    selectedMerchantId,
    page
  }))
  .stage('success', (payload: MerchantDetails, length?: number) => ({
    payload,
    length
  }));

const setStatus = (id: number, status: MerchantFull['status']) => ({
  id,
  status
});

/**
 * Set merchant status
 */
const setMerchantStatus = make('[merchant] set status')
  .stage(setStatus)
  .stage('success', setStatus)
  .stage('finish');

/**
 * Set selected merchant
 */
const setSelectedMerchant = make('[merchant] set selected')
  .stage((payload: Merchant) => payload)
  .stage('finish');

/**
 * Page unmount
 */
const unmount = make('[merchant] unmount').stage((key: string) => key);

const getRefundBookings = make('[merchant] getRefundBookings').stage('success');

export {
  setMerchantStatus,
  getMerchants,
  getMerchantDetails,
  setSelectedMerchant,
  unmount
};
