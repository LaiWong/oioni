import { reducer } from 'redux-chill';
import { MerchantState } from './state';
import {
  getMerchantDetails,
  getMerchants,
  setMerchantStatus,
  setSelectedMerchant,
  unmount
} from './actions';

/**
 * General state
 */
const merchant = reducer(new MerchantState())
  .on(unmount, (state, key) => {
    state.merchantDetails[key] = null;
    state.paginationLength.othersLength = 1;
  })
  .on(getMerchants.success, (state, { total, merchants, length }) => {
    state.total = total;
    state.merchants = merchants;
    state.paginationLength.merchantsLength = length;
  })
  .on(getMerchantDetails.success, (state, { payload, length }) => {
    state.merchantDetails = { ...state.merchantDetails, ...payload };
    state.paginationLength.othersLength = length;
  })
  .on(setMerchantStatus.success, (state, { status }) => {
    state.selectedMerchant.status = status;

    localStorage.selectedMerchantMerchant = JSON.stringify(
      state.selectedMerchant
    );
  })
  .on(setSelectedMerchant, (state, payload) => {
    state.selectedMerchant = payload;

    localStorage.selectedMerchantMerchant = JSON.stringify(
      state.selectedMerchant
    );
  });

export { merchant };
