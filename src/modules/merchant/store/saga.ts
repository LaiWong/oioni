import { Payload, Saga } from 'redux-chill';
import { getMerchantDetails, getMerchants, setMerchantStatus } from './actions';
import { StoreContext } from '@store/context';
import { call, put } from 'redux-saga/effects';
import { snack } from '@store/snackbar';
import i18next from 'i18next';

class MerchantSaga {
  private perPageCount = 9;

  /**
   * Get merchants
   */
  @Saga(getMerchants)
  public *getMerchants(
    { search, page }: Payload<typeof getMerchants>,
    { merchant }: StoreContext
  ) {
    try {
      const {
        data: {
          result: { total, totalPages, merchants }
        }
      } = yield call(merchant.merchants, {
        search,
        page,
        perPage: this.perPageCount
      });

      yield put(
        getMerchants.success({
          total,
          length: totalPages,
          merchants
        })
      );
    } catch (error) {
      yield put(snack('message.cannotGetMerchants', 'error'));
    }
  }

  /**
   * Get merchant bookings
   */
  @Saga(getMerchantDetails.bookings)
  public *getMerchantBookings(
    {
      selectedMerchantId,
      page,
      selectedDates
    }: Payload<typeof getMerchantDetails.bookings>,
    { merchant }: StoreContext
  ) {
    try {
      const {
        data: {
          result: { totalPages, bookings }
        }
      } = yield call(merchant.merchantDetails, {
        id: +selectedMerchantId,
        page,
        key: 'bookings',
        perPage: this.perPageCount,
        selectedDates,
        excludeDeleted: true
      });

      yield put(getMerchantDetails.success({ bookings }, totalPages));
    } catch (error) {
      yield put(snack('message.cannotGetMerchants', 'error'));
    }
  }

  /**
   * Get merchant salons
   */
  @Saga(getMerchantDetails.salons)
  public *getMerchantSalons(
    { selectedMerchantId, page }: Payload<typeof getMerchantDetails.salons>,
    { merchant }: StoreContext
  ) {
    try {
      const {
        data: {
          result: { totalPages, stores }
        }
      } = yield call(merchant.merchantDetails, {
        id: +selectedMerchantId,
        key: 'stores',
        page,
        perPage: this.perPageCount
      });

      yield put(getMerchantDetails.success({ salons: stores }, totalPages));
    } catch (error) {
      yield put(snack('message.cannotGetMerchants', 'error'));
    }
  }

  /**
   * Get merchant services
   */
  @Saga(getMerchantDetails.services)
  public *getMerchantServices(
    { selectedMerchantId, page }: Payload<typeof getMerchantDetails.services>,
    { merchant }: StoreContext
  ) {
    try {
      const {
        data: {
          result: { products, totalPages }
        }
      } = yield call(merchant.merchantDetails, {
        id: +selectedMerchantId,
        key: 'services',
        page,
        perPage: this.perPageCount
      });

      yield put(getMerchantDetails.success({ services: products }, totalPages));
    } catch (error) {
      yield put(snack('message.cannotGetMerchants', 'error'));
    }
  }

  /**
   * Get merchant employees
   */
  @Saga(getMerchantDetails.employees)
  public *getMerchantEmployees(
    { selectedMerchantId, page }: Payload<typeof getMerchantDetails>,
    { merchant }: StoreContext
  ) {
    try {
      const {
        data: {
          result: { employees, totalPages }
        }
      } = yield call(merchant.merchantDetails, {
        id: selectedMerchantId,
        key: 'employees',
        page,
        perPage: this.perPageCount
      });

      yield put(getMerchantDetails.success({ employees }, totalPages));
    } catch (error) {
      yield put(snack('message.cannotGetMerchants', 'error'));
    }
  }

  /**
   * Get merchant profile
   */
  @Saga(getMerchantDetails.profile)
  public *getMerchantProfile(
    selectedMerchantId: Payload<typeof getMerchantDetails>,
    { merchant }: StoreContext
  ) {
    try {
      const response = yield call(merchant.merchantDetails, {
        id: selectedMerchantId,
        key: ''
      });

      yield put(
        getMerchantDetails.success({ profile: response.data.merchant })
      );
    } catch (error) {
      yield put(snack('message.cannotGetMerchants', 'error'));
    }
  }

  /**
   * Set merchant status
   */
  @Saga(setMerchantStatus)
  public *setMerchantStatus(
    { id, status }: Payload<typeof setMerchantStatus>,
    { merchant }: StoreContext
  ) {
    try {
      yield call(merchant.setStatus, id, status);
      yield put(setMerchantStatus.success(id, status));
      yield put(snack('message.merchantStatusSaved', 'info', { status }));
    } catch (error) {
      yield put(snack('message.cannotSetMerchantStatus', 'error'));
    }
  }
}

export { MerchantSaga };
