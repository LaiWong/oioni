import { Merchant, MerchantDetails } from '@api';

const { selectedMerchantMerchant } = localStorage;

class MerchantState {
  /**
   * Total merchants
   */
  public total: number = 0;

  /**
   * Merchant list
   */
  public merchants: Merchant[] = [];

  /**
   * Merchant details
   */
  public merchantDetails: MerchantDetails = {};

  /**
   * Pagination length
   */
  public paginationLength = {
    merchantsLength: 1,
    othersLength: 1
  };

  /**
   * Selected merchant
   */
  public selectedMerchant: Merchant = selectedMerchantMerchant
    ? JSON.parse(selectedMerchantMerchant)
    : null;
}

export { MerchantState };
