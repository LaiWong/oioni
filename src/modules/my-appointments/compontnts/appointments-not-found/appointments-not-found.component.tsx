import * as React from 'react';
import * as styles from './appointments-not-found.scss';
import { FC } from 'react';
import { useAppointmentsNotFoundProps } from './appointments-not-found.props';
import { hoc } from '@core';

/**
 * Renders AppointmentsNotFound
 */
const AppointmentsNotFound = hoc(useAppointmentsNotFoundProps, ({ t }) => (
  <div className={styles.appointmentsNotFound}>
    <img
      className={styles.image}
      src={require('img/no-appointments.png')}
      alt='not found'
    />

    <div className={styles.text}>
      {t('employeePanel.myBookings.errors.noBookings')}
    </div>
  </div>
));

export { AppointmentsNotFound };
