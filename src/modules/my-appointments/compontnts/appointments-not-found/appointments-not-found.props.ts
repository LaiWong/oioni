import { useTranslation } from 'react-i18next';

const useAppointmentsNotFoundProps = () => {
  const { t } = useTranslation();

  return { t };
};

export { useAppointmentsNotFoundProps };
