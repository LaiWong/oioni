import * as React from 'react';
import * as styles from './appointments-time-list.scss';
import classNames from 'classnames';
import { hoc } from '@core';
import { useAppointmentsTimeList } from './appointments-time-list.props';
import { AppointmentsNotFound } from '../appointments-not-found';
import { pastAppointmentStyles } from './appointments-time-list.data';

/**
 * Renders AppointmentsTimeList
 */
const AppointmentsTimeList = hoc(
  useAppointmentsTimeList,
  ({
    t,
    isTimeLineVisible,
    timeList,
    formattedAppointments,
    timeLineTop,
    isBreakVisible,
    breakTop,
    breakHeight,
    isPast
  }) =>
    formattedAppointments.length ? (
      <div className={classNames(styles.appointmentsTimeList)}>
        <div className={styles.time}>
          {timeList?.map(time => (
            <div className={styles.timeItem} key={time}>
              {time}
            </div>
          ))}
        </div>
        <div className={styles.appointmentGrid}>
          <div>
            {timeList?.map(time => (
              <div className={styles.appointmentGridItem} key={time} />
            ))}
          </div>

          {formattedAppointments?.map(
            ({
              id,
              name,
              customerName,
              top,
              height,
              sizeClassName,
              onClick,
              bgColor,
              color
            }) => (
              <div
                className={classNames(sizeClassName, styles.appointmentTile)}
                onClick={onClick}
                key={id}
                style={{
                  top,
                  height,
                  color: isPast ? pastAppointmentStyles.color : color,
                  background: isPast ? pastAppointmentStyles.bgColor : bgColor
                }}
              >
                <div
                  className={styles.leftLine}
                  style={{
                    background: isPast ? pastAppointmentStyles.color : color
                  }}
                />
                <div className={styles.serviceName}>{name}</div>
                <div className={styles.customerName}>{customerName}</div>
                <div className={styles.appointmentId}>{id}</div>
              </div>
            )
          )}

          {isBreakVisible && (
            <div
              className={styles.break}
              style={{ top: breakTop, height: breakHeight }}
            >
              {t('employeePanel.myBookings.launchBreak')}
            </div>
          )}
        </div>

        {isTimeLineVisible && (
          <div
            className={styles.timeLine}
            style={{
              top: timeLineTop
            }}
          />
        )}
      </div>
    ) : (
      <AppointmentsNotFound />
    )
);

export { AppointmentsTimeList };
