const sizes = {
  scheduleStart: 17,
  minuteHeight: 140 / 60,
  appointmentPadding: 4,
  borderHeight: 1,
  openedHeight: 82
};

const pastAppointmentStyles = {
  color: '#aaaaaa',
  bgColor: '#f4f4f4'
};

export { pastAppointmentStyles, sizes };
