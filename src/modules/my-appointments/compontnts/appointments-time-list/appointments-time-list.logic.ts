import * as styles from './appointments-time-list.scss';
import moment from 'moment';
import { sizes } from './appointments-time-list.data';
import { useMemo, useState } from 'react';
import { Appointment } from '@api';
import { useAppointmentsWithColors } from '@app/hooks';

type FormattedAppointments = {
  top: number;
  height: number;
  name: string;
  customerName: string;
  sizeClassName: string;
  onClick: () => void;
  serviceCategoryId: number;
  id: number;
}[];

const useCalendarAppointments = ({
  appointments,
  scheduleStart,
  scheduleEnd
}) => {
  const [openedAppointmentId, setOpenedAppointmentId] = useState<
    Appointment['id']
  >(null);

  const formattedAppointments: FormattedAppointments = useMemo(
    () =>
      appointments
        .filter(
          ({ booking_date_time, completed_on }) =>
            moment(booking_date_time)
              .tz('UTC')
              .isSameOrAfter(scheduleStart) &&
            moment(completed_on)
              .tz('UTC')
              .isSameOrBefore(scheduleEnd)
        )
        .map(
          ({
            duration,
            name,
            customerName,
            id,
            booking_date_time,
            serviceCategoryId
          }) => {
            const isOpened = id === openedAppointmentId;

            const minutesFromStart = moment(booking_date_time)
              .tz('UTC')
              .diff(scheduleStart, 'minutes');

            const top =
              sizes.scheduleStart +
              minutesFromStart * sizes.minuteHeight +
              sizes.appointmentPadding;

            const height = isOpened
              ? sizes.openedHeight
              : duration * sizes.minuteHeight -
                2 * sizes.appointmentPadding -
                sizes.borderHeight;

            let sizeClassName;
            switch (true) {
              case isOpened:
                sizeClassName = styles.opened;

                break;
              case duration === 15:
                sizeClassName = styles.quarter;

                break;
              case duration === 30:
                sizeClassName = styles.half;

                break;
              default:
                sizeClassName = styles.big;
            }

            const onClick = () => {
              if (duration > 30) return;

              setOpenedAppointmentId(isOpened ? null : id);
            };

            return {
              top,
              height,
              name,
              customerName,
              sizeClassName,
              onClick,
              serviceCategoryId,
              id
            };
          }
        ),
    [appointments, scheduleStart, scheduleEnd, openedAppointmentId]
  );

  const calendarAppointments = useAppointmentsWithColors(formattedAppointments);

  return calendarAppointments;
};

export { useCalendarAppointments };
