import { Appointment, TimeSheet } from '@api';
import moment, { Moment } from 'moment-timezone';
import { useInterval } from '@core';
import { useState, useMemo, useEffect } from 'react';
import { getTimeList } from '@my-appointments/utils';
import { getTime } from '@my-appointments/utils';
import { useTranslation } from 'react-i18next';
import { sizes } from './appointments-time-list.data';
import { useCalendarAppointments } from './appointments-time-list.logic';
import { useSelector } from 'react-redux';
import { State } from '@store';

/**
 * Props
 */
type AppointmentsTimeListProps = {
  /**
   * Selected date
   */
  selectedDate: string;

  /**
   * Appointments
   */
  appointments: Appointment[];

  /**
   * Time sheet
   */
  timeSheet: TimeSheet;
};

const useAppointmentsTimeList = ({
  selectedDate,
  timeSheet,
  appointments
}: AppointmentsTimeListProps) => {
  const { t } = useTranslation();

  const schedule = timeSheet?.schedules.find(
    one => moment(one.date).format('YYYY-MM-DD') === selectedDate
  );
  const blackout = timeSheet?.blackouts?.find(
    one => moment(one.date).format('YYYY-MM-DD') === selectedDate
  );

  const [UTCnow, setUTCNow] = useState<Moment>(moment.utc());
  const isPast = moment(schedule.date)
    .add(1, 'd')
    .isBefore(UTCnow);

  const [scheduleStart, scheduleStartHour, scheduleStartMinute] = useMemo(
    () => getTime(schedule.date, schedule.time_from),
    [schedule.date, schedule.time_from]
  );

  const [scheduleEnd, scheduleEndHour, scheduleEndMinute] = useMemo(
    () => getTime(schedule.date, schedule.time_to),
    [schedule.date, schedule.time_to]
  );

  const formattedAppointments = useCalendarAppointments({
    appointments,
    scheduleStart,
    scheduleEnd
  });

  const timeList = useMemo(
    () =>
      getTimeList({
        scheduleEndHour,
        scheduleEndMinute,
        scheduleStartHour,
        scheduleStartMinute
      }),
    [scheduleEndHour, scheduleEndMinute, scheduleStartHour, scheduleStartMinute]
  );

  const { isBreakVisible, breakTop, breakHeight } = useMemo(() => {
    const isBreakVisible = Boolean(blackout);
    if (!isBreakVisible) return { isBreakVisible };

    const [blackoutStart] = getTime(blackout.date, blackout.time_from);
    const [blackoutEnd] = getTime(blackout.date, blackout.time_to);

    const minuteFromStart = blackoutStart.diff(scheduleStart, 'minutes');
    const minuteFromStartToEnd = blackoutEnd.diff(blackoutStart, 'minutes');

    const breakHeight = minuteFromStartToEnd * sizes.minuteHeight;
    const breakTop = sizes.scheduleStart + minuteFromStart * sizes.minuteHeight;

    return { breakTop, breakHeight, isBreakVisible };
  }, [blackout]);

  const { isTimeLineVisible, timeLineTop } = useMemo(() => {
    const now = moment();

    UTCnow.set('hours', now.get('hours'));
    UTCnow.set('minutes', now.get('minutes'));

    const minuteFromStart = UTCnow.diff(scheduleStart, 'minutes');
    const isTimeLineVisible = UTCnow.isBetween(scheduleStart, scheduleEnd);
    const timeLineTop =
      sizes.scheduleStart + minuteFromStart * sizes.minuteHeight;

    return { isTimeLineVisible, timeLineTop };
  }, [UTCnow, scheduleStart, scheduleEnd]);

  useInterval(() => {
    setUTCNow(moment.utc());
  }, 60000);

  return {
    t,
    timeList,
    isTimeLineVisible,
    formattedAppointments,
    timeLineTop,
    isBreakVisible,
    breakTop,
    breakHeight,
    isPast
  };
};

export { useAppointmentsTimeList };
