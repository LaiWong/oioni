import * as React from 'react';
import * as styles from './date-select.scss';
import classNames from 'classnames';
import { useDateSelect } from './date-select.props';
import { hoc } from '@core';

/**
 * Renders <DateSelect />
 */
const DateSelect = hoc(
  useDateSelect,
  ({ selectedDay, days, daysRef, onDayClick }) => (
    <div className={styles.daySelect}>
      {days.map(({ isPast, dayNumber, dayCaption }) => (
        <div
          className={classNames(styles.selectItem, {
            [styles.pastDate]: isPast,
            [styles.picked]: dayNumber === selectedDay
          })}
          key={dayNumber}
          onClick={() => onDayClick(dayNumber)}
          ref={el => (daysRef.current[dayNumber] = el)}
        >
          <span className={styles.date}>{dayNumber}</span>
          <span className={styles.dayOfWeek}>{dayCaption}</span>
        </div>
      ))}
    </div>
  )
);

export { DateSelect };
