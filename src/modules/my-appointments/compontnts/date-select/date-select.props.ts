import moment from 'moment';
import { useMemo, useRef } from 'react';
import { useMounted, useShortWeekdays } from '@core';

/**
 * Props
 */
type DateSelectProps = {
  /**
   * Value
   */
  value: string;

  /**
   * On change
   */
  onChange: (day: string) => void;
};

const useDateSelect = ({ value, onChange }: DateSelectProps) => {
  const daysRef = useRef<HTMLDivElement[]>([]);

  const shortWeekdays = useShortWeekdays();
  const selectedDay = moment(value).get('date');
  const selectedMonth = moment(value).get('month');
  const selectedYear = moment(value).get('year');

  const days = useMemo(() => {
    const now = moment();
    const date = moment(new Date(selectedYear, selectedMonth, 1));
    const days = [];

    while (date.get('month') === selectedMonth) {
      days.push({
        dayNumber: date.get('date'),
        dayCaption: shortWeekdays[date.get('day')],
        isPast: date.add(1, 'd').isBefore(now)
      });
    }

    return days;
  }, [selectedMonth, selectedYear]);

  const onDayClick = (dayNumber: number) => {
    onChange(
      moment(new Date(selectedYear, selectedMonth, dayNumber)).format(
        'YYYY-MM-DD'
      )
    );
  };

  useMounted(() => {
    daysRef.current[selectedDay].scrollIntoView({
      block: 'end',
      inline: 'center'
    });
  });

  return {
    selectedDay,
    days,
    daysRef,
    onDayClick
  };
};

export { useDateSelect };
