import * as React from 'react';
import * as styles from './month-select.scss';
import { Icon } from '@core/components/icon';
import classNames from 'classnames';
import { useMonthSelect } from './month-select.props';
import { hoc } from '@core';
import moment from 'moment';
import Picker from 'react-day-picker';

/**
 * Renders MonthSelect
 */
const MonthSelect = hoc(
  useMonthSelect,
  ({
    title,
    selectedDate,
    calendarDate,
    shortWeekdays,
    monthAppointments,
    onMonthClick,
    onDateChange
  }) => (
    <div className={styles.monthSelect}>
      <Picker
        fixedWeeks
        month={calendarDate}
        weekdaysShort={shortWeekdays}
        className={styles.calendar}
        classNames={styles as any}
        firstDayOfWeek={1}
        captionElement={() => null}
        renderDay={date => {
          const formattedDate = moment(date).format('YYYY-MM-DD');
          const caption = moment(date).get('date');
          const isPast = moment(date)
            .add(1, 'd')
            .isBefore(moment());
          const hasAppointments = monthAppointments.some(
            monthAppointment => monthAppointment[formattedDate]
          );
          const isPicked = selectedDate === formattedDate;

          return (
            <div
              className={classNames(styles.calendarDate, {
                [styles.hasAppointments]: hasAppointments,
                [styles.picked]: isPicked,
                [styles.past]: isPast
              })}
              onClick={() => onDateChange(formattedDate)}
            >
              {caption}
            </div>
          );
        }}
        navbarElement={({ previousMonth, nextMonth }) => (
          <div className={styles.navigation}>
            <Icon
              className={styles.icon}
              name='arrow-left'
              onClick={() => onMonthClick(previousMonth)}
            />
            <span>{title}</span>
            <Icon
              className={styles.icon}
              name='arrow-right'
              onClick={() => onMonthClick(nextMonth)}
            />
          </div>
        )}
      />
    </div>
  )
);

export { MonthSelect };
