import { useDispatch, useSelector } from 'react-redux';
import moment from 'moment';
import { useMemo } from 'react';
import { State } from '@store';
import { useMonths, useShortWeekdays } from '@core';

/**
 * Props
 */
type MonthSelectProps = {
  /**
   * Value
   */
  value: string;

  /**
   * Selected date
   */
  selectedDate: string;

  /**
   * On change
   */
  onChange: (month: string) => void;

  /**
   * On date change
   */
  onDateChange: (date: string) => void;
};

const useMonthSelect = ({ value, onChange }: MonthSelectProps) => {
  const { monthAppointments } = useSelector(
    (state: State) => state.employee.myAppointments
  );

  const months = useMonths();
  const shortWeekdays = useShortWeekdays();
  const calendarDate = useMemo(() => moment(value).toDate(), [value]);
  const selectedMonth = moment(value).get('month');
  const selectedYear = moment(value).get('year');

  const title = `${months[selectedMonth]} ${selectedYear}`;

  const onMonthClick = (month: Date) => {
    onChange(moment(month).format('YYYY-MM'));
  };

  return {
    title,
    calendarDate,
    shortWeekdays,
    monthAppointments,
    onMonthClick
  };
};

export { useMonthSelect };
