import { MyAppointmentsTab } from '@api';
import { useTranslation } from 'react-i18next';

const useMyAppointmentsTabs = () => {
  const { t } = useTranslation();

  return [
    { title: t('employeePanel.myBookings.day'), id: MyAppointmentsTab.day },
    { title: t('employeePanel.myBookings.month'), id: MyAppointmentsTab.month }
  ];
};

export { useMyAppointmentsTabs };
