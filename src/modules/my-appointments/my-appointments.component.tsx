import * as React from 'react';
import * as styles from './my-appointments.scss';
import { AppointmentsTimeList, DateSelect, MonthSelect } from './compontnts';
import classNames from 'classnames';
import { useMyAppointments } from './my-appointments.props';
import { hoc } from '@core';
import { MyAppointmentsTab } from '@api';

/**
 * Renders MyAppointments
 */
const MyAppointments = hoc(
  useMyAppointments,
  ({
    t,
    hasSchedule,
    currentTab,
    date,
    month,
    myAppointmentsTabs,
    dateAppointments,
    timeSheet,
    ready,
    onTabClick,
    onDateChange,
    onMonthChange
  }) => (
    <div className={styles.myAppointments}>
      <div className={styles.wrapper}>
        <h1>{t('employeePanel.myBookings.title')}</h1>

        <div className={styles.select}>
          {myAppointmentsTabs.map(({ id, title }) => (
            <div
              key={id}
              onClick={() => onTabClick(id)}
              className={classNames(styles.selectItem, {
                [styles.active]: id === currentTab
              })}
            >
              {title}
            </div>
          ))}
        </div>
      </div>

      {currentTab === MyAppointmentsTab.day ? (
        <DateSelect value={date} onChange={onDateChange} />
      ) : (
        <MonthSelect
          value={month}
          onChange={onMonthChange}
          selectedDate={date}
          onDateChange={onDateChange}
        />
      )}

      {ready &&
        (hasSchedule ? (
          <AppointmentsTimeList
            selectedDate={date}
            appointments={dateAppointments}
            timeSheet={timeSheet}
          />
        ) : (
          <div className={styles.noSchedule}>
            {t('employeePanel.myBookings.errors.noSchedule')}
          </div>
        ))}
    </div>
  )
);

export { MyAppointments };
