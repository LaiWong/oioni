import { useEffect, useState } from 'react';
import moment from 'moment';
import { MyAppointmentsTab } from '@api';
import { useDispatch, useSelector } from 'react-redux';
import { State } from '@store';
import {
  getDateAppointments,
  getMonthAppointments,
  getTimeSheet
} from './store';
import { useTranslation } from 'react-i18next';
import { useMyAppointmentsTabs } from './hooks';

const useMyAppointments = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const {
    dateAppointments,
    timeSheet,
    fetching: {
      dateAppointments: dateAppointmentsFetching,
      timeSheet: timeSheetFetching
    }
  } = useSelector((state: State) => state.employee.myAppointments);

  const myAppointmentsTabs = useMyAppointmentsTabs();
  const ready = !dateAppointmentsFetching && !timeSheetFetching;
  const [currentTab, setCurrentTab] = useState(MyAppointmentsTab.day);
  const [date, setDate] = useState(moment().format('YYYY-MM-DD'));
  const [month, setMonth] = useState(moment().format('YYYY-MM'));
  const hasSchedule = timeSheet?.schedules?.find(
    (one) => moment(one.date).format('YYYY-MM-DD') === date
  );

  const onTabClick = (tab: MyAppointmentsTab) => {
    setCurrentTab(tab);
  };

  const onDateChange = (date: string) => {
    setDate(date);
    setCurrentTab(MyAppointmentsTab.day);
  };

  const onMonthChange = (month: string) => {
    setMonth(month);
  };

  useEffect(() => {
    dispatch(getDateAppointments({ date }));
    dispatch(getTimeSheet({ date }));
  }, [date]);

  useEffect(() => {
    dispatch(getMonthAppointments({ month }));
  }, [month]);

  return {
    t,
    hasSchedule,
    currentTab,
    date,
    month,
    myAppointmentsTabs,
    dateAppointments,
    timeSheet,
    ready,
    onTabClick,
    onDateChange,
    onMonthChange
  };
};

export { useMyAppointments };
