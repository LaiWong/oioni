import { make } from 'redux-chill';
import { Appointment, TimeSheet } from '@api';

/**
 * Get date appointments
 */
const getDateAppointments = make('[my-appoinments] get date appointments')
  .stage((payload: { date: string }) => payload)
  .stage('success', (dateAppointments: Appointment[]) => dateAppointments);

/**
 * Get time sheet
 */
const getTimeSheet = make('[profile] get schedule')
  .stage((payload: { date: string }) => payload)
  .stage('success', (timeSheet: TimeSheet) => timeSheet);

/**
 * Get month appointments
 */
const getMonthAppointments = make('[profile] get month appointments')
  .stage((payload: { month: string }) => payload)
  .stage('success', (monthAppointments: Object[]) => monthAppointments);

export { getDateAppointments, getTimeSheet, getMonthAppointments };
