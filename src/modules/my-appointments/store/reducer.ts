import { reducer } from 'redux-chill';
import { MyAppointmentsState } from './state';
import { appointments } from '@appointments';
import {
  getDateAppointments,
  getMonthAppointments,
  getTimeSheet
} from './actions';

/**
 * General state
 */
const myAppointments = reducer(new MyAppointmentsState())
  .on(getDateAppointments, state => {
    state.fetching.dateAppointments = true;
  })
  .on(getDateAppointments.success, (state, dateAppointments) => {
    state.dateAppointments = dateAppointments;
    state.fetching.dateAppointments = false;
  })
  .on(getTimeSheet, state => {
    state.fetching.timeSheet = true;
  })
  .on(getTimeSheet.success, (state, timeSheet) => {
    state.timeSheet = timeSheet;
    state.fetching.timeSheet = false;
  })
  .on(getMonthAppointments, state => {
    state.fetching.monthAppointments = true;
  })
  .on(getMonthAppointments.success, (state, monthAppointments) => {
    state.monthAppointments = monthAppointments;
    state.fetching.monthAppointments = false;
  });

export { myAppointments };
