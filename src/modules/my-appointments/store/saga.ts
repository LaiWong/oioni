import { Saga, Payload } from 'redux-chill';
import {
  getDateAppointments,
  getMonthAppointments,
  getTimeSheet
} from './actions';
import { StoreContext } from '@store/context';
import { put, call, select } from 'redux-saga/effects';
import { State } from '@store';
import { snack } from '@store/snackbar';
import i18next from 'i18next';

class MyAppointmentsSaga {
  /**
   * Get date appointments
   */
  @Saga(getDateAppointments)
  public *getDateAppointments(
    { date }: Payload<typeof getDateAppointments>,
    { myAppointments }: StoreContext
  ) {
    try {
      const { id } = yield select((state: State) => state.general.employeeUser);

      const {
        data: { result }
      } = yield call(myAppointments.get, id, { date });

      yield put(getDateAppointments.success(result));
    } catch (e) {
      yield put(snack('message.cannotGetDateBookings', 'error'));
    }
  }

  /**
   * Get schedule
   */
  @Saga(getTimeSheet)
  public *getTimeSheet(
    { date }: Payload<typeof getTimeSheet>,
    { myAppointments }: StoreContext
  ) {
    try {
      const { id } = yield select((state: State) => state.general.employeeUser);

      const {
        data: {
          result: { scheduleResults }
        }
      } = yield call(myAppointments.getTimeSheet, { employee_id: id, date });

      yield put(getTimeSheet.success(scheduleResults[0]));
    } catch (e) {
      yield put(snack('message.cannotGetTimeSheet', 'error'));
    }
  }

  /**
   * Get month appointments
   */
  @Saga(getMonthAppointments)
  public *getMonthAppointments(
    { month }: Payload<typeof getMonthAppointments>,
    { myAppointments }: StoreContext
  ) {
    try {
      const { id } = yield select((state: State) => state.general.employeeUser);

      const {
        data: { result }
      } = yield call(myAppointments.get, id, { month });

      yield put(getMonthAppointments.success(result));
    } catch (e) {
      yield put(snack('message.cannotGetMonthBookings', 'error'));
    }
  }
}

export { MyAppointmentsSaga };
