import { Appointment, TimeSheet } from '@api';

class MyAppointmentsState {
  /**
   * Date appointments
   */
  dateAppointments: Appointment[] = [];

  /**
   * Time sheet
   */
  timeSheet: TimeSheet;

  /**
   * Month appointments
   */
  monthAppointments: Object[] = [];

  /**
   * Fetching state
   */
  public fetching = {
    dateAppointments: false,
    monthAppointments: false,
    timeSheet: false
  };
}

export { MyAppointmentsState };
