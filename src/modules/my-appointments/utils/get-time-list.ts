const getTimeList = ({
  scheduleEndHour,
  scheduleStartHour,
  scheduleEndMinute,
  scheduleStartMinute
}) =>
  scheduleEndHour &&
  Array(
    (Number(scheduleEndHour) - Number(scheduleStartHour)) * 2 +
      (Number(scheduleStartMinute) + Number(scheduleEndMinute)) / 30 +
      1
  )
    .fill(0)
    .map(
      (_, index) =>
        `${Math.floor(Number(scheduleStartHour) + 0.5 * index)}:${
          Number(scheduleStartMinute) == 0
            ? index % 2 === 0
              ? '00'
              : '30'
            : index % 2 === 0
            ? '30'
            : '00'
        }`
    );

export { getTimeList };
