import moment, { Moment } from 'moment-timezone';

const getTime = (date: Date, time: string): [Moment, number, number] => {
  const [hour, minute] = time?.split(':').map(one => Number(one));

  const momentDate = moment(date)
    .tz('UTC')
    .set('hours', hour)
    .set('minutes', minute);

  return [momentDate, hour, minute];
};

export { getTime };
