import { Button, Field, Form, FormError, Group, hoc } from '@core';
import * as React from 'react';
import { useChangeModalProps } from './change-modal.props';
import * as styles from './change-modal.scss';
import classNames from 'classnames';

/**
 * Renders Change modal
 */
const ChangeModal = hoc(
  useChangeModalProps,
  ({
    form,
    error,
    onClose,
    onEyeClick,
    isShown,
    isEmailChanging,
    isPasswordChanging
  }) => (
    <Form className={styles.changeModal} form={form}>
      <div className={styles.header}>
        <h2>{isEmailChanging ? 'Change email' : 'Create new password'}</h2>
        {isEmailChanging && (
          <h4>
            To change your email, enter your <br /> password first
          </h4>
        )}
      </div>

      <div className={styles.container}>
        <Group className={styles.fields}>
          <Field.Input
            name='password'
            label={`${isEmailChanging ? 'Enter' : 'New'} password`}
            type={isShown.old ? 'text' : 'password'}
            disabled={false}
          />
        </Group>
        <img
          className={styles.eye}
          src={require('img/eye-icon.png')}
          alt='Eye icon'
          onClick={() => onEyeClick('old')}
        />
      </div>

      <div className={styles.container}>
        <Group className={styles.fields}>
          <Field.Input
            name={isEmailChanging ? 'email' : 'newPassword'}
            label={`${isEmailChanging ? 'New email' : 'Confirm new password'}`}
            type={isEmailChanging || isShown.new ? 'text' : 'password'}
            disabled={false}
          />
        </Group>
        {isPasswordChanging && (
          <img
            className={styles.eye}
            src={require('img/eye-icon.png')}
            alt='Eye icon'
            onClick={() => onEyeClick('new')}
          />
        )}
      </div>

      <FormError>{error}</FormError>

      <div className={styles.buttons}>
        <div
          className={classNames(styles.button, styles.buttonCustom)}
          onClick={onClose}
        >
          <p>Cancel</p>
        </div>

        <Button
          className={styles.button}
          theme='primary'
          size='md'
          type='submit'
        >
          Save
        </Button>
      </div>
      <p className={styles.title}>
        If you have any questions, please call us <br />{' '}
        <span>+372 56822602</span>, or send email <br />{' '}
        <span>info@charme.ee</span>. We will answer you as <br /> soon as
        possible
      </p>
    </Form>
  )
);

export { ChangeModal };
