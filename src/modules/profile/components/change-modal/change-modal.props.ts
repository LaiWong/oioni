import { useDispatch, useSelector } from 'react-redux';
import { State } from '@store';
import { useFormik } from 'formik';
import { formats, useMeta } from '@core';
import { useTranslation } from 'react-i18next';
import { object, string } from 'yup';
import { changeEmail, changePassword } from '@profile/store';
import { useMemo, useState } from 'react';
import { snack } from '@store/snackbar';

/**
 * Profile props
 */
const useChangeModalProps = (props: {
  onClose?: () => void;
  onSave?: () => void;
  changing?: string;
  token?: string;
  merchantId?: string;
}) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();

  const { error, oldEmail } = useSelector(
    ({
      merchant: {
        profile: { merchant, error }
      },
      auth: { isEmployee, isMerchant }
    }: State) => ({
      error,
      oldEmail: merchant?.email,
      isEmployee,
      isMerchant
    })
  );

  const [isShown, setIsShown] = useState({ old: false, new: false });
  const [timer, setTimer] = useState<any>();

  const { isEmailChanging, isPasswordChanging } = useMemo(
    () => ({
      isEmailChanging: props.changing.toLowerCase() === 'email',
      isPasswordChanging: props.changing.toLowerCase() === 'password'
    }),
    [props.changing]
  );

  const form = useFormik({
    enableReinitialize: true,
    initialValues: {
      password: '',
      email: '',
      newPassword: ''
    },
    onSubmit: ({ password, newPassword, email }, { setSubmitting }) => {
      if (isPasswordChanging) {
        const { token, merchantId } = props;

        if (password !== newPassword) {
          form.setErrors({ newPassword: "Passwords don't match" });
          return;
        }

        dispatch(
          changePassword(
            {
              email: oldEmail,
              newPassword,
              password,
              token,
              merchantId
            },
            () => {
              setSubmitting(true);
              props.onSave();
            }
          )
        );
      }

      if (isEmailChanging) {
        dispatch(
          changeEmail({ oldEmail, email, password }, () => {
            setSubmitting(true);
            props.onClose();
            dispatch(snack('message.changeEmailSentLink'));
          })
        );
      }
    },
    validationSchema: object({
      password: isPasswordChanging
        ? string()
            .required('New password is required')
            .matches(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$/, {
              message:
                'Minimum eight characters, at least one uppercase letter, one lowercase letter and one number'
            })
        : string().required('Password is required'),
      email:
        isEmailChanging &&
        string()
          .required('Email is required')
          .test('email', 'Invalid Email', value => formats.email.check(value)),
      newPassword:
        isPasswordChanging &&
        string()
          .matches(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$/, {
            message:
              'Minimum eight characters, at least one uppercase letter, one lowercase letter and one number'
          })
          .required('Confirm your password')
    }),
    validateOnChange: true
  });

  const onEyeClick = (key: string) => {
    clearTimeout(timer);

    setIsShown({ ...isShown, [key]: true });

    const t = setTimeout(() => setIsShown({ ...isShown, [key]: false }), 2000);
    setTimer(t);
  };

  useMeta({ title: t('auth.registration.meta.title') }, []);

  return {
    t,
    form,
    error,
    isShown,
    onEyeClick,
    isEmailChanging,
    isPasswordChanging,
    ...props
  };
};

export { useChangeModalProps };
