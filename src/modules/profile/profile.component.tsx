import {
  Button,
  Field,
  Form,
  FormError,
  Group,
  hoc,
  Language,
  Modal
} from '@core';
import React, { Fragment } from 'react';
import { useProfileProps } from './profile.props';
import * as styles from './profile.scss';
import classNames from 'classnames';
import { ChangeModal } from '@profile/components';
import { Trans } from 'react-i18next';

/**
 * Renders Profile
 */
const Profile = hoc(
  useProfileProps,
  ({
    t,
    ml,
    form,
    language,
    onLanguageChange,
    changingEmail,
    onChangeEmailClick,
    onPasswordChangeClick,
    emailVerified,
    changeDisabled,
    isShown,
    onEyeClick,
    profileError,
    merchant,
    onVeryfyEmail
  }) => (
    <Fragment>
      {changingEmail && (
        <Modal className={styles.modal}>
          <ChangeModal onClose={onChangeEmailClick} changing={'email'} />
        </Modal>
      )}
      <Form className={styles.profile} form={form}>
        <div className={styles.header}>
          <h2>{t('merchantPanel.tabs.profile')}</h2>
        </div>

        <div className={styles.fields}>
          <Group
            title={`${t('auth.registration.form.general')}*`}
            titleClassName={styles.groupTitle}
          >
            <Field.Input
              name='merchant.merchant_name'
              label={t('auth.registration.form.merchantName')}
            />
            <Field.Input
              className={styles.credentials}
              name='merchant.phone'
              label={t('auth.registration.form.phone')}
              disabled
            />
            <Field.Input
              name={ml`merchant.settings.contact.name`}
              label={t('auth.registration.form.contactPersonName')}
            />
          </Group>

          <div className={styles.venue}>
            <Group
              title={t('auth.registration.form.loginInfo')}
              titleClassName={styles.groupTitle}
            >
              <Field.Input
                className={styles.credentials}
                name='merchant.email'
                label={t('auth.registration.form.email')}
                disabled={changeDisabled}
              />
              <Field.Input
                className={styles.credentials}
                type={isShown ? 'text' : 'password'}
                name='merchant.password'
                value={
                  changeDisabled
                    ? '00000000000000'
                    : form.values.merchant.password
                }
                label={t('auth.registration.form.password')}
                disabled={changeDisabled}
              />
            </Group>
            {!changeDisabled && (
              <img
                className={styles.eye}
                src={require('img/eye-icon.png')}
                alt='Eye icon'
                onClick={onEyeClick}
              />
            )}
            {merchant && !emailVerified && (
              <Trans
                i18nKey='auth.registration.form.confimEmail'
                components={[
                  <p
                    key='0'
                    className={classNames(styles.confirm, styles.title)}
                  >
                    <span key='1' onClick={onVeryfyEmail} />
                  </p>
                ]}
              />
            )}
            {changeDisabled && (
              <Fragment>
                <p
                  className={classNames(styles.email, styles.title)}
                  onClick={onChangeEmailClick}
                >
                  {t('auth.registration.form.changeEmail')}
                </p>
                <p
                  className={classNames(styles.password, styles.title)}
                  onClick={onPasswordChangeClick}
                >
                  {t('auth.registration.form.changePassword')}
                </p>
              </Fragment>
            )}
          </div>

          <div className={styles.venue}>
            <div className={styles.language}>
              <Language value={language} onChange={onLanguageChange} />
            </div>
            <Group
              title={t('auth.registration.form.venueName')}
              titleClassName={styles.groupTitle}
            >
              <Field.Input
                name={ml`merchant.settings.venue.legalName`}
                label={t('auth.registration.form.legalName')}
              />
              <Field.Input
                name='merchant.settings.venue.registrationCode'
                label={t('auth.registration.form.registrationCode')}
              />
            </Group>
          </div>

          <Group
            title={t('auth.registration.form.bankDetails')}
            titleClassName={styles.groupTitle}
          >
            <Field.Input
              name='merchant.settings.vat'
              label={t('auth.registration.form.vat')}
            />
            <Field.Input name='merchant.settings.iban' label='IBAN' />
          </Group>
        </div>

        <FormError>{profileError}</FormError>

        <Button
          className={styles.button}
          theme={form.isValid ? 'primary' : 'secondary'}
          size='md'
          type='submit'
        >
          {t('auth.registration.form.btn.save')}
        </Button>
      </Form>
    </Fragment>
  )
);

export { Profile };
