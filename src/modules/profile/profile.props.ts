import { useDispatch, useSelector } from 'react-redux';
import { State } from '@store';
import { useFormik } from 'formik';
import { useMeta, useMultiLanguage } from '@core';
import { Language, MLString, ProfileSave } from '@api';
import { useTranslation } from 'react-i18next';
import { useEffect, useMemo, useState } from 'react';
import {
  getProfile,
  profileSave,
  sendLinkForChangingPassword
} from '@profile/store';
import { profileSchema } from '@profile/validation';
import { navigate } from '@store/router';
import { useRouteMatch } from 'react-router';
import { snack } from '@store/snackbar';

/**
 * Profile props
 */
const useProfileProps = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();

  const {
    params: { language: lang }
  } = useRouteMatch<{ language: Language }>();

  const [language, onLanguageChange, ml] = useMultiLanguage(lang);

  const {
    phone,
    countryId,
    merchantName,
    settings,
    id,
    emailVerified,
    merchant,
    profileError
  } = useSelector(
    ({
      auth: { countryId, login, merchantName },
      general,
      merchant: {
        profile: { merchant, error }
      }
    }: State) => ({
      countryId: merchant?.country_id || countryId,
      phone: merchant?.phone || general.phone || login,
      merchantName: merchant?.merchant_name || merchantName,
      settings: merchant?.settings || {},
      id: merchant?.id,
      emailVerified: merchant?.emailVerified,
      merchant,
      profileError: error
    })
  );

  const [changingEmail, setChangingEmail] = useState(false);
  const [isShown, setIsShown] = useState(false);
  const [timer, setTimer] = useState<any>();

  const form = useFormik<ProfileSave>({
    enableReinitialize: true,
    initialValues: {
      merchant: {
        id,
        phone,
        merchant_name: merchantName,
        country_id: countryId,
        settings: {
          venue: {
            registrationCode: settings.venue?.registrationCode,
            legalName: settings.venue?.legalName || new MLString()
          },
          contact: {
            phone,
            name: settings.contact?.name || new MLString()
          },
          business: {
            phone,
            email: merchant?.email
          },
          vat: settings.vat || '',
          iban: settings.iban || ''
        },
        password: '',
        email: merchant?.email || ''
      }
    },
    onSubmit: ({ merchant }, { setSubmitting }) => {
      dispatch(
        profileSave({ merchant }, () => {
          merchant.emailVerification &&
            dispatch(snack('message.verificationEmailLink'));
          setSubmitting(false);
        })
      );
    },
    validationSchema: profileSchema(!!merchant),
    validateOnChange: true
  });

  const changeDisabled = useMemo(() => !!merchant?.email, [merchant]);

  const onChangeEmailClick = () => {
    setChangingEmail(!changingEmail);
  };

  const onPasswordChangeClick = () => {
    dispatch(sendLinkForChangingPassword({ email: merchant?.email }));
  };

  const onEyeClick = () => {
    clearTimeout(timer);

    setIsShown(true);

    const t = setTimeout(() => setIsShown(false), 2000);
    setTimer(t);
  };

  useMeta({ title: t('auth.registration.meta.title') }, []);

  useEffect(() => {
    if (!localStorage.getItem('idtoken')) {
      if (!localStorage.getItem('login')) {
        dispatch(navigate('/'));
      }
      return;
    }

    dispatch(getProfile());
  }, []);

  useEffect(() => {
    form.setFieldValue(
      'merchant.settings.contact.phone',
      form.values.merchant.phone
    );

    form.setFieldValue(
      'merchant.settings.business.phone',
      form.values.merchant.phone
    );

    form.setFieldValue(
      'merchant.settings.business.email',
      form.values.merchant.email
    );
  }, [form.values]);

  const onVeryfyEmail = () => {
    form.setFieldValue('merchant.emailVerification', true);
    form.submitForm();
  };

  return {
    t,
    ml,
    form,
    language,
    onLanguageChange,
    onChangeEmailClick,
    onPasswordChangeClick,
    onEyeClick,
    changingEmail,
    emailVerified,
    changeDisabled,
    isShown,
    profileError,
    merchant,
    onVeryfyEmail
  };
};

export { useProfileProps };
