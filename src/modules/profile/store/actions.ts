import { make } from 'redux-chill';
import { MerchantFull, ProfileSave } from '@api';

/**
 * Profile save
 */
const profileSave = make('[auth] profile register')
  .stage((data: ProfileSave, callback: () => any) => ({ data, callback }))
  .stage('success', (data: { merchant: MerchantFull }) => ({ data }))
  .stage('failure', (error: string) => error)
  .stage('finish');

/**
 * Get profile
 */
const getProfile = make('[profile] get')
  .stage('success', (merchant: MerchantFull) => merchant)
  .stage('failure')
  .stage('finish');

/**
 * Change password
 */
const changePassword = make('[profile] change password')
  .stage(
    (
      data: {
        email: string;
        password: string;
        newPassword: string;
        token: string;
        merchantId: string;
      },
      callback
    ) => ({
      data,
      callback
    })
  )
  .stage('success')
  .stage('failure', (error: string) => error)
  .stage('finish');

/**
 * Change email
 */
const changeEmail = make('[profile] change email')
  .stage(
    (
      data: { oldEmail: string; email: string; password: string },
      callback
    ) => ({
      data,
      callback
    })
  )
  .stage('success')
  .stage('failure', (error: string) => error)
  .stage('finish');

/**
 * Send link for changing password
 */
const sendLinkForChangingPassword = make('[profile] send link')
  .stage((data: { email: string }) => ({ data }))
  .stage('success')
  .stage('failure', (error: string) => error);

export {
  getProfile,
  changePassword,
  changeEmail,
  sendLinkForChangingPassword,
  profileSave
};
