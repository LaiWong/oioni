import { reducer } from 'redux-chill';
import { ProfileState } from './state';
import {
  changePassword,
  getProfile,
  sendLinkForChangingPassword,
  profileSave
} from './actions';

const profile = reducer(new ProfileState())
  .on(getProfile, (state) => {
    state.fetching = true;
  })
  .on(getProfile.success, (state, merchant) => {
    state.merchant = merchant;
  })
  .on(getProfile.finish, (state) => {
    state.fetching = false;
  })
  .on([changePassword, changePassword.success], (state) => {
    state.error = null;
  })
  .on(profileSave.failure, (state, error) => {
    state.error = error;
  })
  .on(profileSave.success, (state) => {
    state.error = null;
  })
  .on(changePassword.failure, (state, error) => {
    state.error = error;
  })
  .on(
    [sendLinkForChangingPassword, sendLinkForChangingPassword.success],
    (state) => {
      state.error = null;
    }
  )
  .on(sendLinkForChangingPassword.failure, (state, error) => {
    state.error = error;
  });

export { profile };
