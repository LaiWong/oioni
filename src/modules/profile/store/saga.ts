import { Payload, Saga } from 'redux-chill';
import {
  changeEmail,
  changePassword,
  getProfile,
  profileSave,
  sendLinkForChangingPassword
} from './actions';
import { StoreContext } from '@store/context';
import { call, put, select } from 'redux-saga/effects';
import { State } from '@store';
import { snack } from '@store/snackbar';
import i18next from 'i18next';
import { MerchantFull, ProfileSave } from '@api';
import { navigate } from '@store/router';

const makeMerchantForSave = (
  merchant: MerchantFull,
  changes: ProfileSave
): ProfileSave => ({
  merchant:
    changes?.merchant?.password !== ''
      ? {
          phone: changes?.merchant?.phone || merchant?.phone,
          merchant_name:
            changes?.merchant?.merchant_name || merchant?.merchant_name,
          country_id: changes?.merchant?.country_id || merchant?.country_id,
          settings: {
            venue: {
              registrationCode: merchant?.settings?.venue?.registrationCode,
              legalName: merchant?.settings?.venue?.legalName,
              ...changes?.merchant?.settings?.venue
            },
            contact: {
              phone: merchant?.settings?.contact?.phone,
              name: merchant?.settings?.contact?.name,
              ...changes?.merchant?.settings?.contact
            },
            business: {
              phone: merchant?.settings?.business?.phone,
              email: merchant?.settings?.business?.email,
              ...changes?.merchant?.settings?.business
            },
            vat: changes?.merchant?.settings?.vat || merchant?.settings?.vat,
            iban: changes?.merchant?.settings?.iban || merchant?.settings?.iban
          },
          email: changes?.merchant?.email || merchant?.email,
          password: changes?.merchant?.password
        }
      : {
          phone: changes?.merchant?.phone || merchant?.phone,
          merchant_name:
            changes?.merchant?.merchant_name || merchant?.merchant_name,
          country_id: changes?.merchant?.country_id || merchant?.country_id,
          settings: {
            venue: {
              registrationCode: merchant?.settings?.venue?.registrationCode,
              legalName: merchant?.settings?.venue?.legalName,
              ...changes?.merchant?.settings?.venue
            },
            contact: {
              phone: merchant?.settings?.contact?.phone,
              name: merchant?.settings?.contact?.name,
              ...changes?.merchant?.settings?.contact
            },
            business: {
              phone: merchant?.settings?.business?.phone,
              email: merchant?.settings?.business?.email,
              ...changes?.merchant?.settings?.business
            },
            vat: changes?.merchant?.settings?.vat || merchant?.settings?.vat,
            iban: changes?.merchant?.settings?.iban || merchant?.settings?.iban
          },
          email: changes?.merchant?.email || merchant?.email,
          emailVerification: changes.merchant.emailVerification
        }
});

class ProfileSaga {
  /**
   * Save merchant
   */
  @Saga(profileSave)
  public *profileRegister(
    { data, callback }: Payload<typeof profileSave>,
    { merchant }: StoreContext
  ) {
    try {
      const {
        merchant: { profile }
      }: State = yield select();

      const merchantToSave = yield call(
        makeMerchantForSave,
        profile.merchant,
        data
      );

      const response = yield call(merchant.save, merchantToSave);

      if (
        Object.keys(response.data.merchant).includes('success') &&
        !response.data.merchant.success
      ) {
        yield put(profileSave.failure(response.data.merchant.message));
        return;
      }

      yield put(profileSave.success(response.data));
      if (!data.merchant.emailVerification) yield put(navigate('/'));
    } catch (error) {
      yield put(profileSave.failure('message.somethingWentWrong'));
    } finally {
      yield call(callback);
    }
  }

  /**
   * Get profile
   */
  @Saga(getProfile)
  public *get(_, { merchant }: StoreContext) {
    try {
      const {
        data: { merchant: user }
      } = yield call(merchant.getUserData);

      yield put(getProfile.success(user));
    } catch (error) {
      yield put(snack('message.cannotGetProfile', 'error'));
    }
  }

  /**
   * Change password
   */
  @Saga(changePassword)
  public *changePassword(
    {
      data: { email, password, newPassword, merchantId, token },
      callback
    }: Payload<typeof changePassword>,
    { merchant }: StoreContext
  ) {
    try {
      yield call(merchant.changePassword, {
        id: merchantId,
        password: newPassword,
        token
      });

      yield put(changePassword.success());
      yield call(callback);
      yield put(snack('message.passwordSuccessfullyChanged'));
    } catch (error) {
      yield put(changePassword.failure('message.somethingWentWrongTryAgain'));
    }
  }

  /**
   * Change email
   */
  @Saga(changeEmail)
  public *changeEmail(
    {
      data: { oldEmail, email, password },
      callback
    }: Payload<typeof changeEmail>,
    storeContext: StoreContext
  ) {
    let correctPassword = true;

    try {
      const response = yield call(storeContext.auth.login, {
        email: oldEmail,
        password
      });

      correctPassword = response.data.success;
    } catch (err) {
      yield put(changePassword.failure('message.invalidCredentials'));
      correctPassword = false;
    }

    if (!correctPassword) {
      return;
    }

    yield put(
      profileSave(
        {
          merchant: {
            email,
            settings: {
              business: {
                email
              }
            }
          }
        },
        callback
      )
    );
  }

  /**
   * Send link for changing password
   */
  @Saga(sendLinkForChangingPassword)
  public *sendLink(
    { data: { email } }: Payload<typeof sendLinkForChangingPassword>,
    { merchant }: StoreContext
  ) {
    try {
      yield call(merchant.sendLink, {
        email
      });

      yield put(sendLinkForChangingPassword.success());
      yield put(snack('message.changePasswordSentLink'));
    } catch (err) {
      yield put(
        sendLinkForChangingPassword.failure(
          'message.somethingWentWrongTryAgain'
        )
      );
    }
  }
}

const sagas = [new ProfileSaga()];

export { ProfileSaga, sagas };
