import { MerchantFull, Salon } from '@api';

class ProfileState {
  /**
   * Account merchant
   */
  public merchant: MerchantFull;

  /**
   * Error message
   */
  public error: string = null;

  /**
   * Fetching profile
   */
  public fetching = false;

  /**
   * Temporary
   * Salon for display
   */
  public salon: Salon = null;
}

export { ProfileState };
