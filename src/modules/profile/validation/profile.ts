import { number, object, string } from 'yup';
import { formats } from '@core';
import i18next from 'i18next';

/**
 * Profile form schema
 */
const profileSchema = (isRegistration: boolean) =>
  object({
    merchant: object({
      phone: string()
        .required('message.validation.profile.requiredPhone')
        .test('phone', 'message.validation.profile.invalidPhone', value =>
          formats.phoneNumber.check(value)
        ),
      merchant_name: string().required(
        'message.validation.profile.requiredMerchant'
      ),
      country_id: number(),
      settings: object({
        venue: object({
          registrationCode: string(),
          legalName: string(),
          brandName: string()
        }),
        contact: object({
          phone: string(),
          name: string()
        }),
        business: object({
          phone: string(),
          email: string()
            .required('message.validation.profile.requiredEmail')
            .test('email', 'message.validation.profile.invalidEmail', value =>
              formats.email.check(value)
            ),
          address: string()
        }),
        vat: string(),
        iban: string()
      }),
      password:
        !isRegistration &&
        string()
          .matches(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$/, {
            message: 'message.validation.profile.minMaxCharacters'
          })
          .required('message.validation.profile.requiredPassword')
    })
  });

export { profileSchema };
