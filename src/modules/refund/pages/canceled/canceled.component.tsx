import {
  Body,
  Cell,
  formatPrice,
  Head,
  hoc,
  Pagination,
  Row,
  Table
} from '@core';
import classNames from 'classnames';
import moment from 'moment';
import * as React from 'react';
import { useCanceledBookingsProps } from './canceled.props';
import styles from './canceled.scss';

/**
 * Renders Canceled Bookings
 */
const Canceled = hoc(
  useCanceledBookingsProps,
  ({
    bookingTitles,
    bookings,
    paginationLength,
    onUpdateClick,
    onCancelClick,
    onPageChange
  }) => (
    <div>
      <Table>
        <Head>
          <Row>
            {bookingTitles?.map((title, index) => (
              <Cell
                key={index}
                atCenter={title.atCenter}
                atEnd={title.atEnd}
                bold={title.bold}
              >
                {title.name}
              </Cell>
            ))}
          </Row>
        </Head>
        <Body>
          {bookings.map(
            ({
              order_id,
              order_item_id,
              refund_percent,
              price,
              booking_date_time,
              status
            }) => {
              let isActive = status == 5 ? true : false;
              const [state, setstate] = React.useState(isActive);
              React.useEffect(() => {
                setstate(isActive);
              }, [status]);

              return (
                <Row key={order_item_id}>
                  <Cell>{order_id}</Cell>
                  <Cell>{formatPrice(price)}</Cell>
                  <Cell atCenter>{refund_percent}</Cell>
                  <Cell>
                    {moment(booking_date_time)
                      .utc(false)
                      .format('HH:mm YYYY-MM-DD')}
                  </Cell>
                  <Cell className={styles.buttons}>
                    <div className={styles.modalSwitcher}>
                      <p className={state ? styles.active : ''}>not Paid</p>
                      <div
                        className={classNames(
                          styles.switcher,
                          styles[`switcher${state ? 'Active' : 'NotActive'}`]
                        )}
                        onClick={
                          state
                            ? () => {
                                setstate(!state);
                                onCancelClick({ order_item_id, mark: false });
                              }
                            : () => {
                                setstate(!state);
                                onUpdateClick({ order_item_id, mark: true });
                              }
                        }
                      >
                        <div
                          className={classNames(
                            styles.toggle,
                            styles[`toggle${state ? 'Active' : 'NotActive'}`]
                          )}
                        />
                      </div>
                      <p className={!state ? styles.active : ''}>Paid</p>
                    </div>
                  </Cell>
                </Row>
              );
            }
          )}
        </Body>
      </Table>
      <Pagination length={paginationLength} onPageChange={onPageChange} />
    </div>
  )
);

export { Canceled };
