import { getRefundBookings, updateRefundBooking } from '@appointments';
import { State } from '@store';
import { useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';
import { RouteChildrenProps } from 'react-router';
import { usePagination } from '@core';

const useCanceledBookingsProps = (props: RouteChildrenProps) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();

  const {
    refundCanceledBookings: bookings,
    paginationLength: { totalCanceled }
  } = useSelector((state: State) => state.merchant.appointments);

  const { selectedPage, onPageChange } = usePagination();

  const bookingTitles = [
    { name: 'OrderID', bold: true, atEnd: false },
    { name: 'Price', bold: true, atEnd: false },
    { name: 'Refund Percent', bold: true, atEnd: false, atCenter: true },
    { name: 'Booking Date', bold: true, atEnd: false },
    { name: '', atEnd: true }
  ];

  const onUpdateClick = data => {
    dispatch(updateRefundBooking(data));
  };

  const onCancelClick = data => {
    dispatch(updateRefundBooking(data));
  };

  useEffect(() => {
    dispatch(getRefundBookings({ page: selectedPage }));
  }, [selectedPage]);

  return {
    t,
    bookingTitles,
    bookings,
    paginationLength: totalCanceled,
    onPageChange,
    onUpdateClick,
    onCancelClick,
    ...props
  };
};

export { useCanceledBookingsProps };
