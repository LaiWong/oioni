import {
  Body,
  Cell,
  formatPrice,
  Head,
  hoc,
  Pagination,
  Row,
  Table
} from '@core';
import * as React from 'react';
import { useFailedBookingsProps } from './failed.props';
import styles from './failed.scss';
import classNames from 'classnames';

/**
 * Renders Failed Bookings
 */
const Failed = hoc(
  useFailedBookingsProps,
  ({
    bookingTitles,
    bookings,
    paginationLength,
    onPageChange,
    onUpdateClick,
    onCancelClick
  }) => (
    <div>
      <Table>
        <Head>
          <Row>
            {bookingTitles?.map((title, index) => (
              <Cell
                key={index}
                atCenter={title.atCenter}
                atEnd={title.atEnd}
                bold={title.bold}
              >
                {title.name}
              </Cell>
            ))}
          </Row>
        </Head>
        <Body>
          {bookings.map(
            ({ order_id, price, status, settings: { info } }, index) => {
              let isActive = status == 24 ? true : false;
              const [state, setstate] = React.useState(isActive);
              React.useEffect(() => {
                setstate(isActive);
              }, [status]);
              return (
                <Row key={index}>
                  <Cell>{order_id}</Cell>
                  <Cell>{formatPrice(price)}</Cell>
                  <Cell atCenter>{info}</Cell>
                  <Cell className={styles.buttons}>
                    <div className={styles.modalSwitcher}>
                      <p className={state ? styles.active : ''}>not Payed</p>
                      <div
                        className={classNames(
                          styles.switcher,
                          styles[`switcher${state ? 'Active' : 'NotActive'}`]
                        )}
                        onClick={
                          state
                            ? () => {
                                setstate(!state);
                                onUpdateClick({ order_id, mark: true });
                              }
                            : () => {
                                setstate(!state);
                                onCancelClick({ order_id, mark: false });
                              }
                        }
                      >
                        <div
                          className={classNames(
                            styles.toggle,
                            styles[`toggle${state ? 'Active' : 'NotActive'}`]
                          )}
                        />
                      </div>
                      <p className={!state ? styles.active : ''}>Payed</p>
                    </div>
                    {/* <Button
                  onClick={() => onUpdateClick({ order_id, mark: true })}
                  size='sm'
                >
                  Update
                </Button>
                <Button
                  onClick={() => onCancelClick({ order_id, mark: false })}
                  theme='secondary'
                  size='sm'
                >
                  Cancel
                </Button> */}
                  </Cell>
                </Row>
              );
            }
          )}
        </Body>
      </Table>
      <Pagination length={paginationLength} onPageChange={onPageChange} />
    </div>
  )
);

export { Failed };
