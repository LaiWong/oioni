import { getRefundBookings, updateRefundBooking } from '@appointments';
import { State } from '@store';
import { useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';
import { RouteChildrenProps } from 'react-router';
import { usePagination } from '@core';

const useFailedBookingsProps = (props: RouteChildrenProps) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();

  const {
    refundFailProcessingBookings: bookings,
    paginationLength: { totalFailProcessing }
  } = useSelector((state: State) => state.merchant.appointments);

  const { selectedPage, onPageChange } = usePagination();

  const bookingTitles = [
    { name: 'OrderID', bold: true, atEnd: false },
    { name: 'Price', bold: true, atEnd: false },
    { name: 'info', bold: true, atEnd: false, atCenter: true },
    // { name: 'Booking Date', bold: true, atEnd: false },
    { name: '', atEnd: true }
  ];

  const onUpdateClick = data => {
    dispatch(updateRefundBooking(data));
  };

  const onCancelClick = data => {
    dispatch(updateRefundBooking(data));
  };

  useEffect(() => {
    dispatch(getRefundBookings({ page: selectedPage }));
  }, [selectedPage]);

  return {
    t,
    bookingTitles,
    bookings,
    paginationLength: totalFailProcessing,
    onPageChange,
    onUpdateClick,
    onCancelClick,
    dispatch,
    props
  };
};

export { useFailedBookingsProps };
