import { hoc } from '@core';
import * as React from 'react';
import { Redirect, Route, Switch } from 'react-router';
import { NavLink } from 'react-router-dom';
import { Canceled } from './pages/canceled';
import { Failed } from './pages/failed';
import { useRefundBookingsProps } from './refund.props';
import styles from './refund.scss';

/**
 * Renders Refund Bookings
 */
const Refund = hoc(useRefundBookingsProps, ({ match: { path, url } }) => (
  <div className={styles.container}>
    <div className={styles.navigations}>
      <NavLink
        className={styles.navLink}
        activeClassName={styles.active}
        to={`${url}/canceled`}
      >
        Canceled
      </NavLink>
      <NavLink
        className={styles.navLink}
        activeClassName={styles.active}
        to={`${url}/failed`}
      >
        Failed
      </NavLink>
    </div>

    <Switch>
      <Route exact path={`${path}/canceled`} component={Canceled} />
      <Route exact path={`${path}/failed`} component={Failed} />
      <Redirect to={`${url}/canceled`} />
    </Switch>
  </div>
));

export { Refund };
