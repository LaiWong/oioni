import { useTranslation } from 'react-i18next';
import { RouteChildrenProps } from 'react-router';

/**
 * <Bookings /> props
 */
const useRefundBookingsProps = (props: RouteChildrenProps) => {
  const { t } = useTranslation();

  return {
    t,
    props
  };
};

export { useRefundBookingsProps };
