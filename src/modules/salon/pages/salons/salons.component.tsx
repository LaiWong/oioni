import * as React from 'react';
import { useSalonsProps } from './salons.props';
import * as styles from './salons.scss';
import {
  Body,
  Button,
  Cell,
  Head,
  hoc,
  Pagination,
  Row,
  Search,
  Table
} from '@core';
import classNames from 'classnames';

/**
 * <Salons />
 */
const Salons = hoc(
  useSalonsProps,
  ({
    t,
    uml,
    query,
    salons,
    paginationLength,
    onPageChange,
    onEditClick,
    onWidgetClick,
    onAddClick,
    onQueryChange,
    widgetBtnDisabled
  }) => (
    <div className={styles.salons}>
      <div className={styles.header}>
        <h2 className={styles.title}>{t('merchantPanel.salon.list.title')}</h2>
        <Search
          value={query}
          onChange={onQueryChange}
          className={styles.search}
          placeholder={t('merchantPanel.salon.list.searchPlaceholder')}
        />
        <div style={{ display: 'flex', marginLeft: 'auto' }}>
          <Button
            {...{ disabled: widgetBtnDisabled }}
            className={classNames(styles.button, styles.buttonWidget)}
            theme='primary'
            onClick={onWidgetClick}
          >
            {t('merchantPanel.salon.widget')}
          </Button>
          <Button
            className={styles.button}
            theme='primary'
            onClick={onAddClick}
          >
            {t('merchantPanel.salon.list.add')}
          </Button>
        </div>
      </div>
      <Table>
        <Head>
          <Row>
            <Cell colSpan={2}>{t('merchantPanel.salon.list.name')}</Cell>
            <Cell>{t('merchantPanel.salon.list.address')}</Cell>
            <Cell>{t('merchantPanel.salon.list.contacts')}</Cell>
            <Cell />
          </Row>
        </Head>
        <Body>
          {salons?.map(salon => {
            const {
              address,
              contacts,
              settings: { name, banners }
            } = salon;

            return (
              <Row className={styles.salonRow} key={salon.id}>
                <Cell>
                  <div className={styles.banners}>
                    {banners?.map(url => <img key={url} src={url} />) || '-'}
                  </div>
                </Cell>
                <Cell>{uml(name)}</Cell>
                <Cell>{address}</Cell>
                <Cell>{contacts}</Cell>
                <Cell>
                  <Button
                    className={styles.editSalon}
                    size='xs'
                    theme='teritary'
                    onClick={() => onEditClick(salon)}
                  >
                    {t('merchantPanel.salon.list.edit')}
                  </Button>
                </Cell>
              </Row>
            );
          })}
        </Body>
      </Table>

      <Pagination length={paginationLength} onPageChange={onPageChange} />
    </div>
  )
);

export { Salons };
