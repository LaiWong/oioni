import { useDispatch, useSelector } from 'react-redux';
import { State } from '@store';
import { useEffect, useRef, useState } from 'react';
import { navigate } from '@store/router';
import { useMeta, useMultiLanguageMapper, usePagination } from '@core';
import { getSalons } from '@salon/store';
import { Salon } from '@api';
import { useTranslation } from 'react-i18next';

/**
 * <Salons /> props
 */
const useSalonsProps = () => {
  const {
    t,
    i18n: { language }
  } = useTranslation();
  const dispatch = useDispatch();
  const { salons, paginationLength } = useSelector(
    (state: State) => state.merchant.salon
  );

  const { selectedPage, onPageChange } = usePagination();

  const [query, setQuery] = useState('');
  const interval = useRef<any>();
  const uml = useMultiLanguageMapper(language);

  const onWidgetClick = () => {
    dispatch(navigate('/salon/widget'));
  };

  const onAddClick = () => {
    dispatch(navigate('/salon/add'));
  };

  const onQueryChange = (search: string) => {
    setQuery(search);

    clearTimeout(interval.current);

    interval.current = setTimeout(() => {
      dispatch(getSalons({ search }));
    }, 300);
  };

  const onEditClick = (salon: Salon) => {
    dispatch(navigate('/salon/edit/' + salon.id));
  };

  useEffect(() => {
    dispatch(getSalons({ page: selectedPage }));
  }, [selectedPage]);

  useMeta({ title: 'Salons - Oioni Administration Console' }, []);

  return {
    t,
    uml,
    query,
    salons,
    paginationLength,
    onPageChange,
    onWidgetClick,
    onAddClick,
    onEditClick,
    onQueryChange,
    widgetBtnDisabled: !salons?.length
  };
};

export { useSalonsProps };
