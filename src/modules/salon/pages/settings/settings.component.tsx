import { Employee, Product } from '@api';
import {
  Button,
  Confirm,
  Field,
  Form,
  FormError,
  Group,
  hoc,
  Language,
  Select,
  TimeRange
} from '@core';
import { InputNumber } from '@core/components/input-number';
import classNames from 'classnames';
import * as React from 'react';
import { Fragment } from 'react';
import { useSettingsProps } from './settings.props';
import * as styles from './settings.scss';

/**
 * Renders Settings
 */
const Settings = hoc(
  useSettingsProps,
  ({
    t,
    ml,
    uml,
    lang,
    form,
    isNew,
    errors,
    formRef,
    employees,
    addresses,
    workingTime,
    countryTimezones,
    canAddEmployee,
    onCancelClick,
    onAddressSelect,
    onTimezoneSelect,
    filterEmployeesByAvailability,
    onAddressChange,
    totalServices,
    onLanguageChange,
    onServiceSelect,
    onAddServiceClick,
    onWorkingTimeChange,
    formattedEmployees,
    onAddEmployeeClick,
    onEmployeeChange,
    isDeletingPopupVisible,
    onDeleteClick,
    onCloseDeletingPopup,
    onConfirmDeleteClick,
    confirmRef,
    onRemoveServiceClick,
    onRemoveEmployeeClick,
    filterServiceByAvailability
  }) => (
    <Fragment>
      {!isNew && (
        <div
          className={classNames(styles.overlay, {
            [styles.overlayFull]: isDeletingPopupVisible
          })}
        />
      )}
      <Form form={form} formRef={formRef}>
        <div
          className={classNames(styles.settings, {
            [styles.settingsEdit]: !isNew
          })}
        >
          <div className={styles.header}>
            <h2 className={styles.title}>
              <div className={styles.titleCaption}>
                {isNew
                  ? t('merchantPanel.salon.add.title')
                  : t('merchantPanel.salon.edit.title')}
              </div>
              <Language value={lang} onChange={onLanguageChange} />
            </h2>
            <div className={styles.buttons}>
              <Button
                className={classNames(styles.button, styles.buttonColored)}
                type='submit'
                theme='primary'
              >
                {t('merchantPanel.salon.form.save')}
              </Button>
              <Button
                className={classNames(styles.button, styles.buttonSecondary)}
                onClick={onCancelClick}
                theme='secondary'
              >
                {t('merchantPanel.salon.form.cancel')}
              </Button>
            </div>
          </div>

          <div className={styles.content}>
            <div className={styles.column}>
              <Group
                className={styles.fieldGroup}
                title={t('merchantPanel.salon.form.general')}
              >
                <Field.Input
                  name={ml`settings.name`}
                  label={t('merchantPanel.salon.form.name')}
                />
                <Field.Input
                  suggest
                  label={t('merchantPanel.salon.form.address')}
                  suggestions={addresses}
                  name='address'
                  onSuggestionSelect={onAddressSelect}
                  on={{
                    change: onAddressChange
                  }}
                />
                <Select
                  className={styles.horizontalGroupInput}
                  label={t('merchantPanel.salon.form.timezone')}
                  disabled={!form.values.city || !isNew}
                  options={countryTimezones}
                  value={form.values.timezone}
                  onChange={onTimezoneSelect}
                />
                <Field.Input name='email' label={'Email'} />
                <Field.Input
                  name='contacts'
                  label={t('merchantPanel.salon.form.contacts')}
                />
              </Group>
              <Group
                className={styles.fieldGroup}
                title={t('merchantPanel.salon.form.workingTime')}
              >
                <TimeRange
                  name='workingTime'
                  value={workingTime}
                  onChange={onWorkingTimeChange}
                  input={({ id, value, format, onChange }) => (
                    <InputNumber
                      id={id}
                      value={value}
                      format={format}
                      onChange={onChange}
                    />
                  )}
                />
              </Group>
              <FormError>{errors.save}</FormError>
            </div>
            <div className={styles.column}>
              <Group
                className={styles.fieldGroup}
                title={t('merchantPanel.salon.form.banners')}
              >
                <Field.Upload
                  name='settings.banners'
                  multi
                  accept={['png', 'jpeg', 'jpg', 'webp']}
                />
              </Group>

              <div className={styles.photoDescription}>
                {t('common.uploadFile.validFileDescription')}
              </div>
            </div>

            {formattedEmployees.map((employee, index) => {
              const { employeeId } = employee;

              const canAddService =
                employee.services.length !==
                employees.find(one => one.id == employeeId)?.supportingServices;

              const canDeleteService = employee.services.length > 1;

              return (
                <div className={styles.contentRow} key={index}>
                  <div className={styles.column}>
                    <Group
                      className={styles.fieldGroup}
                      title={
                        index === 0 && t('merchantPanel.salon.form.employees')
                      }
                    >
                      <Select
                        options={filterEmployeesByAvailability(
                          employeeId,
                          employees
                        )}
                        shape={{
                          name: (option: Employee) => option.name
                        }}
                        value={employeeId}
                        onChange={value => onEmployeeChange(employeeId, value)}
                      />
                    </Group>
                    <Button
                      type='button'
                      theme='secondary'
                      className={styles.removeEmployee}
                      onClick={() => onRemoveEmployeeClick(employeeId)}
                    >
                      {t('merchantPanel.salon.form.removeEmployee')}
                    </Button>
                  </div>
                  {employeeId && (
                    <div className={styles.column}>
                      <Group
                        className={styles.fieldGroup}
                        title={
                          index === 0 && t('merchantPanel.salon.form.services')
                        }
                      >
                        {employee.services.map((service, serviceIndex) => (
                          <Select
                            options={filterServiceByAvailability(
                              employeeId,
                              service
                            )}
                            shape={{
                              name: (option: Product) =>
                                uml(option.settings.name)
                            }}
                            value={service}
                            key={serviceIndex}
                            after={
                              canDeleteService && (
                                <div
                                  className={styles.remove}
                                  onClick={() =>
                                    onRemoveServiceClick(employeeId, service)
                                  }
                                />
                              )
                            }
                            onChange={value =>
                              onServiceSelect(employeeId, serviceIndex, value)
                            }
                          />
                        ))}
                      </Group>
                      {canAddService && (
                        <Button
                          type='button'
                          theme='secondary'
                          className={styles.addService}
                          onClick={() => onAddServiceClick(employeeId)}
                        >
                          {t('merchantPanel.salon.form.addService')}
                        </Button>
                      )}
                    </div>
                  )}
                </div>
              );
            })}
          </div>
          <div className={styles.employeeControls}>
            {canAddEmployee && (
              <Button type='button' onClick={onAddEmployeeClick}>
                {t('merchantPanel.salon.form.addEmployee')}
              </Button>
            )}
            {!isNew && (
              <Button type='button' theme='teritary' onClick={onDeleteClick}>
                {t('merchantPanel.salon.form.deleteSalon')}
              </Button>
            )}
          </div>
        </div>
      </Form>
      {isDeletingPopupVisible && (
        <Confirm className={styles.deletingPopup} confirmRef={confirmRef}>
          <div className={styles.deletingPopupTitle}>
            {t('merchantPanel.salon.deleteDisclaimer.title')}
          </div>
          <div className={styles.deletingPopupDescription}>
            {/* {t('merchantPanel.salon.deleteDisclaimer.text')} */}
            {t('merchantPanel.salon.deleteDisclaimer.text1')} <br />
            {t('merchantPanel.salon.deleteDisclaimer.text2')}
          </div>
          <div className={styles.deletingPopupControls}>
            <Button
              type='button'
              theme='teritary'
              onClick={onCloseDeletingPopup}
            >
              {t('merchantPanel.salon.deleteDisclaimer.cancel')}
            </Button>
            <Button type='button' onClick={onConfirmDeleteClick}>
              {t('merchantPanel.salon.deleteDisclaimer.delete')}
            </Button>
          </div>
        </Confirm>
      )}
    </Fragment>
  )
);

export { Settings };
