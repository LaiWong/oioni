import { Language, Salon } from '@api';
import {
  formats,
  useClickOutside,
  useMeta,
  useMultiLanguage,
  useMultiLanguageMapper
} from '@core';
import {
  deleteSalon,
  getAddressCoords,
  getAddressOptions,
  getSalon,
  getSalonEmployees,
  getSalonServices,
  saveSalon,
  unmount
} from '@salon/store';
import { State } from '@store';
import { navigate } from '@store/router';
import { useFormik } from 'formik';
import { useEffect, useRef, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useRouteMatch } from 'react-router';
import { array, lazy, number, object, ref, string } from 'yup';
import { useTranslation } from 'react-i18next';
import { useSchemaTranslationsUpdate } from '@app/hooks';
import i18next from 'i18next';
import moment from 'moment';
import 'moment-timezone';

const defaultValues = {} as Salon;

const WORKING_TIME_FORMAT = 'HH:mm';

let isOnSelectAddress = false;

/**
 * Validation schema
 */
const schema = () =>
  object({
    name: string()
      .required('message.addTranslationForNameInYourCurrentLanguge')
      .nullable(true)
      .max(40, 'merchantPanel.salon.form.errors.salonNameTooLong')
      .label('merchantPanel.salon.form.name'),
    settings: object({
      banners: array(
        string()
          .required('Invalid file')
          .nullable(true)
      )
        .required('merchantPanel.salon.form.errors.bannerIsRequired')
        .min(1, 'merchantPanel.salon.form.errors.bannerIsRequired'),
      name: string()
        .required()
        .nullable(true)
        .max(40, 'merchantPanel.salon.form.errors.salonNameTooLong')
        .label('merchantPanel.salon.form.name')
    }),
    address: string()
      .nullable(true)
      .required()
      .label('merchantPanel.salon.form.address'),
    timezone: string()
      .nullable(true)
      .required('merchantPanel.salon.form.errors.timezoneIsRequired')
      .label('merchantPanel.salon.form.timezone'),
    contacts: string()
      .nullable(true)
      .required('merchantPanel.salon.form.errors.contactsAreRequired')
      .label('merchantPanel.salon.form.contacts'),
    latitude: number()
      .nullable(true)
      .required('merchantPanel.salon.form.errors.selectAddressFromList'),
    longitude: number()
      .nullable(true)
      .required('merchantPanel.salon.form.errors.selectAddressFromList'),
    time_from: string()
      .nullable(true)
      .required('merchantPanel.salon.form.errors.timeFromIsRequired'),
    time_to: string()
      .nullable(true)
      .test(
        'isAfterStart',
        'merchantPanel.salon.form.errors.timeOrder',
        function(value) {
          return moment(value, WORKING_TIME_FORMAT).isAfter(
            moment(this.resolve(ref('time_from')), WORKING_TIME_FORMAT),
            'minute'
          );
        }
      )

      .required('merchantPanel.salon.form.errors.timeToIsRequired'),
    services: array(
      object({
        productId: string()
          .nullable(true)
          .required('merchantPanel.salon.form.errors.selectServiceFromList'),
        employees: array(
          string()
            .nullable(true)
            .required('merchantPanel.salon.form.errors.selectEmployeeFromList')
        )
      })
    ),
    email: string()
      .required('Email is required')
      .test('email', 'merchantPanel.salon.form.errors.invalidEmail', value =>
        formats.email.check(value)
      )
  });

type FormattedEmployees = {
  employeeId: number;
  services: number[];
}[];

type WorkingTime = {
  time_from: string;
  time_to: string;
};

type TimeZone = {
  id: string;
  name: string;
};

/**
 * Use settings props
 */
const useSettingsProps = () => {
  const { t, i18n } = useTranslation();
  const dispatch = useDispatch();
  const {
    params: { id, language }
  } = useRouteMatch<{ id: string; language: Language }>();
  const confirmRef = useRef();
  const formRef = useRef();

  const [lang, setLang, ml] = useMultiLanguage(language);
  const uml = useMultiLanguageMapper(lang);

  const {
    salon,
    errors,
    services: totalServices,
    employees,
    addresses
  } = useSelector((state: State) => state.merchant.salon);

  const isNew = !Boolean(id);

  const [formattedEmployees, setFormattedEmployees] = useState<
    FormattedEmployees
  >([]);
  const [isDeletingPopupVisible, setIsDeletingPopupVisible] = useState(false);
  const [workingTime, setWorkingTime] = useState<WorkingTime>({
    time_from: salon?.time_from,
    time_to: salon?.time_to
  });
  const [countryTimezones, setCountryTimezones] = useState<Array<TimeZone>>([]);

  const form = useFormik({
    onSubmit: (values, { setSubmitting }) => {
      const formattedServices = [];

      formattedEmployees.forEach(({ employeeId, services }) => {
        if (employeeId === null) return;

        services.forEach(service => {
          if (service === null) return;

          const targetService = formattedServices.find(
            ({ productId }) => productId == service
          );

          if (targetService) {
            targetService.employees.push(employeeId);
          }

          formattedServices.push({
            productId: Number(service),
            employees: [employeeId]
          });
        });
      });

      dispatch(
        saveSalon(
          {
            ...values,
            phone: values.contacts,
            services: formattedServices
          },
          () => setSubmitting(false)
        )
      );
    },
    validationSchema: lazy(schema),
    enableReinitialize: true,
    initialValues: salon || defaultValues,
    validateOnChange: false
  });

  useSchemaTranslationsUpdate(form);

  const hasNoServices = !Boolean(totalServices?.length);

  const canAddEmployee = formattedEmployees.length !== employees.length;

  const onCancelClick = () => {
    dispatch(navigate('/salon'));
  };

  const filterServiceByAvailability = (
    targetEmployeeId: number,
    serviceId: number
  ) =>
    totalServices.filter(option => {
      if (option.id == serviceId) return true;

      const formattedEmployee = formattedEmployees.find(
        ({ employeeId }) => employeeId === targetEmployeeId
      );

      const employee = employees.find(({ id }) => id === targetEmployeeId);

      if (!formattedEmployee.services?.length || !employee) return true;

      return (
        formattedEmployee.services.every(service => service != option.id) &&
        employee.products.some(product => product === option.id)
      );
    });

  const filterEmployeesByAvailability = (
    id: number,
    options: typeof employees
  ) =>
    options.filter(option => {
      if (option.id == id) return true;

      if (!formattedEmployees.length) return true;

      return formattedEmployees.every(one => one.employeeId != option.id);
    });

  const onAddressChange = (query: string) => {
    if (query.length < 3) {
      if (isNew) form.setFieldValue('timezone', '');
      return;
    }

    dispatch(getAddressOptions(query));
  };

  const onDeleteClick = () => {
    setIsDeletingPopupVisible(true);
  };

  const onCloseDeletingPopup = () => {
    setIsDeletingPopupVisible(false);
  };

  const onConfirmDeleteClick = () => {
    dispatch(deleteSalon(salon));
  };

  const onTimezoneSelect = (timezone: string) => {
    form.setFieldValue('timezone', timezone);
  };

  const matchCountryTimezones = (country_code: string) => {
    const timezones = moment.tz.zonesForCountry(country_code);
    const timezoneOptions = timezones.map(i => ({ id: i, name: i }));
    setCountryTimezones(timezoneOptions);
  };

  const onAddressSelect = suggestion => {
    dispatch(
      getAddressCoords(
        suggestion.id,
        ({ lat, lng, city, country, country_code }) => {
          form.setFieldValue('latitude', lat);
          form.setFieldValue('longitude', lng);
          form.setFieldValue('city', city);
          form.setFieldValue('country', country);
          matchCountryTimezones(country_code);
        }
      )
    );

    if (!id) return;

    isOnSelectAddress = true;

    setTimeout(() => {
      isOnSelectAddress = false;
    }, 100);
  };

  const onAddEmployeeClick = () => {
    setFormattedEmployees([
      ...formattedEmployees,
      {
        employeeId: null,
        services: []
      }
    ]);
  };

  const onEmployeeChange = (prevEmployeeId: number, newEmployeeId: number) => {
    setFormattedEmployees(
      formattedEmployees.map(({ employeeId, services }) => {
        if (employeeId === prevEmployeeId && employeeId !== newEmployeeId) {
          return {
            services: employees.find(({ id }) => id === newEmployeeId).products,
            employeeId: newEmployeeId
          };
        }

        return {
          services,
          employeeId
        };
      })
    );
  };

  const onAddServiceClick = (targetEmployeeId: number) => {
    setFormattedEmployees(
      formattedEmployees.map(({ employeeId, services }) => {
        if (employeeId === targetEmployeeId) {
          return {
            employeeId,
            services: [...services, null]
          };
        }

        return {
          employeeId,
          services
        };
      })
    );
  };

  const onRemoveEmployeeClick = (targetEmployeeId: number) => {
    setFormattedEmployees(
      formattedEmployees.filter(
        ({ employeeId }) => employeeId !== targetEmployeeId
      )
    );
  };

  const onWorkingTimeChange = (workingTime: WorkingTime) => {
    form.setFieldValue('time_from', workingTime.time_from);
    form.setFieldValue('time_to', workingTime.time_to);
  };

  const onServiceSelect = (
    targetEmployeeId: number,
    targetServiceIndex: number,
    newService: number
  ) => {
    setFormattedEmployees(
      formattedEmployees.map(({ employeeId, services }) => {
        if (employeeId === targetEmployeeId) {
          return {
            employeeId,
            services: services.map((service, index) => {
              if (index === targetServiceIndex) {
                return newService;
              }

              return service;
            })
          };
        }

        return { employeeId, services };
      })
    );
  };

  const onRemoveServiceClick = (
    targetEmployeeId: number,
    targetServiceId: number
  ) => {
    setFormattedEmployees(
      formattedEmployees.map(({ employeeId, services }) => {
        if (employeeId === targetEmployeeId) {
          return {
            employeeId,
            services: services.filter(service => service !== targetServiceId)
          };
        }

        return { employeeId, services };
      })
    );
  };

  useClickOutside(formRef, () => {
    if (!id || isDeletingPopupVisible || isOnSelectAddress) return;

    dispatch(navigate('/salon'));
  });

  useClickOutside(confirmRef, onCloseDeletingPopup);

  useEffect(() => {
    dispatch(getSalonServices());
    dispatch(getSalonEmployees());
    dispatch(getSalon(id ? Number(id) : null));
  }, []);

  useEffect(() => {
    const employeesId = Array.from(
      form.values.services?.reduce(
        (acc, { employees }) => new Set([...acc, ...employees]),
        new Set<number>()
      ) || []
    );

    const formattedEmployees = employeesId
      .filter(employeeId => employees.some(one => one.id === employeeId))
      .map(employeeId => {
        const services = form.values.services
          .filter(service => service.employees.includes(employeeId))
          .map(service => service.productId);

        return {
          employeeId,
          services
        };
      });

    if (!formattedEmployees.length) return;

    setFormattedEmployees(formattedEmployees);
  }, [form.values.services]);

  useEffect(() => {
    const { time_from, time_to } = form.values;

    if (
      time_from !== workingTime.time_from ||
      time_to !== workingTime.time_to
    ) {
      setWorkingTime({
        time_from,
        time_to
      });
    }
  }, [form.values.time_from, form.values.time_to]);

  useEffect(() => {
    if (!form?.values?.settings?.name) return;

    setTimeout(() => {
      form.setFieldValue(
        'name',
        form.values.settings.name[i18n.language] || ''
      );
    }, 100);
  }, [form.values.settings?.name, i18n.language]);

  useMeta(
    {
      title: `${isNew ? 'New' : 'Edit'} salon - Oioni Administration Console`
    },
    []
  );

  useEffect(() => {
    if (!salon) return;

    let formattedEmployees: {
      employeeId: number;
      services: number[];
    }[] = [];

    salon.services?.map(({ employees, productId }) => {
      employees?.map((value, index) => {
        if (
          formattedEmployees?.some(({ employeeId }) => employeeId === value)
        ) {
          const employee = formattedEmployees.find(
            ({ employeeId }) => employeeId === value
          );

          formattedEmployees = formattedEmployees.filter(
            ({ employeeId }) => employeeId !== value
          );

          if (employee.services.includes(+productId)) return;

          formattedEmployees.push({
            employeeId: employee.employeeId,
            services: [...employee.services, +productId]
          });
          return;
        }

        formattedEmployees.push({ employeeId: value, services: [+productId] });
      });
    });

    if (salon.timezone) {
      setCountryTimezones([{ id: salon.timezone, name: salon.timezone }]);
    }

    setFormattedEmployees(formattedEmployees);
  }, [salon]);

  useEffect(
    () => () => {
      dispatch(unmount());
    },
    []
  );

  return {
    t,
    ml,
    uml,
    lang,
    form,
    isNew,
    errors,
    filterEmployeesByAvailability,
    formRef,
    addresses,
    workingTime,
    countryTimezones,
    onDeleteClick,
    onCloseDeletingPopup,
    onConfirmDeleteClick,
    employees,
    isDeletingPopupVisible,
    onEmployeeChange,
    formattedEmployees,
    totalServices,
    hasNoServices,
    onCancelClick,
    canAddEmployee,
    onAddressSelect,
    onAddressChange,
    onTimezoneSelect,
    onServiceSelect,
    onAddServiceClick,
    onAddEmployeeClick,
    onRemoveServiceClick,
    onWorkingTimeChange,
    confirmRef,
    onRemoveEmployeeClick,
    onLanguageChange: setLang,
    filterServiceByAvailability
  };
};

export { useSettingsProps };
