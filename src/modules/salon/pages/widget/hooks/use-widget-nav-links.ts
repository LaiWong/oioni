import { useTranslation } from 'react-i18next';
import { ReactNode } from 'react';
import { useRouteMatch } from 'react-router';

const useWidgetNavLinks = (): {
  to: string;
  name: ReactNode;
  admin?: boolean;
}[] => {
  const { t } = useTranslation();
  const { url } = useRouteMatch();
  return [
    {
      to: `${url}/own`,
      name: t('merchantPanel.widget.yourWebsite')
    },
    {
      to: `${url}/oioni`,
      name: t('merchantPanel.widget.oioniDomain')
    }
  ];
};

export { useWidgetNavLinks };
