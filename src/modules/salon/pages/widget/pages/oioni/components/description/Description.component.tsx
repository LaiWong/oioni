import React from 'react';
import styles from './description.scss';
import { Field } from '@core';
import classNames from 'classnames';
import { ErrorMessage } from 'formik';

export const Description = ({ t, form }) => {
  const { errors, touched, setFieldTouched } = form;
  return (
    <div className={styles.description}>
      <Field.Textarea
        className={classNames(styles.myClass, {
          [styles.error]: errors.description && touched.description
        })}
        name='description'
        placeholder={t(
          'merchantPanel.widget.oioni.description.placeholder.addDescription'
        )}
        onKeyDown={() => setFieldTouched('description', true)}
      />
      <ErrorMessage
        name='description'
        component='span'
        className={styles.messageError}
      />
    </div>
  );
};
