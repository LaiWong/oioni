import React, { useRef } from 'react';
import styles from './domain.scss';
import { Field } from '@core';
import { ErrorMessage } from 'formik';
import classNames from 'classnames';
import { getWidget, isExisDomain } from '@salon/pages/widget/store/actions';
import { useDispatch } from 'react-redux';

export const Domain = ({ t, form, disabled }) => {
  const { errors, touched, setFieldTouched } = form;
  return (
    <div className={styles.domain}>
      <label>
        <span>
          {t('merchantPanel.widget.oioni.domain.title.chooseDomainName')}
        </span>
        <Field.Input
          disabled={disabled}
          name='domain'
          label={t('merchantPanel.widget.oioni.domain.label.yourSiteName')}
          className={classNames({
            [styles.error]: errors.domain && touched.domain
          })}
          onKeyDown={() => {
            setFieldTouched('domain', true);
          }}
        />
      </label>
      <ErrorMessage
        name='domain'
        component='span'
        className={styles.messageError}
      />
    </div>
  );
};
