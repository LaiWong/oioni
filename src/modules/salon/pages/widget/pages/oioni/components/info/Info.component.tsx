import React from 'react';
import styles from './info.scss';
import { Field } from '@core';
import { ErrorMessage } from 'formik';
import className from 'classnames';

export const Info = ({ t, form }) => {
  const { errors, touched, setFieldTouched } = form;
  return (
    <div className={styles.info}>
      <label>
        <span>{t('merchantPanel.widget.oioni.info.title.salonName')}</span>
        <Field.Input
          label={t('merchantPanel.widget.oioni.info.label.salonName')}
          name='info'
          onKeyDown={() => setFieldTouched('info', true)}
          className={className({
            [styles.error]: errors.info && touched.info
          })}
        />
      </label>
      <ErrorMessage
        name='info'
        component='div'
        className={styles.messageError}
      />
    </div>
  );
};
