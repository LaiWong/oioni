import React from 'react';
import styles from './language.scss';
import { Field } from '@core';

export const Language = ({ t, slots }) => {
  const shape = {
    name: (option: { id: number; name: string }) => `${option.name}`
  };
  return (
    <div className={styles.language}>
      <Field.Select
        label={t(
          'merchantPanel.widget.oioni.language.placeholder.defaultLanguage'
        )}
        name='language'
        options={slots}
        className={styles.select}
        // shape={shape}
      />
    </div>
  );
};
