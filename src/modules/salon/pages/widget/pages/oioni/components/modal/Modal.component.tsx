import React from 'react';
import { Confirm, Button } from '@core';
import classNames from 'classnames';
import styles from './modal.scss';

type PropsTypes = {
  onCloseEdit: () => any;
  onEditClickSuccess: () => any;
  onEditClickCancel: () => any;
};

const EditModalWindow: React.FC<PropsTypes> = ({
  onCloseEdit,
  onEditClickSuccess,
  onEditClickCancel
}) => (
  <div className={styles.container}>
    <Confirm onClose={onCloseEdit} className={styles.confirm}>
      <div className={styles.info}>
        <div className={styles.image}>
          <img src={require('img/snackbar-error.svg')} alt='WARNING' />
        </div>
        <div>
          <h3>This domain already exists!</h3>
          <h3>If You want to edit it, click OK</h3>
        </div>
      </div>
      <div className={styles.btnWrapper}>
        <Button
          type='button'
          size='md'
          theme='primary'
          onClick={onEditClickSuccess}
          className={classNames(styles.save)}
        >
          OK
        </Button>
        <Button
          type='button'
          size='md'
          theme='teritary'
          onClick={onEditClickCancel}
          className={classNames(styles.cancel)}
        >
          Cancel
        </Button>
      </div>
    </Confirm>
  </div>
);
export { EditModalWindow };
