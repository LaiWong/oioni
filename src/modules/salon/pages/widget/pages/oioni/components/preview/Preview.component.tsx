import React from 'react';
import styles from './preview.scss';
import { Field } from '@core';

export const Preview = ({ t, form }) => {
  const { values } = form;
  return (
    <div className={styles.preview}>
      <div
        style={{
          backgroundImage: `url(${values.background})`,
          backgroundPosition: 'center',
          backgroundSize: 'cover',
          backgroundRepeat: 'no-repeat'
        }}
        className={styles.previewWidget}
      >
        <p
          className={styles.siteDescription}
          style={{ color: form.values.textColor }}
        >
          {form.values.description}
        </p>
        <div>
          <div className={styles.widgetDemo}>
            <div className={styles.widget}>
              <div
                className={styles.widgetHeader}
                style={{
                  background: form.values.coverColor,
                  color: form.values.textColor
                }}
              >
                <div className={styles.widgetHeaderBlock1}>
                  <div
                    style={{ color: form.values.textColor || '#3c3c3c' }}
                    className={styles.salonName}
                  >
                    Ilusalong Maribell
                  </div>
                  <div className={styles.logo}>
                    <img
                      src={require('img/widget-oioni-logo.svg')}
                      alt='logo'
                    />
                  </div>
                </div>
                <div className={styles.widgetHeaderBlock2}>
                  <div
                    style={{ color: form.values.textColor }}
                    className={styles.widgetTitle}
                  >
                    Online booking
                  </div>
                </div>
              </div>
              <div className={styles.widgetService}>
                <div>
                  <img
                    src={require('img/widget-services-logo.svg')}
                    alt='services'
                  />
                </div>
                <div className={styles.serviceTitle}>
                  {/* {t('merchantPanel.widget.demoWidget.Services')} */}
                  Services
                </div>
              </div>
              <div className={styles.widgetSpecialist}>
                <div>
                  <img
                    src={require('img/widget-specialists-logo.svg')}
                    alt='specialists'
                  />
                </div>
                <div className={styles.specialistsTitle}>Specialists</div>
              </div>
              <div className={styles.widgetDateAndTime}>
                <div>
                  <img
                    src={require('img/widget-time.svg')}
                    alt='date and time'
                  />
                </div>
                <div className={styles.dateTitle}>Date and time</div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className={styles.colors}>
        <Field.ColorPicker
          value={form.values.coverColor}
          name='coverColor'
          label={t('merchantPanel.widget.oioni.colorpicker.label.coverColor')}
          onChange={color => {
            form.setFieldValue('coverColor', color.hex);
          }}
        />
        <Field.ColorPicker
          value={form.values.textColor}
          name='widgetColor'
          label={t(
            'merchantPanel.widget.oioni.colorpicker.label.titleTextColor'
          )}
          onChange={color => form.setFieldValue('textColor', color.hex)}
        />
      </div>
    </div>
  );
};
