import React from 'react';
import styles from './upload.scss';
import Dropzone from 'react-dropzone-uploader';
import 'react-dropzone-uploader/dist/styles.css';
import { useDispatch } from 'react-redux';
import { upload } from '@store/general';
import classNames from 'classnames';
import { ErrorMessage } from 'formik';

export const Upload = ({ t, form }) => {
  const dispatch = useDispatch();
  const { errors, touched } = form;
  const handleChangeStatus = ({ remove }, status, files) => {
    const f = [files[0].file];
    if (status === 'done') {
      form.setSubmitting(true);
      dispatch(
        upload(f, urls => {
          form.setFieldValue('background', urls[0]);
          form.setSubmitting(false);
        })
      );
      remove();
    }
  };

  return (
    <React.Fragment>
      <div className={classNames(styles.upload)}>
        <Dropzone
          onChangeStatus={handleChangeStatus}
          maxFiles={1}
          multiple={false}
          canCancel={false}
          inputContent={t('merchantPanel.widget.oioni.upload.dragOrBrowseFile')}
          styles={{
            dropzone: {
              width: 487,
              height: 263,
              overflow: 'hidden',
              border:
                errors.background && touched.background
                  ? '2px dashed red'
                  : '1px dashed #AAAAAA',
              borderRadius: 16,
              textAlign: 'center'
            },
            inputLabel: {
              color: '#AAAAAA',
              fontSize: 19,
              fontStyle: 'normal',
              fontWeight: 'normal',
              lineHeight: '130%',
              cursor: 'pointer',
              padding: '7rem',
              textAlign: 'center'
            }
          }}
        />
        <ErrorMessage
          name='background'
          component='span'
          className={styles.messageError}
        />
      </div>
    </React.Fragment>
  );
};
