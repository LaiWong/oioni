import { Form, hoc, Select } from '@core';
import classNames from 'classnames';
import * as React from 'react';
import { Fragment } from 'react';
import { useOioniProps } from './oioni.props';
import * as styles from './oioni.scss';
import { Info } from './components/info/Info.component';
import { Domain } from './components/domain/Domain.component';
import { Description } from './components/description/Description.component';
import { Language } from './components/language/Language.component';
import { Upload } from './components/upload/Upload.component';
import { Preview } from './components/preview/Preview.component';
import { EditModalWindow } from './components/modal/Modal.component';
import { enviroment } from '@env';

/**
 * Renders Oioni domain type component
 */

const Oioni = hoc(
  useOioniProps,
  ({
    t,
    form,
    disabled,
    shape,
    onEditClickSuccess,
    onEditClickCancel,
    languages,
    onPreviewClick,
    onCloseEdit,
    editVisible,
    btnPreviewRef,
    selectedWidgetId,
    widget,
    widgets,
    onSelectWidgetChange,
    domainFieldDisabled
  }) => (
    <Fragment>
      {editVisible && (
        <EditModalWindow
          onCloseEdit={onCloseEdit}
          onEditClickSuccess={onEditClickSuccess}
          onEditClickCancel={onEditClickCancel}
        />
      )}
      <div className={styles.oioni}>
        <Form className={styles.form} form={form}>
          <div style={{ display: 'flex', marginLeft: 'auto' }}>
            <Select
              value={selectedWidgetId}
              options={widgets}
              className={classNames(styles.select)}
              placeholder={t(
                'merchantPanel.widget.oioni.select.placeholder.selectWidget'
              )}
              onChange={onSelectWidgetChange}
              shape={shape}
            />
          </div>
          <div className={styles.widgetInfo}>
            <Info t={t} form={form} />
            <Domain t={t} form={form} disabled={domainFieldDisabled} />
          </div>
          <div className={styles.widgetInfo}>
            <Description t={t} form={form} />
            <Language t={t} slots={languages} />
          </div>
          <div className={styles.widgetBackground}>
            <Upload t={t} form={form} />
            <Preview t={t} form={form} />
          </div>
          <div className={styles.buttons}>
            {/* <a
              ref={btnPreviewRef}
              onClick={onPreviewClick}
              className={classNames(styles.buttonPreview, {
                [styles.disabledLink]: disabled
              })}
              target='_blank'
              href={
                form.values.domain
                  ? `https://${form.values.domain}.${enviroment.sub_domain}oioni.com`
                  : 'https://oioni.com'
              }
            >
              {t('merchantPanel.widget.oioni.btn.preview')}
            </a> */}
          </div>
        </Form>
      </div>
    </Fragment>
  )
);

export { Oioni };
