import { useRouteMatch } from 'react-router';

/**
 * Use widget oioni domain type props
 */
const useOioniProps = props => props;

export { useOioniProps };
