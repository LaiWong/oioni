import { Button, Field, Form, hoc } from '@core';
import * as React from 'react';
import { Fragment } from 'react';
import { useOwnProps } from './own.props';
import * as styles from './own.scss';
import logo from 'img/widget-oioni-logo.svg';
import { getCode } from './utils/getCode';

/**
 * Renders Own domain type component
 */
const Own = hoc(
  useOwnProps,
  ({
    t,
    form,
    formRef,
    languages,
    coverColor,
    onColorCoverChange,
    textColor,
    onColorTextChange,
    onCopyClick,
    codeValue,
    merchant_id
  }) => (
    <Fragment>
      <div className={styles.own}>
        <Form className={styles.form} form={form} formRef={formRef}>
          <div className={styles.fieldGroupInfo}>
            <label>
              <span>
                {t('merchantPanel.widget.form.group.label.title.widgetInfo')}
              </span>
              <Field.Input
                // {...{ disabled }}
                name='info'
                label={t('merchantPanel.widget.info.salonName')}
              />
            </label>
          </div>
          <div className={styles.fieldGroupLanguage}>
            <Field.Select
              label={t('merchantPanel.widget.form.group.label.defaultLanguage')}
              className={styles.select}
              name='language'
              options={languages}
            />
          </div>
          <div className={styles.fieldGroupWidgetCoverColor}>
            <Field.ColorPicker
              value={coverColor}
              name='widgetColor'
              label={t('merchantPanel.widget.colorpicker.label.coverColor')}
              onChange={onColorCoverChange}
            />
          </div>
          <div className={styles.fieldGroupWidgetTextColor}>
            <Field.ColorPicker
              value={textColor}
              name='widgetColor'
              label={t('merchantPanel.widget.colorpicker.label.titleTextColor')}
              onChange={onColorTextChange}
            />
          </div>
          <div className={styles.fieldGroupWidgetCode}>
            <span>{t('merchantPanel.widget.own.section.title.code')}</span>
            <textarea
              ref={codeValue}
              className={styles.textarea}
              readOnly
              value={getCode({
                merchant_id: merchant_id,
                salon: form.values.info,
                language: languages.find(el => el.id === form.values.language)
                  .code,
                cover_color: form.values.coverColor.replace('#', '%23'),
                text_color: form.values.textColor.replace('#', '%23')
              })}
            />
            <Button
              type='button'
              className={styles.button}
              theme='teritary'
              onClick={onCopyClick}
            >
              {t('merchantPanel.widget.own.section.code.btn.copyCode')}
            </Button>
            <p>{t('merchantPanel.widget.own.section.code.installation')}</p>
          </div>
          <div className={styles.widgetDemo}>
            <div className={styles.widget}>
              <div
                className={styles.widgetHeader}
                style={{
                  background: form.values.coverColor,
                  color: form.values.textColor
                }}
              >
                <div className={styles.widgetHeaderBlock1}>
                  <div
                    style={{ color: form.values.textColor }}
                    className={styles.salonName}
                  >
                    Salon Name
                  </div>
                  <div className={styles.logo}>
                    <img src={logo} alt='logo' />
                  </div>
                </div>
                <div className={styles.widgetHeaderBlock2}>
                  <div
                    style={{ color: form.values.textColor }}
                    className={styles.widgetTitle}
                  >
                    Online booking
                  </div>
                </div>
              </div>
              <div className={styles.widgetService}>
                <div>
                  <img
                    src={require('img/widget-services-logo.svg')}
                    alt='services'
                  />
                </div>
                <div className={styles.serviceTitle}>Services</div>
              </div>
              <div className={styles.widgetSpecialist}>
                <div>
                  <img
                    src={require('img/widget-specialists-logo.svg')}
                    alt='specialists'
                  />
                </div>
                <div className={styles.specialistsTitle}>Specialists</div>
              </div>
              <div className={styles.widgetDateAndTime}>
                <div>
                  <img
                    src={require('img/widget-time.svg')}
                    alt='date and time'
                  />
                </div>
                <div className={styles.dateTitle}>Date and time</div>
              </div>
            </div>
          </div>
        </Form>
      </div>
    </Fragment>
  )
);

export { Own };
