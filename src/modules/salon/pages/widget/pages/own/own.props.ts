import { Language } from '@api';
import { useMultiLanguage, useMultiLanguageMapper } from '@core';
import { State } from '@store';
import { useFormik } from 'formik';
import { useRef } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useRouteMatch } from 'react-router';
import { useTranslation } from 'react-i18next';
import { useSchemaTranslationsUpdate } from '@app/hooks';

/**
 * Use widget own domain type props
 */
const useOwnProps = () => {
  const { t } = useTranslation();
  const merchant_id = useSelector(
    (state: State) => state.general.merchantUser.id
  );
  const formRef = useRef();
  const route = useRouteMatch<{ language: string }>();
  const languages = [
    {
      id: 1,
      name: t('merchantPanel.widget.select.option.English'),
      code: 'en'
    },
    {
      id: 2,
      name: t('merchantPanel.widget.select.option.Russian'),
      code: 'ru'
    },
    {
      id: 3,
      name: t('merchantPanel.widget.select.option.Estonian'),
      code: 'et'
    },
    {
      id: 4,
      name: t('merchantPanel.widget.select.option.Ukrainian'),
      code: 'ua'
    }
  ];
  const getDefaultLanguage = defaultLanguage =>
    languages.find(option => option.code === defaultLanguage).id;
  const [lang, setLang, ml] = useMultiLanguage(Language.et);
  const form = useFormik({
    initialValues: {
      info: '',
      language: getDefaultLanguage(route.params.language),
      coverColor: '#002445',
      textColor: '#b8ddfe'
    },
    onSubmit: values => {}
  });
  useSchemaTranslationsUpdate(form);
  const {
    values: { coverColor, textColor }
  } = form;
  const onColorCoverChange = color => {
    form.setFieldValue('coverColor', color.hex);
  };
  const onColorTextChange = color => {
    form.setFieldValue('textColor', color.hex);
  };
  const codeValue = useRef() as React.MutableRefObject<HTMLTextAreaElement>;
  const onCopyClick = () => {
    const el = document.createElement('textarea');
    el.value = codeValue.current.value;
    document.body.appendChild(el);
    el.select();
    document.execCommand('copy');
    document.body.removeChild(el);
  };

  return {
    t,
    ml,
    formRef,
    form,
    languages,
    coverColor,
    onColorCoverChange,
    textColor,
    onColorTextChange,
    onCopyClick,
    merchant_id,
    codeValue
  };
};
export { useOwnProps };
