import { enviroment } from '@env';

const getCode = ({
  merchant_id,
  salon,
  language,
  cover_color,
  text_color
}: {
  merchant_id: number;
  salon: string;
  language: string;
  cover_color: string;
  text_color: string;
}) => `
    <script>
        const btnDefaultStyles = {
        position: "fixed",
        right: "15px",
        bottom: "20px",
        display: "flex",
        "justify-content": "center",
        "align-items": "center",
        width: "auto",
        height: "46px",
        "background-color": "#fff",
        "border-radius": "16px",
        outline: "none",
        border: "none",
        border: "1px solid lightblue",
        cursor: "pointer",
        "font-style": "normal",
        "font-weight": "bold",
        "line-height": "20px",
        "letter-spacing": "-0.01em",
        "text-align": "center",
        color: "#000000 !important",
        "z-index": 99999,
        "padding": "0 1rem"
        };
        const btn = document.createElement("button");
        document.body.appendChild(btn);
        btn.innerHTML ="Book with ${salon}";
        let btnStyles = "";
        for (let key in btnDefaultStyles) {
        btnStyles += key + ": " + btnDefaultStyles[key] + ";";
        }
        btn.setAttribute("style", btnStyles);
        const defaultStyles = {
        width: "400px",
        height: "600px",
        "border-radius": "16px",
        "box-shadow": "rgba(100, 100, 111, 0.2) 0px 7px 29px 0px",
        position: "fixed",
        bottom: "70px",
        right: "20px",
        display: "block",
        };
        const iframe = document.createElement("iframe");
        document.body.appendChild(iframe);
        iframe.setAttribute("frameborder", "0");
        iframe.setAttribute(
        "src",
        "${enviroment.widget_base_url}/?merchant_id=${merchant_id}&background_color=${cover_color}&text_color=${text_color}&language=${language}"
        );
        let styles = "";
        for (let key in defaultStyles) {
        styles += key + ": " + defaultStyles[key] + ";";
        }
        iframe.setAttribute("style", styles);
        btn.onclick = function () {
        var display = window.getComputedStyle(iframe, null).display;
        if (display === "none") {
            iframe.style.display = "block";
        } else {
            iframe.style.display = "none";
        }
        };
    </script>
  `;
export { getCode };
