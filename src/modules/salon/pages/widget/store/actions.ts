import { make } from 'redux-chill';
import { Language } from '@api';

type WidgetConfig = {
  id: number;
  merchant_id: number;
  widget_id: number;
  domain: string;
  name: string;
  description: string;
  language: Language;
  cover_color: string;
  text_color: string;
  image_url: string;
};

const createWidget = make('[widget] create')
  .stage((config: any, callback: Function) => ({
    config,
    callback
  }))
  .stage('success', widget => widget);

const isExisDomain = make('[widget] search widget by domain').stage(
  ({ domain, callback }: { domain: string; callback?: Function }) => ({
    domain,
    callback
  })
);

const getWidget = make('[widget] get widget by domain name')
  .stage(({ domain, callback }: { domain: string; callback: Function }) => ({
    domain,
    callback
  }))
  .stage('success', widget => widget);

const getWidgets = make('[widget] get merchant widgets')
  .stage(
    ({
      merchant_id,
      callback
    }: {
      merchant_id: number;
      callback?: Function;
    }) => ({ merchant_id, callback })
  )
  .stage('success', widgets => widgets);

const updateWidget = make('[widget] update widget')
  .stage(({ widget, callback }: { widget: any; callback?: Function }) => ({
    widget,
    callback
  }))
  .stage('success', widgets => widgets);

const deleteWidget = make('[widget] delete widget').stage(
  (data: { merchant_id: number; widget_id: number }) => data
);

const uploadWidgetImage = make('[widget] upload background image').stage(
  url => url
);
export {
  isExisDomain,
  createWidget,
  getWidget,
  getWidgets,
  updateWidget,
  deleteWidget,
  uploadWidgetImage
};
