import { reducer } from 'redux-chill';
import { WidgetState } from './state';
import { createWidget, getWidget, getWidgets, updateWidget } from './actions';

const widget = reducer(new WidgetState())
  .on(createWidget.success, (state, widget) => (state.widget = widget))
  .on(getWidget.success, (state, widget) => (state.widget = widget))
  .on(getWidgets.success, (state, widgets) => (state.widgets = widgets))
  .on(updateWidget.success, (state, widget) => (state.widget = widget));

export { widget };
