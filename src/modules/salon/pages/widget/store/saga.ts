import { Saga, Payload } from 'redux-chill';
import {
  getWidget,
  createWidget,
  getWidgets,
  updateWidget,
  deleteWidget,
  isExisDomain
} from './actions';
import { StoreContext } from '@store/context';
import { put, call } from 'redux-saga/effects';
import i18next from 'i18next';
import { snack } from '@store/snackbar';

class WidgetSaga {
  @Saga(isExisDomain)
  public *isExisDomain(
    { domain, callback }: Payload<typeof isExisDomain>,
    { widget }: StoreContext
  ) {
    try {
      const { data } = yield call(widget.getWidgetByDomain, { domain });
      callback(!Boolean(data.widget));
    } catch (error) {
      throw new Error(error);
    }
  }

  @Saga(getWidget)
  public *getWidget(
    { domain, callback }: Payload<typeof getWidget>,
    { widget }: StoreContext
  ) {
    try {
      const { data } = yield call(widget.getWidgetByDomain, { domain });
      yield put(getWidget.success(data.widget));
      callback(data.widget);
    } catch (error) {
      throw new Error(error);
    }
  }
  @Saga(getWidgets)
  public *getWidgets(
    { merchant_id, callback }: Payload<typeof getWidgets>,
    { widget }: StoreContext
  ) {
    try {
      const {
        data: { result }
      } = yield call(widget.getMerchantWidgets, { merchant_id });
      yield put(getWidgets.success(result));
      callback && callback();
    } catch (error) {
      throw new Error(error);
    }
  }

  @Saga(createWidget)
  public *createWidget(
    { config, callback }: Payload<typeof createWidget>,
    { widget }: StoreContext
  ) {
    try {
      const {
        merchant_id,
        domain,
        name,
        description,
        language: { code },
        cover_color,
        text_color,
        background
      } = config;
      const {
        data: { result }
      } = yield call(widget.createMerchantWidget, {
        merchant_id,
        domain,
        name,
        description,
        language: code,
        cover_color,
        text_color,
        image_url: background
      });
      yield put(createWidget.success(result));
      const disabled = !Boolean(result);
      yield callback(disabled, result);
      yield put(snack('message.widgetCreated'));
    } catch (error) {
      yield put(snack('message.notCreatedWidget', 'error'));
    } finally {
    }
  }
  @Saga(updateWidget)
  public *updateWidget(
    data: Payload<typeof updateWidget>,
    { widget }: StoreContext
  ) {
    try {
      const {
        widget: {
          cover_color,
          description,
          domain,
          id,
          image_url,
          language,
          merchant_id,
          name,
          text_color
        },
        callback
      } = data;
      const {
        data: {
          result: { updatedRows }
        }
      } = yield call(widget.updateMerchantWidget, {
        merchant_id,
        widget_id: id,
        domain,
        name,
        description,
        language,
        cover_color,
        text_color,
        image_url
      });
      yield put(createWidget.success(updatedRows[0]));
      callback(updatedRows[0]);
      yield put(snack('message.widgetUpdatedSuccessfully'));
    } catch (error) {
      yield put(snack('message.widgetNotUpdated', 'error'));
    }
  }

  @Saga(deleteWidget)
  public *deleteWidget(
    { merchant_id, widget_id }: Payload<typeof deleteWidget>,
    { widget }: StoreContext
  ) {
    try {
      const response = yield call(widget.deleteWidget, {
        merchant_id,
        widget_id
      });
      yield put(getWidgets({ merchant_id }));
      yield put(snack('message.widgetDeletedSuccessfully'));
    } catch (error) {
      yield put(snack('message.widgetNotDeleted', 'error'));
    }
  }
}

export { WidgetSaga };
