import { Widget } from '@api';

class WidgetState {
  public widget: Widget = null;
  public widgets: Widget[] = [];
}
export { WidgetState };
