type Language = {
  id: number;
  name: string;
  code: string;
};

const getLanguages = (t): Language[] => [
  {
    id: 1,
    name: t('merchantPanel.widget.select.option.English'),
    code: 'en'
  },
  {
    id: 2,
    name: t('merchantPanel.widget.select.option.Russian'),
    code: 'ru'
  },
  {
    id: 3,
    name: t('merchantPanel.widget.select.option.Estonian'),
    code: 'et'
  },
  {
    id: 4,
    name: t('merchantPanel.widget.select.option.Ukrainian'),
    code: 'ua'
  }
];

export { getLanguages };
