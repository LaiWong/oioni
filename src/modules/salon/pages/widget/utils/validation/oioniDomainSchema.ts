import * as Yup from 'yup';
import { isExisDomain } from '../../store/actions';

const oioniDomainSchema = (t, interval, dispatch, isValidate) =>
  Yup.object().shape({
    domain: Yup.string()
      .trim()
      .nullable(false)
      .matches(
        /^[a-z]+[a-z0-9]*$/,
        t('merchantPanel.widget.oioni.validation.domain.notValidDomain!')
      )
      .min(4, t('merchantPanel.widget.oioni.validation.domain.tooShort!'))
      .max(200, t('merchantPanel.widget.oioni.validation.domain.tooLong!'))
      .required(t('merchantPanel.widget.oioni.validation.domain.Required!'))
      .test(
        'isExist',
        t('merchantPanel.widget.oioni.validation.domain.exist'),
        function(domain) {
          if (isValidate && domain) {
            return new Promise((resolve, reject) => {
              clearTimeout(interval.current);
              interval.current = setTimeout(() => {
                dispatch(
                  isExisDomain({
                    domain,
                    callback: resolve
                  })
                );
              }, 300);
            });
          } else {
            return true;
          }
        }
      ),
    info: Yup.string()
      .min(1, t('merchantPanel.widget.oioni.validation.info.tooShort!'))
      .max(30, t('merchantPanel.widget.oioni.validation.info.tooLong!'))
      .required(t('merchantPanel.widget.oioni.validation.info.Required!')),
    description: Yup.string()
      .trim()
      .min(2, t('merchantPanel.widget.oioni.validation.description.tooShort!'))
      .max(200, t('merchantPanel.widget.oioni.validation.description.tooLong!'))
      .required(
        t('merchantPanel.widget.oioni.validation.description.Required!')
      ),
    background: Yup.string()
      .nullable(false)
      .required(t('merchantPanel.widget.oioni.validation.upload.Required!'))
  });

export { oioniDomainSchema };
