import { Product, Employee } from '@api';
import {
  Button,
  Field,
  Form,
  FormError,
  Group,
  hoc,
  Language,
  Select,
  Confirm
} from '@core';
import classNames from 'classnames';
import * as React from 'react';
import { Fragment } from 'react';
import { useWidgetProps } from './widget.props';
import * as styles from './widget.scss';
import { Switch, Route } from 'react-router';
import { Oioni } from './pages/oioni';
import { Own } from './pages/own';
import { NavLink } from 'react-router-dom';
import { enviroment } from '@env';

/**
 * Renders Widget
 */
const Widget = hoc(useWidgetProps, props => {
  const {
    t,
    links,
    match,
    isOioniWidget,
    onDeleteWidgetHandler,
    btnDeleteVisible,
    btnSaveDisabled,
    form: { submitForm },
    btnPreviewDisabled,
    btnPreviewRef,
    onPreviewClick,
    selectedWidgetId,
    form
  } = props;
  return (
    <Fragment>
      <div className={styles.widget}>
        <div className={styles.header}>
          <h2 className={styles.title}>
            {t('merchantPanel.widget.title.widgetForOnlineBooking')}
          </h2>

          <div
            className={classNames(styles.buttons, {
              [styles.buttonsVisible]: isOioniWidget
            })}
          >
            <a
              ref={btnPreviewRef}
              onClick={onPreviewClick}
              className={classNames(styles.buttonPreview, {
                [styles.buttonPreviewDisabled]: btnPreviewDisabled
              })}
              target='_blank'
              href={
                form.values.domain
                  ? `https://${form.values.domain}.${enviroment.sub_domain}oioni.com`
                  : 'https://oioni.com'
              }
            >
              {t('merchantPanel.widget.oioni.btn.preview')}
            </a>
            <Button
              // {...{ disabled: !selectedWidgetId }}
              type='button'
              size='md'
              theme='teritary'
              onClick={onDeleteWidgetHandler}
              className={classNames(styles.buttonDelete, {
                [styles.buttonDeleteVisible]: btnDeleteVisible
              })}
            >
              {t('merchantPanel.widget.oioni.btn.delete')}
            </Button>
            <Button
              {...{ disabled: btnSaveDisabled }}
              type='submit'
              size='md'
              theme='primary'
              onClick={submitForm}
              className={classNames(styles.buttonSave)}
            >
              {t('merchantPanel.widget.oioni.btn.save')}
            </Button>
          </div>
        </div>
        <div className={styles.container}>
          <div className={styles.tabs}>
            {links.map((link, index) => (
              <NavLink
                key={index}
                className={classNames(styles.link)}
                activeClassName={styles.linkActive}
                to={link.to}
              >
                {link.name}
              </NavLink>
            ))}
          </div>
          <Switch>
            <Route path={match.path + '/own'} component={Own} exact />
            <Route
              path={match.path + '/oioni'}
              render={() => <Oioni {...props} />}
              exact
            />
          </Switch>
        </div>
      </div>
    </Fragment>
  );
});

export { Widget };
