import { Language, Widget } from '@api';
import { State } from '@store';
import { useFormik } from 'formik';
import { useEffect, useRef, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useRouteMatch, useLocation } from 'react-router';
import { useTranslation } from 'react-i18next';
import { useSchemaTranslationsUpdate } from '@app/hooks';
import { useWidgetNavLinks } from './hooks/use-widget-nav-links';
import {
  getWidgets,
  updateWidget,
  getWidget,
  createWidget,
  deleteWidget
} from './store/actions';
import { getLanguages } from './utils/languages';
import { oioniDomainSchema } from './utils/validation/oioniDomainSchema';
import styles from './widget.scss';
import { isEmpty } from './utils/isEmptyObject';

/**
 * Use widget props
 */
const useWidgetProps = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const links = useWidgetNavLinks();
  const { pathname } = useLocation();
  const match = useRouteMatch<{ id: string }>();
  const route = useRouteMatch<{ language: string }>();
  const [selectedWidgetId, setSelectedWidget] = useState<number | null>(null);
  let btnPreviewDisabled = !selectedWidgetId;
  useEffect(() => {
    dispatch(getWidgets({ merchant_id }));
  }, []);
  const interval = useRef<any>();

  const widgets = useSelector((state: State) => state.widget.widgets);
  // const widget = useSelector((state: State) => state.widget.widget);
  const merchant_id = useSelector(
    (state: State) => state.general.merchantUser.id
  );
  const isOioniWidget = Boolean(pathname.match(/\/widget\/oioni/));
  const languages = getLanguages(t);
  const getDefaultLanguage = defaultLanguage =>
    languages.find(option => option.code === defaultLanguage)?.id;
  const widgetDomain = useSelector(
    (state: State) => state.widget.widgets[0]?.domain
  );

  const [edit, setEdit] = useState<Boolean>(false);
  const [editVisible, setEditVisible] = useState(false);
  const getLanguage = id => languages.find(option => option.id === id);

  const domainFieldDisabled = selectedWidgetId;
  const widg = widgets.find(w => w.id === selectedWidgetId);
  const btnPreviewRef = useRef<HTMLAnchorElement>();
  const [isValidateDomain, setIsValidateDomain] = useState(true);
  useEffect(() => {}, [widgets]);
  const form = useFormik({
    initialValues: {
      info: widg?.name || '',
      domain: widg?.domain || '',
      description: widg?.description || '',
      language:
        getDefaultLanguage(widg?.language) ||
        getDefaultLanguage(route.params.language),
      background: widg?.image_url,
      coverColor: widg?.cover_color ? `#${widg?.cover_color}` : '#3c3c3c',
      textColor: widg?.text_color ? `#${widg?.text_color}` : '#ffff'
    },
    enableReinitialize: true,
    validationSchema: oioniDomainSchema(
      t,
      interval,
      dispatch,
      isValidateDomain
    ),
    onSubmit: (
      {
        info,
        domain,
        description,
        language,
        coverColor,
        textColor,
        background
      },
      { setSubmitting, resetForm }
    ) => {
      setSubmitting(true);
      dispatch(
        getWidget({
          domain,
          callback: widget => {
            if (widget) {
              const {
                domain,
                name,
                description,
                image_url,
                language,
                cover_color,
                text_color
              } = widget;
              form.setFieldValue('domain', domain);
              form.setFieldValue('info', name);
              form.setFieldValue('description', description);
              form.setFieldValue('background', image_url);
              form.setFieldValue('coverColor', `#${cover_color}`);
              form.setFieldValue('textColor', `#${text_color}`);
              form.setFieldValue(
                'language',
                languages.find(option => option.code === language).id
              );
              dispatch(
                updateWidget({
                  widget: {
                    merchant_id: widget.merchant_id,
                    id: widget.id,
                    domain: domain,
                    name: form.values.info,
                    description: form.values.description,
                    language: getLanguage(form.values.language)
                      .code as Language,
                    cover_color: form.values.coverColor.substring(1),
                    text_color: form.values.textColor.substring(1),
                    image_url: form.values.background
                  },
                  callback: widget => {
                    form.setSubmitting(false);
                    setEdit(false);
                    dispatch(getWidgets({ merchant_id }));
                    btnPreviewRef.current.classList.add(styles.pulse);
                    setSelectedWidget(widget?.id);
                  }
                })
              );
            } else {
              dispatch(
                createWidget(
                  {
                    merchant_id,
                    domain,
                    name: info,
                    description,
                    language: getLanguage(language),
                    cover_color: coverColor.substring(1),
                    text_color: textColor.substring(1),
                    background
                  },
                  (value, widget) => {
                    setSubmitting(false);
                    dispatch(getWidgets({ merchant_id }));
                    btnPreviewRef.current.classList.add(styles.pulse);
                    setSelectedWidget(widget?.id);
                  }
                )
              );
            }
          }
        })
      );
      setIsValidateDomain(false);
    }
  });
  useSchemaTranslationsUpdate(form);
  const onPreviewClick = (event: React.MouseEvent<HTMLAnchorElement>) => {
    btnPreviewDisabled
      ? event.preventDefault()
      : dispatch(getWidgets({ merchant_id }));
    btnPreviewRef.current.classList.remove(styles.pulse);
  };
  const btnSaveDisabled = form.isSubmitting || !Boolean(isEmpty(form.errors));
  const onSelectWidgetChange = (id: number) => {
    setIsValidateDomain(false);
    setSelectedWidget(id);
  };
  const onDeleteWidgetHandler = () => {
    dispatch(deleteWidget({ merchant_id, widget_id: selectedWidgetId }));
    setSelectedWidget(null);
  };
  const shape = {
    name: (option: Widget) => option.domain
  };

  const btnDeleteVisible = selectedWidgetId;

  return {
    t,
    links,
    match,
    onSelectWidgetChange,
    isOioniWidget,
    edit,
    form,
    shape,
    btnPreviewDisabled,
    languages,
    widgetDomain,
    onPreviewClick,
    btnSaveDisabled,
    btnDeleteVisible,
    editVisible,
    btnPreviewRef,
    selectedWidgetId,
    widgets,
    onDeleteWidgetHandler,
    domainFieldDisabled
  };
};

export { useWidgetProps };
