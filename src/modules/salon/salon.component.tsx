import * as React from 'react';
import * as styles from './salon.scss';
import { Route, Switch, RouteComponentProps } from 'react-router-dom';
import { Settings, Salons } from './pages';
import { Widget } from './pages/widget';

/**
 * Renders Salon
 */
const Salon: React.FC<RouteComponentProps> = ({ match }) => (
  <div className={styles.salon}>
    <Route path={match.path + '/edit/:id'} component={Settings} />
    <Switch>
      <Route path={match.path + '/add'} component={Settings} exact />
      <Route path={match.path + '/widget'} component={Widget} />
      <Route path={match.path} component={Salons} />
    </Switch>
  </div>
);

export { Salon };
