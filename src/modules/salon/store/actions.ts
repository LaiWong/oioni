import { make } from 'redux-chill';
import { Salon, Product, Employee } from '@api';
import { SalonState } from './state';

/**
 * Get salon
 */
const getSalon = make('[salon] get salon')
  .stage((id?: number) => id)
  .stage('success', (employee: Salon) => employee)
  .stage('finish');

/**
 * Save salon
 */
const saveSalon = make('[salon] save')
  .stage((data: Salon, callback) => ({
    data,
    callback
  }))
  .stage('success')
  .stage('failure', (error: string) => error)
  .stage('finish');

/**
 * Get salons
 */
const getSalons = make('[salon] get')
  .stage((params: { search?: string; page?: number } = {}) => params)
  .stage(
    'success',
    (payload: { totalPages: number; salons: Salon[] }) => payload
  )
  .stage('finish');

/**
 * Get salon employees
 */
const getSalonEmployees = make('[salon] get employees')
  .stage('success', (employees: Employee[]) => employees)
  .stage('failure')
  .stage('finish');

/**
 * Get salon services
 */
const getSalonServices = make('[salon] get salon services')
  .stage('success', (services: Product[]) => services)
  .stage('failure')
  .stage('finish');

/**
 * Get address options
 */
const getAddressOptions = make('[salon] get address options')
  .stage((query: string) => query)
  .stage('success', (options: SalonState['addresses']) => options)
  .stage('failure')
  .stage('finish');

/**
 * Get coords
 */
const getAddressCoords = make(
  '[salon] get address coords'
).stage(
  (
    id: string,
    callback: (coords: {
      lat: number;
      lng: number;
      city: string;
      country: string;
      country_code: string;
    }) => any
  ) => ({ id, callback })
);

/**
 * Delete salon
 */
const deleteSalon = make('[salon] delete').stage((salon: Salon) => salon);

/**
 * Unmount
 */
const unmount = make('[salon] unmount');

export {
  deleteSalon,
  getSalon,
  getSalons,
  saveSalon,
  getAddressCoords,
  getSalonServices,
  getSalonEmployees,
  getAddressOptions,
  unmount
};
