import { reducer } from 'redux-chill';
import { SalonState } from './state';
import {
  getAddressOptions,
  getSalon,
  getSalonEmployees,
  getSalons,
  getSalonServices,
  saveSalon,
  unmount
} from './actions';

/**
 * General state
 */
const salon = reducer(new SalonState())
  .on(getSalonEmployees.success, (state, employees) => {
    state.employees = employees;
  })
  .on(getSalonServices.success, (state, services) => {
    state.services = services;
  })
  .on(getSalons.success, (state, { salons, totalPages }) => {
    state.salons = salons;
    state.paginationLength = totalPages;
  })

  .on(saveSalon.failure, (state, error) => {
    state.errors.save = error;
  })
  .on(getSalon.success, (state, salon) => {
    state.salon = salon;
  })
  .on(getAddressOptions.success, (state, options) => {
    state.addresses = options;
  })
  .on(unmount, state => {
    state.salon = null;
    state.services = [];
    state.employees = [];
  });

export { salon };
