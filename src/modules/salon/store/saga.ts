import {
  deleteSalon,
  getAddressCoords,
  getAddressOptions,
  getSalon,
  getSalonEmployees,
  getSalons,
  getSalonServices,
  saveSalon
} from './actions';
import { Payload, Saga } from 'redux-chill';
import { StoreContext } from '@store/context';
import { State } from '@store';
import { call, delay, put, select } from 'redux-saga/effects';
import { snack } from '@store/snackbar';
import { Salon, SalonStatus } from '@api';
import { navigate } from '@store/router';
import { AxiosError } from 'axios';

class SalonSaga {
  private perPageCount = 9;

  /**
   * Get salons list
   */
  @Saga(getSalons)
  public *getSalons(
    { page, search }: Payload<typeof getSalons>,
    { salon }: StoreContext
  ) {
    try {
      const {
        general: { merchantUser }
      }: State = yield select();

      const response = yield call(salon.get, {
        search,
        page,
        perPage: this.perPageCount,
        merchant: merchantUser.id,
        status: SalonStatus.active
      });

      yield put(
        getSalons.success({
          totalPages: response.data.result.totalPages,
          salons: response.data.result.stores
        })
      );
    } catch (error) {
      yield put(snack('message.cannotGetSalons', 'error'));
    }
  }

  /**
   * Get salon
   */
  @Saga(getSalon)
  public *getSalon(id: Payload<typeof getSalon>, { salon }: StoreContext) {
    try {
      const {
        general: { merchantUser }
      }: State = yield select();

      if (!id) {
        yield put(getSalon.success({} as Salon));

        return;
      }

      const response = yield call(salon.get, {
        id,
        merchant: merchantUser.id
      });

      yield put(getSalon.success(response.data.result));
    } catch (error) {
      yield put(snack('message.cannotGetSalon', 'error'));
    }
  }

  /**
   * Save salon
   */
  @Saga(saveSalon)
  public *saveSalon(
    { data, callback }: Payload<typeof saveSalon>,
    { salon }: StoreContext
  ) {
    try {
      const {
        general: { merchantUser }
      }: State = yield select();

      const payload = {
        ...data,
        merchant_id: merchantUser.id
      };

      const isEdit = data?.id;

      yield isEdit ? call(salon.put, payload) : call(salon.post, payload);

      yield put(snack('message.salonSaved'));

      if (isEdit) {
        yield put(getSalons());
      }

      yield put(navigate('/salon'));
    } catch (err) {
      yield call(callback);

      const error = err as AxiosError;

      if (error.response.data?.message?.code === 5052) {
        yield put(saveSalon.failure('chooseAvailableCountryAdress'));

        return;
      }

      if (
        error.response.data?.message?.some(({ param }) => param === 'email')
      ) {
        yield put(saveSalon.failure('Invalid email'));

        return;
      }

      yield put(saveSalon.failure('message.somethingWentWrong'));
    }
  }

  /**
   * Get salon employees
   */
  @Saga(getSalonEmployees)
  public *getSalonEmployees(_, { employee }: StoreContext) {
    try {
      const {
        general: { merchantUser }
      }: State = yield select();

      const response = yield call(employee.get, {
        merchant: merchantUser.id,
        includeProducts: true
      });

      yield put(getSalonEmployees.success(response.data.result.employees));
    } catch (error) {
      yield put(snack('message.cannotGetEmployees', 'error'));
    }
  }

  /**
   * Get salon services
   */
  @Saga(getSalonServices)
  public *getSalonServices(_, { product }: StoreContext) {
    try {
      const {
        general: { merchantUser }
      }: State = yield select();

      const response = yield call(product.get, {
        merchant: merchantUser.id
      });

      yield put(getSalonServices.success(response.data.products));
    } catch (error) {
      yield put(snack('message.cannotGetSalonServices', 'error'));
    }
  }

  /**
   * Get options
   */
  @Saga(getAddressOptions)
  public *getAddressOptions(
    query: string,
    { google: { autocomplete } }: StoreContext
  ) {
    yield delay(300);

    const get = () =>
      new Promise((resolve, reject) =>
        autocomplete.getPlacePredictions(
          { input: query, types: ['address'] },
          resolve,
          reject
        )
      );

    const results = yield call(get);

    yield put(
      getAddressOptions.success(
        results
          ? results.map(({ place_id, description }) => ({
              id: place_id,
              name: description
            }))
          : []
      )
    );
  }

  /**
   * Get coords via callback
   */
  @Saga(getAddressCoords)
  public *getAddressCoords(
    { id, callback }: Payload<typeof getAddressCoords>,
    { google: { geocoder } }: StoreContext
  ) {
    const location = yield call(
      async () =>
        new Promise((resolve, reject) => {
          geocoder.geocode({ placeId: id }, (results, status) => {
            if (status != 'OK') return reject();

            const {
              geometry: { location },
              address_components
            } = results[0];

            resolve({
              lat: location.lat(),
              lng: location.lng(),
              city: address_components?.find(
                ({ types }) => types[0] === 'locality'
              )?.long_name,
              country: address_components?.find(
                ({ types }) => types[0] === 'country'
              )?.long_name,
              country_code: address_components?.find(
                ({ types }) =>
                  types.includes('country') && types.includes('political')
              )?.short_name
            });
          });
        })
    );

    yield call(callback, location);
  }

  /**
   * Delete salon
   */
  @Saga(deleteSalon)
  public *deleteService(
    salon: Payload<typeof deleteSalon>,
    { salon: salonAPI }: StoreContext
  ) {
    try {
      yield call(salonAPI.put, {
        ...salon,
        phone: salon.contacts,
        status: SalonStatus.disabled
      });

      yield put(snack('message.salonDeleted'));

      yield put(getSalons());
      yield put(navigate('/salon'));
    } catch (error) {
      yield put(snack('message.cannotDeleteSalon', 'error'));
    }
  }
}

const sagas = [new SalonSaga()];

export { SalonSaga, sagas };
