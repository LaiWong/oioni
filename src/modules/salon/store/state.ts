import { Employee, Product, Salon } from '@api';

class SalonState {
  /**
   * Salons list
   */
  public salons: Salon[] = [];

  /**
   * Pagination length
   */
  public paginationLength: number;

  /**
   * Opened salon
   */
  public salon: Salon = null;

  /**
   * Employees
   */
  public employees: Employee[] = [];

  /**
   * Services
   */
  public services: Product[] = [];

  /**
   * Address options
   */
  public addresses: { id: string; name: string }[] = [];

  /**
   * Erros map
   */
  public errors = {
    save: null
  };
}

export { SalonState };
