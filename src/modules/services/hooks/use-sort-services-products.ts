import { Product } from '@api';
import { useSort } from '@core';

const sortByElements = [
  'serviceName',
  'serviceCategory',
  'price',
  'salePrice',
  'duration'
];

const useSortServicesProducts = (services: Product[] = []) => {
  const {
    sortBy,
    direction,
    sortedElements: sortedServices,
    setSortBy,
    setDirection,
    setSortedElements: setSortedServices,
    sort
  } = useSort<Product>(services);

  const onSortClick = (sortBy: string) => {
    const sortByIndex = sortByElements.indexOf(sortBy);
    const servicesToSort = [...sortedServices];

    switch (sortByIndex) {
      case 0: {
        servicesToSort.sort((a, b) => sort(a, b, 'name'));

        break;
      }
      case 1: {
        servicesToSort.sort((a, b) => sort(a, b, 'category.name'));

        break;
      }
      case 2: {
        servicesToSort.sort((a, b) => sort(a, b, 'old_price', true));

        break;
      }
      case 3: {
        servicesToSort.sort((a, b) => sort(a, b, 'price', true));

        break;
      }
      case 4: {
        servicesToSort.sort((a, b) => sort(a, b, 'duration'));

        break;
      }
    }

    setDirection(direction === 'up' ? 'down' : 'up');
    setSortedServices(servicesToSort);
    setSortBy(sortBy);
  };

  return { sortedServices, sortBy, direction, onSortClick, setSortBy };
};

export { sortByElements, useSortServicesProducts };
