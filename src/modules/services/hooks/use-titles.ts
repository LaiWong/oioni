import { useTranslation } from 'react-i18next';
import { useMemo } from 'react';

/**
 * Titles
 */
const useTitles = (
  // Key is equal to component name
  key
): {
  name: string;
  atEnd?: boolean;
  bold?: boolean;
  atCenter?: boolean;
}[] => {
  const { t } = useTranslation();

  const titles = useMemo(
    () => ({
      'service-list': [
        {
          name: t('merchantPanel.service.list.serviceName')
        },
        {
          name: t('merchantPanel.service.list.serviceCategory')
        },
        {
          name: t('merchantPanel.service.list.price')
        },
        {
          name: t('merchantPanel.service.list.salePrice')
        },
        {
          name: t('merchantPanel.service.list.duration')
        },
        {}
      ]
    }),
    [t]
  );

  return titles[key.toLowerCase()];
};

export { useTitles };
