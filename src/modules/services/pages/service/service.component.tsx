import {
  Button,
  Field,
  Form,
  FormError,
  Group,
  hoc,
  Input,
  Language
} from '@core';
import classNames from 'classnames';
import * as React from 'react';
import { Fragment } from 'react';
import { durations } from './service.data';
import { useServiceProps } from './service.props';
import * as styles from './service.scss';
import { Trans } from 'react-i18next';

/**
 * Renders Service
 */
const Service = hoc(
  useServiceProps,
  ({
    t,
    ml,
    fee,
    lang,
    form,
    names,
    isNew,
    errors,
    formRef,
    categories,
    withdrawalSum,
    comissionSum,
    onCancelClick,
    isDeleteConfirmVisible,
    onDeleteClick,
    onCancelDeleteClick,
    onDeleteConfirmClick,
    onLanguageChange
  }) => (
    <Fragment>
      {!isNew && <div className={styles.overlay} />}
      <Form form={form} formRef={formRef}>
        <div
          className={classNames(styles.service, {
            [styles.serviceEdit]: !isNew
          })}
        >
          <div className={styles.header}>
            <h2 className={styles.title}>
              <div className={styles.titleCaption}>
                {isNew
                  ? t('merchantPanel.service.add.title')
                  : t('merchantPanel.service.edit.title')}
              </div>
              <Language value={lang} onChange={onLanguageChange} />
            </h2>
            <div className={styles.buttons}>
              <Button
                className={classNames(styles.button, styles.buttonColored)}
                type='submit'
                theme='primary'
              >
                {t('merchantPanel.service.form.save')}
              </Button>
              <Button
                className={classNames(styles.button, styles.buttonSecondary)}
                onClick={onCancelClick}
                theme='secondary'
              >
                {t('merchantPanel.service.form.cancel')}
              </Button>
            </div>
          </div>

          <div className={styles.content}>
            <div className={styles.column}>
              <Group
                className={styles.fieldGroup}
                title={t('merchantPanel.service.form.name')}
                titleClassName={styles.fieldGroupTitle}
                headerClassName={styles.fieldGroupHeader}
              >
                <Field.Select
                  name='category_id'
                  options={categories}
                  label={'merchantPanel.service.form.serviceCategory'}
                />
                <Field.Input
                  suggest
                  suggestions={names}
                  label={'merchantPanel.service.form.serviceName'}
                  name={ml`settings.name`}
                />
              </Group>

              <Group
                className={classNames(styles.price, styles.fieldGroup)}
                title={t('merchantPanel.service.form.price')}
                titleClassName={styles.fieldGroupTitle}
                headerClassName={styles.fieldGroupHeader}
                helper={
                  <div className={styles.fee}>
                    <Trans
                      i18nKey='merchantPanel.service.form.feeDescription'
                      values={{ withdrawalSum, fee, comissionSum }}
                      components={[
                        <strong key='1'>withdrawalSum</strong>,
                        <strong key='2'>fee</strong>,
                        <strong key='3'>comissionSum</strong>
                      ]}
                    />
                  </div>
                }
              >
                <Input
                  label={'merchantPanel.service.form.currency'}
                  value='EUR'
                  disabled
                />
                <Group.Row>
                  <Field.Input
                    className={styles.input}
                    name='old_price'
                    label={'merchantPanel.service.form.price'}
                    type={'number'}
                  />
                  <Field.Input
                    className={styles.input}
                    name='discount'
                    label={'merchantPanel.service.form.discount'}
                  />
                  <Field.Input
                    className={styles.input}
                    label={'merchantPanel.service.form.salePrice'}
                    name='price'
                    disabled
                  />
                </Group.Row>
              </Group>
              <FormError>{errors.save}</FormError>
            </div>
            <div className={styles.column}>
              <div className={styles.flex}>
                <Group
                  className={classNames(styles.duration, styles.fieldGroup)}
                  title={t('merchantPanel.service.form.duration')}
                  titleClassName={styles.fieldGroupTitle}
                  headerClassName={styles.fieldGroupHeader}
                >
                  <Field.Select
                    label={'merchantPanel.service.form.time'}
                    name='duration'
                    options={durations(t)}
                  />
                </Group>
              </div>

              <Group
                className={classNames(styles.description, styles.fieldGroup)}
                title={t('merchantPanel.service.form.description.title')}
                titleClassName={styles.fieldGroupTitle}
                headerClassName={styles.fieldGroupHeader}
              >
                <Field.Textarea
                  className={styles.textarea}
                  name={ml`settings.description`}
                  placeholder={t(
                    'merchantPanel.service.form.description.placeholder'
                  )}
                />
              </Group>
            </div>
          </div>
          {!isNew &&
            (isDeleteConfirmVisible ? (
              <Fragment>
                <Button
                  className={styles.cancelDeleteService}
                  type='button'
                  size='xs'
                  theme='teritary'
                  onClick={onCancelDeleteClick}
                >
                  {t('merchantPanel.service.form.cancelDeleteService')}
                </Button>
                <Button
                  className={styles.deleteServiceConfirm}
                  onClick={onDeleteConfirmClick}
                  size='xs'
                  theme='primary'
                  type='button'
                >
                  {t('merchantPanel.service.form.deleteServiceConfirm')}
                </Button>
              </Fragment>
            ) : (
              <Button
                className={styles.deleteService}
                size='xs'
                theme='teritary'
                type='button'
                onClick={onDeleteClick}
              >
                {t('merchantPanel.service.form.deleteService')}
              </Button>
            ))}
        </div>
      </Form>
    </Fragment>
  )
);

export { Service };
