/**
 * Durations list in minutes
 */

const durations = t =>
  new Array(32).fill(1).map((_, index) => {
    const minutes = (index + 1) * 15;
    const hours = minutes / 60;

    const text = {
      hours: Math.ceil(hours),
      minutes: minutes % 60
    };
    // const ending = text.hours > 1 ? 's' : ''; 'merchantPanel.service.time.duration.hour.ending'
    const h = t('merchantPanel.service.time.duration.hour.title');
    const m = t('merchantPanel.service.time.duration.minutes');

    return {
      id: minutes,
      name: text.minutes
        ? (hours > 1 ? `${text.hours - 1} ${h} ` : '') +
          (text.minutes + ` ${m}`)
        : `${text.hours} ${h}`
    };
  });

export { durations };
