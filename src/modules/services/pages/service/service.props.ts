import { useDispatch, useSelector } from 'react-redux';
import { useRouteMatch } from 'react-router';
import { useEffect, useMemo, useRef, useState } from 'react';
import { State } from '@store';
import { useFormik } from 'formik';
import {
  deleteService,
  getService,
  getServiceNames,
  saveService
} from '@services/store';
import { navigate } from '@store/router';
import { useClickOutside, useMeta, useMultiLanguage } from '@core';
import { serviceSchema } from '@services/validation';
import { lazy } from 'yup';
import { useTranslation } from 'react-i18next';
import { Language } from '@api';
import { useSchemaTranslationsUpdate } from '@app/hooks';
import { useMultiLanguageMapper } from '@core/hooks/use-multie-language';
import { getCategories } from '@store/general';

/**
 * Use service
 */
const useServiceProps = () => {
  const { t, i18n } = useTranslation();
  const dispatch = useDispatch();

  const {
    params: { id, language }
  } = useRouteMatch<{ id: string; language: Language }>();

  const formRef = useRef();

  const [lang, setLang, ml] = useMultiLanguage(language);
  const uml = useMultiLanguageMapper(lang);

  const {
    merchant: {
      services: { service, names, errors }
    },
    general: { categories }
  } = useSelector((state: State) => state);

  const isNew = !Boolean(id);

  const form = useFormik({
    onSubmit: (values, { setSubmitting }) => {
      dispatch(saveService(values, () => setSubmitting(false)));
    },

    validationSchema: lazy(serviceSchema),
    initialValues: useMemo(() => {
      if (!service) return {} as typeof service & { discount: number };
      return {
        ...service,
        discount: service.price ? +(service.old_price - service.price) : 0
      };
    }, [service]),
    enableReinitialize: true
  });
  useEffect(() => {
    if (!form?.values?.settings?.name) return;

    setTimeout(() => {
      form.setFieldValue(
        'name',
        form.values.settings.name[i18n.language] || ''
      );
    }, 100);
  }, [form.values.settings?.name, i18n.language]);

  useSchemaTranslationsUpdate(form);
  const onCancelClick = () => {
    dispatch(navigate('/services'));
  };
  const fee = 10;
  const sum = form.values.price;
  const withdrawalSum = isNaN(sum) || sum <= 0 ? 'n/a' : sum;
  const comissionSum = (form.values.price * (fee / 100)).toFixed(2);

  const [isDeleteConfirmVisible, setIsDeleteConfirmVisible] = useState(false);

  const onDeleteClick = () => {
    setIsDeleteConfirmVisible(true);
  };

  const onCancelDeleteClick = () => {
    setIsDeleteConfirmVisible(false);
  };

  const onDeleteConfirmClick = () => {
    dispatch(deleteService(service));
  };

  useClickOutside(formRef, () => {
    if (!id) return;

    dispatch(navigate('/services'));
  });

  useEffect(() => {
    dispatch(getCategories());
    dispatch(getServiceNames());
    dispatch(getService(id ? Number(id) : null));
  }, []);

  useEffect(() => {
    const { discount, old_price = 0 } = form.values;
    const price = old_price - discount;

    form.setFieldValue('price', price);
  }, [form.values.discount, form.values.old_price]);

  useMeta(
    {
      title: `${isNew ? 'New' : 'Edit'} service - Oioni Administration Console`
    },
    []
  );

  return {
    t,
    ml,
    fee,
    lang,
    form,
    isNew,
    errors,
    service,
    formRef,
    categories: categories.map(category => ({
      id: category.id,
      name: uml(category.settings?.name)
    })),
    withdrawalSum,
    comissionSum,
    onCancelClick,
    isDeleteConfirmVisible,
    onDeleteClick,
    onCancelDeleteClick,
    onDeleteConfirmClick,
    onLanguageChange: setLang,
    names: names.filter(item => {
      const settings = form?.values?.settings;

      if (!settings || !settings[lang]) return false;

      return item.toLowerCase().includes(settings[lang]?.name?.toLowerCase());
    })
  };
};

export { useServiceProps };
