import { SyntheticEvent, useEffect, useRef, useState } from 'react';
import { getServices, setServicesOrder } from '@services/store';
import { navigate } from '@store/router';
import { Product } from '@api';
import { useMeta, useMultiLanguageMapper, usePagination } from '@core';
import { useDispatch, useSelector } from 'react-redux';
import { State } from '@store';
import { useTranslation } from 'react-i18next';
import { useSortServicesProducts, useTitles } from '@services/hooks';

type DraggingElementCoords = {
  top: number;
  left: number;
};

/**
 * Use services list
 */
const useServicesList = () => {
  const {
    t,
    i18n: { language }
  } = useTranslation();
  const dispatch = useDispatch();
  const { products, paginationLength } = useSelector(
    (state: State) => state.merchant.services
  );

  const [formattedServices, setFormattedServices] = useState([]);

  const {
    sortedServices,
    sortBy,
    direction,
    onSortClick,
    setSortBy
  } = useSortServicesProducts(formattedServices);
  const { selectedPage, onPageChange } = usePagination(() => {
    setSortBy('');
  });

  const serviceListTitles = useTitles('service-list');

  const [query, setQuery] = useState('');
  const interval = useRef<any>();
  const uml = useMultiLanguageMapper(language);

  const servicesRef = useRef([]);

  const [draggingElement, setdraggingElement] = useState<number>(null);
  const [draggingElementCoords, setdraggingElementCoords] = useState<
    DraggingElementCoords
  >(null);

  const isAbove = (indexA: number, indexB: number) => {
    const rectA = servicesRef.current[indexA].getBoundingClientRect();
    const rectB = servicesRef.current[indexB].getBoundingClientRect();

    return rectA.top + rectA.height / 2 < rectB.top + rectB.height / 2;
  };

  const swap = (indexA: number, indexB: number) => {
    setFormattedServices(
      formattedServices.map((service, index) => {
        switch (index) {
          case indexA:
            return formattedServices[indexB];
          case indexB:
            return formattedServices[indexA];
          default:
            return service;
        }
      })
    );
  };

  const onMouseDown = (
    { clientY, clientX }: React.MouseEvent<HTMLTableRowElement, MouseEvent>,
    index: number
  ) => {
    setdraggingElement(index);
    setdraggingElementCoords({
      top: clientY,
      left: clientX
    });
  };

  const onMouseUp = () => {
    setdraggingElement(null);

    dispatch(setServicesOrder(formattedServices.map(({ id }) => id)));
  };

  const onMouseMove = ({ clientY, clientX }: MouseEvent) => {
    setdraggingElementCoords({
      top: clientY,
      left: clientX
    });

    const prevElement = draggingElement - 1;
    const nextElement = draggingElement + 1;
    const lastElement = servicesRef.current.length - 1;

    if (nextElement <= lastElement && isAbove(nextElement, draggingElement)) {
      swap(draggingElement, nextElement);
      setdraggingElement(nextElement);
    }

    if (prevElement >= 0 && isAbove(draggingElement, prevElement)) {
      swap(draggingElement, prevElement);
      setdraggingElement(prevElement);
    }
  };

  const onAddClick = () => {
    dispatch(navigate('/services/add'));
  };

  const onEditClick = (e: SyntheticEvent, product: Product) => {
    e.stopPropagation();

    dispatch(navigate('/services/edit/' + product.id));
  };

  const onQueryChange = (search: string) => {
    setQuery(search);

    clearTimeout(interval.current);

    interval.current = setTimeout(() => {
      dispatch(getServices({ search }));
    }, 300);
  };

  useEffect(() => {
    if (draggingElement === null) return;

    document.addEventListener('mousemove', onMouseMove);
    return () => document.removeEventListener('mousemove', onMouseMove);
  }, [draggingElement]);

  useEffect(() => {
    setFormattedServices(products);
  }, [products]);

  useEffect(() => {
    dispatch(getServices({ page: selectedPage }));
  }, [selectedPage]);

  useMeta({ title: 'Services - Oioni Administration Console' }, []);

  return {
    t,
    onMouseDown,
    onMouseUp,
    uml,
    query,
    sortBy,
    direction,
    onSortClick,
    serviceListTitles,
    paginationLength,
    draggingElement,
    servicesRef,
    language,
    onAddClick,
    sortedServices,
    draggingElementCoords,
    onEditClick,
    onQueryChange,
    onPageChange
  };
};

export { useServicesList };
