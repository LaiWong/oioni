import {
  Body,
  Button,
  Cell,
  formatPrice,
  Head,
  hoc,
  Pagination,
  Row,
  Search,
  Table
} from '@core';
import * as React from 'react';
import { Fragment } from 'react';
import * as styles from './services-list.scss';
import classNames from 'classnames';
import { useServicesList } from './service-list.props';
import { sortByElements } from '@services/hooks';

/**
 * Renders ServicesList
 */
const ServicesList = hoc(
  useServicesList,
  ({
    t,
    uml,
    sortedServices,
    onAddClick,
    onEditClick,
    draggingElement,
    draggingElementCoords,
    servicesRef,
    query,
    sortBy,
    direction,
    onSortClick,
    serviceListTitles,
    paginationLength,
    onQueryChange,
    onMouseDown,
    onMouseUp,
    onPageChange
  }) => (
    <div className={styles.servicesList}>
      <div className={styles.header}>
        <h2 className={styles.title}>
          {t('merchantPanel.service.list.title')}
        </h2>
        <Search
          value={query}
          onChange={onQueryChange}
          className={styles.search}
          placeholder={t('merchantPanel.service.list.searchPlaceholder')}
        />
        <Button className={styles.button} theme='primary' onClick={onAddClick}>
          {t('merchantPanel.service.list.add')}
        </Button>
      </div>
      <Table>
        <Head>
          <Row>
            {serviceListTitles?.map(
              ({ name, atEnd, atCenter, bold }, index) => (
                <Cell
                  key={index}
                  className={styles.sortElements}
                  atEnd={atEnd}
                  atCenter={atCenter}
                  bold={bold}
                  onClick={() => onSortClick(sortByElements[index])}
                >
                  {name}
                  {index === sortByElements.indexOf(sortBy) && (
                    <img
                      className={styles.sortDirection}
                      style={{
                        transform: `rotate(${
                          direction === 'up' ? '90' : '-90'
                        }deg)`
                      }}
                      src={require('img/arrow-left.png')}
                      alt={direction}
                    />
                  )}
                </Cell>
              )
            )}
          </Row>
        </Head>
        <Body>
          {sortedServices.map((product, index) => {
            const isDragging = draggingElement === index;
            return (
              <Fragment key={product.id}>
                {isDragging && (
                  <Row className={styles.serviceRowPlaceholder}>
                    <Cell colSpan={6} />
                  </Row>
                )}

                <Row
                  style={draggingElementCoords}
                  className={classNames(styles.serviceRow, {
                    [styles.serviceRowDragging]: isDragging
                  })}
                  elementRef={element => (servicesRef.current[index] = element)}
                  onMouseDown={e => onMouseDown(e, index)}
                  onMouseUp={onMouseUp}
                >
                  <Cell>{uml(product.settings.name)}</Cell>
                  <Cell>
                    {uml(product.category?.settings?.name)}
                    {/* {product.category?.settings?.name.uk} FIX IT !!! */}
                  </Cell>
                  <Cell>{formatPrice(product.old_price)}</Cell>
                  <Cell>{formatPrice(product.price)}</Cell>
                  <Cell>{product.duration}</Cell>
                  {!isDragging && (
                    <Cell>
                      <Button
                        className={styles.editService}
                        size='xs'
                        theme='teritary'
                        onMouseDown={e => onEditClick(e, product)}
                      >
                        {t('merchantPanel.service.list.edit')}
                      </Button>
                    </Cell>
                  )}
                </Row>
              </Fragment>
            );
          })}
        </Body>
      </Table>
      <Pagination length={paginationLength} onPageChange={onPageChange} />
    </div>
  )
);

export { ServicesList };
