import * as React from 'react';
import { Route, Switch } from 'react-router-dom';
import { Service, ServicesList } from './pages';
import { ServicesProps } from './services.props';
import * as styles from './services.scss';

/**
 * Renders Services
 */
const Services: React.FC<ServicesProps> = ({ match }) => (
  <div className={styles.services}>
    <Route path={match.path + '/edit/:id'} component={Service} />
    <Switch>
      <Route path={match.path + '/add'} component={Service} exact />
      <Route path={match.path} component={ServicesList} />
    </Switch>
  </div>
);

export { Services };
