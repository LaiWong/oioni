import { RouteComponentProps } from 'react-router';

/**
 * Props
 */
type ServicesProps = RouteComponentProps;

export { ServicesProps };
