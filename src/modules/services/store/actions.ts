import { make } from 'redux-chill';
import { Product } from '@api';

/**
 * Get service
 */
const getService = make('[services] get service')
  .stage((id?: number) => id)
  .stage('success', (service: Product) => service)
  .stage('finish');

/**
 * Save service
 */
const saveService = make('[service] save')
  .stage((data: Product, callback) => ({
    data,
    callback
  }))
  .stage('success')
  .stage('failure', (error: string) => error)
  .stage('finish');

/**
 * Delete service
 */
const deleteService = make('[service] delete').stage(
  (service: Product) => service
);

/**
 * Get services
 */
const getServices = make('[services] get')
  .stage((params: { search?: string; page?: number } = {}) => params)
  .stage(
    'success',
    (payload: { totalPages: number; products: Product[] }) => payload
  )
  .stage('finish');

/**
 * Get service names
 */
const getServiceNames = make('[services] get names').stage(
  'success',
  (names: string[]) => names
);

/**
 * Get services order
 */
const setServicesOrder = make('[services] set order').stage(
  (products: number[]) => products
);

export {
  getService,
  getServices,
  saveService,
  deleteService,
  getServiceNames,
  setServicesOrder
};
