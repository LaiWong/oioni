import { reducer } from 'redux-chill';
import { ServicesState } from './state';
import {
  getService,
  getServiceNames,
  getServices,
  saveService
} from './actions';

/**
 * General state
 */
const services = reducer(new ServicesState())
  .on(getService.success, (state, service) => {
    state.service = service;

    if (!state.service.settings) state.service.settings = {};
  })
  .on(getServices.success, (state, { products, totalPages }) => {
    state.products = products;
    state.paginationLength = totalPages;
  })
  .on(getServiceNames.success, (state, names) => {
    state.names = names;
  })
  .on(saveService.failure, (state, error) => {
    state.errors.save = error;
  });

export { services };
