import { Payload, Saga } from 'redux-chill';
import {
  deleteService,
  getService,
  getServiceNames,
  getServices,
  saveService,
  setServicesOrder
} from './actions';
import { StoreContext } from '@store/context';
import { call, put, select } from 'redux-saga/effects';
import { State } from '@store';
import { Product, ProductStatus } from '@api';
import { navigate } from '@store/router';
import { snack } from '@store/snackbar';

const makeService = ({
  id,
  created_at,
  updated_at,
  category,
  product_employees,
  priority,
  discount,
  image_url,
  ...rest
}: Product) => ({ ...rest });

class ServicesSaga {
  private perPageCount = 9;

  /**
   * Get service
   */
  @Saga(getService)
  public *getService(
    id: Payload<typeof getService>,
    { product }: StoreContext
  ) {
    try {
      if (!id) {
        yield put(getService.success({} as Product));

        return;
      }

      const {
        general: { merchantUser }
      }: State = yield select();

      const response = yield call(product.getOne, {
        id,
        merchant: merchantUser.id
      });

      yield put(getService.success(response.data.products[0]));
    } catch (error) {
      yield put(snack('message.cannotGetService', 'error'));
    }
  }

  /**
   * Save service
   */
  @Saga(saveService)
  public *saveService(
    { data, callback }: Payload<typeof saveService>,
    { product }: StoreContext
  ) {
    try {
      const {
        general: { merchantUser }
      }: State = yield select();

      const service = {
        ...makeService(data),
        merchant_id: merchantUser.id
      };

      const isEdit = data?.id;

      yield isEdit
        ? call(product.update, data.id, service)
        : call(product.add, service);

      yield put(snack('message.serviceSaved'));

      if (isEdit) {
        yield put(getServices());
      }

      yield put(navigate('/services'));
    } catch (error) {
      yield put(saveService.failure('message.somethingWentWrong'));
    } finally {
      yield call(callback);
    }
  }

  /**
   * Delete service
   */
  @Saga(deleteService)
  public *deleteService(
    service: Payload<typeof deleteService>,
    { product }: StoreContext
  ) {
    try {
      const { id } = service;

      yield call(product.update, id, {
        ...makeService(service),
        status: ProductStatus.disabled
      });

      yield put(snack('message.serviceDeleted'));

      yield put(getServices());
      yield put(navigate('/services'));
    } catch (error) {
      yield put(snack('message.cannotDeleteService', 'error'));
    }
  }

  /**
   * Get services list
   */
  @Saga(getServices)
  public *getServices(
    { page, search }: Payload<typeof getServices>,
    { product }: StoreContext
  ) {
    try {
      const {
        general: { merchantUser }
      }: State = yield select();

      const response = yield call(product.get, {
        page,
        name: search,
        perPage: this.perPageCount,
        merchant: merchantUser.id,
        status: ProductStatus.active
      });

      yield put(
        getServices.success({
          totalPages: response.data.totalPages,
          products: response.data.products
        })
      );
    } catch (error) {
      yield put(snack('message.cannotGetServices', 'error'));
    }
  }

  /**
   * Get service names
   */
  @Saga(getServiceNames)
  public *getServiceNames(_, { product }: StoreContext) {
    try {
      const response = yield call(product.names);

      yield put(
        getServiceNames.success(response.data.services.map(item => item.name))
      );
    } catch (error) {
      yield put(snack('message.cannotGetServicesNames', 'error'));
    }
  }

  /**
   * Get services order
   */
  @Saga(setServicesOrder)
  public *setServicesOrder(
    products: Payload<typeof setServicesOrder>,
    { product }: StoreContext
  ) {
    try {
      const {
        general: { merchantUser }
      }: State = yield select();

      yield call(product.setOrder, {
        merchant_id: merchantUser.id,
        products
      });
    } catch (error) {
      yield put(snack('message.cannotSetServicesOrder', 'error'));
    }
  }
}

const sagas = [new ServicesSaga()];

export { ServicesSaga, sagas };
