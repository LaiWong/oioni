import { Product } from '@api';

class ServicesState {
  /**
   * Opened service
   */
  public service: Product;

  /**
   * Names list
   */
  public names: string[] = [];

  /**
   * Services list
   */
  public products: Product[] = [];

  /**
   * Pagination length
   */
  public paginationLength: number;

  /**
   * Errors
   */
  public errors = {
    save: null
  };
}

export { ServicesState };
