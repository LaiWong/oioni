import { object, string, number } from 'yup';
import i18next from 'i18next';

/**
 * Service schema
 */
const serviceSchema = ({ old_price }) =>
  object({
    category_id: string()
      .required('merchantPanel.service.form.requiredServiceCategory')
      .nullable(true),
    name: string()
      .required('message.addTranslationForNameInYourCurrentLanguge')
      .nullable(true)
      .max(40, 'merchantPanel.service.form.errors.serviceNameTooLong'),
    duration: number()
      .required('merchantPanel.service.form.requiredDuration')
      .nullable(true),
    settings: object({
      name: string()
        .required()
        .nullable(true)
        .max(40, 'merchantPanel.service.form.errors.serviceNameTooLong')
        .label('merchantPanel.service.form.serviceName')
    }),
    old_price: number()
      .typeError('merchantPanel.service.form.errors.priceMustBeNumber')
      .required('merchantPanel.service.form.requiredPrice')
      .nullable(true),
    discount: number()
      .typeError('merchantPanel.service.form.errors.discountMustBeNumber')
      .required()
      .nullable(true)
      .max(
        old_price,

        'merchantPanel.service.form.errors.discountMustBeLessThanPrice'
      )
      .label('merchantPanel.service.form.discount')
  });

export { serviceSchema };
