import React from 'react';
import classNames from 'classnames';
import styles from './styles.module.scss';
import { hoc } from '@core';
import { useCalendarProps } from './calendar.props';
import { Header } from './components/header/Header';
import { WeekDays } from './components/weekDays/WeekDays';
import { Cells } from './components/cells/Cells';

const Calendar = hoc(
  useCalendarProps,
  ({ data, currentDate, nextMonth, prevMonth, onDayClick, selectedD }) => (
    <div className={classNames(styles.calendar)}>
      <Header
        currentDate={currentDate}
        nextMonth={nextMonth}
        prevMonth={prevMonth}
      />
      <WeekDays currentDate={currentDate} />
      <Cells data={data} onDayClick={onDayClick} selectedD={selectedD} />
    </div>
  )
);
export default Calendar;
