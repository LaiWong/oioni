import { State } from '@store';
import {
  getCalendar,
  getCalendarData,
  setCurrentDate,
  setSelectedDays
} from '@worktimes/store';
import { addMonths, subMonths, getMonth } from 'date-fns';
import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useRouteMatch } from 'react-router';

export const useCalendarProps = ({ onDayClick, selectedD }) => {
  const dispatch = useDispatch();
  const currentDate = useSelector(
    (state: State) => state.worktime.calendar.currentDate
  );
  const calendarData = useSelector(
    (state: State) => state.worktime.calendar.calendarData
  );
  useEffect(() => {
    dispatch(getCalendar(currentDate));
  }, [currentDate, calendarData]);
  const store_id = useSelector(
    (state: State) =>
      state.worktime.general.selectedSalon?.id ||
      state.worktime.general.selectedSalon
  );
  const employee_id = useSelector(
    (state: State) => state.worktime.general.employee?.id
  );
  const {
    params: { id }
  } = useRouteMatch<{ id: string }>();

  useEffect(() => {
    store_id &&
      employee_id &&
      dispatch(getCalendarData({ employee_id, store_id, currentDate }));
  }, [currentDate, employee_id, store_id]);
  const data = useSelector(
    (state: State) => state.worktime.calendar.daysWithSchedules
  );
  const nextMonth = () => {
    dispatch(setCurrentDate(addMonths(currentDate, 1)));
  };
  const prevMonth = () => {
    if (getMonth(currentDate) === getMonth(new Date())) {
      return;
    } else {
      dispatch(setCurrentDate(subMonths(currentDate, 1)));
    }
  };
  const onSelectedDay = day => {
    dispatch(setSelectedDays(day));
  };

  return {
    data,
    currentDate,
    nextMonth,
    prevMonth,
    onSelectedDay,
    onDayClick,
    selectedD
  };
};
