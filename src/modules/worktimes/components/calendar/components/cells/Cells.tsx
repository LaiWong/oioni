import React from 'react';
import classNames from 'classnames';
import styles from './styles.scss';
import { Day } from '../day/Day.component';
import { useSelector } from 'react-redux';
import { State } from '@store';

export const Cells = ({ data, selectedD, onDayClick }) => (
  <div className={styles.container}>
    {data.map((day, index) => (
      <Day
        key={index}
        selectedD={selectedD}
        onDayClick={onDayClick}
        day={day}
      />
    ))}
  </div>
);
