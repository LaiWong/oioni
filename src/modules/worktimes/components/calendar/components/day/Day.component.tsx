import React, { useEffect, useState } from 'react';
import { useDaysProps } from './day.props';
import styles from './styles.module.scss';
import classNames from 'classnames';
import { useDispatch } from 'react-redux';
import { format } from 'date-fns';
import { DayModel } from '../../models/day.model';
import { hoc } from '@core';

export const Day = hoc(
  useDaysProps,
  ({ day, onDayClick, salon, selectedD }: any) => (
    <div
      onClick={event => {
        salon && onDayClick(event, day);
      }}
      className={classNames(styles.day, {
        [styles.withSchdule]: day?.schedule,
        [styles.disabled]: day?.disabled,
        [styles.active]: Array.from(selectedD)?.some(
          el => el === day?.formatDate
        )
      })}
    >
      {format(day.fullDate ? day?.fullDate : 1, 'd')}
    </div>
  )
);
