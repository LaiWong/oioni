import { eachDayOfInterval, format } from 'date-fns';

export const calculateRangeOfDates = (start: string, end: string): any => {
  try {
    if (start && end) {
      let range = [new Date(start), new Date(end)];
      range.sort((a, b) => +a - +b);
      range = eachDayOfInterval({
        start: range[0],
        end: range[1]
      });
      return range.forEach(date => format(date, 'yyyy-MM-dd'));
    } else if (start) {
      return format(new Date(start), 'yyyy-MM-dd');
    } else if (end) {
      return format(new Date(end), 'yyyy-MM-dd');
    } else {
      return [];
    }
  } catch (error) {
    console.log(error);
  }
};
