import { State } from '@store';
import { setRangeOfDates, setSelectedDays, setRange } from '@worktimes/store';
import { useState, useMemo, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { DayModel } from '../../models/day.model';
import { calculateRangeOfDates } from './calculateRangeOfDates';
import { format } from 'date-fns';
import { log } from 'console';

export const useDaysProps = ({ day, onDayClick, selectedD }) => {
  const dispatch = useDispatch();
  const selectedDays = useSelector(
    (state: State) => state.worktime.calendar.selectedDays
  );
  const salon = useSelector(
    (state: State) => state.worktime.general.selectedSalon
  );

  return { day, onDayClick, selectedDays, salon, selectedD };
};
