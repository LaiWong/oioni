import React from 'react';
import classNames from 'classnames';
import styles from './styles.scss';

import arrowLeft from '../../../../static/worktime-arrowLeft.svg';
import arrowRight from '../../../../static/worktime-arrowRight.svg';
import { format } from 'date-fns';

export const Header = ({ prevMonth, nextMonth, currentDate }) => {
  const dateFormat = 'MMMM yyyy';
  return (
    <div className={classNames(styles.header, styles.row)}>
      <div className={classNames(styles.column)}>
        <div className={classNames(styles.icon)} onClick={prevMonth}>
          <img src={arrowLeft} alt='arrow Left' />
        </div>
      </div>
      <div className={classNames(styles.column)}>
        <span className={styles.date}>{format(currentDate, dateFormat)}</span>
      </div>
      <div className={classNames(styles.column, styles.colEnd)}>
        <div className={classNames(styles.icon)} onClick={nextMonth}>
          <img src={arrowRight} alt='arrow right' />
        </div>
      </div>
    </div>
  );
};
