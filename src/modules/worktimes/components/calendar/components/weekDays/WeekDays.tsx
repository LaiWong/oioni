import React from 'react';
import { startOfWeek, format, addDays } from 'date-fns';
import classNames from 'classnames';
import styles from './styles.scss';

export const WeekDays = ({ currentDate }) => {
  const dateFormat = 'EEE';
  const days = [];
  let startDate = startOfWeek(currentDate, { weekStartsOn: 1 });
  for (let i = 0; i < 7; i++) {
    days.push(
      <div className={styles.column} key={i}>
        {format(addDays(startDate, i), dateFormat)}
      </div>
    );
  }
  return <div className={classNames(styles.days)}>{days}</div>;
};
