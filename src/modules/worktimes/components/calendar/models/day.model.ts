export type schedule = {
  id: number;
  employee_id: number;
  store_id: number;
  date: string;
  time_from: string;
  timeslot_from: number;
  time_to: string;
  short_date: string;
  timeslot_to: number;
};

export type blackout = {
  id: number;
  store_id: number;
  employee_id: number;
  time_from: string;
  timeslot_from: number;
  date: string;
  timeslot_to: number;
  time_to: string;
  short_date: null | string;
};
export type DayModel = {
  id: number;
  fullDate: Date;
  formatDate: string;
  blackouts: blackout | blackout[] | null;
  schedule: schedule | null;
  disabled: boolean;
};
