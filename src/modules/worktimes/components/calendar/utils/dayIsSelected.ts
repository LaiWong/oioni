import { DayModel } from '../models/day.model';

export const dayIsSelected = (selectedDays, day) => {
  if (day) {
    const editDay = selectedDays.find(
      (el: DayModel) => el.formatDate === day.formatDate
    );
    if (editDay) {
      return (selectedDays = [
        ...selectedDays.filter(
          (el: DayModel) => el.formatDate !== day.formatDate
        )
      ]);
    } else {
      return (selectedDays = [...selectedDays, day]);
    }
  }

  return (selectedDays = [...selectedDays, day]);
};
