import React, { useRef, useState } from 'react';
// import './styles.css';
// import styles from './styles.module.scss'
import classNames from 'classnames';
import { useDispatch } from 'react-redux';

export const Select: React.FC<{
  styles: any;
  optionsList: Array<{ id: number; name: string }>;
  action: Function;
}> = ({ styles, optionsList, action }) => {
  const dispatch = useDispatch();
  const [defaultSelectText, setDefaultSelectText] = useState<any>(
    'Please, select a salon'
  );
  const [showOptionList, setShowOptionList] = useState<boolean>(false);
  const [currentOption, setCurrentOption] = useState(0);
  let myRef = useRef<any>(null);
  const handleListDisplay = () => {
    setShowOptionList(!showOptionList);
  };
  const handleOptionClick = (e: React.MouseEvent, id: any) => {
    setDefaultSelectText(e.currentTarget.getAttribute('data-name'));
    setCurrentOption(id);
    const salon = optionsList.find(option => option.id === id);
    salon && dispatch(action(salon));
    setShowOptionList(false);
  };
  return (
    <div ref={myRef} className={classNames(styles.customSelectContainer)}>
      <div
        className={classNames(styles.selectedText, {
          [styles.active]: showOptionList
        })}
        onClick={handleListDisplay}
      >
        {defaultSelectText}
      </div>
      {showOptionList && (
        <ul className={classNames(styles.selectOptions)}>
          {optionsList.map(option => (
            <li
              className={classNames(
                styles.customSelectOption,
                currentOption === option.id ? styles.currentOption : ''
              )}
              data-name={option.name}
              key={option.id}
              onClick={e => handleOptionClick(e, option.id)}
            >
              {option.name}
            </li>
          ))}
        </ul>
      )}
    </div>
  );
};
