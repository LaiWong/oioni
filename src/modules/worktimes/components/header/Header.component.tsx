import React from 'react';
import { useHeaderProps } from './header.props';
import classNames from 'classnames';
import styles from './styles.module.scss';
import { hoc } from '@core';

export const Header = hoc(useHeaderProps, ({ t, employee }) => (
  <div className={classNames(styles.header)}>
    <div className={classNames(styles.headerTitle)}>
      {t('merchantPanel.agenda.workTime')}
    </div>
    <div className={classNames(styles.headerSpecialist)}>
      <div className={classNames(styles.headerSpecialistLogo)}>
        <img src={employee?.image_url} alt='specialist' />
      </div>
      <div className={classNames()}>
        <div className={classNames(styles.headerSpecialistName)}>
          {employee?.name}
        </div>
        <div className={classNames(styles.headerSpecialistCategory)}>
          Hairstylist
        </div>
      </div>
    </div>
  </div>
));
