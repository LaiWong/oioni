import { State } from '@store';
import { useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';

export const useHeaderProps = () => {
  const { t, i18n } = useTranslation();
  const employee = useSelector(
    (state: State) => state.worktime.general.employee
  );

  return { t, employee };
};
