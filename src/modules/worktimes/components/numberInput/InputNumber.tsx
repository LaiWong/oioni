import React, { useEffect, useState } from 'react';
import styles from './styles.module.scss';
import classNames from 'classnames';
import increase from '../../../static/worktime-up.svg';
import decrease from '../../../static/worktime-down.svg';
import { useDispatch } from 'react-redux';
import { FormikProps } from 'formik';
import { format } from 'date-fns';

const InputNumber: React.FC<{
  id: string;
  handleChange?: any;
  value: string;
  timeFormat: string;
  formikProps: any;
  disabled?: boolean;
}> = ({ id, handleChange, value, formikProps, timeFormat, disabled }) => (
  <div
    className={classNames(styles.inputNumber, {
      [styles.error]:
        formikProps.errors[`${id}`] && formikProps.touched[`${id}`]
    })}
  >
    <input
      disabled={disabled}
      id={id}
      onChange={e =>
        formikProps.setFieldValue(
          id,
          timeFormat === 'HH'
            ? format(new Date().setHours(+e.currentTarget.value), timeFormat)
            : format(new Date().setMinutes(+e.currentTarget.value), timeFormat)
        )
      }
      className={classNames(styles.input)}
      type='number'
      value={value}
    />
    {formikProps.errors[`${id}`] && (
      <div className={classNames(styles.errorMessage)}>
        {formikProps.errors[`${id}`]}
      </div>
    )}
  </div>
);
export { InputNumber };
