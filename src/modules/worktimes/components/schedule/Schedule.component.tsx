import React from 'react';
import styles from './styles.module.scss';
import classNames from 'classnames';
import { ScheduleList } from './list/ScheduleList.component';

export const Schedule = ({ setRanges, formikProps }: any) => (
  <div className={classNames(styles.schedule)}>
    <ScheduleList formikProps={formikProps} setRanges={setRanges} />
  </div>
);
