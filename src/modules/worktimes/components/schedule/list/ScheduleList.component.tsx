import React, { useEffect } from 'react';
import { useScheduleListProps } from './scheduleListProps';
import styles from './styles.module.scss';
import classNames from 'classnames';
import { ScheduleItem } from './scheduleItem/ScheduleItem.component';
import { hoc } from '@core';

export const ScheduleList = hoc(
  useScheduleListProps,
  ({ formikProps, selectedSalon, scheduleList, setRanges }: any) => {
    useEffect(() => {}, [selectedSalon]);
    return (
      <div className={classNames(styles.scheduleList)}>
        {scheduleList.map((array, index) => (
          <ScheduleItem
            formikProps={formikProps}
            setRanges={setRanges}
            key={index}
            el={array}
          />
        ))}
      </div>
    );
  }
);
