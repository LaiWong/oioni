import React from 'react';
import classNames from 'classnames';
import styles from './styles.module.scss';
import { useScheduleProps } from './schedule.props';
import { hoc, Button } from '@core';
import { format } from 'date-fns';

export const ScheduleItem = hoc(
  useScheduleProps,
  ({ t, disabled, schedule, el, deleteScheduleHandler, editHandler }) => (
    <div className={classNames(styles.scheduleItem_wrapper)}>
      <div className={classNames(styles.title)}>
        {t('merchantPanel.agenda.schedules.title.Schedule')}
      </div>
      <div className={classNames(styles.scheduleItem)}>
        <div className={classNames(styles.date)}>
          <div>{format(new Date(schedule[0].schedule.date), 'LLLL')}</div>
          <div className={classNames(styles.days)}>
            {schedule.map((element, index) => (
              <span className={classNames(styles.list)} key={index}>
                {format(new Date(element.schedule.date), 'd')},
              </span>
            ))}
          </div>
        </div>
        <div className={classNames(styles.worktime)}>
          <div className={classNames(styles.worktimeTitle)}>
            {t('merchantPanel.agenda.schedules.worktime.title.WorkingTime')}
          </div>
          <div className={classNames(styles.worktimeValue)}>
            {schedule[0].schedule.time_from.slice(0, 5)}-
            {schedule[0].schedule.time_to.slice(0, 5)}
          </div>
        </div>
        <div className={classNames(styles.timeoff)}>
          <div className={classNames(styles.timeoffTitle)}>
            {t('merchantPanel.agenda.schedules.timeoff.title.TimeOff')}
            (optional)
          </div>
          <div className={classNames(styles.timeoffValue)}>
            {schedule[0].timeoff
              ? `${schedule[0].timeoff?.time_from.slice(
                  0,
                  5
                )} - ${schedule[0].timeoff?.time_to.slice(0, 5)}`
              : t('merchantPanel.agenda.schedules.timeoff.WithoutTimeoff')}
          </div>
        </div>
        <div className={classNames(styles.buttons)}>
          <Button
            // disabled={disabled}
            theme={true ? 'secondary' : 'primary'}
            size='sm'
            type='button'
            onClick={() => editHandler(el)}
            className={classNames(styles.buttonEdit)}
          >
            {t('merchantPanel.agenda.schedules.buttons.Edit')}
          </Button>
          <Button
            theme={true ? 'secondary' : 'primary'}
            size='sm'
            type='button'
            onClick={() => deleteScheduleHandler(el)}
            className={classNames(styles.buttonDelete)}
          >
            {t('merchantPanel.agenda.schedules.buttons.Delete')}
          </Button>
        </div>
      </div>
    </div>
  )
);
