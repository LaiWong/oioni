import { deleteSchedulesById, editSchedules } from '@worktimes/store';
import { format } from 'date-fns';
import { useDispatch } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { useEffect } from 'react';

export const useScheduleProps = ({ el, setRanges, formikProps }) => {
  const { t, i18n } = useTranslation();
  const dispatch = useDispatch();
  const disabled = true;
  const schedule = el;
  const deleteScheduleHandler = list => {
    dispatch(deleteSchedulesById(list));
    setRanges(new Set());
  };
  const editHandler = list => {
    dispatch(editSchedules(list));
    const dates: string | string[] = [];
    for (let index = 0; index < list.length; index++) {
      const element = list[index];
      dates.push(format(new Date(element.schedule.date), 'yyyy-MM-dd'));
    }
    setRanges(new Set(dates));
    formikProps.setFieldValue(
      'worktimeHoursFrom',
      list[0]?.schedule.time_from.split(':')[0]
    );
    formikProps.setFieldValue(
      'worktimeMinutesFrom',
      list[0]?.schedule.time_from.split(':')[1]
    );
    formikProps.setFieldValue(
      'worktimeHoursTo',
      list[0]?.schedule.time_to.split(':')[0]
    );
    formikProps.setFieldValue(
      'worktimeMinutesTo',
      list[0]?.schedule.time_to.split(':')[1]
    );

    if (list[0].timeoff) {
      formikProps.setFieldValue(
        'timeoffHoursFrom',
        list[0]?.timeoff?.time_from.split(':')[0]
      );
      formikProps.setFieldValue(
        'timeoffMinutesFrom',
        list[0]?.timeoff?.time_from.split(':')[1]
      );
      formikProps.setFieldValue(
        'timeoffHoursTo',
        list[0]?.timeoff?.time_to.split(':')[0]
      );
      formikProps.setFieldValue(
        'timeoffMinutesTo',
        list[0]?.timeoff?.time_to.split(':')[1]
      );
      formikProps.setFieldValue('addTimeoff', true);
    } else {
      formikProps.setFieldValue('timeoffHoursFrom', '00');
      formikProps.setFieldValue('timeoffMinutesFrom', '00');
      formikProps.setFieldValue('timeoffHoursTo', '00');
      formikProps.setFieldValue('timeoffMinutesTo', '00');
      formikProps.setFieldValue('addTimeoff', false);
    }
  };

  return {
    t,
    disabled,
    schedule,
    el,
    deleteScheduleHandler,
    editHandler,
    setRanges,
    formikProps
  };
};
