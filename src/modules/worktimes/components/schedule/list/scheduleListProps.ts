import { State } from '@store';
import { getScheduleList } from '@worktimes/store';
import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useRouteMatch } from 'react-router';

export const useScheduleListProps = ({ setRanges, formikProps }) => {
  const dispatch = useDispatch();
  const {
    params: { id }
  } = useRouteMatch<{ id: string }>();
  const store_id = useSelector(
    (state: State) =>
      state.worktime.general.selectedSalon?.id ||
      state.worktime.general.selectedSalon
  );
  const calendarData = useSelector(
    (state: State) => state.worktime.calendar.calendarData
  );
  useEffect(() => {
    dispatch(getScheduleList());
  }, [store_id, calendarData, id]);

  const selectedSalon = useSelector(
    (state: State) => state.worktime.general.selectedSalon
  );

  const scheduleList = useSelector(
    (state: State) => state.worktime.schedule.scheduleList
  );
  return {
    selectedSalon,
    scheduleList,
    setRanges,
    formikProps
  };
};
