import React from 'react';
import classNames from 'classnames';
import styles from './selectOfSalons.module.scss';
import SalonStyles from './styles.module.scss';
import { useSalonsProps } from './select.props';
import { hoc, Select } from '@core';

// import { Select } from '../dropdown';
import { setSelectedSalon } from '@worktimes/store/general';
import { useDispatch } from 'react-redux';

export const Salons = hoc(useSalonsProps, ({ t, salons, selectedSalon }) => {
  const dispatch = useDispatch();
  return (
    <div className={classNames(SalonStyles.salons)}>
      <div className={classNames(SalonStyles.salonsTitle)}>
        {t('merchantPanel.agenda.chooseSalon')}
      </div>
      <div>
        <Select
          value={selectedSalon}
          options={salons}
          placeholder={t('merchantPanel.agenda.pleaseSelectASalon!')}
          onChange={option => {
            dispatch(setSelectedSalon(option));
          }}
        />
      </div>
    </div>
  );
});
