import { State } from '@store';
import { useEffect } from 'react';
import { useSelector } from 'react-redux';
import { useRouteMatch } from 'react-router';
import { useTranslation } from 'react-i18next';

export const useSalonsProps = () => {
  const { t, i18n } = useTranslation();
  const {
    params: { id }
  } = useRouteMatch<{ id: string }>();

  useEffect(() => {}, [id]);
  const salons = useSelector((state: State) => state.worktime.general.salons);
  const selectedSalon = useSelector(
    (state: State) => state.worktime.general.selectedSalon
  );

  return {
    t,
    salons,
    selectedSalon
  };
};
