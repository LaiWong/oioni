import React from 'react';
import { useTimerProps } from './timer.props';
import classNames from 'classnames';
import styles from './styles.module.scss';
import { TimeController } from './components/controller/TimeController.component';
import { hoc, Button, Form } from '@core';
import { Formik } from 'formik';
import Calendar from '../calendar/Calendar.component';
// import { Button } from '../button/Button.component';
import { Schedule } from '../schedule';
import { validatioSchema } from './utils/validationSchema';
import { Controller } from './components/formik/Controller.component';

export const Timer = hoc(
  useTimerProps,
  ({ t, disabled, form, onDayClick, selectedD, setRanges }) => (
    <Form className={classNames(styles.container)} form={form}>
      <div className={classNames(styles.timer)}>
        <div className={classNames(styles.calendar)}>
          <Calendar selectedD={selectedD} onDayClick={onDayClick} />
        </div>
        <div className={classNames(styles.timeController)}>
          <TimeController disabled={disabled} formikProps={form} />
          <div className={classNames(styles.buttons)}>
            <Button
              {...{ disabled }}
              theme={true ? 'secondary' : 'primary'}
              size='sm'
              type='submit'
              className={classNames(styles.buttonsSave)}
            >
              {t('merchantPanel.agenda.buttons.save')}
            </Button>
          </div>
        </div>
      </div>
      <Schedule formikProps={form} setRanges={setRanges} />
    </Form>
  )
);
