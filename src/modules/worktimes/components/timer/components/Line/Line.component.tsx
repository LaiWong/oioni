import React from 'react';
import styles from './line.scss';
import classNames from 'classnames';
import line from '../../../../static/controller-line.svg';

export const Line = () => (
  <div className={classNames(styles.line)}>
    <img src={line} alt='line' />
  </div>
);
