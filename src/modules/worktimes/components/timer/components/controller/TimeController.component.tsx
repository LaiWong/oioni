import React from 'react';
import styles from './styles.module.scss';
import { useTimeControllerProps } from './timeController.props';
import { Worktime } from './worktime/Worktime.component';
import { Timeoff } from './timeoff/Timeoff.component';
import { hoc } from '@core';

export const TimeController = hoc(
  useTimeControllerProps,
  ({ formikProps, disabled }: any) => (
    <div className={styles.controller}>
      <Worktime disabled={disabled} formikProps={formikProps} />
      <Timeoff disabled={disabled} formikProps={formikProps} />
    </div>
  )
);
