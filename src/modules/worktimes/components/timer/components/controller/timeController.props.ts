import { removeAllBlackouts } from '@worktimes/store';
import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { State } from '@store';

export const useTimeControllerProps = ({ formikProps, disabled }) => {
  const dayIsSelected = false;
  return { formikProps, dayIsSelected, disabled };
};
