import React from 'react';
import { useTimeoffProps } from './timeoff.props';
import styles from './styles.scss';
import { InputNumber } from '../../../../numberInput/InputNumber';
import cross from '../../../../../static/worktime-cross1.svg';
import line from '../../../../../static/worktime-line.svg';
import { hoc } from '@core';
import { Field } from 'formik';

export const Timeoff = hoc(
  useTimeoffProps,
  ({ t, deleteTimeoffHandler, formikProps, disabled }) => (
    <div className={styles.timeoff}>
      <div className={styles.title}>
        {formikProps.values.addTimeoff && (
          <div className={styles.optional}>
            {t('merchantPanel.agenda.controller.title.timeOff')}
            (optional)
          </div>
        )}
        <div className={styles.checkbox}>
          <span className={styles.checkboxTitle}>
            {t('merchantPanel.agenda.controller.checkbox.title.addTimeoff?')}
          </span>
          <Field name='addTimeoff' type='checkbox' />
        </div>
      </div>
      {formikProps.values.addTimeoff && (
        <div className={styles.container}>
          <div className={styles.buttonWrapper}>
            <InputNumber
              disabled={
                !disabled && formikProps.values.addTimeoff ? false : true
              }
              id='timeoffHoursFrom'
              value={formikProps.values.timeoffHoursFrom}
              formikProps={formikProps}
              timeFormat='HH'
            />
            <InputNumber
              disabled={
                !disabled && formikProps.values.addTimeoff ? false : true
              }
              id='timeoffMinutesFrom'
              value={formikProps.values.timeoffMinutesFrom}
              formikProps={formikProps}
              timeFormat='mm'
            />
          </div>
          <div className={styles.line}>
            <img src={line} alt='line' />
          </div>
          <div className={styles.buttonWrapper}>
            <InputNumber
              disabled={
                !disabled && formikProps.values.addTimeoff ? false : true
              }
              id='timeoffHoursTo'
              value={formikProps.values.timeoffHoursTo}
              formikProps={formikProps}
              timeFormat='HH'
            />
            <InputNumber
              disabled={
                !disabled && formikProps.values.addTimeoff ? false : true
              }
              id='timeoffMinutesTo'
              value={formikProps.values.timeoffMinutesTo}
              formikProps={formikProps}
              timeFormat='mm'
            />
            <div className={styles.icon}>
              <img
                onClick={deleteTimeoffHandler}
                src={cross}
                alt='default value'
              />
            </div>
          </div>
        </div>
      )}
    </div>
  )
);
