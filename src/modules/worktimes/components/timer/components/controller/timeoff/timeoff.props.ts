import { State } from '@store';
import { useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';

export const useTimeoffProps = ({ formikProps }) => {
  const { t } = useTranslation();
  const deleteTimeoffHandler = () => {
    formikProps.setFieldValue('timeoffHoursFrom', '00');
    formikProps.setFieldValue('timeoffHoursTo', '00');
    formikProps.setFieldValue('timeoffMinutesFrom', '00');
    formikProps.setFieldValue('timeoffMinutesTo', '00');
  };
  const disabled = useSelector(
    (state: State) => !state.worktime.calendar.selectedDays.length
  );
  return {
    t,
    deleteTimeoffHandler,
    formikProps,
    disabled
  };
};
