import React from 'react';
import { useWorktimeProps } from './worktime.props';
import classNames from 'classnames';
// import styles from '../styles.module.scss';
import styles from './styles.scss';
import { InputNumber } from '../../../../numberInput/InputNumber';
import line from '../../../../../static/worktime-line.svg';
import cross from '../../../../../static/worktime-cross1.svg';
import { hoc } from '@core';

export const Worktime = hoc(
  useWorktimeProps,
  ({ t, setTimeoffDefaultValueHandler, formikProps, disabled }) => (
    <div className={styles.worktime}>
      <div className={styles.title}>
        {t('merchantPanel.agenda.controller.title.workingTime')}
      </div>
      <div className={styles.container}>
        <div className={styles.buttonWrapper}>
          <InputNumber
            disabled={disabled}
            id='worktimeHoursFrom'
            value={formikProps.values.worktimeHoursFrom}
            formikProps={formikProps}
            timeFormat='HH'
          />
          <InputNumber
            disabled={disabled}
            id='worktimeMinutesFrom'
            value={formikProps.values.worktimeMinutesFrom}
            formikProps={formikProps}
            timeFormat='mm'
          />
        </div>
        <div className={styles.line}>
          <img src={line} alt='line' />
        </div>
        <div className={styles.buttonWrapper}>
          <InputNumber
            disabled={disabled}
            id='worktimeHoursTo'
            value={formikProps.values.worktimeHoursTo}
            formikProps={formikProps}
            timeFormat='HH'
          />
          <InputNumber
            disabled={disabled}
            id='worktimeMinutesTo'
            value={formikProps.values.worktimeMinutesTo}
            formikProps={formikProps}
            timeFormat='mm'
          />
          <div className={classNames(styles.icon)}>
            <img
              onClick={setTimeoffDefaultValueHandler}
              src={cross}
              alt='default value'
            />
          </div>
        </div>
      </div>
    </div>
  )
);
