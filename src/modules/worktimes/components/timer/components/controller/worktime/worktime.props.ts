import { State } from '@store';
import { useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';

export const useWorktimeProps = ({ formikProps, disabled }) => {
  const { t, i18n } = useTranslation();
  const setTimeoffDefaultValueHandler = () => {
    formikProps.setFieldValue('worktimeHoursFrom', '00');
    formikProps.setFieldValue('worktimeHoursTo', '00');
    formikProps.setFieldValue('worktimeMinutesFrom', '00');
    formikProps.setFieldValue('worktimeMinutesTo', '00');
  };
  // const disabled = useSelector((state: State) =>
  //   state.worktime.calendar.selectedDays.length > 0 ||
  //   !state.worktime.controller.disabled
  //     ? false
  //     : true
  // );
  // const disabled = useSelector((state: State) =>
  //   state.worktime.calendar.selectedDays.length > 0 ||
  //   !state.worktime.calendar.controllerDisabled
  //     ? false
  //     : true
  // );
  // const disabled = true;
  return { t, setTimeoffDefaultValueHandler, formikProps, disabled };
};
