import { Button, Field, Form, FormError, Group, hoc } from '@core';
import React from 'react';
import { useControllerProps } from './controller.props';
import styles from './controller.scss';
import classNames from 'classnames';
import { Line } from '../Line/Line.component';

export const Controller = hoc(useControllerProps, ({ t, mask, form }) => (
  <Form className={classNames(styles.form)} form={form}>
    <span className={classNames(styles.title)}>Working time</span>
    <div className={classNames(styles.group)}>
      <Field.Input
        className={classNames(styles.input, {
          [styles.error]: form.errors.worktimeHoursFrom
        })}
        type='number'
        name='worktimeHoursFrom'
      />
      <Field.Input
        className={classNames(styles.input, {
          [styles.error]: form.errors.worktimeMinutesFrom
        })}
        type='number'
        name='worktimeMinutesFrom'
      />
      <Line />
      {/* <FormError>{form.errors}</FormError> */}
      <Field.Input
        className={classNames(styles.input, {
          [styles.error]: form.errors.worktimeHoursTo
        })}
        type='number'
        name='worktimeHoursTo'
      />
      <Field.Input
        className={classNames(styles.input, {
          [styles.error]: form.errors.worktimeMinutesTo
        })}
        type='number'
        name='worktimeMinutesTo'
      />
    </div>
    <span className={classNames(styles.title)}>Time off (optional)</span>
    <div className={classNames(styles.group)}>
      <Field.Input
        className={classNames(styles.input, {
          [styles.error]: form.errors.timeoffHoursFrom
        })}
        type='number'
        name='timeoffHoursFrom'
      />
      <Field.Input
        className={classNames(styles.input, {
          [styles.error]: form.errors.timeoffMinutesFrom
        })}
        type='number'
        name='timeoffMinutesFrom'
        {...{ max: 59 }}
      />
      <Line />
      {/* <FormError>{form.errors}</FormError> */}
      <Field.Input
        className={classNames(styles.input, {
          [styles.error]: form.errors.timeoffHoursTo
        })}
        type='number'
        name='timeoffHoursTo'
      />
      <Field.Input
        className={classNames(styles.input, {
          [styles.error]: form.errors.timeoffMinutesTo
        })}
        type='number'
        name='timeoffMinutesTo'
      />
      <FormError>
        {/* {form.errors.worktimeHoursTo || form.errors.worktimeMinutesTo} */}
      </FormError>
    </div>

    <Button className={styles.button} type='submit' theme='primary' size='md'>
      {t('Save')}
    </Button>
  </Form>
));
