import { useSchemaTranslationsUpdate } from '@app/hooks';
import { State } from '@store';
import { useFormik } from 'formik';
import { useEffect, useMemo } from 'react';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';
import { lazy } from 'yup';
import { controllerSchema } from './validations/controllerSchema';

export const useControllerProps = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const form = useFormik({
    initialValues: {
      worktimeHoursFrom: 0,
      worktimeMinutesFrom: 0,
      worktimeHoursTo: 0,
      worktimeMinutesTo: 0,
      timeoffHoursFrom: 0,
      timeoffMinutesFrom: 0,
      timeoffHoursTo: 0,
      timeoffMinutesTo: 0
    },
    onSubmit: (props, { setSubmitting }) =>
      new Promise((resolve, reject) => {
        setSubmitting(true);
        resolve(
          setTimeout(() => {
            alert(props);
          }, 2000)
        );
      }).then(() => {
        setSubmitting(false);
      }),
    validationSchema: () => controllerSchema()
  });

  useSchemaTranslationsUpdate(form);

  const { values } = form;

  // const mask = useMemo(
  //   () =>
  //     countries
  //       .find(one => one.id == values.country)
  //       ?.validations?.replace(/_/gi, '9'),
  //   [values.country]
  // );

  const mask = useMemo(() => {
    // return /values/.replace(/_/gi, '9');
  }, [values]);

  useEffect(() => {
    // dispatch(getCountries());
  }, []);

  return {
    t,
    mask,
    form
  };
};
