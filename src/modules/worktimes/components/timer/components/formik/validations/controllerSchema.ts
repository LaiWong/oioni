import { object, string } from 'yup';
import * as Yup from 'yup';
import { testers } from '@core';
import i18next from 'i18next';

/**
 * Login form schema
 */
const controllerSchema = () =>
  object({
    worktimeHoursFrom: Yup.number()
      .min(0, 'Mininum 0 hours')
      .max(23, 'Maxinum 23 hours')
      .required('Required field'),
    worktimeHoursTo: Yup.number()
      .min(0, 'Mininum 0 hours')
      .max(23, 'Maxinum 23 hours')
      .required('Required field'),
    worktimeMinutesFrom: Yup.number()
      .min(0, 'Mininum 0 minutes')
      .max(59, 'Maxinum 59 minutes')
      .required('Required field'),
    worktimeMinutesTo: Yup.number()
      .min(0, 'Mininum 0 minutes')
      .max(59, 'Maxinum 59 minutes')
      .required('Required field'),
    timeoffHoursFrom: Yup.number()
      .min(0, 'Mininum 0 hours')
      .max(23, 'Maxinum 23 hours'),
    timeoffHoursTo: Yup.number()
      .min(0, 'Mininum 0 hours')
      .max(23, 'Maxinum 23 hours'),
    timeoffMinutesFrom: Yup.number()
      .min(0, 'Mininum 0 minutes')
      .max(59, 'Maxinum 59 minutes'),
    timeoffMinutesTo: Yup.number()
      .min(0, 'Mininum 0 minutes')
      .max(59, 'Maxinum 59 minutes')
  });

export { controllerSchema };
