import { State } from '@store';
import { updateSchedules, upsert, setRangeOfDates } from '@worktimes/store';
import { useDispatch, useSelector } from 'react-redux';
import { useFormik } from 'formik';
import { validatioSchema } from './utils/validationSchema';
import { useState } from 'react';
import { eachDayOfInterval } from 'date-fns/esm';
import { format } from 'date-fns';
import { useTranslation } from 'react-i18next';

export const useTimerProps = () => {
  const { t, i18n } = useTranslation();
  const [ranges, setRanges] = useState<any>(new Set());
  const [start, setStart] = useState<any>(null);
  const disabled = Boolean(Array.from(ranges).length) === false ? true : false;
  const dispatch = useDispatch();
  const form = useFormik({
    initialValues: {
      worktimeHoursFrom: '00',
      worktimeHoursTo: '00',
      worktimeMinutesFrom: '00',
      worktimeMinutesTo: '00',
      timeoffHoursFrom: '00',
      timeoffHoursTo: '00',
      timeoffMinutesFrom: '00',
      timeoffMinutesTo: '00',
      addTimeoff: false
    },
    onSubmit: (values, { setSubmitting, resetForm }) => {
      const data = {
        times: values,
        dates: Array.from(ranges),
        callback: () => {
          resetForm();
          setSubmitting(false);
          setRanges(new Set());
        }
      };
      dispatch(upsert(data));
    },
    validationSchema: validatioSchema(t)
  });

  const calculateRangeOfDates = (start: string, end: string) => {
    if (start && end) {
      let range: string[] | Date[] = [new Date(start), new Date(end)];
      range.sort((a, b) => +a - +b);
      range = eachDayOfInterval({
        start: range[0],
        end: range[1]
      });
      range = range.map(date => format(date, 'yyyy-MM-dd'));
      return range;
    } else if (start) {
      let range: string[] | Date[] = [new Date(start), new Date(start)];
      range.sort((a, b) => +a - +b);
      range = eachDayOfInterval({
        start: range[0],
        end: range[1]
      });
      range = range.map(date => format(date, 'yyyy-MM-dd'));
      return range;
    } else if (end) {
      let range: string[] | Date[] = [new Date(end), new Date(end)];
      range.sort((a, b) => +a - +b);
      range = eachDayOfInterval({
        start: range[0],
        end: range[1]
      });
      range = range.map(date => format(date, 'yyyy-MM-dd'));
      return range;
    }
  };

  const onDayClick = (event, values) => {
    if (values.disabled) {
      return;
    }
    if (event.shiftKey) {
      if (start) {
        const newRanges = new Set(ranges);
        const newRange = calculateRangeOfDates(start, values.formatDate);
        for (let i = 0; i < newRange.length; i++) {
          const element = newRange[i];
          if (ranges.has(element)) {
            setRanges(newRanges);
          } else {
            newRanges.add(element);
            setRanges(newRanges);
          }
        }
        setStart(null);
      } else {
        const newRanges = new Set(ranges);
        setStart(values.formatDate);
        newRanges.add(values.formatDate);
        setRanges(newRanges);
      }
    } else if (event.ctrlKey) {
      if (ranges.has(values.formatDate)) {
        const newRanges = new Set(ranges);
        newRanges.delete(values.formatDate);
        setRanges(newRanges);
      } else {
        const newRanges = new Set(ranges);
        newRanges.add(values.formatDate);
        setRanges(newRanges);
      }
    } else {
      const newRanges = new Set();
      setRanges(newRanges.add(values.formatDate));
      setStart(values.formatDate);
    }

    // dispatch(setRangeOfDates(ranges));
  };
  return {
    t,
    form,
    dispatch,
    disabled,
    onDayClick,
    selectedD: ranges,
    setRanges
  };
};
