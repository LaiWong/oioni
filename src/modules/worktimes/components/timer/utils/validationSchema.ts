import * as yup from 'yup';

export const validatioSchema = t =>
  yup.object().shape({
    worktimeHoursFrom: yup
      .number()
      .transform((value, originalValue) => value)
      .lessThan(
        yup.ref('worktimeHoursTo'),
        `${t(
          'merchantPanel.agenda.controller.validation.worktimeHours.startingTimeCannotBeBeforeEndTime'
        )}`
      )
      .required('This field is required!'),
    worktimeHoursTo: yup
      .number()
      .transform((value, originalValue) => value)
      .moreThan(
        yup.ref('worktimeHoursFrom'),
        `${t(
          'merchantPanel.agenda.controller.validation.worktimeHours.endTimeCannotBeBeforeStartTime'
        )}`
      )
      .required('This field is required!'),
    worktimeMinutesFrom: yup
      .number()
      .transform((value, originalValue) => value),
    worktimeMinutesTo: yup.number().transform((value, originalValue) => value),
    timeoffHoursFrom: yup.number().transform((value, originalValue) => value),
    // .lessThan(yup.ref('timeoffHoursTo'), 'must be less than timeoff to'),
    timeoffHoursTo: yup.number().transform((value, originalValue) => value),
    // .moreThan(yup.ref('timeoffHoursFrom'), 'must be more than timeoff from'),
    // .oneOf([yup.ref('timeoffHoursFrom'), null], 'Passwords must match'),
    timeoffMinutesFrom: yup.number().transform((value, originalValue) => value),
    timeoffMinutesTo: yup.number().transform((value, originalValue) => value)
  });
