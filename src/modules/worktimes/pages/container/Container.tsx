import React, { useEffect } from 'react';
import classNames from 'classnames';
import styles from './container.module.scss';
import { Salons, Schedule, Timer } from '@worktimes/components';
import { useSelector } from 'react-redux';
import { State } from '@store';

export const Container = () => {
  const disabled = useSelector((state: State) =>
    state.worktime.general.selectedSalon ? false : true
  );
  return (
    <div className={classNames(styles.wrapper)}>
      <div className={classNames(styles.container)}>
        <Salons />
        {disabled ? null : <Timer />}
      </div>
    </div>
  );
};
