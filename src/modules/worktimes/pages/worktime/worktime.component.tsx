import { hoc } from '@core';
import classNames from 'classnames';
import React from 'react';
import { Header } from '../../components/header/Header.component';
import { Container } from '../container/Container';
import { useWorktimeProps } from './worktime.props';
import * as styles from './worktime.scss';

const Worktime = hoc(useWorktimeProps, () => (
  <div className={classNames(styles.worktime)}>
    <Header />
    <Container />
  </div>
));
export { Worktime };
