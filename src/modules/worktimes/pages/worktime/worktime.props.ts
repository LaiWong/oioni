import { State } from '@store';
import { getOneEmployee, getSalons, setSelectedSalon } from '@worktimes/store';
import { useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';
import { useRouteMatch } from 'react-router';

const useWorktimeProps = () => {
  const { t } = useTranslation();
  const {
    params: { id }
  } = useRouteMatch<{ id: string }>();

  const merchant_id = useSelector(
    (state: State) => state.general.merchantUser.id
  );

  const dispatch = useDispatch();
  useEffect(() => {
    id ? dispatch(getOneEmployee(merchant_id, Number(id))) : null;
  }, [id]);
  useEffect(() => {
    dispatch(getSalons(id ? Number(id) : null));
    dispatch(setSelectedSalon(null));
  }, [id]);

  return {};
};
export { useWorktimeProps };
