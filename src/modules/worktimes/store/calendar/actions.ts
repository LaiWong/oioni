import { DayModel } from '@worktimes/components/calendar/models/day.model';
import { make } from 'redux-chill';
import { Day } from 'src/api/models/worktime';

export const setCurrentDate = make('[calendar]set current date]').stage(
  (date: Date) => date
);
export const setSelectedDays = make('[calendar]set selected days]').stage(
  (day: DayModel) => day
);

export const setRangeOfDates = make('[calendar]set range of dates]').stage(
  (date: string | [] | string[]) => date
);

export const getCalendar = make('[calendar]get calendar')
  .stage(calendar => calendar)
  .stage('success', data => data);
export const getCalendarData = make('[calendar]get calendar date')
  .stage(date => date)
  .stage('success', data => data)
  .stage('error', error => error);
export const editSelectedDates = make('[calendar]edit calendar date').stage(
  dates => dates
);
export const setRange = make('[calendar] set start range').stage(
  (data: any) => data
);
export const setDaysWithSchedule = make(
  '[calendar] set days with schedule'
).stage((days: Day[]) => days);
