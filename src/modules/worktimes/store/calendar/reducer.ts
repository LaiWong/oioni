import { DayModel } from '@worktimes/components/calendar/models/day.model';
import { addDays, eachDayOfInterval, format } from 'date-fns';
import { reducer } from 'redux-chill';
import { date } from 'yup';
import {
  getCalendar,
  getCalendarData,
  setCurrentDate,
  setRangeOfDates,
  setSelectedDays,
  editSelectedDates,
  setRange,
  setDaysWithSchedule
} from './actions';
import { CalendarState } from './state';
import { start } from 'repl';
import { log } from 'console';
import { stat } from 'fs';

export const CalendarReducer = reducer(new CalendarState())
  .on(setCurrentDate, (state, date) => (state.currentDate = date))
  .on(setSelectedDays, (state, day) => {
    if (day) {
      const editDay = state.selectedDays.find(
        (el: DayModel) => el.formatDate === day.formatDate
      );
      if (editDay) {
        return (state.selectedDays = [
          ...state.selectedDays.filter(
            (el: DayModel) => el.formatDate !== day.formatDate
          )
        ]);
      } else {
        return (state.selectedDays = [...state.selectedDays, day]);
      }
    }

    return (state.selectedDays = [...state.selectedDays, day]);
  })
  .on(getCalendarData.success, (state, data) => (state.calendarData = data))
  .on(getCalendar.success, (state, data) => (state.calendarDays = data))
  .on(setRangeOfDates, (state, date) => {
    if (state.selectedDays.length >= 2) {
      return (state.selectedDays = [date]);
    }
    let dates = [];
    const dateIsSelected = state.selectedDays.find(el => el === date);
    if (dateIsSelected) {
      return (state.selectedDays = [
        ...state.selectedDays.filter(el => el !== date)
      ]);
    } else {
      const newState = [...state.selectedDays, date];
      newState.sort(function(a, b) {
        a = new Date(a);
        b = new Date(b);
        return a > b ? 1 : a < b ? -1 : 0;
      });
      const start = newState[0];
      let end = newState[newState.length - 1];

      dates = eachDayOfInterval({
        start: new Date(start),
        end: new Date(end)
      }).map(el => format(el, 'yyyy-MM-dd'));
    }
    return (state.selectedDays = dates);
  })
  .on(editSelectedDates, (state, dates) => (state.selectedDays = dates))
  .on(setRange, (state, data) => {
    return { ...state, selectedDays: data };
  })
  .on(setDaysWithSchedule, (state, days) => (state.daysWithSchedules = days));
