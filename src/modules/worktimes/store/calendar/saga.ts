import { State } from '@store';
import { StoreContext } from '@store/context';
import {
  endOfMonth,
  endOfWeek,
  format,
  startOfMonth,
  startOfWeek
} from 'date-fns';
import { Saga } from 'redux-chill';
import { call, put, select } from 'redux-saga/effects';
import { getCalendar, getCalendarData, setDaysWithSchedule } from './actions';
import { getMountMatrix } from '@worktimes/utils/getMountMatrix';

class CalendarSaga {
  @Saga(getCalendar)
  public *getCalendar(currentDate) {
    const state = yield select(
      (state: State) => state.worktime.calendar.calendarData
    );
    const schedules = state.schedules || [];
    const blackouts = state.blackouts || [];
    try {
      const data = yield getMountMatrix(currentDate, schedules, blackouts);
      yield put(setDaysWithSchedule(data));
      yield put(getCalendar.success(data));
    } catch (error) {
      console.log(error);
    }
  }
  @Saga(getCalendarData)
  public *getCalendarDate(
    { employee_id, store_id, currentDate }: any,
    { schedule }: StoreContext
  ) {
    try {
      const response = yield call(schedule.getCalendarData, {
        employee_id,
        store_id,
        date_from: format(startOfMonth(currentDate), 'yyyy-MM-dd'),
        date_to: format(endOfMonth(currentDate), 'yyyy-MM-dd')
      });
      const data = response.data.result;
      yield put(getCalendarData.success(data));
    } catch (error) {
      console.log('error in saga getCalendarData', error);
    }
  }
}
export { CalendarSaga };
