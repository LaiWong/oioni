// type Day = {
//   id: number;
//   date: string | Date;
//   dayNumer: string | number;
//   dayOfWeek: string;
//   schedule: {
//     worktime: {
//       from: 'hours';
//       to: 'minutes';
//     };
//     blackouts: [
//       {
//         id: number;
//         from: 'hours';
//         to: 'minutes';
//       }
//     ];
//   };
//   isSelected: boolean;
// };

import { Day } from 'src/api/models/worktime';
import { getMountMatrix } from '@worktimes/utils/getMountMatrix';

class CalendarState {
  public currentDate = new Date();
  public calendarDays: any = [];
  public calendarData: any[] | [] = [];
  // public daysOfMonth: Day[] | [] = [];
  public selectedDays: any[] | [] = [];
  public controllerDisabled: boolean = Boolean(this.selectedDays);
  //new State
  public daysWithSchedules: Day[] | any = getMountMatrix(this.currentDate);
}

export { CalendarState };
