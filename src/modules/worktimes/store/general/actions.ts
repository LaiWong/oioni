import { Employee } from '@api';
import { make } from 'redux-chill';

export const getSalons = make('[general agenda] get salons')
  .stage(employee_id => employee_id)
  .stage('success', salons => salons);
export const setSelectedSalon = make('[general agenda] set salon name').stage(
  (salon: any) => salon
);
export const getOneEmployee = make('[general agenda]')
  .stage((merchant_id: number, employee_id: number) => ({
    merchant_id,
    employee_id
  }))
  .stage('success', (employee: Employee) => employee);
