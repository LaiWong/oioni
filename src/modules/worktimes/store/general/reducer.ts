import { reducer } from 'redux-chill';
import { GeneralAgendaState } from './state';
import { getOneEmployee, getSalons, setSelectedSalon } from './actions';

export const GeneraAgendaReducer = reducer(new GeneralAgendaState())
  .on(getSalons.success, (state, salons) => (state.salons = salons))
  .on(setSelectedSalon, (state, salon) => (state.selectedSalon = salon))
  .on(getOneEmployee.success, (state, employee) => (state.employee = employee));
