import { State } from '@store';
import { StoreContext } from '@store/context';
import { Saga } from 'redux-chill';
import { call, put, select } from 'redux-saga/effects';
import { getOneEmployee, getSalons } from './actions';

export class GeneralAgendaSaga {
  @Saga(getSalons)
  public *getSalons(employee_id: any, { salon }: StoreContext) {
    try {
      const {
        general: { merchantUser }
      }: State = yield select();
      const response = yield call(salon.getMerchantSalons, merchantUser.id);
      const result = response.data.result[0].stores;
      const salonsOfEmoloyees = [];
      for (let i = 0; i < result.length; i++) {
        const salon = result[i];
        if (salon.employee_stores.some(el => el.employee_id === employee_id)) {
          salonsOfEmoloyees.push(salon);
        }
      }
      yield put(getSalons.success(salonsOfEmoloyees));
    } catch (error) {}
  }
  @Saga(getOneEmployee)
  public *getEmployee(
    { merchant_id, employee_id }: { merchant_id: number; employee_id: number },
    { employee }: StoreContext
  ) {
    try {
      const response = yield call(employee.getOne, merchant_id, employee_id);
      yield put(getOneEmployee.success(response.data.result));
    } catch (error) {}
  }
}
