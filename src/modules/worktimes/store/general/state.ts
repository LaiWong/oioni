import { Employee } from '@api';

export class GeneralAgendaState {
  public salons: Array<any> | [] = [];
  public selectedSalon: any = null;
  public employee: Employee = null;
}
