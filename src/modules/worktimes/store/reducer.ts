import { combineReducers } from 'redux';
import { ScheduleReducer } from './schedule/reducer';
import { CalendarReducer } from './calendar/reducer';
import { GeneraAgendaReducer } from './general';

export const worktime = combineReducers({
  general: GeneraAgendaReducer,
  schedule: ScheduleReducer,
  calendar: CalendarReducer
});
