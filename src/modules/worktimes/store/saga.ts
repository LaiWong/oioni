import { CalendarSaga } from './calendar/saga';
import { GeneralAgendaSaga } from './general';
import { ScheduleSaga } from './schedule/saga';

export const WorktimeSagas = [
  new GeneralAgendaSaga(),
  new ScheduleSaga(),
  new CalendarSaga()
];
