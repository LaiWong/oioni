import { make } from 'redux-chill';

export type Schedule = {
  id: number;
  employee_id: number;
  store_id: number;
  date: string;
  time_from: string;
  timeslot_from: number;
  time_to: string;
  short_date: string;
  timeslot_to: number;
};

export const getSchedules = make('[schedule] get schedule')
  .stage((data: any) => data)
  .stage('success', (schedules: Schedule[]) => schedules)
  .stage('failure', (error: string) => error);

export const createSchedule = make('[schedule] create schedule')
  .stage((data: any) => data)
  .stage('success', (response: any) => response)
  .stage('failure', (error: any) => error);

export const deleteSchedule = make('[schedule] delete schedule').stage(
  schedule => schedule
);

export const updateSchedules = make('[schedule] update schedules')
  .stage((data: any) => data)
  .stage('success', (response: any) => response)
  .stage('failure', (error: any) => error);

export const deleteSchedules = make('[schedule] delete schedules')
  .stage((data: any) => data)
  .stage('success', (response: any) => response)
  .stage('failure', (error: any) => error);

export const getScheduleList = make(
  '[schedule] getSceduleList schedules'
).stage('success', (data: any) => data);
export const deleteSchedulesById = make(
  '[schedule] delete schedules by id'
).stage((data: any) => data);

export const updateSchedulesById = make(
  '[schedule] update schedules by id'
).stage((data: any) => data);

export const upsert = make(
  '[schedules blackouts] upsert schedules and blackouts'
).stage(
  (data: {
    times: {
      worktimeHoursFrom: string;
      worktimeHoursTo: string;
      worktimeMinutesFrom: string;
      worktimeMinutesTo: string;
      timeoffHoursFrom: string;
      timeoffHoursTo: string;
      timeoffMinutesFrom: string;
      timeoffMinutesTo: string;
      addTimeoff: boolean;
    };
    dates: any;
    callback: () => any;
  }) => data
);
export const editSchedules = make('[schedule] edit').stage(list => list);

export const removeAllBlackouts = make('[blackouts] remove all blackouts');
