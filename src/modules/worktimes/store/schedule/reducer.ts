import { reducer } from 'redux-chill';
import {
  createSchedule,
  deleteSchedule,
  getScheduleList,
  getSchedules,
  Schedule,
  updateSchedules,
  editSchedules
} from './actions';
import { ScheduleState } from './state';

export const ScheduleReducer = reducer(new ScheduleState())
  .on(getScheduleList.success, (state, list) => (state.scheduleList = list))
  .on(editSchedules, (state, list) => (state.editList = list));
