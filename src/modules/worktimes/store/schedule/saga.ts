import { State } from '@store';
import { StoreContext } from '@store/context';
import { Payload, Saga } from 'redux-chill';
import { call, put, select } from 'redux-saga/effects';
import { getCalendarData } from '../calendar';
import { deleteSchedulesById, getScheduleList, upsert } from './actions';
import { AxiosError } from 'axios';
import { snack } from '@store/snackbar';

class ScheduleSaga {
  @Saga(getScheduleList)
  public *getScheduleList() {
    try {
      let scheduleList = [];

      const daysWithSchedules = yield select(
        (state: State) => state.worktime.calendar.calendarData
      );

      daysWithSchedules.schedules?.map(schedule => {
        const timeoff = daysWithSchedules.blackouts.find(
          blackout => blackout.date === schedule.date
        );
        return scheduleList.push({
          schedule,
          timeoff
        });
      });

      let result: any = [];

      for (let index = 0; index < scheduleList.length; index++) {
        const element = scheduleList[index];
        const a = scheduleList.filter(
          el =>
            el.schedule.time_from === element.schedule.time_from &&
            el.schedule.time_to === element.schedule.time_to &&
            el.timeoff?.time_from === element.timeoff?.time_from &&
            el.timeoff?.time_to === element.timeoff?.time_to
        );
        result.push(a);
      }

      function removeDuplicates(arr) {
        const result = [];
        const duplicatesIndices = [];
        // Перебираем каждый элемент в исходном массиве
        arr.forEach((current, index) => {
          if (duplicatesIndices.includes(index)) return;

          result.push(current);

          // Сравниваем каждый элемент в массиве после текущего
          for (
            let comparisonIndex = index + 1;
            comparisonIndex < arr.length;
            comparisonIndex++
          ) {
            const comparison = arr[comparisonIndex];
            const currentKeys = Object.keys(current);
            const comparisonKeys = Object.keys(comparison);

            // Проверяем длину массивов
            if (currentKeys.length !== comparisonKeys.length) continue;

            // Проверяем значение ключей
            const currentKeysString = currentKeys
              .sort()
              .join('')
              .toLowerCase();

            const comparisonKeysString = comparisonKeys
              .sort()
              .join('')
              .toLowerCase();

            if (currentKeysString !== comparisonKeysString) continue;

            // Проверяем индексы ключей
            let valuesEqual = true;

            for (let i = 0; i < currentKeys.length; i++) {
              const key = currentKeys[i];

              if (current[key] !== comparison[key]) {
                valuesEqual = false;
                break;
              }
            }

            if (valuesEqual) duplicatesIndices.push(comparisonIndex);
          } // Конец цикла
        });

        return result;
      }

      result = removeDuplicates(result);

      result.forEach(array => {
        array.sort(
          (a: any, b: any) =>
            +new Date(a.schedule.date) - +new Date(b.schedule.date)
        );
      });

      yield put(getScheduleList.success(result));
    } catch (error) {
      console.log(error);
    }
  }

  @Saga(deleteSchedulesById)
  public *deleteSchedulesById(
    list: Payload<typeof deleteSchedulesById>,
    { schedule, blackout }: StoreContext
  ) {
    try {
      const store_id = yield select(
        (state: State) => state.worktime.general.selectedSalon
      );

      const currentDate = yield select(
        (state: State) => state.worktime.calendar.currentDate
      );

      const employee_id = yield select(
        (state: State) => state.worktime.general.employee.id
      );

      const scheduleData = { ids: [] };
      const blackoutData = { ids: [] };

      for (let i = 0; i < list.length; i++) {
        const element = list[i];
        scheduleData.ids.push(element.schedule.id);
        if (element.timeoff?.id) {
          blackoutData.ids.push(element.timeoff?.id);
        }
      }

      const responseSchedules = yield call(
        schedule.deleteSchedulesById,
        scheduleData
      );

      if (blackoutData.ids.length) {
        const responseBlackout = yield call(
          blackout.deleteBlackoutsById,
          blackoutData
        );
      }

      yield put(
        getCalendarData({
          employee_id,
          store_id,
          currentDate: currentDate // new Date()
        })
      );
    } catch (error) {
      console.log('error in saga deleteSchedulesById', error);
    }
  }

  @Saga(upsert)
  public *updateSchedulesById(
    { times, dates, callback }: Payload<typeof upsert>,
    { schedule, blackout }: StoreContext
  ) {
    try {
      const currentDate = yield select(
        (state: State) => state.worktime.calendar.currentDate
      );

      const employee_id = yield select(
        (state: State) => state.worktime.general?.employee?.id
      );

      const store_id = yield select(
        (state: State) => state.worktime.general?.selectedSalon
      );

      const scheduleData = {
        employee_id,
        store_id,
        time_from: `${times.worktimeHoursFrom}:${times.worktimeMinutesFrom}`,
        time_to: `${times.worktimeHoursTo}:${times.worktimeMinutesTo}`,
        dates
      };

      const responseSchedule = yield call(
        schedule.upsertSchedules,
        scheduleData
      );

      if (times.addTimeoff) {
        const blackoutsData = {
          employee_id,
          store_id,
          time_from: `${times.timeoffHoursFrom}:${times.timeoffMinutesFrom}`,
          time_to: `${times.timeoffHoursTo}:${times.timeoffMinutesTo}`,
          dates
        };

        const responseBlackout = yield call(
          blackout.upsertBlackouts,
          blackoutsData
        );
      } else {
        const {
          worktime: {
            schedule: { editList }
          }
        }: State = yield select();

        if (editList) {
          const ids = [];
          editList.forEach(element => {
            if (element.timeoff) {
              ids.push(element.timeoff.id);
            }
          });
          ids.length &&
            blackout.deleteBlackoutsById({
              ids
            });
        }
      }

      yield put(getCalendarData({ employee_id, store_id, currentDate }));
    } catch (err) {
      const error = err as AxiosError;

      if (error.response.data.code === 9006) {
        const { storeInfo } = JSON.parse(`${error.response.data.message}`);

        yield put(
          snack('employeeTimeIsNotAvailable', 'error', {
            storeName: storeInfo.name
          })
        );
      }

      console.log('error in saga upsert', error);
    } finally {
      yield call(callback);
    }
  }
}

export { ScheduleSaga };
