import { CalendarState } from './calendar';
import { GeneralAgendaState } from './general';
import { ScheduleState } from './schedule';

export type AgendaState = {
  general: GeneralAgendaState;
  schedule: ScheduleState;
  calendar: CalendarState;
};
