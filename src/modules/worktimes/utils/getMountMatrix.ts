import eachDayOfInterval from 'date-fns/eachDayOfInterval';
import endOfISOWeek from 'date-fns/endOfISOWeek';
import endOfMonth from 'date-fns/endOfMonth';
import isSameMonth from 'date-fns/isSameMonth';
import startOfISOWeek from 'date-fns/startOfISOWeek';
import startOfMonth from 'date-fns/startOfMonth';
import eachWeekOfInterval from 'date-fns/eachWeekOfInterval';
import { format } from 'date-fns';

export const getMountMatrix = (date, schedules = [], blackouts = []) => {
  const convert = day => {
    const current = new Date(
      new Date().getFullYear(),
      new Date().getMonth(),
      new Date().getDate()
    );
    const disabled = current > day || !isSameMonth(date, day);
    return {
      id: Math.ceil(Math.random() * +day),
      fullDate: day,
      formatDate: format(day, 'yyyy-MM-dd'),
      schedule: null,
      blackouts: null,
      disabled: disabled
    };
  };
  const matrix = eachWeekOfInterval(
    {
      start: startOfMonth(date),
      end: endOfMonth(date)
    },
    { weekStartsOn: 1 }
  );

  let data = matrix.map(weekDay =>
    eachDayOfInterval({
      start: startOfISOWeek(weekDay),
      end: endOfISOWeek(weekDay)
    }).map(day => convert(day))
  );

  for (let index = 0; index < data.length; index++) {
    const rows = data[index];
    for (let j = 0; j < rows.length; j++) {
      let dayInOneWeek = rows[j];
      let day = dayInOneWeek.formatDate;
      const hasSchedule = schedules.filter(
        schedule => format(new Date(schedule.date), 'yyyy-MM-dd') === day
      );
      if (hasSchedule.length > 0) {
        // rows[j].disabled = true;
        rows[j].schedule = hasSchedule[0];
      }
      const hasBlackouts = blackouts.filter(
        blackout => format(new Date(blackout.date), 'yyyy-MM-dd') === day
      );
      if (hasBlackouts.length > 0) {
        rows[j].blackouts = hasBlackouts;
      }
    }
  }
  const days = [];
  for (let i = 0; i < data.length; i++) {
    const array = data[i];
    for (let j = 0; j < array.length; j++) {
      const day = array[j];
      days.push(day);
    }
  }

  return days;
};
