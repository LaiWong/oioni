import * as React from 'react';
import { Worktime } from './pages';
import { getMountMatrix } from './utils/getMountMatrix';

export const Worktimes = () => <Worktime />;
