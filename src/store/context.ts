import {
  AppointmentsService,
  AuthService,
  CategoryService,
  CustomersService,
  EmployeeService,
  FinancesService,
  MerchantService,
  MyAppointmentsService,
  ProductService
} from '@api';
import { History } from 'history';
import { Store } from 'redux';
import axios, { AxiosError, AxiosRequestConfig } from 'axios';
import { SalonService } from 'src/api/services/salon.service';
import { ScheduleService } from 'src/api/services/schedules.service';
import { BlackoutService } from 'src/api/services/blackouts.service';
import { WidgetService } from 'src/api/services/widget.service';
import { logout } from '@store/general';
import { CountryService } from 'src/api/services/country.service';
import { LanguageService } from 'src/api/services/language.service';
import { CurrencyService } from 'src/api/services/currency';
import { PaymentService } from 'src/api/services/payment.service';

/**
 * Get context
 */
const getContext = (history: History, store: Store) => {
  const request = (defaults: AxiosRequestConfig) => {
    const instance = axios.create(defaults);

    return async (
      { headers = {}, ...config }: AxiosRequestConfig,
      enabled = true,
      disabledToken = false
    ) => {
      try {
        headers.channel = 'admin';
        headers['Cache-Control'] = 'no-cache';
        const token = localStorage.getItem('idtoken');
        const response = await instance({
          ...config,
          headers: token
            ? {
                idtoken: token,
                ...headers
              }
            : headers
        });

        const idToken =
          response?.headers?.idtoken || response?.headers?.idToken;

        if (idToken && !disabledToken) {
          localStorage.setItem('idtoken', idToken);
        }

        return response;
      } catch (e) {
        const error: AxiosError = e;

        switch (true) {
          case enabled && error.response.status == 401: {
            store.dispatch(logout());
            break;
          }

          default: {
            throw error;
          }
        }
      }
    };
  };

  const { google }: any = window;

  return {
    store,
    history,
    auth: new AuthService(request),
    salon: new SalonService(request),
    product: new ProductService(request),
    customer: new CustomersService(request),
    employee: new EmployeeService(request),
    merchant: new MerchantService(request),
    category: new CategoryService(request),
    appointments: new AppointmentsService(request),
    myAppointments: new MyAppointmentsService(request),
    schedule: new ScheduleService(request),
    blackout: new BlackoutService(request),
    widget: new WidgetService(request),
    google: {
      geocoder: new google.maps.Geocoder(),
      autocomplete: new google.maps.places.AutocompleteService()
    },
    finances: new FinancesService(request),
    country: new CountryService(request),
    language: new LanguageService(request),
    currency: new CurrencyService(request),
    financeModel: new PaymentService(request)
  };
};

/**
 * Saga context
 */
type StoreContext = ReturnType<typeof getContext>;

export { StoreContext, getContext };
