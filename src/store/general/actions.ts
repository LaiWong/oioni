import { make } from 'redux-chill';
import { Category, Employee, Language, MerchantFull, UserType } from '@api';

/**
 * Authorize
 */
const authorize = make('[general] authorize').stage(
  (payload: {
    merchant: MerchantFull;
    employee: Employee;
    userType: UserType;
  }) => payload
);

/**
 * Startup
 */
const startup = make('[general] startup')
  .stage(
    'success',
    (payload: {
      merchant: MerchantFull;
      employee: Employee;
      userType: UserType;
    }) => payload
  )
  .stage('finish');

/**
 * Logout
 */
const logout = make('[general] logout').stage('success');

/**
 * Upload file
 */
const upload = make('[general] upload')
  .stage((files: File[], callback: (url: string[]) => any) => ({
    files,
    callback
  }))
  .stage('success', (url: string[]) => url);

/**
 * Set language
 */
const setLanguage = make('[general] set language')
  .stage((language: Language) => language)
  .stage('success');

/**
 * Init i18n
 */
const initI18n = make('[general] init i18n').stage('success');

/**
 * Get categories list
 */
const getCategories = make('[general] get categories').stage(
  'success',
  (categories: Category[]) => categories
);

export {
  upload,
  authorize,
  startup,
  logout,
  setLanguage,
  initI18n,
  getCategories
};
