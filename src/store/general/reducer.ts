import { reducer } from 'redux-chill';
import { authorize, getCategories, startup } from './actions';
import { GeneralState } from './state';
import { profileSave } from '@profile/store';
import { UserType } from '@api';

/**
 * General state
 */
const general = reducer(new GeneralState())
  .on(
    [authorize, startup.success],
    (state, { employee, merchant, userType }) => {
      if (!userType) return;

      localStorage.userType = userType;
      state.userType = userType;

      switch (userType) {
        case UserType.admin:
          state.merchantUser = merchant;

          break;
        case UserType.employee:
          state.employeeUser = employee;

          break;

        case UserType.merchant:
          state.merchantUser = merchant;

          break;
      }

      state.authorized = true;
    }
  )
  .on(startup.finish, state => {
    state.ready = true;
  })
  .on(profileSave.success, (state, { data }) => {
    state.merchantUser = data.merchant;
  })
  .on(getCategories.success, (state, categories) => {
    state.categories = categories;
  });

export { general };
