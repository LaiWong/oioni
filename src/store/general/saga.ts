import { Payload, Saga } from 'redux-chill';
import {
  getCategories,
  initI18n,
  logout,
  setLanguage,
  startup,
  upload
} from './actions';
import { StoreContext } from '@store/context';
import { all, call, put, take } from 'redux-saga/effects';
import { snack } from '@store/snackbar';
import { Language, UserType } from '@api';
import { init } from 'src/localization';
import i18next from 'i18next';

class GeneralSaga {
  /**
   * Startup
   */
  @Saga(startup)
  public *startup(
    _,
    { merchant: merchantAPI, employee: employeeAPI }: StoreContext
  ) {
    try {
      yield put(initI18n());
      yield take(initI18n.success.type);

      const { userType, idtoken } = localStorage;
      if (!idtoken) {
        return;
      }

      let merchant, employee;

      switch (userType) {
        case UserType.employee: {
          const response = yield call(employeeAPI.getUserData);
          employee = response.data.employee;

          break;
        }

        case UserType.merchant:
        case UserType.admin: {
          const response = yield call(merchantAPI.getUserData);
          merchant = response.data.merchant;
          break;
        }
      }

      yield put(startup.success({ merchant, employee, userType }));
    } finally {
      yield put(startup.finish());
    }
  }

  /**
   * Logout
   */
  @Saga(logout)
  public *logout(_, { auth, history }: StoreContext) {
    try {
      localStorage.removeItem('idtoken');

      const [start, language] = history.location.pathname.split('/');

      window.location.pathname = `/${language}/auth/login`;
    } catch (error) {
      yield put(snack('message.somethingWentWrong', 'error'));
    }
  }

  /**
   * Upload file
   */
  @Saga(upload)
  public *upload(
    { files, callback }: Payload<typeof upload>,
    { merchant }: StoreContext
  ) {
    try {
      const urls = (yield all(
        files.map(file => call(merchant.upload, file))
      ))?.map(response => response.data.location);

      yield call(callback, urls);

      yield put(upload.success(urls));
    } catch (error) {
      yield put(snack('message.cannotUploadFile', 'error'));
    }
  }

  /**
   * Init i18n
   */
  @Saga(initI18n)
  public *initI18n(_, { history, merchant }: StoreContext) {
    try {
      const language = history.location.pathname.split('/')[1] as Language;

      const {
        data: { data: translations }
      } = yield call(merchant.getTranslations, language);

      yield call(init, language, translations);

      yield put(initI18n.success());
    } catch (e) {
      yield put(snack('message.cannotGetTranslations', 'error'));
    }
  }

  /**
   * Set language
   */
  @Saga(setLanguage)
  public *setLanguage(
    language: Payload<typeof setLanguage>,
    { history, merchant }: StoreContext
  ) {
    try {
      const pathWithoutLanguage = history.location.pathname
        .split('/')
        .slice(2)
        .join('/');

      history.push(`/${language}/${pathWithoutLanguage}`);

      const {
        data: { data: translations }
      } = yield call(merchant.getTranslations, language);

      i18next.addResourceBundle(language, 'common', translations);
      i18next.changeLanguage(language);

      yield put(setLanguage.success());
    } catch (e) {
      yield put(snack('message.cannotSetLanguage', 'error'));
    }
  }

  /**
   * Get categories
   */
  @Saga(getCategories)
  public *getCategories(_, { category }: StoreContext) {
    try {
      const response = yield call(category.get);

      yield put(getCategories.success(response.data.result));
    } catch (error) {
      yield put(snack('message.cannotGetCategories', 'error'));
    }
  }
}

export { GeneralSaga };
