import { Category, Employee, MerchantFull, UserType } from '@api';

const { phone } = localStorage;

class GeneralState {
  /**
   * Merchant phone
   */
  public phone: string = phone || null;

  /**
   * Authorized user merchant
   */
  public merchantUser: MerchantFull;

  /**
   * Authorized user employee
   */
  public employeeUser: Employee;

  /**
   * Authorized user type
   */
  public userType: UserType;

  /**
   * Is app ready
   */
  public ready: boolean = false;

  /**
   * Is authorized
   */
  public authorized: boolean = false;

  /**
   * Categories list
   */
  public categories: Category[] = [];
}

export { GeneralState };
