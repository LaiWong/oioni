import { router } from './router';
import { general } from './general';
import { snackbar } from './snackbar';
import { auth } from '@auth';
import { profile } from '@profile';
import { dashboard } from '@dashboard';
import { appointments } from '@appointments';
import { finances } from '@finances';
import { services } from '@services';
import { employees } from '@employees';
import { customers } from '@customers';
import { salon } from '@salon';
import { merchant } from '@merchant';
import { combineReducers } from 'redux';
import { myAppointments } from '@my-appointments';
import { worktime } from '@worktimes';
import { widget } from '@salon/pages/widget/store/reducer';
import { country } from '@country/store';
import { language } from '@languages';
import { currency } from '@currency/store';

const app = {
  auth,
  router,
  general,
  snackbar,
  merchant: combineReducers({
    merchant,
    services,
    employees,
    customers,
    dashboard,
    appointments,
    profile,
    salon
  }),
  finances,
  employee: combineReducers({
    myAppointments
  }),
  worktime,
  widget,
  country,
  language,
  currency
};

export { app };
