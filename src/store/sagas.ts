import { RouterSaga } from './router';
import { GeneralSaga } from './general';
import { SnackbarSaga } from './snackbar';
import { AuthSaga } from '@auth';
import { ProfileSaga } from '@profile';
import { DashboardSaga } from '@dashboard';
import { AppointmentsSaga } from '@appointments';
import { ServicesSaga } from '@services';
import { EmployeesSaga } from '@employees';
import { CustomersSaga } from '@customers';
import { SalonSaga } from '@salon';
import { MerchantSaga } from '@merchant';
import { MyAppointmentsSaga } from '@my-appointments';
import { WorktimeSagas } from '@worktimes';
import { WidgetSaga } from '@salon/pages/widget/store/saga';
import { FinancesSaga } from '@finances';
import { CountrySaga } from '@country/store';
import { CurrencySaga } from '@currency';
import { LanguageSaga } from '@languages';

/**
 * App sagas
 */
const sagas = [
  new AuthSaga(),
  new SalonSaga(),
  new RouterSaga(),
  new GeneralSaga(),
  new ProfileSaga(),
  new SnackbarSaga(),
  new ServicesSaga(),
  new MerchantSaga(),
  new EmployeesSaga(),
  new CustomersSaga(),
  new DashboardSaga(),
  new AppointmentsSaga(),
  new MyAppointmentsSaga(),
  new WidgetSaga(),
  new FinancesSaga(),
  new CountrySaga(),
  new LanguageSaga(),
  new CurrencySaga(),
  ...WorktimeSagas
];

export { sagas };
