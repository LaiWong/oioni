import { make } from 'redux-chill';
import { ReactNode } from 'react';
import { SnackbarState } from './state';

/**
 * Show snackbar
 */
const snack = make('[snackbar] snack')
  .stage(
    (
      content: ReactNode,
      type: SnackbarState['type'] = 'info',
      options: SnackbarState['options'] = {}
    ) => ({
      type,
      content,
      options
    })
  )
  .stage('end');

export { snack };
