import { reducer } from 'redux-chill';
import { SnackbarState } from './state';
import { snack } from './actions';

/**
 * General state
 */
const snackbar = reducer(new SnackbarState())
  .on(snack, (state, { type, content, options }) => {
    state.type = type;
    state.content = content;
    state.options = options;
  })
  .on(snack.end, state => {
    state.content = null;
  });

export { snackbar };
