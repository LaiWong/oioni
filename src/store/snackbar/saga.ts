import { Saga } from 'redux-chill';
import { snack } from './actions';
import { delay, put } from 'redux-saga/effects';

class SnackbarSaga {
  /**
   * Snack
   */
  @Saga(snack)
  public *snack() {
    yield delay(5000);

    yield put(snack.end());
  }
}

export { SnackbarSaga };
