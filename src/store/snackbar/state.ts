import { TOptions } from 'i18next';
import { ReactNode } from 'react';

class SnackbarState {
  /**
   * Current snack
   */
  public content: ReactNode = null;

  /**
   * Info style
   */
  public type: 'info' | 'warning' | 'error';

  public options: string | TOptions;
}

export { SnackbarState };
