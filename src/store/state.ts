import { AuthState } from '@auth';
import { router } from './router';
import { GeneralState } from './general';
import { ProfileState } from '@profile';
import { ServicesState } from '@services';
import { AppointmentsState } from '@appointments';
import { DashboardState } from '@dashboard';
import { SnackbarState } from './snackbar';
import { EmployeesState } from '@employees';
import { CustomersState } from '@customers';
import { SalonState } from '@salon';
import { MerchantState } from '@merchant';
import { MyAppointmentsState } from '@my-appointments';
import { AgendaState } from '@worktimes';
import { WidgetState } from '@salon/pages/widget/store/state';
import { FinancesState } from '@finances';
import { CountryState } from '@country/store';
import { LanguageState } from '@languages';
import { CurrencyState } from 'src/modules/currency/store';

/**
 * App state
 */

type State = {
  auth: AuthState;
  general: GeneralState;
  snackbar: SnackbarState;
  router: ReturnType<typeof router>;
  merchant: {
    merchant: MerchantState;
    employees: EmployeesState;
    customers: CustomersState;
    services: ServicesState;
    dashboard: DashboardState;
    appointments: AppointmentsState;
    profile: ProfileState;
    salon: SalonState;
  };
  employee: {
    myAppointments: MyAppointmentsState;
  };
  worktime: AgendaState;
  widget: WidgetState;
  finances: FinancesState;
  country: CountryState;
  language: LanguageState;
  currency: CurrencyState;
};

export { State };
