declare module '*.scss' {
  const content: { [className: string]: string };
  export = content;
}

declare module '*.css' {
  // const content: { [className: string]: string };
  // export = content;
  export = any;
}

declare module '*.png' {
  export = any;
}

declare module '*.svg' {
  export = any;
}
